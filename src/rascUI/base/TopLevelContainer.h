/*
 * TopLevelContainer.h
 *
 *  Created on: 2 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_BASE_TOPLEVELCONTAINER_H_
#define RASCUI_BASE_TOPLEVELCONTAINER_H_

#include <unordered_map>
#include <functional>
#include <cstddef>
#include <GL/gl.h>

#include "../util/Rectangle.h"
#include "../util/Location.h"
#include "Container.h"

namespace rascUI {
    class RepaintController;
    class FunctionQueue;
    class Bindings;
    class Theme;
    enum class CursorType;

    /**
     * TopLevelContainer is a Container, (a Component which stores Components).
     * In the rascUI system, any Component that the user wishes to use, must be added to an instance of TopLevelContainer.
     *
     * From there, to use the UI system, the user must call the following TopLevelContainer methods:
     * All of the following methods are TOP LEVEL EVENTS, which means that if the TopLevelContainer owns
     * it's function queues, beforeEvent and afterEvent will be updated, regardless of whether the TOP LEVEL EVENT resulted
     * in any component events being called.
     *
     *   > draw
     *     - This must be called each frame, and passed the current view area as a Rectangle. This method also requires
     *       the time elapsed in seconds since the previous frame.
     *     - This method draws all Components within this TopLevelComponent.
     *     - This should be used for drawing UI if we are re-drawing the entire UI each frame, such as in a game.
     *
     *   > bufferedDraw
     *     - This implements buffered repainting, and will do nothing if we do not have a repaint controller.
     *     - This re-draws the entire user interface, but only if the repaint controller reports that at least one
     *       component wants repainting, or if the view area has changed.
     *     - Otherwise, the user interface is drawn using the Theme's redrawFromBuffer method.
     * 
     *   > doPartialRepaint
     *     - This performs a cycle of the partial repaint system, and will do nothing if partial repaint is not enabled.
     *       This will draw only the components which are supposed to be redrawn, according to the RepaintController.
     *     - This should be used for drawing UI if we should only re-draw what has changed, such as in a desktop application.
     *
     *   > mouseMove
     *     - This must be called each time the mouse moves, and must be passed the current mouse position. This method will
     *       return true if the user is NOT mousing over any Components (ignoring the TopLevelComponent).
     *     - This method handles all mouse entry/leave events for Components within this TopLevelComponent.
     *
     *   > mouseEvent
     *     - NOTE: This will use the mouse bindings defined by our associated Bindings instance.
     *     - This must be called each time there is a mouse event, and must be passed the mouse button, state and position.
     *       This method returns true if the user has NOT clicked on any of the Components (ignoring the TopLevelComponent).
     *     - This method handles clicking on all components within this TopLevelComponent.
     *
     *   > keyPress
     *     - NOTE: This will use the key bindings defined by our associated Bindings instance.
     *     - This must be called each time there is a key press event, and must be passed the key that has been pressed, and
     *       whether it is a special key. This method returns true if the UI system has NOT used this key event to do
     *       anything useful.
     *     - This method handles key presses for the keyboard events within this TopLevelComponent. Keyboard events are used
     *       internally for the keyboard integration of selection, and selection locking (such as text entry components).
     *
     *   > keyRelease
     *     - NOTE: This will use the key bindings defined by our associated Bindings instance.
     *     - This must be called each time there is a key release event, and must be passed the key that has been pressed, and
     *       whether it is a special key. This method returns true if the UI system has NOT used this key event to do
     *       anything useful.
     *     - This method handles key releases for the keyboard events within this TopLevelComponent. Keyboard events are used
     *       internally for the keyboard integration of selection, and selection locking (such as text entry components).
     *
     *   > dummyEvent
     *     - This does nothing other than lock to the mutex for the purpose of updating the function queues.
     *       This should be used if there is no periodically called event, such as redraw every frame.
     *
     *
     * IMPORTANT NOTICE ABOUT SYNCHRONISATION
     * ======================================
     *
     *     The UI system is primarily designed to work only in a single thread.
     *
     *     All methods (draw, mouseMove, mouseEvent, keyPress, keyRelease) should be called
     *     from the same thread, such as the GLUT thread.
     *
     *     It may be necessary to modify parts of the UI from outside this thread, such as
     *     enabling/disabling buttons when things happen, or modifying the component hierarchy.
     *     If you wish to do this, USE THE FUNCTION QUEUES (see documentation for beforeEvent specifically).
     *                             ***********************
     */
    class TopLevelContainer : public Container {
    private:
        friend class Component;
        friend void Container::escapeKeyboardSelection(void);

        /**
         * This is used as the scale if null scale pointers are given.
         */
        const static GLfloat defaultScale;

        /**
         * These must be set to point at GLfloats that specify the scale we should draw.
         */
        const GLfloat * xScalePtr,
                      * yScalePtr;

        /**
         * This stores the value of the view rectangle passed to us in the draw method, last time the draw
         * method was run.
         * If this has changed, recalculate is called.
         */
        Rectangle lastView;

        /**
         * This variable allows recalculating to be triggered on demand.
         * Initially this is true to cause us to recalculate on the first frame.
         */
        bool recalculateNextFrame;

        /**
         * This stores the value that xScalePtr and yScalePtr had last time. If this has changed, recalculate is called.
         * These variables are also returned when getXScale and getYScale are called.
         */
        GLfloat lastXScale, lastYScale;

        /**
         * This stores the most recent view position of the mouse.
         */
        GLfloat lastMouseViewX, lastMouseViewY;

        /**
         * If this is true, then a mouse move event will be forced next time, even if the mouse has not moved.
         * This is initially true to force the first mouse move event.
         *
         * This is part of the system to avoid Windows' duplicate mouse move events.
         */
        bool forceNextMouseMove;

        /**
         * If this is true, then selection will be locked to the currently selected Keyboard component, until
         * the user clicks their mouse elsewhere.
         */
        bool selectionLocked;

        /**
         * If selection was just unlocked with a key press event, we want to ignore that key's release event.
         * Otherwise, we could unlock selection with a key press, then lock it again with it's release event.
         * That would be irritating.
         */
        int selectionUnlockIgnoreKey;

        /**
         * These store the actual sets used for currentMouseOver and newMouseOver.
         */
        std::unordered_set<Component *> set0, set1;

        /**
         * These store a set of components which are currently mouse-overed.
         * Each time the mouse moves, the components with the mouse over them are added to newMouseOver.
         * Events are then called from the difference between the sets, then the pointers are swapped.
         */
        std::unordered_set<Component *> * currentMouseOver, * newMouseOver;

        /**
         * This stores the components that the user has currently clicked on, for each mouse button.
         * NOTE: Only components pressed by the MOUSE are stored here, mouse events for keyboard-selected components
         *       are done separately.
         */
        std::unordered_map<int, std::unordered_set<Component *>> currentMousePress;

        /**
         * Override our setTopLevel method, so that if we (a top-level container) are added to another container,
         * a warning is produced.
         */
        virtual void setTopLevel(TopLevelContainer * topLevel) override;

        /**
         * This stores the current component selected by keyboard integration. If this has a value, then that component
         * is selected, and keyboard integration is currently enabled.
         * If this has a value of NULL, then keyboard selection is DISABLED.
         */
        Component * keyboardSelected;

        /**
         * Mouse and keyboard selection is bound into this container. If this is set to something other than this, components
         * outside of selectionBound will NOT receive ANY mouse or keyboard events.
         * This should be used for popup menus, for example.
         */
        Container * selectionBound;

        /**
         * This is true while the user holds down the enter key on the component selected by the keyboard.
         */
        bool keyboardSelectedEnterKeyPressed;

        /**
         * This is the next component that will be used for keyboard selection, next time keyboard selection goes from
         * being disabled to enabled.
         *
         * --> Currently this is calculated as the most recent component to have a mouse enter event.
         *
         * --> If this is NULL, then an active component search will be used instead.
         */
        Component * prospectiveKeyboardComponent;

        /**
         * The elapsed time (in seconds) since the last frame. This is set for us each time draw is called.
         */
        GLfloat elapsed;

        /**
         * This is the repaint controller, used to control partial repaints.
         * This only exists if partial repainting is enabled, and is dynamically allocated by TopLevelContainer
         * when TopLevelContainer is created, if allowPartialRepaint is passed as true.
         */
        RepaintController * repaintController;

        /**
         * These keep track of whether we own the beforeEvent and afterEvent function queues respectively.
         * If we *do* own them, we must update them ourselves at the start/end of each top level event, and
         * we must delete them in our destructor. Otherwise, neither of these things are required - we
         * should not delete them because we don't own them, and it is not our responsibility to update them.
         */
        bool ownBeforeEvent, ownAfterEvent;

    protected:
        /**
         * This pointer is sent to Theme as user data, in the beforeDraw method.
         * This can be used by a custom subclass of TopLevelContainer to send useful data to the Theme.
         * For more information see the documentation in Theme.h, and the documentation for the platform system.
         */
        void * beforeDrawThemeUserData;

    public:

        /**
         * The theme we are using. This is public so it can be accessed by any of our child components, and so the user
         * can easily change it. There is little point in implementing a getter/setter method here.
         */
        Theme * theme;

        /**
         * This instance stores all of our mouse and keyboard bindings.
         * An instance of this should be given in the constructor. Default bindings are available from
         * rascUI::Bindings::defaultOpenGL and rascUI::Bindings::defaultX11.
         */
        const Bindings * bindings;

        // ---------------------------------- Function queues -------------------------------------

        /**
         * This is updated at the start of ALL (groups of) top level events. This is still updated even if
         * the event does not do anything: for example, a mouse move event would still update this if the
         * event is aborted due to the mouse coordinates not changing.
         *
         * If code outside the UI system wants to make changes to the component hierarchy, this function
         * queue is most appropriate to use. This is because it will be updated at the START of the next
         * event, from within the UI thread.
         *
         * Note that function queues are thread safe, so it is safe to add events to this function queue
         * from any thread, at any time.
         *
         * If we own our function queues (we created them) then:
         *  - This is updated by TopLevelContainer, WITHIN the top-level event functions,
         *    at the start of EACH event function.
         * If we do NOT own our function queues (they were provided in our constructor):
         *  - This MUST be updated before running each (group of) top level events.
         *  - This is the case if we are running associated with a Context.
         *
         * Be careful here:
         *   > Component mouse release and leave events can be triggered by removing a component which is mouse-overed.
         *   > In this case functions added by these events will not be run immediately, instead these functions will sit in
         *     the queue until next time any event happens.
         *   > This is because a call to remove components from the hierarchy, CANNOT AND MUST NOT cause events to run,
         *     since remove is NOT a top level event.
         *
         * So if you are removing components, DO NOT use a function queue from a mouse leave or release event, unless it does not
         * matter that the event may be delayed (THE COMPONENT MAY HAVE EVEN BEEN REMOVED FROM THE HIERARCHY BY THEN).
         * Mouse click events are always fine.
         */
        FunctionQueue * beforeEvent;

        /**
         * This is updated at the end of ALL (groups of) top level events. This is still updated even if
         * the event does not do anything: for example, a mouse move event would still update this if the
         * event is aborted due to the mouse coordinates not changing.
         *
         * If, during the running of a normal event, a component within the hierarchy wants to make changes to
         * the component hierarchy, this FunctionQueue is most appropriate to use, (since it is guaranteed to run
         * at the end of all (groups of) top level events, within the UI thread).
         *
         * This is also appropriate to use if a component within the hierarchy wants to make changes OUTSIDE
         * the UI system, (such as those which require locking to a mutex).
         *
         * Note that function queues are thread safe, so it is safe to add events to this function queue
         * from any thread, at any time.
         *
         * If we own our function queues (we created them) then:
         *  - This is updated by TopLevelContainer, WITHIN the top-level event functions,
         *    at the end of EACH event function.
         * If we do NOT own our function queues (they were provided in our constructor):
         *  - This MUST be updated after running each (group of) top level events.
         *  - This is the case if we are running associated with a Context.
         *
         * Be careful here:
         *   > Component mouse release and leave events can be triggered by removing a component which is mouse-overed.
         *   > In this case functions added by these events will not be run immediately, instead these functions will sit in
         *     the queue until next time any event happens.
         *   > This is because a call to remove components from the hierarchy, CANNOT AND MUST NOT cause events to run,
         *     since remove is NOT a top level event.
         *
         * So if you are removing components, DO NOT use a function queue from a mouse leave or release event, unless it does not
         * matter that the event may be delayed (THE COMPONENT MAY HAVE EVEN BEEN REMOVED FROM THE HIERARCHY BY THEN).
         * Mouse click events are always fine.
         */
        FunctionQueue * afterEvent;

        // ----------------------------------------------------------------------------------------

    private:
        // Don't allow copying of TopLevelContainer: It would really screw up the following:
        //  > The component hierarchy in general: Each component has a pointer to the top level container
        //  > The top level container has a pointer to it's own top level container (itself) - the field topLevel
        //  > Any raw resources we have could be double freed, (such as the repaint controller).
        TopLevelContainer(const TopLevelContainer & other);
        TopLevelContainer & operator = (const TopLevelContainer & other);

    public:
        /**
         * theme:                       The Theme instance to use for drawing all components. This must remain existing for as long as the TopLevelContainer.
         * bindings:                    A Bindings instance to use for keyboard and mouse bindings. This is copied and stored in TopLevelContainer, so the
         *                              instance provided to the constructor can be destroyed after calling the constructor. Bindings instances are conveniently
         *                              generated with the methods rascUI::Bindings::defaultOpenGL and rascUI::Bindings::defaultX11, or can be configured manually.
         * location:                    The Location of this TopLevelContainer, within the screen area. If you wish the TopLevelContainer to fill the entire area,
         *                              then use a default-constructed Location.
         * xScalePtr and yScalePtr:     A pointer to the x and y scale which should be used by the UI, or NULL to use default scale.
         * allowPartialRepaint:         Picks whether or not to use partial repainting mode.
         * beforeEvent and afterEvent:  Function queues for this top level container to use, or NULL if we are to create our own.
         *                              If NULL is provided, TopLevelContainer will create it's own functions queues, and update them from within
         *                              its top level event functions. This would be normal if the UI system is being used as an overlay in OpenGL, for example.
         *                              If pointers to function queues are provided, these will be used by TopLevelContainer (and it's component hierarchy) instead.
         *                              In this case, it will be the USER'S RESPONSIBILITY to update the function queues, before and after (groups of) events.
         *                              This is the case when TopLevelContainer is running alongide a Context, (see the platform interface).
         */
        TopLevelContainer(Theme * theme, const Bindings * bindings, bool allowPartialRepaint = false, FunctionQueue * beforeEvent = NULL, FunctionQueue * afterEvent = NULL);
        TopLevelContainer(Theme * theme, const Bindings * bindings, const Location & location, bool allowPartialRepaint = false, FunctionQueue * beforeEvent = NULL, FunctionQueue * afterEvent = NULL);
        TopLevelContainer(Theme * theme, const Bindings * bindings, const GLfloat * xScalePtr, const GLfloat * yScalePtr, bool allowPartialRepaint = false, FunctionQueue * beforeEvent = NULL, FunctionQueue * afterEvent = NULL);
        TopLevelContainer(Theme * theme, const Bindings * bindings, const GLfloat * xScalePtr, const GLfloat * yScalePtr, const Location & location, bool allowPartialRepaint = false, FunctionQueue * beforeEvent = NULL, FunctionQueue * afterEvent = NULL);

        virtual ~TopLevelContainer(void);

        // -------------------------- Regular methods that should be called during the program ------------------------------------

        /**
         * This method will draw all of the UI components contained within this.
         * The user should specify a rectangle representing the view area. If this has changed since last time, the sizes
         * of all components will be recalculated.
         *
         * The time elapsed must be specified here, as some components need to know about time. This should be the
         * amount of time since draw, bufferedDraw or doPartialRepaint was previously called.
         *
         * This should be used for drawing UI, when re-drawing the entire UI each
         * frame is required: total repaint mode, see documentation in Theme.h.
         *
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        void draw(const Rectangle & view, GLfloat elapsed);
        
        /**
         * This method draws all UI components if the RepaintController shows that some components want repainting,
         * or if the view area has changed. Otherwise, the UI system is drawn using the Theme's redrawFromBuffer method.
         * 
         * The time elapsed must be specified here, as some components need to know about time. This should be the
         * amount of time since draw, bufferedDraw or doPartialRepaint was previously called.
         * 
         * This should be used for drawing UI, when re-drawing the entire UI each
         * time there is a change is required: buffered repaint mode, see documentation in Theme.h.
         * 
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        void bufferedDraw(const Rectangle & view, GLfloat elapsed);

        /**
         * This method will draw all of the UI components which need repainting, according to the RepaintController.
         * The user should specify a rectangle representing the view area. If this has changed since last time, the sizes
         * of all components will be recalculated.
         *
         * The time elapsed must be specified here, as some components need to know about time. This should be the
         * amount of time since draw, bufferedDraw or doPartialRepaint was previously called.
         *
         * This should be used for drawing UI if we should only re-draw what has changed, such as in a
         * desktop application: partial repaint mode, see documentation in Theme.h.
         *
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        void doPartialRepaint(const Rectangle & view, GLfloat elapsed);

        /**
         * This should be called whenever the mouse moves. This method will trigger events onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseLeave.
         *
         * NOTE: GLUT (and consequently wool) provides separate methods for mouse move and mouse drag. THIS METHOD MUST BE CALLED DURING
         *       BOTH OF THESE METHODS, SINCE THE INTERNAL IMPLEMENTATION OF TopLevelContainer DECIDES FOR ITSELF WHICH EVENTS TO CALL.
         *
         * This method will return:
         *     TRUE: If the mouse is not hovering over any component, so the program should process events for stuff behind the UI
         *    FALSE: If the mouse is over a component.
         *
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        inline bool mouseMove(GLfloat viewX, GLfloat viewY) {
            return internalMouseMove(viewX, viewY, true);
        }

        /**
         * This should be called whenever a mouse event happens.
         * This method will return:
         *     TRUE: If the user did not click on any UI component, so the program should process mouse events for stuff behind the UI.
         *    FALSE: If the user has clicked on a UI component.
         *
         * NOTE: This will use the mouse bindings defined by our associated Bindings instance.
         *
         * The parameter state should be an int value representing whether this mouse event is a press or release
         * event. This should be specified with one of the two values: (See bottom of this file):
         *    rascUI::MouseEvent::PRESS  or  rascUI::MouseEvent::RELEASE.
         * (Alternatively, the two constants GLUT_DOWN and GLUT_UP can be used).
         *
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        inline bool mouseEvent(int button, int state, GLfloat viewX, GLfloat viewY) {
            return internalMouseEvent(button, state, viewX, viewY, true);
        }

        /**
         * A dummy event. This does nothing other than updated the function queues, if the TopLevelContainer owns them.
         * This should be used if there is no periodically called event, such as redraw every frame.
         *
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        void dummyEvent(void);

        // ---------------------------- Misc regular methods ----------------------------------------------------------------------

        /**
         * This method sets the selection bound for this top level container.
         * Mouse and keyboard selection is bound into this container. After calling this, components outside the specified container
         * will never receive any keyboard or mouse events. This should be used for popup menus, for example.
         *
         * NOTE: This method will do nothing if the container is not currently part of our component hierarchy.
         *
         * ALSO: This method automatically resets all selection.
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void setSelectionBound(Container * container);

        /**
         * This can be used to reset the selection bound to the default. This allows all components in the hierarchy to be selected
         * again. This is the same as setting the top level container's selection bound to itself.
         *
         * NOTE: This method automatically resets all selection.
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        inline void resetSelectionBound(void) {
            setSelectionBound(this);
        }

        /**
         * This method returns the current selection bound, see setSelectionBound and resetSelectionBound.
         */
        inline Container * getSelectionBound(void) const {
            return selectionBound;
        }

        /**
         * Returns the time elapsed (in seconds) since the last frame. This elapsed time is set by our draw method.
         */
        inline GLfloat getElapsed(void) const {
            return elapsed;
        }

        /**
         * This method sets the specified component as the current keyboard selected component.
         * In essence this sets the focus to that component.
         *
         * This method will do nothing if the component is a container, the component is outside the selection bound,
         * or the component is not keyboard selectable.
         *
         * NOTE: THIS METHOD MODIFIES THE SELECTION SYSTEM, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void setKeyboardSelected(Component * component);

        /**
         * This method returns the currently selected keyboard component (see keyboardSelected), or NULL if none currently
         * exists.
         */
        inline Component * getKeyboardSelected(void) const {
            return keyboardSelected;
        }

        /**
         * This accesses the repaint controller contained within this top level container.
         * If partial repainting is not enabled, this will be NULL.
         */
        inline RepaintController * getRepaintController(void) const {
            return repaintController;
        }

        /**
         * Returns true if partial repainting is enabled, (i.e: the repaint controller exists (not NULL), because
         * enablePartialRepaint was passed as true in the constructor).
         */
        inline bool isPartialRepaintEnabled(void) const {
            return repaintController != NULL;
        }
        
        /**
         * Sets the "force empty repaint" flag on repaint controller, if partial repaint is enabled (else does nothing).
         * Typically, this causes the platform implementation to call doPartialRepaint even if no components require
         * repainting. That notifies the theme, by calling the theme's beforeDraw and afterDraw.
         * This is helpful when some change has been made which needs the theme to respond, such as a change to the
         * mouse cursor. For components which don't repaint in response to the mouse entering/exiting, this can be called
         * in onMouseEnter/onMouseLeave (etc) to allow a custom mouse cursor, without unnecessary repainting. See
         * Component's getCursorType.
         */
        void forceEmptyRepaint(void) const;
        
        /**
         * Returns the most recent (current) view rectangle that the TopLevelContainer has been given.
         * Components (and Themes) can use this to find out the current bounds that the TopLevelContainer
         * has been given.
         * If the partial repaint system is in use, the Theme should avoid performing any operations
         * outside this area.
         */
        inline const Rectangle & getLastView(void) const {
            return lastView;
        }
        
        /**
         * Gets the "highest priority" (i.e: largest enum value) of the cursor types in the components currently being
         * mouse-overed (i.e: in currentMouseOver). This can be used by themes in beforeDraw (or afterDraw), to update
         * the current cursor type. See documentation for CursorType in Theme.h, and Component's getCursorType.
         */
        CursorType getHighestPriorityCursorType(void) const;

        // ---------------------------- Scale management stuff --------------------------------------------------------------------

        /**
         * This can be used to change which scale pointers we use. NULL values here will default to a
         * a scale of 1.0f, 1.0f. THIS MUST NOT BE CALLED WHILE DRAWING.
         */
        void setScalePointers(const GLfloat * xScalePtr, const GLfloat * yScalePtr);

        /**
         * This allows components to access our x scale.
         */
        inline GLfloat getXScale(void) const {
            return lastXScale;
        }

        /**
         * This allows components to access our y scale.
         */
        inline GLfloat getYScale(void) const {
            return lastYScale;
        }

        // ========================================= Keyboard integration ===================================================

        /*
         * The UI system supports navigation by keyboard only, by using:
         *  > Next component key / previous component key  (arrow keys by default)
         *  > Next container key / previous container key  (tab and ` key by default)
         *
         * The component keys swap focus within a container only, the container keys swap focus between containers.
         */

    private:
        /**
         * Ensures that keyboard selection leaves this container. Used internally
         * within Container::escapeKeyboardSelection.
         */
        void escapeKeyboardSelectionContainer(Container * container);
        
        /**
         * This is used internally when keyboard integration is enabled, to clear the state from the user's mouse pointer.
         * This method:
         *     > Searches through components in currentMousePress, removes them from it, gives them a mouse release event, NOT a mouse click event.
         *     > Searches through components in currentMouseOver, removes them from it, gives them a mouse leave event.
         */
        void clearAllMouseOverAndClick(void);

        /**
         * This is used internally to enable keyboard integration.
         *
         * This method returns true if what has happened means we now should not switch to the next component,
         * for example: No selectable component has been found, or we are setting this from the start.
         */
        bool enableKeyboardIntegration(void);

        /**
         * This forces keyboard integration to be cleanly disabled.
         * This method should be called each time we receive a mouse event.
         */
        void disableKeyboardIntegration(void);
        
        /**
         * Used internally to switch keyboard selection from current to the specified
         * component. This must only be used when keyboard selection is already enabled.
         * It is assumed that the new selection should respond to keyboard.
         */
        void switchKeyboardSelection(Component * newSelection);
        
        /**
         * Assuming keyboard integration is enabled, and the current keyboard selection exists, this disables keyboard integration if
         * the keyboard selected component reports that it is no longer keyboard responsive. Returns true if keyboard integration has
         * been disabled. Must be run before performing operations on current keyboard selected component.
         */
        bool disableKeyboardSelectionIfInvalid(void);
        
    public:
        /**
         * This should be called when a key is pressed. If this is a special key event, then the special parameter should be set
         * to true, otherwise it should be false.
         *
         * NOTE: This will use the key bindings defined by our associated Bindings instance.
         *
         * NOTE: The distinction between a special key event and a normal key event is only relevant if the UI system is
         *       using OpenGL, (GLUT). If the UI system is using X11, (XCB), then the "special" parameter should ALWAYS
         *       BE SET TO FALSE.
         *
         * This should be called EVERY time glut passes a key press event, as to include key repeats.
         *
         * Key press events are used for navigation, i.e: Movement of selection from component to component. This is why
         * key repeat is required.
         *
         * Key press is also used for mouse pressing on the current button. The next key repeat is interpreted as releasing the
         * button, so the component gets a click event every other time there is a key repeat.
         *
         * This will return true if we have used to key event to actually do something, and false otherwise.
         * 
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        bool keyPress(int key, bool special);

        /**
         * This should be called when a key is pressed. If this is a special key event, then the special parameter should be set
         * to true, otherwise it should be false.
         *
         * NOTE: This will use the key bindings defined by our associated Bindings instance.
         *
         * NOTE: The distinction between a special key event and a normal key event is only relevant if the UI system is
         *       using OpenGL, (GLUT). If the UI system is using X11, (XCB), then the "special" parameter should ALWAYS
         *       BE SET TO FALSE.
         *
         * This should be called each time glut passes a key release event.
         *
         * Key release events are used for pressing buttons, so the user sees feedback of when they press and release the button.
         *
         * This will return true if we have used to key event to actually do something, and false otherwise.
         * 
         * This is a top level event, so if the TopLevelContainer owns it's own function queues, beforeEvent
         * and afterEvent are guaranteed to be updated here.
         */
        bool keyRelease(int key, bool special);
        
        /**
         * This method can be used to manually run a press navigation action, even if a key hasn't
         * been pressed. This will manage keyboard integration in exactly the same ways as if a key
         * has been pressed, (in fact, keyPress uses this function internally to do its logic).
         * 
         * An example use of runNavigationActionPress and runNavigationActionRelease, is manually pressing
         * a button. The advantage of doing this (as opposed to just running the button's on click function),
         * is this will run everything *as if* the button had been clicked - so it will appear visually
         * to have been clicked.
         * 
         * Be careful when manually sending navigation actions. For example, sending a press navigation action
         * and never sending a release, would cause a button to get "stuck" down, which would be confusing for
         * users. Note that sending multiple consecutive enter press actions will not cause the button to be
         * pressed multiple times - you must send release events after each one also.
         * 
         * This will return true if we have used to key event to actually do something, and false otherwise.
         * 
         * NOTE: Since here we don't know what key has been pressed (there might not even *be* one), this
         *       method does nothing (and returns false) if keyboard selection is locked.
         * 
         * NOTE: This function does not update any function queues, it is assumed that function queues
         *       will be updated by the calling code (i.e: keyPress, usually).
         * 
         * NOTE: THIS METHOD MODIFIES THE SELECTION SYSTEM, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        bool runNavigationActionPress(NavigationAction action);
        
        /**
         * This method can be used to manually run a release navigation action, even if a key hasn't
         * been release. This will manage keyboard integration in exactly the same ways as if a key
         * has been release, (in fact, keyRelease uses this function internally to do its logic).
         *
         * An example use of runNavigationActionPress and runNavigationActionRelease, is manually pressing
         * a button. The advantage of doing this (as opposed to just running the button's on click function),
         * is this will run everything *as if* the button had been clicked - so it will appear visually
         * to have been clicked.
         * 
         * Be careful when manually sending navigation actions. For example, sending a press navigation action
         * and never sending a release, would cause a button to get "stuck" down, which would be confusing for
         * users. Note that sending multiple consecutive enter press actions will not cause the button to be
         * pressed multiple times - you must send release events after each one also.
         * 
         * This will return true if we have used to key event to actually do something, and false otherwise.
         * 
         * NOTE: Since here we don't know what key has been released (there might not even *be* one), this
         *       method does nothing (and returns false) if keyboard selection is locked.
         * 
         * NOTE: This function does not update any function queues, it is assumed that function queues
         *       will be updated by the calling code (i.e: keyRelease, usually).
         * 
         * NOTE: THIS METHOD MODIFIES THE SELECTION SYSTEM, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        bool runNavigationActionRelease(NavigationAction action);

        
        // ============================== Selection locking system ==========================================================

        /**
         * This locks selection to the currently keyboard selected component. This will do nothing if there is no
         * current keyboard selection.
         *
         * Selection locking is used primarily for text inputs.
         *
         * Note that this is different to the selection bound system, since this redirects all keyboard events to a specific
         * component. Selection is not unlocked until unlockSelection is called. unlockSelection is automatically called if
         * the user clicks the mouse outside this component.
         *
         * NOTE: THIS METHOD MODIFIES THE SELECTION SYSTEM, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void lockSelection(void);

        /**
         * This unlocks selection from the current selection lock, or does nothing if the selection is not currently locked.
         * This is automatically called each time the user clicks the mouse.
         *
         * This method returns true if a selection lock has just been disabled, false otherwise.
         *
         * NOTE: THIS METHOD MODIFIES THE SELECTION SYSTEM, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        bool unlockSelection(void);

        /**
         * Returns true if the selection is currently locked to a component, false otherwise.
         */
        inline bool isSelectionLocked(void) const {
            return selectionLocked;
        }



        /**
         * Returns the component currently used as the selection lock, or NULL if none exists.
         * If selection locking is enabled, this will return exactly the same as getKeyboardSelected().
         */
        Component * getSelectionLock(void) const;


    private:
        /**
         * This is used internally to send raw key events to the currently selection-locked component.
         * This method returns:
         *   > true if there is currently a selection lock and the rest of the key event should be ignored,
         *     or if we have just unlocked selection.
         *   > false otherwise, if the reset of the key event should be used.
         */
        bool processKeyEventIfSelectionLocked(NavigationAction action, int key, bool special, bool isKeyPress);
    public:


        // ==================================================================================================================



        /**
         * This should be used by containers to trigger a recalculation of the view when components are added.
         */
        inline void triggerRecalculateNextFrame(void) {
            recalculateNextFrame = true;
        }



    private:
        // This allows us to control whether we will actually update any function queues.
        // Doing this allows internalMouseEvent to recursively call itself without problems.
        // It must call itself in the case of scroll buttons, where their mouse events must be immediately released.
        // If it calls itself, the child call must NOT update any function queues, since that is the job
        // of the parent call.
        bool internalMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY, bool updateFunctionQueues);

        // This internal version allows nested calls of mouseMove. If updateFunctionQueues is specified as false, no function queues will be updated.
        bool internalMouseMove(GLfloat viewX, GLfloat viewY, bool updateFunctionQueues);

        /**
         * This is called each time a component is removed from the hierarchy.
         * Here we ensure that the component is removed from all mouse and keyboard interaction data structures, it's state
         * is reset to default, and is removed from the RepaintController.
         *
         * NOTE: This must only be called on normal components, or containers where this method has ALREADY been called
         *       for all of it's children.
         */
        void topLevelRemove(Component * component);
    };



    /**
     * This is provided purely for the method mouseEvent.
     * These two values, rascUI::MouseEvents::PRESS and rascUI::MouseEvents::RELEASE respectively
     * represent the two constants GLUT_DOWN and GLUT_UP.
     * This is provided so that when using TopLevelContainer, the GLUT library header is not required.
     */
    class MouseEvents {
    public:
        const static int PRESS,
                         RELEASE;
    };
}

#endif
