/*
 * ThemeGroup.h
 *
 *  Created on: 1 Jul 2019
 *      Author: wilson
 */

#ifndef SRC_RASCUI_BASE_THEMEGROUP_H_
#define SRC_RASCUI_BASE_THEMEGROUP_H_

#include <vector>
#include "Theme.h"

namespace rascUI {

    /**
     * A ThemeGroup allows multiple theme implementations to be grouped together.
     * For any method called in the ThemeGroup, the same respective method
     * is called for all of the ThemeGroup's child Theme instances.
     *
     * The methods are called in order that the child theme instances were added to the ThemeGroup,
     * WITH THE EXCEPTION OF THE afterDraw and destroy METHODS, WHERE the methods are called
     * in REVERSE order that the instances were added to the group.
     *
     * This allows, for example, a base Theme implementation and a secondary Theme implementation
     * to be linked together. The first one could do common actions like performing double
     * buffering, and the second could just be a pure Theme implementation. This would allow
     * the second one to be swapped out, without duplicating the code in the base theme.
     *
     * For Theme getter methods, (like customGetTextColour and customGetCharWidth), the method from
     * the last theme in the group is run.
     */
    class ThemeGroup : public rascUI::Theme {
    private:
        std::vector<Theme *> themes;
    public:

        /**
         * To create a ThemeGroup, a template theme is required.
         * The ThemeGroup sets the following properties from the template theme:
         *   normalComponentWidth,
         *   normalComponentHeight,
         *   uiBorder,
         *   customColourMainText,
         *   customColourSecondaryText,
         *   customColourInactiveText,
         *   customColourBackplaneMain,
         *   customColourBackplaneLight,
         *   customColourBackplaneDark.
         * NOTE: The template theme is NOT automatically added to the ThemeGroup,
         *       and the template theme MUST continue to exist for the duration of the time that the ThemeGroup is used.
         */
        ThemeGroup(Theme * templateTheme);
        virtual ~ThemeGroup(void);

        /**
         * Adds a child theme to this group.
         *  The methods are called in order that the child theme instances were added to the ThemeGroup,
         * WITH THE EXCEPTION OF THE afterDraw METHOD, WHERE the methods are called
         * in REVERSE order that the instances were added to the group.
         */
        void addTheme(Theme * theme);

    protected:
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
        virtual void afterDraw(const Rectangle & drawnArea) override;
        virtual void redrawFromBuffer(void) override;
        virtual void * getDrawBuffer(void) override;
    protected:
        virtual void destroy(void) override;
    public:
        virtual void onChangeViewArea(const Rectangle & view) override;
        virtual void drawBackPanel(const Location & location) override;
        virtual void drawFrontPanel(const Location & location) override;
        virtual void drawText(const Location & location, const std::string & string) override;
        virtual void drawButton(const Location & location) override;
        virtual void drawTextEntryBox(const Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const Location & location) override;
        virtual void drawScrollContentsBackground(const Location & location) override;
        virtual void drawScrollPuck(const Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const Location & location, bool vertical) override;
        virtual void drawFadePanel(const Location & location) override;
        virtual void drawCheckboxButton(const Location & location) override;
        virtual void drawProgressBar(const Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const Location & location) override;
        virtual void drawSlider(const Location & location, GLfloat position, bool vertical, bool inverted) override;

        virtual void customSetDrawColour(void * colour) override;
        virtual void * customGetTextColour(const Location & location) override;
        virtual void customDrawTextMouse(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextNoMouse(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextTitle(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void getTextBaseOffsets(State state, GLfloat & offX, GLfloat & offY) override;
    };
}

#endif
