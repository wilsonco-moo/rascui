/*
 * Container.cpp
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#include "Container.h"
#include "TopLevelContainer.h"
#include <iostream>

namespace rascUI {

    Container::Container(const Location & location) : Component(location) {
    }

    Container::~Container(void) {
    }

    void Container::setTopLevel(TopLevelContainer * topLevel) {
        if (this->topLevel != topLevel) {
            this->topLevel = topLevel;
            for (Component * comp : components) {
                comp->setTopLevel(topLevel);
            }
            // Tell our location to update it's state, since we have just been added to a top level container.
            location.updateState(this);
        }
    }

    void Container::resetTopLevel(void) {
        for (Component * component : components) {
            component->resetTopLevel();
        }
        Component::resetTopLevel();
    }

    void Container::addIfMouseOver(GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver) {
        // If the mouse is over us, and we are visible:
        if (location.cache.contains(viewX, viewY) && isVisible()) {
            mouseOver->insert(this);
            for (Component * comp : components) {
                comp->addIfMouseOver(viewX, viewY, mouseOver);
            }
        }
    }

    void Container::add(Component * component) {
        if (component->parent == NULL) {
            components.push_back(component);
            component->iter = --components.end();
            component->parent = this;
            component->setTopLevel(topLevel);

            // If we actually did add the component, call onAdd.
            onAdd(component);

            // If we have a top level container, tell it to recalculate it's view next frame.
            if (topLevel != NULL) {
                topLevel->triggerRecalculateNextFrame();
            }
        }
    }
    
    void Container::addBefore(const Component * siblingComponent, Component * component) {
        if (component->parent == NULL && siblingComponent->parent == this) {
            component->iter = components.insert(siblingComponent->iter, component);
            component->parent = this;
            component->setTopLevel(topLevel);

            // If we actually did add the component, call onAdd.
            onAdd(component);

            // If we have a top level container, tell it to recalculate it's view next frame.
            if (topLevel != NULL) {
                topLevel->triggerRecalculateNextFrame();
            }
        }
    }
    
    void Container::addAfter(const Component * siblingComponent, Component * component) {
        if (component->parent == NULL && siblingComponent->parent == this) {
            std::list<Component *>::iterator iter = siblingComponent->iter;
            ++iter;
            component->iter = components.insert(iter, component);
            component->parent = this;
            component->setTopLevel(topLevel);

            // If we actually did add the component, call onAdd.
            onAdd(component);

            // If we have a top level container, tell it to recalculate it's view next frame.
            if (topLevel != NULL) {
                topLevel->triggerRecalculateNextFrame();
            }
        }
    }
    
    void Container::remove(Component * component) {
        if (component->parent == this) {

            // If they have a top level container, i.e: They are a sub-component of a top level container, not just a sub-component
            // of a random container:
            if (component->topLevel != NULL) {
                // If removing a container, make sure keyboard selection is removed from it. This ensures keyboard selection
                // does not remain on no longer existing components.
                if (component->isContainer()) {
                    ((Container *)component)->escapeKeyboardSelection();
                }
                
                // Call resetTopLevel - this will recursively go through this component's hierarchy, calling resetTopLevel
                // and it's top level component's topLevelRemove method. This does two things:
                //  > Ensures the topLevel field of all components is reset
                //  > Ensures the top level container knows about every single component that is removed.
                component->resetTopLevel();
            }

            components.erase(component->iter);                    // Erase from our components list
            component->parent = NULL;                             // Reset the parent pointer to NULL
            component->iter = std::list<Component *>::iterator(); // Reset the iterator to a default constructed one.

            // If we actually did remove the component, call onRemove
            onRemove(component);
        }
    }

    void Container::clear(void) {
        while(!components.empty()) {
            remove(components.back());
        }
    }

    void Container::clearHierarchy(void) {
        // First clear the hierarchy of subcontainers
        for (Component * component : components) {
            if (component->isContainer()) {
                ((Container *)component)->clearHierarchy();
            }
        }
        // Next clear ourself.
        clear();
    }

    void Container::escapeKeyboardSelection(void) {
        getTopLevel()->escapeKeyboardSelectionContainer(this);
    }
    
    void Container::printIndent(int indent) {
        for (int i = 0; i < indent; i++) {
            std::cout << ' ';
        }
        std::cout << name;
        std::cout << " {\n";
        int nextIndent = indent + 4;
        for (Component * component : components) {
            component->printIndent(nextIndent);
        }
        for (int i = 0; i < indent; i++) {
            std::cout << ' ';
        }
        std::cout << "};\n";
    }

    void Container::recalculate(const Rectangle & parentSize) {
        Component::recalculate(parentSize);
        for (Component * comp : components) {
            comp->recalculate(location.cache);
        }
    }

    void Container::onDraw(void) {
        for (Component * comp : components) {
            if (comp->location.getState() != State::invisible) {
                comp->onDraw();
            }
        }
    }

    // Provide stub implementations of these methods, so implementing classes do not need to override them.
    void Container::onAdd(Component * component) {}
    void Container::onRemove(Component * component) {}



    bool Container::shouldRespondToKeyboard(void) const {
        for (Component * comp : components) {
            if (comp->shouldRespondToKeyboard()) return true;
        }
        return false;
    }

    void Container::onChildKeyboardSelect(Component * component) {
        // If we are the selection bound, don't call this method for our parent.
        if (this != getTopLevel()->getSelectionBound()) {
            getParent()->onChildKeyboardSelect(this);
        }
    }

    // ====================================== Repaint system =====================================================

    void Container::boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) {

        // First get the bounded area - the repaint area which we are assumed to take up.
        Rectangle boundedArea = getBoundedArea();

        // If we explicitly want repainting, or we overlap the bounding box area
        if (wantsRepaint.find(this) != wantsRepaint.end() || boundingBox.overlaps(boundedArea)) {
            onDraw();
            boundingBox += boundedArea;
            return;
        }

        // If we don't overlap the bounding box, and none of our child components want repainting, do nothing.
        if (childWantsRepaint.find(this) == childWantsRepaint.end()) {
            return;
        }

        // Now, we can assume that at least one child component wants repainting, but nothing already overlaps us,
        // so just run boundedRepaint for all visible child components.
        for (Component * component : components) {
            if (component->location.getState() != State::invisible) {
                component->boundedRepaint(boundingBox, wantsRepaint, childWantsRepaint);
            }
        }
    }

    // ===========================================================================================================

    bool Container::isContainer(void) const {
        return true;
    }

    // -------------------------------------------- Keyboard navigation methods ----------------------------------------------

    Component * Container::getFirstKeyboardResponsiveComponent(void) const {
        // Loop through all components.
        for (Component * component : components) {
            // Return the first one we find that is not a container, and is keyboard responsive.
            if (!component->isContainer() && component->shouldRespondToKeyboard()) {
                return component;
            }
        }
        // If none are found, return NULL.
        return NULL;
    }
    Component * Container::getFirstKeyboardResponsiveComponentDepth(void) const {
        // Loop through all components.
        for (Component * component : components) {
            // Only look at visible components.
            if (component->isVisible()) {
                if (component->isContainer()) {
                    // If the component is a Container, then recursively call.
                    Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentDepth();
                    // If they return a valid component, then return it.
                    if (found != NULL) {
                        return found;
                    }
                } else if (component->shouldRespondToKeyboard()) {
                    // If the component is not a container, but is keyboard responsive, then return it.
                    return component;
                }
            }
        }
        // If none are found, return NULL.
        return NULL;
    }
    Component * Container::getFirstKeyboardResponsiveComponentNonDepth(void) const {
        for (Component * component : components) {
            if (!component->isContainer() && component->shouldRespondToKeyboard()) {
                return component;
            }
        }
        for (Component * component : components) {
            // Only look at visible containers.
            if (component->isContainer() && component->isVisible()) {
                Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentNonDepth();
                if (found != NULL) {
                    return found;
                }
            }
        }
        return NULL;
    }
    Component * Container::getLastKeyboardResponsiveComponent(void) const {
        // Loop through all components backwards.
        std::list<Component *>::const_reverse_iterator iter = components.rbegin();
        while(iter != components.rend()) {
            Component * component = *iter;
            // Return the first one we find that is not a container, and is keyboard responsive.
            if (!component->isContainer() && component->shouldRespondToKeyboard()) {
                return component;
            }
            ++iter;
        }
        // If none are found, return NULL.
        return NULL;
    }
    Component * Container::getLastKeyboardResponsiveComponentDepth(void) const {
        // Loop through all components backwards.
        std::list<Component *>::const_reverse_iterator iter = components.rbegin();
        while(iter != components.rend()) {
            Component * component = *iter;
            // Only look at visible components.
            if (component->isVisible()) {
                if (component->isContainer()) {
                    // If the component is a Container, then recursively call.
                    Component * found = ((Container *)component)->getLastKeyboardResponsiveComponentDepth();
                    // If they return a valid component, then return it.
                    if (found != NULL) {
                        return found;
                    }
                } else if (component->shouldRespondToKeyboard()) {
                    // If the component is not a container, but is keyboard responsive, then return it.
                    return component;
                }
            }
            ++iter;
        }
        // If none are found, return NULL.
        return NULL;
    }
    Component * Container::getLastKeyboardResponsiveComponentNonDepth(void) const {
        std::list<Component *>::const_reverse_iterator iter = components.rbegin();
        while(iter != components.rend()) {
            Component * component = *iter;
            if (!component->isContainer() && component->shouldRespondToKeyboard()) {
                return component;
            }
            ++iter;
        }
        iter = components.rbegin();
        while(iter != components.rend()) {
            Component * component = *iter;
            // Only look at visible containers.
            if (component->isContainer() && component->isVisible()) {
                Component * found = ((Container *)component)->getLastKeyboardResponsiveComponentNonDepth();
                if (found != NULL) {
                    return found;
                }
            }
            ++iter;
        }
        return NULL;
    }







    Component * Container::containerSwitchUpwardSearch(void) const {
        // If we are the root container within the current selection bound, then we have no sibling components to search, so return the first
        // keyboard responsive component within us. This circles selection around to the beginning.
        if (this == topLevel->getSelectionBound()) {
            return getFirstKeyboardResponsiveComponentNonDepth();
        }

        std::list<Component *>::iterator searchIter = iter;
        while(true) {
            searchIter++;
            // If we have searched all sibling components, then ...
            if (searchIter == parent->components.end()) {
                // Recursively call upwards (to our parent).
                return parent->containerSwitchUpwardSearch();
            }
            Component * component = *searchIter;
            // If we have found a visible sibling container - (one that is after us, a sub-container of our parent) ...
            if (component->isContainer() && component->isVisible()) {
                // Then call it's getLastKeyboardResponsiveComponentNonDepth method, and return it if they return non-NULL.
                Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentNonDepth();
                if (found != NULL) {
                    return found;
                }
            }
        }
    }


    Component * Container::containerSwitchDownwardSearch(void) const {
        // First, loop through all of our subcomponents backwards.
        std::list<Component *>::const_reverse_iterator iter = components.rbegin();
        while(iter != components.rend()) {
            Component * component = *iter;

            // If any of these are visible containers, then recursively call their downward search method. This moves selection
            // to the furthest-down component at the right-side of the component hierarchy.
            if (component->isContainer() && component->isVisible()) {
                Component * found = ((Container *)component)->containerSwitchDownwardSearch();
                if (found != NULL) {
                    return found;
                }
            }
            ++iter;
        }

        // If we have no sub-containers we can move selection into, then search ourself for keyboard interactable components.
        // This will return NULL if none are found: The search failed.
        return getFirstKeyboardResponsiveComponent();
    }

}





























