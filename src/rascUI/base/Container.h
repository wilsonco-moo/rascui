/*
 * Container.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_BASE_CONTAINER_H_
#define RASCUI_BASE_CONTAINER_H_

#include "Component.h"

namespace rascUI {

    class TopLevelContainer;

    /**
     * Container is a specialised version of a Component, which has the ability to store an
     * arbitrary number of Components within it.
     *  > In essence, a Container is a Component that contains Components.
     */
    class Container : public Component {
    private:

        // Make TopLevelContainer a friend class, so it can call our overridden addIfMouseOver method.
        friend class TopLevelContainer;
        // Make Component a friend class, for it's keyboard interaction search methods.
        friend class Component;

        /**
         * This stores the components within the container. The components are stored in a list to preserve their order.
         * New components are added to the back of the list.
         * This order defines the order in which components are drawn, and the keyboard navigation order.
         */
        std::list<Component *> components;

        virtual void setTopLevel(TopLevelContainer * topLevel) override;
        virtual void resetTopLevel(void) override;

    protected:
        virtual void addIfMouseOver(GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver) override;

        /**
         * This allows a custom container implementation to call the addIfMouseOver method of one of it's child
         * components. This gets around the protected modifier for calling addIfMouseOver of base Components.
         * This is possible for Container to do, since Container is a friend class of Component.
         */
        inline void callAddIfMouseOverChildComponent(Component * component, GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver) {
            component->addIfMouseOver(viewX, viewY, mouseOver);
        }

    public:
        Container(const Location & location = Location());
        virtual ~Container(void);

        /**
         * This method adds a component to this container.
         * Note: This method is unusual in the fact we do not have to be assigned to a top level container for this method to be called.
         *       Top level containers of subcomponents are automatically assigned.
         *
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void add(Component * component);
        
        /**
         * Works the same as the regular add method, except component is added immediately
         * before siblingComponent, in both draw order and keyboard selection order.
         * The provided sibling component MUST ALREADY BE a child of THIS container,
         * otherwise this method will do nothing.
         * 
         * Note: Some custom container implementations (like SegmentContainer) do not
         *       support addBefore and addAfter.
         * 
         * Note: This method is unusual in the fact we do not have to be assigned to a top level container for this method to be called.
         *       Top level containers of subcomponents are automatically assigned.
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void addBefore(const Component * siblingComponent, Component * component);
        
        /**
         * Works the same as the regular add method, except component is added immediately
         * after siblingComponent, in both draw order and keyboard selection order.
         * The provided sibling component MUST ALREADY BE a child of THIS container,
         * otherwise this method will do nothing. The addBefore and addAfter methods
         * allow components to exist in a different order to which they are added.
         * 
         * Note: Some custom container implementations (like SegmentContainer) do not
         *       support addBefore and addAfter.
         * 
         * Note: This method is unusual in the fact we do not have to be assigned to a top level container for this method to be called.
         *       Top level containers of subcomponents are automatically assigned.
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void addAfter(const Component * siblingComponent, Component * component);

        /**
         * This method removes a component (or component hierarchy) properly from this container, notifying the top level container as to
         * reset the state of all component(s) involved.
         *
         * The component (and all of it's subcomponent's) pointer to the top level container will be reset, and the top level container
         * will be notified of all components removed.
         *
         * Note: When a container is removed, Container::escapeKeyboardSelection is automatically called to ensure
         *       that keyboard selection is moved out of the container.
         * 
         * Note: This method is unusual in the fact we do not have to be assigned to a top level container for this method to be called.
         *       The top level container will not be notified if it does not exist.
         *
         * Note: Removing a component cleanly is MUCH slower than adding it, as a large amount of additional checking must be done to ensure
         *       that the state of all components involved is reset. If removing a container, this method will take time proportional to the
         *       total number of components in that container's hierarchy.
         *
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         *
         * NOTE: BE CAREFUL:
         *       THIS METHOD CAN RESULT IN COMPONENT EVENTS onMouseRelease and onMouseLeave BEING CALLED ON THE
         *       COMPONENT(S) REMOVED. HOWEVER, THIS CALL WILL NOT UPDATE ANY FUNCTION QUEUES, SO ANY FUNCTIONS
         *       ADDDED FROM WITHIN THOSE EVENTS MAY NOT BE CALLED FOR SOME TIME, EVEN POSSIBLY AFTER THE COMPONENT
         *       IS DELETED.
         */
        void remove(Component * component);

        /**
         * Cleanly removes all of our components, using the remove method.
         * Components are removed in reverse order to the order they were added. This is because removing from the start
         * is much less efficient for some types of custom Containers, such as SegmentContainer.
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void clear(void);

        /**
         * This will recursively call clear on all components in our component hierarchy.
         *
         * NOTE: THIS METHOD MODIFIES THE COMPONENT HIERARCHY, DO NOT DIRECTLY CALL THIS FROM WITHIN A COMPONENT EVENT, SUCH AS:
         *       onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         *       OTHERWISE, UNDEFINED BEHAVIOUR WILL RESULT.
         *
         *       FROM THESE EVENTS TopLevelContainer::afterEvent MUST BE USED TO CALL THIS METHOD.
         */
        void clearHierarchy(void);

        /**
         * This returns the number of components we currently have in the container.
         */
        inline size_t getComponentCount(void) const {
            return components.size();
        }
        
        /**
         * Gets our last child component, only valid if getComponentCount is not zero.
         * This is mainly helpful for custom containers: In the onAdd method, comparing
         * this with the added component shows whether the component was added at
         * the back.
         */
        inline Component * getLastComponent(void) {
            return components.back();
        }
        
        /**
         * Ensures that keyboard selection (and prospective keyboard selection), leaves
         * this container, so this container is ready to be removed or made invisible.
         * Keyboard selection is moved using a container switch upward search.
         * 
         * If the container is outside the selection bound, or if no component
         * within the selection bound to move selection to can be found, keyboard
         * integration is disabled. This is automatically called when containers are
         * removed or made invisible.
         */
        void escapeKeyboardSelection(void);
        
    protected:

        /**
         * Container provides a way to print all of it's components easily.
         */
        virtual void printIndent(int indent) override;

        virtual void recalculate(const Rectangle & parentSize) override;
        virtual void onDraw(void) override;

        /**
         * This is called after each time we add a component to this container.
         */
        virtual void onAdd(Component * component);

        /**
         * This is called after each time we remove a component from this container. Implementing
         * classes should use this to remove this component from any internal
         * data structures.
         */
        virtual void onRemove(Component * component);


        /**
         * Here we will return true if at least one of our child components can respond to keyboard actions,
         * and false otherwise.
         */
        virtual bool shouldRespondToKeyboard(void) const override;

        /**
         * This is called whenever keyboard selection is enabled for any of our child components.
         *
         * This method is also called if any children of our child components become keyboard selected, but
         * in that case the parameter supplied should be one of our direct child components.
         *
         * IMPORTANT NOTICE: If a class overrides this method, it MUST call the superclass onChildKeyboardSelect
         *                   method at the end. This ensures that our parent container receives this event too.
         *                   This event propogating up the component tree stops at the selection bound.
         */
        virtual void onChildKeyboardSelect(Component * component);

        // ====================================== Repaint system =====================================================

        /**
         * This method overrides boundedRepaint from Component, and should generally do the same thing.
         *  > Run our normal onDraw method if our getBoundedArea() overlaps the bounding box, or we are explicitly
         *    listed in wantsRepaint, then expand the bounding box.
         *  > If at least one of our children wants repainting, then run this method for all of our visible children.
         */
        virtual void boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) override;

        // ===========================================================================================================

    private:
        /**
         * Here, we return true, since we are a container.
         */
        virtual bool isContainer(void) const override;

    // -------------------------------------------- Keyboard navigation methods ----------------------------------------------
    public:
        /**
         * This method returns the first keyboard-responsive non-container component within THIS container, or NULL if one does not exist.
         * This method DOES NOT recursively search sub-containers.
         * NOTE: This method is virtual so that custom containers can modify the order in which components are selected.
         */
        virtual Component * getFirstKeyboardResponsiveComponent(void) const;

        /**
         * This method returns the first keyboard-responsive non-container component within our component hierarchy, by performing a depth-first
         * search. This method WILL recursively search sub-containers.
         * NULL is returned if one cannot be found.
         * NOTE: This method is virtual so that custom containers can modify the order in which components are selected.
         */
        virtual Component * getFirstKeyboardResponsiveComponentDepth(void) const;

        /**
         * This method returns the first keyboard-responsive non-container component within our component hierarchy, not performing a true depth-based
         * search. This method WILL recursively search sub-containers.
         * NULL is returned if one cannot be found.
         * This method will use neither a depth-based search, nor will it use a true breadth-first search. Instead, it will search all
         * components, THEN call this recursive method on sub-containers. This method is used for container switching in the keyboard interaction,
         * as searching in this order ensures consistent switching order.
         * Note: This is up to twice as expensive as a depth-first search.
         * NOTE: This method is virtual so that custom containers can modify the order in which components are selected.
         */
        virtual Component * getFirstKeyboardResponsiveComponentNonDepth(void) const;

        /**
         * This method returns the last keyboard-responsive non-container component within THIS container, or NULL if one does not exist.
         * This method DOES NOT recursively search sub-containers.
         * NOTE: This method is virtual so that custom containers can modify the order in which components are selected.
         */
        virtual Component * getLastKeyboardResponsiveComponent(void) const;

        /**
         * This method returns the last keyboard-responsive non-container component within our component hierarchy, by performing a depth-first
         * search. This method WILL recursively search sub-containers.
         * NULL is returned if one cannot be found.
         * NOTE: This method is virtual so that custom containers can modify the order in which components are selected.
         */
        virtual Component * getLastKeyboardResponsiveComponentDepth(void) const;

        /**
         * This method returns the last keyboard-responsive non-container component within our component hierarchy, not performing a true depth-based
         * search. This method WILL recursively search sub-containers.
         * NULL is returned if one cannot be found.
         * This method will use neither a depth-based search, nor will it use a true breadth-first search. Instead, it will search all
         * components, THEN call this recursive method on sub-containers. This method is used for container switching in the keyboard interaction,
         * as searching in this order ensures consistent switching order.
         * Note: This is up to twice as expensive as a depth-first search.
         * NOTE: This method is virtual so that custom containers can modify the order in which components are selected.
         */
        virtual Component * getLastKeyboardResponsiveComponentNonDepth(void) const;

        /**
         * This method is used internally when switching container.
         * Here, an upward search for containers to switch to is performed. If we are the root container, then
         * getFirstKeyboardResponsiveComponentNonDepth is returned. Otherwise, this method calls getFirstKeyboardResponsiveComponentNonDepth on all
         * components after us within our parent. If any of them return non-NULL, this component is used. Otherwise, this method will recursively
         * call on our parent.
         */
        Component * containerSwitchUpwardSearch(void) const;

        /**
         * This method performs a specific kind of responsive keyboard component search, useful only for backward container switching.
         * This method:
         *  > First loops backwards through all our sub-components, recursively calls if each one is a container, and returns if they found
         *    a component.
         *  > Otherwise, searches forwards for keyboard interactive components, returning NULL if none are found.
         *
         * That means this method prioritises in the search:
         *  > Sub-containers are picked first, in reverse order that they appear in us
         *  > Our direct components are picked afterwards, in forward order they appear in us
         */
        Component * containerSwitchDownwardSearch(void) const;



    protected:
        // ----------------------------- Accessor methods for child components -----------------------------------

        // These methods get around modifiers on various methods, which must be accessible for custom Container
        // implementations.

        /**
         * This allows a custom container implementation to call the recalculate method of one of it's child
         * components.
         */
        inline void childRecalculate(Component * component, const Rectangle & parentSize) {
            component->recalculate(parentSize);
        }

        /**
         * This allows a custom container implementation to call the onDraw method of one of it's child
         * components.
         */
        inline void childOnDraw(Component * component) {
            component->onDraw();
        }

        /**
         * This allows a custom container implementation to call the isContainer method of one of it's child
         * components.
         */
        inline bool childIsContainer(Component * component) const {
            return component->isContainer();
        }

        /**
         * This allows a custom container implementation to call the shouldRespondToKeyboard method of one of it's child
         * components.
         */
        inline bool childShouldRespondToKeyboard(Component * component) const {
            return component->shouldRespondToKeyboard();
        }

        /**
         * This allows a custom container implementation to call the childBoundedRepaint method of one of it's child
         * components.
         */
        inline void childBoundedRepaint(Component * component, Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) {
            component->boundedRepaint(boundingBox, wantsRepaint, childWantsRepaint);
        }
    };
}

#endif
