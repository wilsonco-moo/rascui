/*
 * TopLevelContainer.cpp
 *
 *  Created on: 2 Oct 2018
 *      Author: wilson
 */

#include "TopLevelContainer.h"

#include <algorithm>
#include <iostream>
#include <cstdlib>

#include "../util/RepaintController.h"
#include "../util/FunctionQueue.h"
#include "../util/Bindings.h"
#include "Theme.h"

namespace rascUI {

    const GLfloat TopLevelContainer::defaultScale = 1.0f;

    const int MouseEvents::PRESS   = 0x0000, //GLUT_DOWN
              MouseEvents::RELEASE = 0x0001; //GLUT_UP

    /**
     * This must be run at the start of all top level events. This updates beforeEvent and
     * afterEvent, if doing so is appropriate. The afterEvent function queue is updated using
     * a FunctionQueueUpdater instance, to ensure it always gets updated at the end, after the
     * function returns.
     * The two parameters should be given true/false values to specify which of the function queues
     * should be updated.
     */
    #define UPDATE_FUNCTION_QUEUES(updateBeforeEvent, updateAfterEvent)                                                            \
        /* Updating afterEvent will be the LAST thing we do, (i.e: AFTER the function returns).                                 */ \
        FunctionQueue::FunctionQueueUpdater UPDATE_FUNC_afterEvent(((updateAfterEvent) && ownAfterEvent) ? afterEvent : NULL);     \
                                                                                                                                   \
        /* Update the beforeEvent function queue before doing any event stuff.                                                  */ \
        if ((updateBeforeEvent) && ownBeforeEvent) { beforeEvent->update(); }


    TopLevelContainer::TopLevelContainer(Theme * theme, const Bindings * bindings, bool allowPartialRepaint, FunctionQueue * beforeEvent, FunctionQueue * afterEvent) :
        TopLevelContainer(theme, bindings, NULL, NULL, Location(), allowPartialRepaint, beforeEvent, afterEvent) {
    }

    TopLevelContainer::TopLevelContainer(Theme * theme, const Bindings * bindings, const Location & location, bool allowPartialRepaint, FunctionQueue * beforeEvent, FunctionQueue * afterEvent) :
        TopLevelContainer(theme, bindings, NULL, NULL, Location(), allowPartialRepaint, beforeEvent, afterEvent) {
    }

    TopLevelContainer::TopLevelContainer(Theme * theme, const Bindings * bindings, const GLfloat * xScalePtr, const GLfloat * yScalePtr, bool allowPartialRepaint, FunctionQueue * beforeEvent, FunctionQueue * afterEvent) :
        TopLevelContainer(theme, bindings, xScalePtr, yScalePtr, Location(), allowPartialRepaint, beforeEvent, afterEvent) {
    }

    TopLevelContainer::TopLevelContainer(Theme * theme, const Bindings * bindings, const GLfloat * xScalePtr, const GLfloat * yScalePtr, const Location & location, bool allowPartialRepaint, FunctionQueue * providedBeforeEvent, FunctionQueue * providedAfterEvent) :
        Container(location),
        lastView(),
        recalculateNextFrame(true),
        lastXScale(0.0f),
        lastYScale(0.0f),
        lastMouseViewX(0.0f),
        lastMouseViewY(0.0f),
        forceNextMouseMove(true),
        selectionLocked(false),
        selectionUnlockIgnoreKey(-1),
        set0(),
        set1(),
        currentMouseOver(&set0),
        newMouseOver(&set1),
        currentMousePress(),
        keyboardSelected(NULL),
        selectionBound(this),
        keyboardSelectedEnterKeyPressed(false),
        prospectiveKeyboardComponent(NULL),
        elapsed(0.0f),
        repaintController(allowPartialRepaint ? new RepaintController(this) : NULL),
        ownBeforeEvent(providedBeforeEvent == NULL), // We own function queues if provided values are NULL.
        ownAfterEvent(providedAfterEvent == NULL),
        beforeDrawThemeUserData(NULL),
        theme(theme),
        bindings(bindings),
        beforeEvent((providedBeforeEvent == NULL) ? new FunctionQueue() : providedBeforeEvent), // Create new function queues if provided values are NULL.
        afterEvent((providedAfterEvent == NULL) ? new FunctionQueue() : providedAfterEvent) {

        setScalePointers(xScalePtr, yScalePtr);

        // Set our topLevel field to ourself, to ensure that this is set for our child components.
        topLevel = this;
    }


    TopLevelContainer::~TopLevelContainer(void) {
        // Delete the before event and after event function queues, but ONLY IF we own them.
        if (ownBeforeEvent) {
            delete beforeEvent;
        }
        if (ownAfterEvent) {
            delete afterEvent;
        }

        // Delete the repaint controller. If partial repaint is disabled, this will be NULL.
        delete repaintController;
    }


    void TopLevelContainer::setTopLevel(TopLevelContainer * topLevel) {
        std::cerr << "RascUI: WARNING: A top level container has been added to another container, THIS MAY CAUSE UNDEFINED BEHAVIOUR.\n";
    }


    void TopLevelContainer::draw(const Rectangle & view, GLfloat elapsed) {
        UPDATE_FUNCTION_QUEUES(true, true)

        // Save the elapsed time.
        this->elapsed = elapsed;

        // There is no point doing any drawing if we are invisible.
        if (location.getState() != State::invisible) {

            // Run the beforeDraw call before we do drawing.
            theme->beforeDraw(beforeDrawThemeUserData);
            
            // If the view area has changed, or we are required to recalculate, force a recalculation and notify the Theme.
            if (recalculateNextFrame || view != lastView || *xScalePtr != lastXScale || *yScalePtr != lastYScale) {
                lastView = view;
                lastXScale = *xScalePtr;
                lastYScale = *yScalePtr;
                recalculateNextFrame = false;
                recalculate(view);
                theme->onChangeViewArea(view);
            }
            
            // Draw the top level container. We MUST start from the top level for drawing, not the selection bound,
            // since components outside the selection bound are still drawn.
            onDraw();
            
            // Run the afterDraw call after we have completed drawing. Since we are not using partial repaint,
            // provide the afterDraw method with the view area.
            theme->afterDraw(view);
        }
    }
    
    void TopLevelContainer::bufferedDraw(const Rectangle & view, GLfloat elapsed) {
        UPDATE_FUNCTION_QUEUES(true, true)
        
        // Do nothing unless we actually have a repaint controller.
        if (isPartialRepaintEnabled()) {

            // Save the elapsed time.
            this->elapsed = elapsed;

            // There is no point doing any drawing if we are invisible.
            if (location.getState() != State::invisible) {
                
                // Run the beforeDraw call before we do drawing.
                theme->beforeDraw(beforeDrawThemeUserData);
                
                // If the view area has changed, or we are required to recalculate, force a recalculation and notify the Theme.
                bool viewAreaChanged = (recalculateNextFrame || view != lastView || *xScalePtr != lastXScale || *yScalePtr != lastYScale);
                if (viewAreaChanged) {
                    lastView = view;
                    lastXScale = *xScalePtr;
                    lastYScale = *yScalePtr;
                    recalculateNextFrame = false;
                    recalculate(view);
                    theme->onChangeViewArea(view);
                }
                
                // Draw from top level if a repaint is required or the view area has changed, otherwise use the Theme's
                // redraw from buffer method.
                if (viewAreaChanged || repaintController->isRepaintRequired()) {
                    repaintController->clearRepaintSets();
                    onDraw();
                } else {
                    theme->redrawFromBuffer();
                }
                
                // Run the afterDraw call after we have completed drawing. Since we are not (properly) using partial repaint,
                // provide the afterDraw method with the view area.
                theme->afterDraw(view);
            }
        }
    }

    void TopLevelContainer::doPartialRepaint(const Rectangle & view, GLfloat elapsed) {
        UPDATE_FUNCTION_QUEUES(true, true)

        // Do nothing unless partial repaint is enabled.
        if (isPartialRepaintEnabled()) {

            // Save the elapsed time.
            this->elapsed = elapsed;

            // There is no point doing any drawing if we are invisible.
            if (location.getState() != State::invisible) {
                
                // Run the beforeDraw call before we do drawing.
                theme->beforeDraw(beforeDrawThemeUserData);
                
                // If the view area has changed, or we are required to recalculate, force a recalculation and notify the Theme.
                if (recalculateNextFrame || view != lastView || *xScalePtr != lastXScale || *yScalePtr != lastYScale) {
                    lastView = view;
                    lastXScale = *xScalePtr;
                    lastYScale = *yScalePtr;
                    recalculateNextFrame = false;
                    recalculate(view);
                    theme->onChangeViewArea(view);
                }

                // Use the repaint controller to do a partial repaint: it returns the total screen area which has changed.
                Rectangle finalBoundingBox = repaintController->doRepaint();
            
                // Run the afterDraw call after we have completed drawing. Provided the theme with the rectangle
                // total area drawn.
                theme->afterDraw(finalBoundingBox);
            }
        }
    }



    void TopLevelContainer::setKeyboardSelected(Component * component) {
        if (!component->isContainer() && component->containedWithin(selectionBound) && component->shouldRespondToKeyboard()) {

            // To set keyboard selected to this component, first disable keyboard integration to ensure
            // that any keyboard selection is removed.
            disableKeyboardIntegration();

            // Next set the prospective keyboard component to this component.
            prospectiveKeyboardComponent = component;

            // Finally enable keyboard integration. This will use the prospective keyboard component to start
            // selection from.
            enableKeyboardIntegration();
        }
    }
    
    void TopLevelContainer::forceEmptyRepaint(void) const {
        if (repaintController != NULL) {
            repaintController->forceEmptyRepaint();
        }
    }
    
    CursorType TopLevelContainer::getHighestPriorityCursorType(void) const {
        // Search current mouse over, find the highest-valued cursor type.
        CursorType highestCursor = CursorType::inherit;
        for (const Component * component : *currentMouseOver) {
            highestCursor = std::max(highestCursor, component->getCursorType());
        }
        return highestCursor;
    }
    
    void TopLevelContainer::setScalePointers(const GLfloat * xScalePtr, const GLfloat * yScalePtr) {
        if (xScalePtr == NULL) {
            this->xScalePtr = &defaultScale;
        } else {
            this->xScalePtr = xScalePtr;
        }

        if (yScalePtr == NULL) {
            this->yScalePtr = &defaultScale;
        } else {
            this->yScalePtr = yScalePtr;
        }
    }


    void TopLevelContainer::setSelectionBound(Container * container) {
        // Check that this container is currently part of our component hierarchy.
        if (container == this || container->topLevel == this) {
            // Next disable keyboard selection,
            disableKeyboardIntegration();
            // then disable mouse selection
            clearAllMouseOverAndClick();
            // Reset the prospective component for keyboard select, if it is not contained within new selection bound.
            if (prospectiveKeyboardComponent != NULL && !prospectiveKeyboardComponent->containedWithin(container)) {
                prospectiveKeyboardComponent = NULL;
            }
            // Finally set the new selection bound
            selectionBound = container;
        }
    }



    /**
     * Returns true if the user is not currently mousing over anything, i.e: The current mouse over set contains
     * nothing, (ignoring the top level container).
     *
     * We are not mousing over anything if there is only one component being moused over, and it is
     * the top level container, (we cannot count mousing over the top level container, as otherwise this would
     * always return false unless there is a selection bound).
     */
    #define NOT_CURRENTLY_MOUSING_OVER_ANYTHING \
        (currentMouseOver->empty() || (currentMouseOver->size() == 1 && *currentMouseOver->begin() == this))


    bool TopLevelContainer::internalMouseMove(GLfloat viewX, GLfloat viewY, bool updateFunctionQueues) {

        // Don't update any of the function queues if we are told not to.
        // This can happen if this is called from within another top level event, where the parent event
        // *should* call all of these things.
        UPDATE_FUNCTION_QUEUES(updateFunctionQueues, updateFunctionQueues)

        // If force next mouse move is disabled, and the view position has not changed, then do nothing.
        // This check is required because Windows seems to repeat mouse move events even when the mouse
        // has not moved (for whatever reason).
        if (!forceNextMouseMove && viewX == lastMouseViewX && viewY == lastMouseViewY) {
            // Return true if we are currently not mousing over anything.
            return NOT_CURRENTLY_MOUSING_OVER_ANYTHING;
        }

        // Update the last mouse position, disable forceNextMouseMove.
        lastMouseViewX = viewX;
        lastMouseViewY = viewY;
        forceNextMouseMove = false;


        // Disable keyboard integration, unless locked selection is enabled, each time we move the mouse.
        if (!isSelectionLocked()) disableKeyboardIntegration();

        if (currentMousePress.empty()) {
            // If no mouse press events are happening, handle the mouse move.

            // Add all mouse-overed components to newMouseOver, within the current selection bound.
            selectionBound->addIfMouseOver(viewX, viewY, newMouseOver);

            // First, process mouse entry methods. Call onMouseEntered for each component which is in the
            // new mouse over set but not the old mouse over set.
            for (Component * comp : *newMouseOver) {
                if (currentMouseOver->find(comp) == currentMouseOver->end()) {
                    comp->onMouseEnter(viewX, viewY);

                    // Set prospectiveKeyboardComponent to the most recently mouse-overed keyboard-responsive component.
                    if (!comp->isContainer() && comp->shouldRespondToKeyboard()) {
                        prospectiveKeyboardComponent = comp;
                    }
                }
            }

            // Next, process mouse move events. Call onMouseMove for each component in the new mouse over set.
            for (Component * comp : *newMouseOver) {
                comp->onMouseMove(viewX, viewY);
            }

            // Then, process mouse leave methods. Call onMouseLeave for each component which is in the old mouse
            // over set but not the new one.
            for (Component * comp : *currentMouseOver) {
                if (newMouseOver->find(comp) == newMouseOver->end()) {
                    comp->onMouseLeave(viewX, viewY);
                }
            }

            // Clear the old mouse over set, then swap the pointers.
            currentMouseOver->clear();
            std::unordered_set<Component *> * swapPtr = currentMouseOver;
            currentMouseOver = newMouseOver;
            newMouseOver = swapPtr;

            // Return true if we are not currently mousing over anything.
            // Note: The mouse over set assigned this step was just swapped, so is now the current mouse over set.
            return NOT_CURRENTLY_MOUSING_OVER_ANYTHING;

        } else {

            // If a mouse press event is currently happening, call onMouseDrag for all components the user has clicked on.
            // Take note of whether we *actually* call onMouseDrag for any (non top level) components.
            bool ret = true;

            for (std::pair<const int, std::unordered_set<Component *>> & setPair : currentMousePress) {
                for (Component * comp : setPair.second) {
                    // Give each component the mouse drag event relating to the mouse button they are assigned to.
                    // This will call onMouseDrag multiple times if multiple mouse buttons are pressed.
                    comp->onMouseDrag(viewX, viewY, setPair.first);
                    
                    // Only count non top level components.
                    if (comp != this) ret = false;
                }
            }

            // This is set to false (user doing something) if we actually call onMouseDrag for a (non top level) component. This
            // allows us to only return false if the mouse is being dragged AND a (non top level) component is receiving the event.
            return ret;
        }
    }



    /**
     * See NOT_CURRENTLY_MOUSING_OVER_ANYTHING.
     * The equivalent for mouse pressing.
     */
    #define NOT_CURRENTLY_CLICKING_ON_ANYTHING \
        (buttonSet->empty() || (buttonSet->size() == 1 && *buttonSet->begin() == this))


    bool TopLevelContainer::internalMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY, bool updateFunctionQueues) {

        // Don't update the function queues if we are told not to. This allows internalMouseEvent to call itself
        // without problems - (the child call must not update functions queues, as that is the responsibility
        // of the parent call).
        UPDATE_FUNCTION_QUEUES(updateFunctionQueues, updateFunctionQueues)

        if (isSelectionLocked()) {
            std::unordered_set<Component *> * buttonSet = &currentMousePress[button];

            // If selection is locked, and...
            if (state == MouseEvents::RELEASE &&                             // The user released their mouse,
                !keyboardSelected->location.cache.contains(viewX, viewY) &&  // mouse is not over our locked component,
                buttonSet->find(keyboardSelected) == buttonSet->end()) {     // and the user did not originally click on our component...

                // Then we must unlock selection.
                unlockSelection();
            }
            // Otherwise, we can safely just process mouse events for the selection locked component.
        } else {
            // If selection is not locked, then a mouse event should always disable keyboard interaction.
            disableKeyboardIntegration();
        }


        switch(state) {
        case MouseEvents::PRESS:
            {
                // First get a pointer to the set of components that have been pressed with this mouse button.
                // This creates a set here automatically, and one should not already exist.
                std::unordered_set<Component *> * buttonSet = &currentMousePress[button];

                // Next call addIfMouseOver, using this set. This will add all components that have been clicked on to this set.
                // Ensure that selection is only carried out within the current selection bound.
                // The method addIfMouseOver will automatically check for selection locking as well.
                selectionBound->addIfMouseOver(viewX, viewY, buttonSet);


                // Loop through each component, and call their onMousePress events.
                for (Component * comp : *buttonSet) {
                    comp->onMousePress(viewX, viewY, button);
                }

                // Save the return value, as automatically releasing mouse scroll wheel buttons will likely affect this.
                // Return true if the mouse is not currently pressed down on any components. This macro ignores mouse presses
                // on the top level container.
                bool returnVal = NOT_CURRENTLY_CLICKING_ON_ANYTHING;

                // Glut never gives us mouse release events for scroll wheel buttons, so automatically release the mouse
                // immediately if this was a scroll wheel mouse event. Doing this avoids the scroll wheel buttons
                // getting "Stuck down".
                // Supply false to updateFunctionQueues, so that we don't update any function queues. We can't update function
                // queues in a nested call.
                if (button == bindings->mouseScrollUpButton || button == bindings->mouseScrollDownButton) {
                    internalMouseEvent(button, MouseEvents::RELEASE, viewX, viewY, false);
                }

                return returnVal;
            }

        case MouseEvents::RELEASE:
            {
                // First get a pointer to the set of components associated with pressing this mouse button.
                // This should exist, and will be empty if this is the respective release event from the user clicking on nothing.
                std::unordered_set<Component *> * buttonSet = &currentMousePress[button];

                // FIRST, call the onMouseClick event, for all active components where the mouse is STILL OVER THEM
                for (Component * comp : *buttonSet) {
                    if (comp->location.cache.contains(viewX, viewY) && comp->isActiveAndVisible()) {
                        comp->onMouseClick(viewX, viewY, button);
                    }
                }

                // NEXT, call the onMouseRelease event, for ALL COMPONENTS, even if the mouse is no longer over them.
                for (Component * comp : *buttonSet) {
                    comp->onMouseRelease(viewX, viewY, button);
                }

                // Before we erase our button set, we must save our return value. Again, we use the macro that ignores
                // mouse clicks on the top level container.
                bool doneNothing = NOT_CURRENTLY_CLICKING_ON_ANYTHING;

                // Now erase out mouse button set, since we have finished processing the mouse release.
                currentMousePress.erase(button);

                // Next call mouseMove: This will cause the mouse leave events to happen for any components we were dragging.
                // Set forceNextMouseMove to true so that the mouse move event will not be ignored, even if the mouse position has
                // not changed. Also provide false to updateFunctionQueues. This is because internalMouseMove is a nested call,
                // so must never update any function queues.
                forceNextMouseMove = true;
                internalMouseMove(viewX, viewY, false);

                // Finally return the value we saved before.
                // If we clicked on no components before - (i.e our button set was empty), we must return true.
                return doneNothing;
            }

            break;

        default: // This should never happen, and is here to avoid a compiler warning.
            return true;
        }
    }

    void TopLevelContainer::dummyEvent(void) {
        // The only thing we need to do here is update the function queues.
        UPDATE_FUNCTION_QUEUES(true, true)
    }

    void TopLevelContainer::escapeKeyboardSelectionContainer(Container * container) {
        // Ignore components outside our hierarchy.
        if (container->getTopLevel() != this) return;
        
        // Reset the prospective component for keyboard select, if it is contained within the container.
        if (prospectiveKeyboardComponent != NULL && prospectiveKeyboardComponent->containedWithin(container)) {
            prospectiveKeyboardComponent = NULL;
        }
        
        // If keyboard selection is already disabled, or is outside the container, there is nothing to do.
        if (keyboardSelected == NULL || !keyboardSelected->containedWithin(container)) {
            return;
        }

        // Keyboard selection is always within selection bound, so if keyboard selection is within the container, and the container is
        // not contained within the selection bound, the container must either *be* the selection bound, or *contain* the selection bound.
        // In that case we can't search for keyboard selection outside the container, so just disable keyboard integration.
        if (!container->containedWithin(selectionBound)) {
            disableKeyboardIntegration();
            return;
        }

        // Otherwise, switch with a container switch upward search. If that returns something within the
        // container, or somehow could not find anything to keyboard select, disable keyboard integration.
        Component * newSelection = container->containerSwitchUpwardSearch();
        if (newSelection == NULL || newSelection->containedWithin(container)) {
            disableKeyboardIntegration();
        } else {
            switchKeyboardSelection(newSelection);
        }
    }
    
    void TopLevelContainer::clearAllMouseOverAndClick(void) {
        // First we must clear the mouse button pressed sets.

        // Loop through the set of pressed components for each mouse button.
        for (std::pair<const int, std::unordered_set<Component *>> & mousePair : currentMousePress) {
            // Loop through the components in each set, call a mouse release without a click.
            for (Component * component : mousePair.second) {
                component->onFakeMouseRelease(mousePair.first);
            }
            // Now clear the set, to make it look like we have not clicked on anything, for each mouse button
            // pressed.
            mousePair.second.clear();
        }

        // Now we go through each mouse-overed component and call their mouse leave event.
        for (Component * component : *currentMouseOver) {
            // Call each component's mouse leave event.
            component->onFakeMouseLeave();
        }
        // Next we must clear the mouse over set.
        currentMouseOver->clear();
    }



    bool TopLevelContainer::enableKeyboardIntegration(void) {
        // If keyboard integration is already enabled, then do nothing and return false.
        if (keyboardSelected != NULL) {
            return false;
        }

        bool notAlreadyMouseOvered = true;

        if (prospectiveKeyboardComponent == NULL) {
            // If there is no prospective component, then get one by running a recursive non-depth search of the
            // selection bound. This ensures we get the first (in selection order) component within the current selection bound.
            keyboardSelected = selectionBound->getFirstKeyboardResponsiveComponentNonDepth();
            if (keyboardSelected == NULL) {
                // If we cannot find a component to select, do nothing.
                // Return true to notify that we should do no further keyboard actions.
                return true;
            }
        } else if (!prospectiveKeyboardComponent->shouldRespondToKeyboard()) {
            // If the prospective keyboard selected no longer responds to keyboard events, since it was set, then search for a new component from
            // it. Try next keyboard component, if that fails try next keyboard container, if that fails, give up. There is no need to reset the
            // prospective keyboard component (even on failure) - it may become keyboard responsive again later.
            keyboardSelected = prospectiveKeyboardComponent->nextKeyboardComponent();
            if (keyboardSelected == prospectiveKeyboardComponent) {
                keyboardSelected = prospectiveKeyboardComponent->nextKeyboardContainer();
                // Note: Must still check against prospective: a contrived component implementation may have just started responding to keyboard again.
                if (keyboardSelected == NULL || keyboardSelected == prospectiveKeyboardComponent) {
                    keyboardSelected = NULL;
                    // If we cannot find a component to select, do nothing.
                    // Return true to notify that we should do no further keyboard actions.
                    return true;
                }
            }
        } else {
            // Use prospectiveKeyboardComponent if it exists
            keyboardSelected = prospectiveKeyboardComponent;
        }

        // Get whether our selected component is currently mouse-overed.
        notAlreadyMouseOvered = (currentMouseOver->find(keyboardSelected) == currentMouseOver->end());

        // Now clear the mouse over and click sets.
        clearAllMouseOverAndClick();

        // Run it's mouse enter event.
        // If it was already mouse-overed, it will receive a mouse leave followed immiediately by this mouse enter.
        keyboardSelected->onFakeMouseEnter();
        // Run the onChildKeyboardSelect event, since the keyboard selection has just changed to a new component.
        keyboardSelected->getParent()->onChildKeyboardSelect(keyboardSelected);

        // Return false (to allow further events), if the component was already mouse-overed. Otherwise return true
        // since this event would have appeared to the user to be consumed by selecting our component.

        return notAlreadyMouseOvered;
    }


    void TopLevelContainer::disableKeyboardIntegration(void) {
        // If the keyboard interaction is enabled, i.e: There is a component selected...
        if (keyboardSelected != NULL) {
            // Before we disable keyboard integration, we must make sure that there is no locked selection.
            unlockSelection();

            // Ensure mouse presses are released
            if (keyboardSelectedEnterKeyPressed) {
                keyboardSelectedEnterKeyPressed = false;
                keyboardSelected->onFakeMouseRelease(bindings->mouseButton);
            }

            // Then run it's fake mouse leave event
            keyboardSelected->onFakeMouseLeave();
            // And nullify it to disable keyboard integration
            keyboardSelected = NULL;
        }
    }
    
    void TopLevelContainer::switchKeyboardSelection(Component * newSelection) {
        // Do nothing if keyboard selection has not changed.
        if (keyboardSelected == newSelection) return;
        
        // First we must give them a mouse release event, (BUT NOT A MOUSE CLICK EVENT), if we have the
        // enter key held down:
        if (keyboardSelectedEnterKeyPressed) {
            keyboardSelected->onFakeMouseRelease(bindings->mouseButton);
            keyboardSelectedEnterKeyPressed = false;
            // This ensures that if we switch selected keyboard components, the mouse press is reset as well.
        }

        // The mouse must leave the old component
        keyboardSelected->onFakeMouseLeave();
        // keyboardSelected must be set to the new component
        keyboardSelected = newSelection;
        // The mouse must enter the new component
        keyboardSelected->onFakeMouseEnter();
        // Run the onChildKeyboardSelect event, since the keyboard selection has just changed to a new component.
        keyboardSelected->getParent()->onChildKeyboardSelect(keyboardSelected);
    }
    
    bool TopLevelContainer::disableKeyboardSelectionIfInvalid(void) {
        if (!keyboardSelected->shouldRespondToKeyboard()) {
            disableKeyboardIntegration();
            return true;
        }
        return false;
    }

    bool TopLevelContainer::keyPress(int key, bool special) {

        UPDATE_FUNCTION_QUEUES(true, true)

        // First, we do a lookup in the key bindings to work out what action relates to this key
        NavigationAction action = bindings->getNavigationAction(key, special);

        // Return false if the selection-locked component has used up this event.
        if (processKeyEventIfSelectionLocked(action, key, special, true)) {
            return false;
        }

        // Use the manual keyboard navigation action press method to do the remainder of the logic.
        return runNavigationActionPress(action);
    }

    bool TopLevelContainer::keyRelease(int key, bool special) {

        UPDATE_FUNCTION_QUEUES(true, true)

        // First, we do a lookup in the key bindings to work out what action relates to this key
        NavigationAction action = bindings->getNavigationAction(key, special);

        // Return false if the selection-locked component has used up this event.
        if (processKeyEventIfSelectionLocked(action, key, special, false)) {
            return false;
        }

        // Use the manual keyboard navigation action release method to do the remainder of the logic.
        return runNavigationActionRelease(action);
    }
    
    bool TopLevelContainer::runNavigationActionPress(NavigationAction action) {
        // Do nothing and return false if selection is locked - we don't know what key triggered
        // the navigation action, so can't send the selection-locked component a press event!
        if (isSelectionLocked()) {
            return false;
        }
        
        // If the key pressed means we do nothing, then return true, since we have done nothing.
        if (action == NavigationAction::noAction) {
            return true;
        }

        // If enableKeyboardIntegration tells us to do nothing, then do nothing, but return
        // false, since enableKeyboardIntegration has actually used this keyboard event to do something.
        if (enableKeyboardIntegration()) {
            return false;
        }

        // We can now assume that keyboardSelected is not NULL.
        
        // Since we're performing actions on keyboard selected, make sure it is still valid. If not, try
        // to re-enable keyboard integration - if that tells us to do nothing, do nothing, (and return
        // false since this is still a valid navigation action). Trying to enable keyboard integration
        // again after keyboard selection was found to be invalid, ensures that a new component is
        // immediately switched to if possible.
        if (disableKeyboardSelectionIfInvalid()) {
            if (enableKeyboardIntegration()) {
                return false;
            }
        }

        // Next ask the selected component what action should be performed.
        action = keyboardSelected->getRequiredKeyboardActionPress(action);

        Component * newSelection;

        switch(action) {
        case NavigationAction::nextComponent:
            newSelection = keyboardSelected->nextKeyboardComponent();
            break;
        case NavigationAction::previousComponent:
            newSelection = keyboardSelected->previousKeyboardComponent();
            break;
        case NavigationAction::nextContainer:
            newSelection = keyboardSelected->nextKeyboardContainer();
            break;
        case NavigationAction::previousContainer:
            newSelection = keyboardSelected->previousKeyboardContainer();
            break;
        case NavigationAction::enter:
            // Set keyboardSelectedEnterKeyPressed to true, run a mouse PRESS (NOT CLICK) event.
            // Only do this if the component wasn't *already* pressed, as this may be the second consecutive keyboard
            // navigation enter press action - possible if someone manually calls runNavigationActionPress, or if two keys are
            // bound to the enter navigation action. Otherwise, we'd be sending consecutive press events (which is wrong!).
            if (!keyboardSelectedEnterKeyPressed) {
                keyboardSelected->onFakeMousePress(bindings->mouseButton);
                keyboardSelectedEnterKeyPressed = true;
            }
            newSelection = keyboardSelected;
            break;
        case NavigationAction::lockSelection:
            lockSelection();
            newSelection = keyboardSelected;
            break;
        default:
            // This should happen if keyboardSelected->getRequiredKeyboardAction returns noAction, or any
            // other irrelevant keyboard action.
            newSelection = keyboardSelected;
            break;
        }
        
        // Component searches may not have found a new selection, if a contrived component implementation
        // returns strange values for shouldRespondToKeyboard.
        if (newSelection != NULL) {
            switchKeyboardSelection(newSelection);
        }
        return false;
    }
        
    bool TopLevelContainer::runNavigationActionRelease(NavigationAction action) {
        // Do nothing and return false if selection is locked - we don't know what key triggered
        // the navigation action, so can't send the selection-locked component a press event!
        if (isSelectionLocked()) {
            return false;
        }
        
        // If the key pressed means we do nothing, then return true, since we have done nothing.
        if (action == NavigationAction::noAction) {
            return true;
        }

        // If enableKeyboardIntegration tells us to do nothing, then do nothing, but return
        // false, since enableKeyboardIntegration has actually used this keyboard event to do something.
        if (enableKeyboardIntegration()) {
            return false;
        }

        // We can now assume that keyboardSelected is not NULL.

        // Since we're performing actions on keyboard selected, make sure it is still valid. If not, try
        // to re-enable keyboard integration - if that tells us to do nothing, do nothing, (and return
        // false since this is still a valid navigation action). Trying to enable keyboard integration
        // again after keyboard selection was found to be invalid, ensures that a new component is
        // immediately switched to if possible.
        if (disableKeyboardSelectionIfInvalid()) {
            if (enableKeyboardIntegration()) {
                return false;
            }
        }
        
        // Next ask the selected component what action should be performed.
        action = keyboardSelected->getRequiredKeyboardActionRelease(action);

        switch(action) {
        case NavigationAction::enter:
            // Only do a click/release event if this is still true, as removing components, switching
            // selection, multiple keys being bound to enter, or a manual call to runNavigationActionRelease
            // could change this. Otherwise, we'd be sending consecutive release events (which is wrong!).
            if (keyboardSelectedEnterKeyPressed) {
                keyboardSelected->onFakeMouseClick(bindings->mouseButton);
                keyboardSelected->onFakeMouseRelease(bindings->mouseButton);
                keyboardSelectedEnterKeyPressed = false;
            }
            break;

        case NavigationAction::lockSelection:
            lockSelection();
            break;

        default:
            break;
        }

        // Return false, since the key specified was defined in our key bindings.
        return false;
    }

    // ============================== Selection locking system ==========================================================

    void TopLevelContainer::lockSelection(void) {
        if (keyboardSelected != NULL) {
            // Since we're performing actions on keyboard selected, make sure it is still valid - if not do nothing.
            if (disableKeyboardSelectionIfInvalid()) {
                return;
            }
            // Only lock selection if there is currently a keyboard selected component.
            selectionLocked = true;
            keyboardSelected->onSelectionLockEnabled();
        }
    }

    bool TopLevelContainer::unlockSelection(void) {
        if (selectionLocked) {
            selectionLocked = false;
            keyboardSelected->onSelectionLockDisabled();
            return true;
        }
        return false;
    }

    Component * TopLevelContainer::getSelectionLock(void) const {
        return selectionLocked ? keyboardSelected : NULL;
    }


    bool TopLevelContainer::processKeyEventIfSelectionLocked(NavigationAction action, int key, bool special, bool isKeyPress) {
        // Ignore the key event, if selectionUnlockIgnoreKey is set, and this is a key release.
        if (!isKeyPress && key == selectionUnlockIgnoreKey) {
            selectionUnlockIgnoreKey = -1;
            return true;
        }
        // Do nothing and return false if selection is not even locked.
        if (!isSelectionLocked()) {
            return false;
        }
        // Since we're performing actions on keyboard selected, make sure it is still valid - if not do nothing.
        // Return false so that the keyboard action is used immediately: It might be a navigation action, and the
        // (now disabled) keyboard selected component should not appear to steal keyboard input.
        if (disableKeyboardSelectionIfInvalid()) {
            return false;
        }
        
        // Run the appropriate press/release method in the currently locked component
        action = isKeyPress ? keyboardSelected->onSelectionLockKeyPress(action, key, special) : keyboardSelected->onSelectionLockKeyRelease(action, key, special);

        // Unlock selection if the select-locked component said that they want us to.
        if (action == NavigationAction::unlockSelection) {
            unlockSelection();
            // If we are unlocking with a key press, we want to later ignore the key release.
            if (isKeyPress) {
                selectionUnlockIgnoreKey = key;
            }
        }
        return true;
    }


    // ==================================================================================================================




    void TopLevelContainer::topLevelRemove(Component * component) {

        // First, if this component is the selection bound, reset the selection bound.
        if (component == selectionBound) {
            resetSelectionBound();
        }

        // Next ensure keyboard interaction is reset for this component.
        if (component == getKeyboardSelected()) {
            disableKeyboardIntegration();
        }

        // If this component is the prospective next keyboard component, then set that to NULL.
        if (component == prospectiveKeyboardComponent) {
            prospectiveKeyboardComponent = NULL;
        }

        // Store whether the mouse was over this component.
        bool wasMouseOver = (currentMouseOver->find(component) != currentMouseOver->end());

        // Then erase it from the current mouse over set.
        currentMouseOver->erase(component);

        // Then remove the component from the current mouse press maps, keeping track of which buttons were pressed.
        // Note: It is safe to leave the maps empty here, as that is handled by mouseEvent method, when the mouse is released.
        //       If the map is empty, that method will simply think that the user didn't press on this (or any) component.
        std::list<int> buttonsPressed;
        for (std::pair<const int, std::unordered_set<Component *>> & pair : currentMousePress) {
            if (pair.second.find(component) != pair.second.end()) {
                buttonsPressed.push_back(pair.first);
                pair.second.erase(component);
            }
        }

        // Next, we should remove this component from the RepaintController's repaint list,
        // (if partial repaint is enabled - i.e: we actually have a repaint controller).
        if (isPartialRepaintEnabled()) {
            repaintController->removeComponent(component);
        }

        // Finally, we must put the component back to it's original state.

        // Only do the next thing if the mouse actually is over, or mouse button is pressed.
        // This avoids doing floating point calculations for the vast majority of components where this won't be the case.
        if (wasMouseOver || !buttonsPressed.empty()) {

            // Call mouse release events for all buttons
            for (int button : buttonsPressed) {
                component->onFakeMouseRelease(button);
            }

            // Then call onMouseLeave. It is safe to assume that we can only have buttons pressed if wasMouseOver is true - this behaviour
            // is handled by the respective mouse move and event methods.
            component->onFakeMouseLeave();

            // NOTE: We cannot call any function queues here, since this method is almost always called while
            //       iterating through components.
        }
    }
}
