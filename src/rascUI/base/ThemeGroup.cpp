/*
 * ThemeGroup.cpp
 *
 *  Created on: 1 Jul 2019
 *      Author: wilson
 */

#include "ThemeGroup.h"

namespace rascUI {

    ThemeGroup::ThemeGroup(Theme * templateTheme) :
        rascUI::Theme() {

        normalComponentWidth       = templateTheme->normalComponentWidth;
        normalComponentHeight      = templateTheme->normalComponentHeight;
        uiBorder                   = templateTheme->uiBorder;
        customColourMainText       = templateTheme->customColourMainText;
        customColourSecondaryText  = templateTheme->customColourSecondaryText;
        customColourInactiveText   = templateTheme->customColourInactiveText;
        customColourBackplaneMain  = templateTheme->customColourBackplaneMain;
        customColourBackplaneLight = templateTheme->customColourBackplaneLight;
        customColourBackplaneDark  = templateTheme->customColourBackplaneDark;

        customTextCharWidth = templateTheme->customTextCharWidth;
        customTitleTextCharWidth = templateTheme->customTitleTextCharWidth;
    }

    ThemeGroup::~ThemeGroup(void) {
    }

    void ThemeGroup::addTheme(Theme * theme) {
        themes.push_back(theme);
    }

    // ---------------------------- User data/setup methods --------------------

    void ThemeGroup::init(void * userData) {
        for (Theme * theme : themes) {
            Theme::initOther(theme, userData);
        }
    }
    void ThemeGroup::beforeDraw(void * userData) {
        for (Theme * theme : themes) {
            theme->beforeDraw(userData);
        }
    }
    void ThemeGroup::afterDraw(const Rectangle & drawnArea) {
        std::vector<Theme *>::reverse_iterator iter = themes.rbegin();
        while(iter != themes.rend()) {
            (*iter)->afterDraw(drawnArea);
            ++iter;
        }
    }
    void ThemeGroup::redrawFromBuffer(void) {
        for (Theme * theme : themes) {
            theme->redrawFromBuffer();
        }
    }
    void * ThemeGroup::getDrawBuffer(void) {
        return themes.back()->getDrawBuffer();
    }
    void ThemeGroup::destroy(void) {
        std::vector<Theme *>::reverse_iterator iter = themes.rbegin();
        while(iter != themes.rend()) {
            Theme::destroyOther(*iter);
            ++iter;
        }
    }

    // -------------------------- Normal operations ----------------------------

    void ThemeGroup::onChangeViewArea(const Rectangle & view) {
        for (Theme * theme : themes) {
            theme->onChangeViewArea(view);
        }
    }
    void ThemeGroup::drawBackPanel(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawBackPanel(location);
        }
    }
    void ThemeGroup::drawFrontPanel(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawFrontPanel(location);
        }
    }
    void ThemeGroup::drawText(const Location & location, const std::string & string) {
        for (Theme * theme : themes) {
            theme->drawText(location, string);
        }
    }
    void ThemeGroup::drawButton(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawButton(location);
        }
    }
    void ThemeGroup::drawTextEntryBox(const Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {
        for (Theme * theme : themes) {
            theme->drawTextEntryBox(location, string, cursorPos, drawCursor);
        }
    }
    void ThemeGroup::drawScrollBarBackground(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawScrollBarBackground(location);
        }
    }
    void ThemeGroup::drawScrollContentsBackground(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawScrollContentsBackground(location);
        }
    }
    void ThemeGroup::drawScrollPuck(const Location & location, bool vertical) {
        for (Theme * theme : themes) {
            theme->drawScrollPuck(location, vertical);
        }
    }
    void ThemeGroup::drawScrollUpButton(const Location & location, bool vertical) {
        for (Theme * theme : themes) {
            theme->drawScrollUpButton(location, vertical);
        }
    }
    void ThemeGroup::drawScrollDownButton(const Location & location, bool vertical) {
        for (Theme * theme : themes) {
            theme->drawScrollDownButton(location, vertical);
        }
    }
    void ThemeGroup::drawFadePanel(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawFadePanel(location);
        }
    }
    void ThemeGroup::drawCheckboxButton(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawCheckboxButton(location);
        }
    }
    void ThemeGroup::drawProgressBar(const Location & location, GLfloat progress, bool vertical, bool inverted) {
        for (Theme * theme : themes) {
            theme->drawProgressBar(location, progress, vertical, inverted);
        }
    }
    void ThemeGroup::drawTooltipBackground(const Location & location) {
        for (Theme * theme : themes) {
            theme->drawTooltipBackground(location);
        }
    }
    void ThemeGroup::drawSlider(const Location & location, GLfloat position, bool vertical, bool inverted) {
        for (Theme * theme : themes) {
            theme->drawProgressBar(location, position, vertical, inverted);
        }
    }

    // -------------------- Custom draw methods -----------------------

    void ThemeGroup::customSetDrawColour(void * colour) {
        for (Theme * theme : themes) {
            theme->customSetDrawColour(colour);
        }
    }
    void * ThemeGroup::customGetTextColour(const Location & location) {
        return themes.back()->customGetTextColour(location);
    }
    void ThemeGroup::customDrawTextMouse(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        for (Theme * theme : themes) {
            theme->customDrawTextMouse(location, offsetX, offsetY, scaleX, scaleY, string);
        }
    }
    void ThemeGroup::customDrawTextNoMouse(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        for (Theme * theme : themes) {
            theme->customDrawTextNoMouse(location, offsetX, offsetY, scaleX, scaleY, string);
        }
    }
    void ThemeGroup::customDrawTextTitle(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        for (Theme * theme : themes) {
            theme->customDrawTextTitle(location, offsetX, offsetY, scaleX, scaleY, string);
        }
    }
    void ThemeGroup::getTextBaseOffsets(State state, GLfloat & offX, GLfloat & offY) {
        themes.back()->getTextBaseOffsets(state, offX, offY);
    }
}
