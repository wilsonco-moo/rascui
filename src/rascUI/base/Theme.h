/*
 * Theme.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_BASE_THEME_H_
#define RASCUI_BASE_THEME_H_

#include <GL/gl.h>
#include <cstddef>
#include <string>

#include "../util/State.h"

namespace rascUI {

    class Location;
    class Rectangle;
    enum class WindowDecorationButtonType;

    /**
     * An instance of Theme is required by TopLevelContainer. Note that a single Theme instance is able to be shared
     * between multiple TopLevelContainer (WindowBase) instances, but only ONE ContextBase.
     *
     *  > The Theme instance is used to pick how to draw each of the components in the TopLevelContainer. These methods are
     *    bundled into such a class to allow easy changing between UI styles. A new Theme implementation is all that is required
     *    to completely change the look and feel of the UI.
     *
     *  > Theme has a series of pure virtual methods, each of which should draw a specific type of component. Each method provides as
     *    parameters two things.
     *    - A Location instance. This should be used to work out the position to draw the specific type of Component.
     *    - Other data relevant to that particular type of component, such as the text to draw in a label.
     *    Note: No data about how to draw the component, or the colours to draw the component is provided (in the exception of the custom
     *          colour system). This allows the Theme implementation complete control over the drawing process, allowing for consistent UI.
     *
     *  > The Location parameter in each of these methods contains a special field: state. This is an enum class defined in State.h.
     *    This should affect how each of the component implementations draws, such as a state of State::mouseOver should cause the
     *    Theme implementation to draw the component more highlighted. It is entirely up to the implementation of the Theme subclass
     *    to decide how to highlight, (or even whether to bother).
     *    See enum class State in State.h for more information about what the Theme implementation should do for each state.
     *
     *  > The Location instances also have xScale and yScale parameters. These values MUST affect the size of stuff drawn by
     *    the Theme implementation.
     *
     *  > NOTE: The Theme implementation does not have to worry about the state State::invisible. Since State::invisible is checked in
     *    the Component and Container classes, we will never receive any draw requests from a component who's state is invisible.
     *    The invisible state causes the Component class not to even call any draw methods.
     *
     *  > As of theme API version 0.3, Theme now includes a series of colour definitions and custom draw methods. These can allow
     *    custom components drawn by the user to remain consistent with all other ordinary components.
     *
     * NOTE: Theme is a header-only interface, and no longer has an attached source file. This is so that
     *       dynamically loaded Theme implementations do not actually have to link to rascUI. Theme also
     *       no longer relies on implementing base virtual methods in this header, as that is not portable
     *       when dynamically loading a shared library.
     *
     *
     * Status methods: init, beforeDraw, afterDraw, destroy (as of theme API 0.4)
     * ==========================================================================
     *
     * As of Theme API 0.4, operations should be structured as follows, where
     * the user data provided to init and beforeDraw operations have a defined
     * scope. For more detailed information, see the documentation for each of
     * these functions.
     *
     *  / init (user data provided: ContextBase)
     *  |      (optional if not using the platform system)
     *  |
     *  |   / beforeDraw (user data provided: WindowBase)
     *  |   |
     *  |   |   theme operation
     *  |   |   (many theme operations may take place)
     *  |   |   ...
     *  |   |
     *  |   \ afterDraw (beforeDraw user data MUST NOT be used after this)
     *  |
     *  |   (many groups of operations may take place)
     *  |   ...
     *  |
     *  \ destroy (init user data MUST NOT be used after this)
     *            (optional if not using the platform system)
     *
     *  (many init/destroy groups may take place)
     *  ...
     * 
     * 
     * Clarification of Theme repaint modes (as of Theme API 0.6)
     * ==========================================================
     * 
     * In general, in rascUI the user interface can be drawn in three different repaint modes. The
     * Theme API is designed to support all of these, although themes are free to only support some
     * of the modes, depending on their use case.
     * 
     * Total repaint mode
     *    This is the simplest mode, and is appropriate for a frame-rate based application. In this
     *    mode, all visible UI components are redrawn every frame. This is useful when building a user
     *    interface for an application which uses legacy immediate mode OpenGL, or any similar system.
     *    This mode does not require a repaint controller, and can be used simply by calling the
     *    "draw" method in TopLevelContainer.
     * 
     * Buffered repaint mode
     *    This mode is also appropriate for frame-rate based applications, although this is a
     *    significant improvement compared to total repaint mode. In buffered repaint mode, each time
     *    the user interface changes it is repainted completely, just like total repaint mode.
     *    However, it is expected that the Theme puts all drawing into some kind of buffer (such as
     *    an OpenGL vertex buffer object) so that in subsequent frames, when the user interface
     *    needs redrawing without change, the Theme's "redrawFromBuffer" method can be used. This
     *    means that the overhead of transferring geometry (etc) to the graphics card for drawing
     *    the user interface, only needs to be done on frames where the user interface has actually
     *    changed (like the user moving their mouse over a button, causing it to repaint). This is
     *    useful when building a user interface for an application which uses modern OpenGL, or any
     *    similar system. This mode requires a repaint controller, although it is only used for
     *    detecting whether or not there has been changes to the UI system. It can be used by calling
     *    the "bufferedDraw" method in TopLevelContainer.
     * 
     * Partial repaint mode
     *    This mode is appropriate for applications which are not frame-rate based. In this mode
     *    drawing only takes place when changes happen in the UI system, and when drawing takes place
     *    ONLY the UI components which have changed are redrawn. Components which are affected by
     *    the change are also redrawn, such as those components which appear in front of a component
     *    which has been redrawn. This is useful when building a user interface for a desktop
     *    application (e.g: an X11 (Xlib/xcb) or native windows API application), which is not a game
     *    and is not frame-rate based. This mode requires a repaint controller, which is used for
     *    detecting which parts of the user interface need re-drawing. It can be used by calling the
     *    "doPartialRepaint" method in TopLevelContainer.
     * 
     */
    class Theme {
    private:
        // Set to true once init has been called, and set to false again once
        // destroy has been called.
        bool initCalled;

        // Don't allow copying Theme instances: Most of the time they hold
        // raw resources.
        Theme(const Theme & other);
        Theme & operator = (const Theme & other);

    public:

        /**
         * This should be updated each time methods are added to Theme, or methods are modified.
         * Only dynamically loaded themes which match this API version should be used.
         */
        #define THEME_API_VERSION "theme-rascUI-0.7"

        // ============================= Properties which MUST be set by Theme implementations ================================

        /**
         * These should contain the preferred minimum width and height for components using this theme.
         *
         * The height should be used as the minimum for any component which contains or draws text, such as a button,
         * toggle button, text entry box etc.
         *
         * The width should be used as the minimum comfortable usable width for a button. This is used for example,
         * for buttons in the scroll pane.
         *
         * Both of these values are used by the layout system, if it uses a theme.
         */
        GLfloat normalComponentWidth, normalComponentHeight;

        /**
         * This should contain the preferred minimum distance to place between two components (such as buttons), which
         * are using this theme. It is perfectly acceptable for this to contain a value of zero, if the theme is designed
         * such that no border is needed.
         *
         * This is used extensively by the layout system, and is probably the most important value which the
         * theme must define.
         */
        GLfloat uiBorder;

        /**
         * This should contain the preferred minimum width and height for window decoration components using this theme,
         * see drawWindowDecorationFrontPanel, drawWindowDecorationText and drawWindowDecorationButton.
         * 
         * These minimum values are only applied to window decoration components, normal components are not expected
         * to be able to draw at these sizes. For example, it is perfectly acceptable for windowDecorationNormalComponentHeight
         * to be smaller than normalComponentHeight, and for normal methods like drawText to be unable to draw at this size.
         */
        GLfloat windowDecorationNormalComponentWidth, windowDecorationNormalComponentHeight;
        
        /**
         * This should contain the preferred minimum distance between window decoration components and the outside of the parent
         * window. Distance between the window decoration components, the child window and themselves, still uses the normal UI
         * border. It is perfectly acceptable for this to contain a value of zero, if no border is intended around child windows.
         * 
         * This border ONLY applies when drawing window decorations: It can be a different value to uiBorder.
         */
        GLfloat windowDecorationUiBorder;

        /**
         * CUSTOM COLOURS.
         * For good compliance with themes, the user should draw in these colours when drawing custom components.
         * Where applicable, these colours should match those used in drawing of standard UI components,
         * to allow custom components to be consistent with the rest of the UI.
         * These colours should be pointers to a colour type appropriate for the graphics system which the theme
         * is targeted for.
         * When doing custom drawing of a component (between beforeDraw and afterDraw), users must be able to use
         * these instances to set the current drawing colour, using the Theme's customSetDrawColour method.
         * For themes which support buffered repaint mode, this should set the current draw colour for the Theme's
         * draw buffer. This, in turn, should affect the text colour drawn by the custom text
         * drawing methods.
         * Alternatively, users could manually adjust the current drawing colour for the target graphics system
         * (e.g: for legacy immediate mode OpenGL they could use glColor3f), although this makes custom drawing
         * less portable. For themes which support buffered repainting, the colour can be manually adjusted by the
         * user if they access the buffer directly using the Theme's "getDrawBuffer" method.
         */

        /**
         * This should define the colour for drawing ordinary text, and should be the same colour as ordinary UI component's text.
         */
        void * customColourMainText;
        /**
         * This should define a colour for text, which is visibly different to customColourMainText, but which stands out less.
         * The theme implementation could choose to make this the same as customColourInactiveText.
         */
        void * customColourSecondaryText;
        /**
         * This should define the colour for inactive text, and should be the same colour as is used for drawing inactive text
         * when ordinary UI components are inactive.
         */
        void * customColourInactiveText;
        /**
         * This should define a colour for use by the user as a general purpose block-colour background panel. This should have
         * a colour similar to the overall colour of an ordinary FrontPanel, BUT CLEARLY DISTINCT FROM an ordinary BackPanel.
         */
        void * customColourBackplaneMain;
        /**
         * These should be light and dark (similar but distinct) variants of customColourBackplaneMain. These MUST both be
         * distinct colours from customColourBackplaneMain, since the main use of these is so the user can custom-draw adjacent
         * backplanes, with a visible division between them.
         */
        void * customColourBackplaneLight,
             * customColourBackplaneDark;

        /**
         * This should be set to the character width of default-drawn text, at default scale.
         */
        GLfloat customTextCharWidth;
        /**
         * The same as customTextCharWidth but for title text.
         */
        GLfloat customTitleTextCharWidth;

        // ======================================================================================================================

        /**
         * The main constructor of Theme.
         * NOTE: As of theme API version 0.3, properties are no longer supplied to this constructor. Properties must instead be
         *       assigned in the constructor of implementing classes. The reasoning behind this change is theme API version 0.3
         *       added many new properties, and supplying these in a list in the constructor has become unmaintainable and awkward.
         */
        Theme(void) :
            initCalled(false) {
        }
        virtual ~Theme(void) {
        }

        /**
         * This returns true once init has been called, and false again
         * once destroy has been called.
         * This is convenient as it allows both Theme and other external code
         * to know the Theme's state.
         */
        inline bool hasInitBeenCalled(void) const {
            return initCalled;
        }

        /**
         * Allows outside code to call the Theme's init function more
         * conveniently, by calling init if it hasn't been called already.
         */
        inline void callInit(void * userData) {
            if (!initCalled) {
                init(userData);
                initCalled = true;
            }
        }

        /**
         * Allows outside code to call the Theme's destroy function more
         * conveniently, by calling destroy if it hasn't been called already.
         */
        inline void callDestroy(void) {
            if (initCalled) {
                destroy();
                initCalled = false;
            }
        }

    protected:
        /**
         * If the platform system IS IN USE, this is ALWAYS called ONCE at the start,
         * BEFORE all calls to beforeDraw/afterDraw, (and their associated groups of operations).
         * If the platform system is NOT IN USE, this may optionally NOT BE CALLED. In this
         * case, the Theme should rely on it's constructor to allocate resources.
         *
         * If the platform system is in use, the provided user data should be a pointer to
         * a ContextBase instance which upcoming Theme operations are related to, (although
         * platform implementations are free to provide a pointer to something else).
         *
         * Calling this method is the responsibility of the appropriate ContextBase
         * subclass being used. This user data MUST NOT BE DESTROYED, until AFTER the
         * Theme's destroy method is called. This way, it is guaranteed that the Theme is
         * able to continue using it until then.
         *
         * In the case that Window themes are manually set, rather than being
         * controlled by the Context, it is the responsibility of the user to
         * manually call init and destroy. For more information, see ContextBase.h.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * Also, the implementation may not actually *require* any user data.
         * The theme can optionally set a default cursor here, see CursorType.
         */
        virtual void init(void * userData) = 0;

        /**
         * Allows Themes to run the init method on other Themes. This is used
         * in ThemeGroup.
         */
        static inline void initOther(Theme * other, void * userData) {
            other->init(userData);
        }

    public:
        /**
         * This is called immediately BEFORE a group of operations, relating to the provided
         * user data. If the platform system is in use, the provided user data should be
         * a pointer to the WindowBase instance, which these operations are related to,
         * (although platform implementations are free to provide a pointer to something else).
         *
         * The user data comes from the protected field "beforeDrawThemeUserData"
         * of TopLevelContainer. This user data MUST NOT BE DESTROYED, until AFTER the Theme's
         * afterDraw method is called. This way, it is guaranteed that the Theme is able to
         * continue using it until then.
         * 
         * For themes which support buffered repainting, beforeDraw notifies the start of a
         * group of buffered draw operations. For themes which are targeted for graphics systems
         * supporting multiple windows (where the platform system is in use), the Theme should
         * hold a separate draw buffer for each window (in the platform system windows are
         * provided as user data to beforeDraw).
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * Also, the implementation may not actually *require* any user data.
         * The theme can optionally set the current cursor here, see CursorType.
         */
        virtual void beforeDraw(void * userData) = 0;

        /**
         * This is called immediately AFTER a group of operations, relating to the user data
         * provided in the beforeDraw method. The Theme implementation should use this to
         * reset/free any setup done in the beforeDraw method, which was associated with the
         * provided user data. After this point, the Theme MUST NO LONGER USE any user data
         * provided in the beforeDraw method, as after this point it is NO LONGER GUARANTEED
         * TO EXIST. After this call, beforeDraw may be called again in the future,
         * possibly with DIFFERENT user data.
         *
         * A Rectangle is provided to this method, which represents the area on-screen which
         * has changed as a result of the most recent group of operations. In partial
         * repainting mode, this represents the final bounding box after drawing. Otherwise,
         * this should just be the TopLevelContainer's view rectangle.
         * 
         * This method is significant for themes which support buffered repainting. If
         * "redrawFromBuffer" has NOT been called since the most recent "beforeDraw" call,
         * during "afterDraw", the theme should ensure that all drawing operations since
         * the most recent "beforeDraw" are loaded into the Theme's appropriate draw buffer,
         * AND draw it (or ensure that it has all been drawn). Otherwise, if "redrawFromBuffer"
         * HAS been called since the most recent "beforeDraw" call, then the Theme does not
         * have to do anything during "afterDraw".
         * 
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * Also, the implementation may have not actually *used* the user data in beforeDraw.
         * The theme can optionally set the current cursor here, see CursorType.
         */
        virtual void afterDraw(const Rectangle & drawnArea) = 0;
        
        /**
         * Implementation of this method is only required for themes which
         * support buffered repainting.
         * 
         * This method must be called between a beforeDraw/afterDraw pair, as
         * the only operation. When this method is called, the Theme
         * implementation should draw everything contained in its internal draw
         * buffer. This should recreate all drawing operations which were called
         * within the most recent corresponding beforeDraw/afterDraw pair in
         * which "redrawFromBuffer" was NOT called (see example below).
         * 
         * Note that for themes which target a graphics system supporting
         * multiple windows (i.e: use the platform system), the Theme should have
         * an independent draw buffer for each window. In that case, this method
         * should draw from the draw buffer appropriate to the window used by
         * the current beforeDraw/afterDraw pair.
         * 
         * Example of order of operations for buffered repainting:
         * 
         * beforeDraw (window 0)
         *    draw operation 0
         *    draw operation 1
         * afterDraw               <-- Should finalise window 0 buffer, and
         *                             draw it within window 0.
         * beforeDraw (window 1)
         *    draw operation 2
         *    draw operation 3
         * afterDraw               <-- Should finalise window 1 buffer, and
         *                             draw it within window 1.
         * beforeDraw (window 0)
         *    redrawFromBuffer     <-- Should recreate results of draw operations
         * afterDraw                   0 and 1 within window 0.
         * 
         * beforeDraw (window 1)
         *    redrawFromBuffer     <-- Should recreate results of draw operations
         * afterDraw                   2 and 3 within window 1.
         */
        virtual void redrawFromBuffer(void) = 0;
        
        /**
         * For themes which support buffered repainting, this should allow
         * manual access to the draw buffer (appropriate to the current
         * beforeDraw/afterDraw pair's window, for themes which target graphics
         * systems supporting multiple windows). This allows users the ability
         * to do custom drawing, for themes which use buffered repainting.
         */
        virtual void * getDrawBuffer(void) = 0;
        
        
    protected:
        /**
         * If the platform system IS IN USE, this is ALWAYS called ONCE at the end,
         * after all calls to beforeDraw/afterDraw (and their associated groups of operations).
         * If the platform system is NOT IN USE, this may optionally NOT BE CALLED. In this
         * case, the Theme should rely on it's destructor to free resources.
         *
         * Calling this method is the responsibility of the appropriate ContextBase
         * subclass beings used. The Theme implementation should use this to reset/free
         * any setup done in the init method, which was associated with the provided user
         * data. After this point, the Theme MUST NO LONGER USE any user data provided
         * in the init method, as after this point it is NO LONGER GUARANTEED TO EXIST.
         * After this call, init may be called again in the future, possibly with DIFFERENT
         * user data.
         *
         * In the case that Window themes are manually set, rather than being
         * controlled by the Context, it is the responsibility of the user to
         * manually call init and destroy. For more information, see ContextBase.h.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * Also, the implementation may have not actually *used* the user data in init.
         */
        virtual void destroy(void) = 0;

        /**
         * Allows Themes to run the destroy method on other Themes. This is used
         * in ThemeGroup.
         */
        static inline void destroyOther(Theme * other) {
            other->destroy();
        }

    public:
        /**
         * This is called each time the view area changes, BEFORE any draw calls
         * with the updated view area. Note that this is called alongside
         * TopLevelContainer recalculations of all components in the hierarchy.
         *
         * ALSO, this is ALWAYS called at least ONCE, at the start, before ANY
         * draw calls to a TopLevelContainer.
         *
         * As of Theme API version 0.4, this MUST now be called within a group of
         * operations (after beforeDraw, and before afterDraw), since this call is relevant
         * to a particular window.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         */
        virtual void onChangeViewArea(const Rectangle & view) = 0;

        /**
         * This method should draw a rectangular box, for use as the background to a menu, or similar.
         *
         * A BackPanel component by default does not respond to the mouse, but the theme could respond to mouse
         * states anyway. That would allow BackPanels to appear selected.
         */
        virtual void drawBackPanel(const Location & location) = 0;

        /**
         * This should draw a rectangular box similar to what is drawn with drawBackPanel. FrontPanels are components
         * that are put in front of a BackPanel, to show a UI section, such as related buttons, or a non-interactive
         * piece of text. A FrontPanel should appear distinct to a button, and look like it is not clickable.
         *
         * A FrontPanel component by default does not respond to the mouse, but the theme could respond to mouse
         * states anyway. That would allow FrontPanels to appear selected.
         */
        virtual void drawFrontPanel(const Location & location) = 0;

        /**
         * This method should draw a piece of text, within the specified location. This method
         * is also used for drawing text over the top of buttons, so it would be ideal for text
         * drawn with this method to respond to different states.
         * Note that some Theme implementations (such as one using only basic Xorg drawing
         * primitives) may not support transparent text. These themes may rely on the
         * fill colour of the most recent component drawn as the background colour for text.
         * As a result, for maximum compatibility, and to ensure that these themes do not
         * draw an ugly box of a different colour around text, Components (such as Labels)
         * which consist only of text, should always be drawn immediately after (and hence
         * added to the hierarchy immediately after) the component behind them.
         */
        virtual void drawText(const Location & location, const std::string & string) = 0;

        /**
         * This method should draw the background for a button, within the specified location.
         * This must respond to mouse states, to provide a suitable amount of feedback from the
         * user's interaction. Text is drawn over the top of this using the drawText method,
         * for buttons in the user interface.
         */
        virtual void drawButton(const Location & location) = 0;

        /**
         * This should draw a text entry box, with the specified location, text and cursor position. The text entry box
         * should respond to different states, so it is possible for the user to be able to tell if the text entry box
         * is currently being edited.
         *
         * The cursor MUST only be drawn if drawCursor is true, so:
         *  > Cursor blinking works correctly
         *  > The cursor is not drawn if the user is not currently editing this text entry box.
         */
        virtual void drawTextEntryBox(const Location & location, const std::string & string, size_t cursorPos, bool drawCursor) = 0;

        /**
         * This should draw the background panel for a (vertical) scroll bar. It is not necessary, (or
         * particularly useful), for this to respond to different states in the location. Here, it is
         * completely acceptable for a Theme implementation to draw exactly the same as a back panel.
         *
         * Note: This will always have a width of normalComponentWidth + 2 * uiBorder.
         */
        virtual void drawScrollBarBackground(const Location & location) = 0;

        /**
         * This should draw the background panel for a scroll pane, i.e: What is displayed behind the components
         * which are scrolling. It is not necessary, (or particularly useful), for this to respond to different
         * states in the location. Here, it is completely acceptable for a Theme implementation to draw exactly
         * the same as a back panel.
         */
        virtual void drawScrollContentsBackground(const Location & location) = 0;

        /**
         * This should draw the puck for a scroll bar. This must respond to all states, like
         * a button. This method is here for fine customisation, it is acceptable for a theme implementation
         * to simply run drawButton here, to make the scroll puck look exactly like a button.
         *
         * If vertical is true, this should be drawn as a vertical scroll puck.
         * If vertical is false, this should be drawn as a horizontal scroll puck.
         *
         * Note: This will usually have a width of normalComponentWidth.
         */
        virtual void drawScrollPuck(const Location & location, bool vertical) = 0;

        /**
         * If vertical is true, this should be drawn as a "scroll up" button.
         * If vertical is false, this should be drawn as a "scroll left" button.
         *
         * It is possible (and preferable) for the theme implementation to simply call drawButton, then draw an
         * extra symbol on top.
         * The scroll up, down, left and right buttons must be visually distinct
         * from each other, and must respond to mouse states like a button should.
         *
         * Note: This will usually have a width of normalComponentWidth, and a height of normalComponentHeight.
         */
        virtual void drawScrollUpButton(const Location & location, bool vertical) = 0;

        /**
         * If vertical is true, this should be drawn as a "scroll down" button.
         * If vertical is false, this should be drawn as a "scroll right" button.
         *
         * It is possible (and preferable) for the theme implementation to simply call drawButton, then draw an
         * extra symbol on top.
         * The scroll up, down, left and right buttons must be visually distinct
         * from each other, and must respond to mouse states like a button should.
         *
         * Note: This will usually have a width of normalComponentWidth, and a height of normalComponentHeight.
         */
        virtual void drawScrollDownButton(const Location & location, bool vertical) = 0;

        /**
         * This should draw a semi-transparent box, for use of fading components behind a popup menu.
         *
         * The panel should be drawn semi-transparent, enough to fade-out components behind it, but not too
         * opaque as to obscure them completely.
         *
         * It is not necessary for the theme to respond to states here.
         */
        virtual void drawFadePanel(const Location & location) = 0;

        /**
         * This method should draw the background for a checkbox based button. It should be shown as
         * "ticked" when the mouse state is selected. The text drawn over the top of this will be
         * shifted right by normalComponentWidth, allowing space for a "checkbox" to the left of the text.
         */
        virtual void drawCheckboxButton(const Location & location) = 0;

        /**
         * If vertical is true, this method should draw a vertical progress bar.
         * If vertical is false, this method should draw a horizontal progress bar.
         *
         * A progress bar is intended as a (non user interactable) visual representation
         * of progress. As a result, progress bars do not need to change depending on mouse
         * states. Note however that it is recommended to display progress bars differently
         * for the inactive state.
         * 
         * The provided progress value represents the position of the bar: 0 should represent
         * minimum progress (left-most for horizontal, top-most for vertical). 1 should represent
         * maximum progress (right-most for horizontal, bottom-most for vertical).
         *
         * If the boolean value inverted is false, the progress bar should be displayed
         * as if it is moving "left to right" or "top to bottom" respective of vertical status.
         * This means that minimum is left/top, and maximum is right/bottom.
         * 
         * If the boolean value inverted is true, the progress bar should be displayed
         * as if it is moving "right to left" or "bottom to top" respective of vertical status.
         * This means that minimum is right/bottom, and maximum is left/top.
         */
        virtual void drawProgressBar(const Location & location, GLfloat progress, bool vertical, bool inverted) = 0;

        /**
         * This should draw a background panel for a tooltip, (informational popup menu when the user
         * leaves their mouse over something for long enough). It is preferable for this to contrast
         * with other types of panel, however still acceptable for a Theme implementation to do the
         * same thing here as a FrontPanel or BackPanel.
         */
        virtual void drawTooltipBackground(const Location & location) = 0;

        /**
         * If vertical is true, this method should draw a vertical slider.
         * If vertical is false, this method should draw a horizontal slider.
         *
         * A slider is similar to a progress bar, however is intended to be user interactable.
         * As a result, it is preferable for the slider to change depending on mouse states.
         *
         * The provided position value represents the position of the slider: 0 should represent
         * minimum position (left-most for horizontal, top-most for vertical). 1 should represent
         * maximum position (right-most for horizontal, bottom-most for vertical).
         *
         * If the boolean value inverted is false, the slider should be displayed
         * as if it is moving "left to right" or "top to bottom" respective of vertical status.
         * This means that minimum is left/top, and maximum is right/bottom.
         * 
         * If the boolean value inverted is true, the slider should be displayed
         * as if it is moving "right to left" or "bottom to top" respective of vertical status.
         * This means that minimum is right/bottom, and maximum is left/top.
         */
        virtual void drawSlider(const Location & location, GLfloat position, bool vertical, bool inverted) = 0;

        /**
         * This method should draw a rectangular box with appropriate border, for use as the background
         * and border of a window decoration. The size of any drawn border should correspond with
         * windowDecorationUiBorder, as typically that should be used to specify the width of the border
         * around child windows.
         * 
         * The window decoration back panel by default does not respond to the mouse, but the theme could
         * respond to mouse states anyway.
         */
        virtual void drawWindowDecorationBackPanel(const Location & location) = 0;
        
        /**
         * This method should draw the front panel used on window decorations, behind the window decoration text.
         * 
         * Since this is a window decoration component, it must be able to be drawn comfortably within the area
         * specified by windowDecorationNormalComponentWidth and windowDecorationNormalComponentHeight.
         * 
         * The window decoration front panel by default does not respond to the mouse, but the theme could
         * respond to mouse states anyway.
         */
        virtual void drawWindowDecorationFrontPanel(const Location & location) = 0;
        
        /**
         * This method should draw a piece of text within the specified location, for use as a window
         * decoration. Typically this text is drawn over the top of the window decoration front panel
         * (i.e: drawWindowDecorationFrontPanel), so:
         *  - The theme can assume this is the text background, for cases where transparent text is not
         *    supported (i.e: basic Xorg drawing primitives).
         *  - Response to different mouse states of the component is optional, as unlike drawText, this
         *    is not typically used for buttons. Mouse states ARE still set, so as with drawWindowDecorationFrontPanel,
         *    the theme can respond to mouse states anyway.
         * 
         * Since this is a window decoration component, it must be able to be drawn comfortably within the area
         * specified by windowDecorationNormalComponentWidth and windowDecorationNormalComponentHeight.
         */
        virtual void drawWindowDecorationText(const Location & location, const std::string & string) = 0;
        
        /**
         * This method should draw the background and foreground for a window decoration button, within the
         * specified location. Text is NOT intended to be drawn over the top: The style/label etc is specified
         * by the type.
         * 
         * As with normal buttons, this must respond to mouse states, to provide a suitable amount of feedback
         * from the user's interaction.
         * 
         * Since this is a window decoration component, it must be able to be drawn comfortably within the area
         * specified by windowDecorationNormalComponentWidth and windowDecorationNormalComponentHeight.
         */
        virtual void drawWindowDecorationButton(const Location & location, WindowDecorationButtonType type) = 0;

        // ------------------------------ Custom drawing operations ------------------------------------

        /**
         * These drawing operations should not be used by any ordinary components, and should be reserved
         * for custom drawing operations by the user. These should not internally
         * specify any particular colour, and MUST use the colour set by the user.
         */

        /**
         * This method should take one of the colours we have defined, (or another one of the same type),
         * and set the drawing colour for further custom draw operations. This allows colour setting to be the same
         * regardless of the graphics system in use, (i.e: OpenGL, XCB etc).
         */
        virtual void customSetDrawColour(void * colour) = 0;

        /**
         * This should get the appropriate text draw colour, (i.e: The same as is used in drawText), for the current
         * state of the specified Location instance. This could return one of the colours defined by us, or another
         * colour of the same type.
         */
        virtual void * customGetTextColour(const Location & location) = 0;

        /**
         * This should draw text in the same way as the normal drawText method.
         * But, the text's position should be offsetted by the specified offset, and it's scale should be
         * multiplied by the specified scale. Note that the offsets MUST NOT be multiplied by the user-provided
         * scale.
         * These methods should be optimised for drawing small text, (i.e: with a user-provided scale of between
         * about 1 and 2). The method customDrawTextTitle should be used when the user wants to draw
         * larger text.
         * The text's colour and optionally position should be adjusted by different location states
         * in customDrawTextMouse, but not customDrawTextNoMouse. The idea here is that customDrawTextNoMouse should
         * be simpler and faster, since no checking of state is needed. Where text appears statically, (i.e: not in
         * a button, and not affected by mouse state), customDrawTextNoMouse should be preferred.
         * Note that this:
         *     theme->customSetDrawColour(theme->customGetTextColour(location));
         *     theme->customDrawTextMouse(location, 0.0f, 0.0f, 1.0f, 1.0f, "Some text");
         * Should always do the same as:
         *     theme->drawText(location, "Some text");
         */
        virtual void customDrawTextMouse(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) = 0;
        virtual void customDrawTextNoMouse(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) = 0;

        /**
         * This method should act the same as customDrawTextMouse, except that it can be optimised for
         * drawing larger text, (i.e: A user-provided scale of 2+). Text colour and offset must respond
         * to the location's state, as this method makes a lot of sense for buttons and similar.
         */
        virtual void customDrawTextTitle(const Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) = 0;

        /**
         * This method should return the offsets from user provided position, which are used
         * for drawing text, when the specified state is in use. These should be
         * identical offsets to those used by drawText, customDrawTextMouse, customDrawTextNoMouse
         * and customDrawTextTitle, when using default location scale.
         */
        virtual void getTextBaseOffsets(State state, GLfloat & offX, GLfloat & offY) = 0;
    };
    
    /**
     * Possible types of window decoration button, for use with
     * drawWindowDecorationButton. An implementation may not need to provide
     * all of these, and additional button types may be added as needed in the
     * future, without an update to theme API version.
     */
    enum class WindowDecorationButtonType {
        close, minimise, maximise, restore
    };
    
    /**
     * List of cursor types which the theme can optionally set, in priority
     * order. See TopLevelContainer's getHighestPriorityCursorType.
     * Each component stores a cursor type: See Component's getCursorType.
     * 
     * Typically, a theme may want to:
     *  - Set a default cursor for the system to use in init. This would be the
     *    default cursor for the display's root window: Whether this is possible
     *    or makes sense depends entirely on the platform implementation.
     *  - Update the current cursor in beforeDraw (or even afterDraw). The 
     *    TopLevelContainer's getHighestPriorityCursorType should be used to
     *    decide which cursor to display.
     *    Note that in partial repaint mode, the cursor will only be updated
     *    when something is repainted. It is expected that components repaint
     *    whenever they want to change their cursor, (see documentation for
     *    Component's getCursorType.
     */
    enum class CursorType {
        /**
         * No specific cursor: Inherit the parent's cursor.
         */
        inherit,
        /**
         * Typically, for top-level windows this will already be set as the
         * default cursor to inherit. 
         */
        arrow,
        /**
         * "Hand" type cursor, for buttons or links.
         */
        hand,
        /**
         * "Text-entry" type cursor, for indicating that the user can enter
         * text.
         */
        text,
        /**
         * For moving in any direction, cursor typically has four arrows.
         */
        move,
        /**
         * For indicating that something can be moved/resized horizontally
         * or vertically.
         */
        moveHorizontally, moveVertically,
        /**
         * For indicating that something can be moved/resized in any of eight
         * directions.
         */
        moveRight, moveUpRight, moveUp, moveUpLeft, moveLeft, moveDownLeft, moveDown, moveDownRight,
        
        COUNT
    };
}

#endif
