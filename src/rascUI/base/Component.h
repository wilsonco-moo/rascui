/*
 * Component.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_BASE_COMPONENT_H_
#define RASCUI_BASE_COMPONENT_H_

#include <unordered_set>
#include <functional>
#include <string>
#include <list>

#include "../util/Location.h"
#include "../util/Bindings.h"

namespace rascUI {

    class TopLevelContainer;
    class Container;
    class RepaintController;
    enum class CursorType;

    /**
     * This (abstract) class defines a base UI component.
     * All components in the rascUI system are subclasses of Component.
     */
    class Component {
    private:
        /**
         * Make Container a friend class, so they can access all of these private fields.
         */
        friend class Container;

        /**
         * Make TopLevelContainer a friend class. This allows it to do two things:
         *  > Set it's topLevel field to itself, ensuring that it's child classes have topLevel set to it.
         *  > Override it's setTopLevel method, so that it produces a warning when a top level container is added to another container,
         *    which should not be done.
         */
        friend class TopLevelContainer;

        /**
         * Make RepaintController a friend class. This allows it to access our boundedRepaint method.
         */
        friend class RepaintController;

        /**
         * This stores our position within our parent's list, and is used by Container to remove components.
         */
        std::list<Component *>::iterator iter;

        /**
         * This is a pointer to our respective top level container. This is NULL unless we have been added to a container
         * at some point.
         * This is assigned a value when setTopLevel is called.
         * NOTE: This is not reassigned to NULL when we are removed.
         */
        TopLevelContainer * topLevel;

        /**
         * This is a pointer to our parent container. This is NULL unless we currently have a parent.
         * This is assigned a value when ware are added to a container, and is reassigned to NULL when we are removed from the container.
         * The top level container holds this value as NULL.
         */
        Container * parent;

        /**
         * This internally stores whether we are active or not, by default this is true.
         * A component that is not active will not respond to mouse events, will not be keyboard selectable, and should be drawn
         * greyed-out by the theme implementation.
         * NOTE: Unlike other things like our mouse state, this is stored internally, and is NOT reset when we are removed from
         *       our top level container.
         */
        bool active;

        /**
         * This internally stores whether we are visible, by default this is true.
         * A component that is not visible will not respond to mouse events, will not be keyboard selectable, and will not be
         * drawn at all. If a container is not visible, all of it's sub-components will also not be drawn, or be
         * selectable/mouse responsive.
         * NOTE: Unlike other things like our mouse state, this is stored internally, and is NOT reset when we are removed from
         *       our top level container.
         */
        bool visible;

        /**
         * This stores an iterator to the state modifier used to make our state inactive.
         */
        std::list<std::function<State(State, Component *)>>::const_iterator inactiveStateModifierIter;

        /**
         * This method:
         *  > If we are a basic Component, this method will simply set our topLevel field.
         *  > If we are a Container, this method will check if our top level is the different to the one provided. If it is, then
         *    the field is set, and the method is called for all of our child components.
         *
         * This method ensures that if we add stuff to a container, THEN add that container to a top level container:
         *  > The original container has a topLevel container of NULL, so do all of it's child components when added.
         *  > When the container is added to the top level container, this method recursively sets the topLevel field of all of it's
         *    child components.
         * This also ensures that if a component tree is moved from one top level container to another, the topLevel field is updated
         * accordingly.
         * Note: If we are adding to a component tree with a top level container that is not set, this method will not recurse, as
         *       all the topLevel values will be the same: NULL.
         */
        virtual void setTopLevel(TopLevelContainer * topLevel);

        /**
         * This method is called when we are removed from a container, and is used to reset out top level container pointer.
         * This also calls our top level container's topLevelRemove method.
         */
        virtual void resetTopLevel(void);

    protected:
        /**
         * This method should add "this" to the specified set, if the mouse position is within our area.
         * This should be overridden by Container, to perform a similar action on itself and all of it's child components.
         *
         * For components, this method must also check for selection locking. If selection locking is enabled on our top level
         * container, this method must only add "this" to the list if we are the component that is locked to selection.
         *
         * Note: This method assumes we have a top level container, and will perform undefined behaviour if we don't.
         *
         * This method is protected so custom implementations of Container can modify which of their child
         * Components are visible at any moment in time.
         */
        virtual void addIfMouseOver(GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver);

    public:
        /**
         * This stores all of our location properties and current state.
         */
        Location location;

        /**
         * This the name of this component, and can be used for debug purposes.
         */
        std::string name;

        Component(const Location & location = Location());
        virtual ~Component(void);

        /**
         * This will print the component hierarchy, for debug purposes.
         */
        inline void print(void) {
            printIndent(0);
        }

    protected:

        /**
         * A helper method for the debug print system.
         */
        virtual void printIndent(int indent);

        /**
         * This method is called whenever resizing must take place.
         * This method should call our location's fitCacheInto method.
         * In the case of Container this method should also call recalculate of all it's child components.
         * The parameter is the area of our parent component.
         *
         * NOTE: Any custom implementations of Component which would benefit from pre-calculating coordinates and scales
         *       etc, should override this method, but ensure that the superclass method is also called.
         */
        virtual void recalculate(const Rectangle & parentSize);

        /**
         * This should draw us on-screen. We should draw ourself into the area defined by location.cache - the precalculated
         * layout-managed rectangle, worked out whenever recalculate is called.
         *
         * NOTE: State::invisible does not need to be checked here, as if our state == State::invisible, this method will never
         *       be called, as our state is checked by the container class.
         */
        virtual void onDraw(void) = 0;


        // ------------- MOUSE EVENTS ----------------

        /**
         * This is called by our top level container when the mouse pointer enters the location cache area of our component.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onMouseEnter(GLfloat viewX, GLfloat viewY);

        /**
         * This is called by our top level container when the mouse pointer moves while over our the area of our component.
         * This is always called after a call to onMouseEnter, and will never be called after the respective onMouseLeave call.
         *
         * It is guaranteed that when not dragging the component, this method will ALWAYS be called when the mouse moves. However,
         * it is NOT GUARANTEED that each time this method is called the mouse WILL HAVE MOVED, there are cases during releasing the mouse
         * button that this method may be called twice with the same position. This is however a very small percentage of the times this method
         * is called.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onMouseMove(GLfloat viewX, GLfloat viewY);

        /**
         * This is called by our top level container when the user presses their mouse button on our component.
         * This is always called after a call to onMouseEnter, and will never be called after the respective onMouseLeave call.
         * The mouse button pressed is provided.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onMousePress(GLfloat viewX, GLfloat viewY, int button);

        /**
         * This is called by our top level container when the user moves their mouse, after pressing the mouse on this component.
         * This is called each time the user moves their mouse, for each mouse button pressed, while holding down the mouse button.
         * This is always called after a call to onMouseEnter and onMousePress. This will never be called after a respective call to
         * onMouseRelease, or onMouseLeave.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onMouseDrag(GLfloat viewX, GLfloat viewY, int button);

        /**
         * This is called by our top level container when the user releases their mouse, WITH THEIR MOUSE POINTER OVER THIS COMPONENT,
         * after originally pressing their mouse while over this component.
         * If the user releases their mouse WHILE THE POINTER IS NOT OVER THIS COMPONENT, this method will NOT be called.
         * This is always called after a call to onMouseEnter and onMousePress. This will always be called BEFORE the respective call to
         * onMouseRelease and onMouseLeave.
         *
         * Note: This method (NOT onMousePress or onMouseRelease) by convention SHOULD BE USED for button clicks.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button);

        /**
         * This is called by our top level container when the user releases their mouse, EVEN IF THE MOUSE POINTER IS NO LONGER OVER THIS COMPONENT,
         * after originally pressing their mouse while over this component.
         * If the user releases their mouse WHILE THE POINTER IS NOT OVER THIS COMPONENT, this method WILL STILL BE CALLED.
         *
         * This will always be called AFTER onMouseClick (if onMouseClick was called), and will ALWAYS be called BEFORE the respective call
         * to onMouseLeave.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button);

        /**
         * This is called by our top level container when the user's mouse pointer leaves this component, after originally entering it.
         * This is always called after a respective call to onMouseEnter.
         * In the case that the user drags their mouse with the mouse button pressed, this method will be called only after they have
         * released their mouse button, even though their mouse pointer may have exited our component earlier.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onMouseLeave(GLfloat viewX, GLfloat viewY);



        // ----------------- SELECTION LOCKING ------------------------



        /**
         * This is called when we receive key press events while we are selection locked. This should be overridden by classes that want
         * to use selection locking functionality. NavigationAction::unlockSelection can be returned from here to unlock selection, and
         * NavigationAction::noAction should be used otherwise.
         *
         * This should be overridden by the user, if useful.
         */
        virtual NavigationAction onSelectionLockKeyPress(NavigationAction defaultAction, int key, bool special);

        /**
         * This is called when we receive key release events while we are selection locked. This should be overridden by classed that want
         * to use selection locking functionality. NavigationAction::unlockSelection can be returned from here to unlock selection, and
         * NavigationAction::noAction should be used otherwise.
         *
         * This should be overridden by the user, if useful.
         *
         * By default, this unlocks selection and repaints, if the keyboard action is NavigationAction::enter.
         * Otherwise, this returns NavigationAction::noAction, and does nothing.
         */
        virtual NavigationAction onSelectionLockKeyRelease(NavigationAction defaultAction, int key, bool special);

        /**
         * This is called when selection is locked for this component.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onSelectionLockEnabled(void);

        /**
         * This is called when selection is unlocked for this component.
         *
         * This should be overridden by the user, if useful.
         */
        virtual void onSelectionLockDisabled(void);

        // ========================================= Keyboard integration ===================================================

        /*
         * See TopLevelContainer.h for more information about keyboard integration.
         */

        /**
         * This method should return whether this component should be able to receive focus via keyboard actions.
         * Components that do nothing, such as text labels should return false here. Containers should return true if at least
         * one of their child components can respond to keyboard actions, and false otherwise.
         */
        virtual bool shouldRespondToKeyboard(void) const = 0;


        /**
         * Each time the user presses a valid keyboard action key, while this component is selected, this method is called.
         * By default, this simply returns the same keyboard action that was passed to the method.
         *
         * Subclasses can override this to produce different behaviours when their component is selected. The idea behind this
         * is for components to be able to customise what happens when the user presses keys, such as in the case of a drop-down menu.
         *
         * It may be the case that the component could select different bits of itself (or something) when the arrow keys are pressed.
         *
         * This is the version of the method for key presses.
         */
        virtual NavigationAction getRequiredKeyboardActionPress(NavigationAction action);

        /**
         * See the documentation for getRequiredKeyboardActionPress.
         * This is the version of the method for key presses.
         */
        virtual NavigationAction getRequiredKeyboardActionRelease(NavigationAction action);


        // ====================================== Repainting system =====================================================

        /**
         * This should call the component's draw method, if we either:
         *   > Are listed in the wantsRepaint set
         *   > Our getBoundedArea() overlaps boundingBox.
         *
         * If we do draw, we should expand the bounding box by our getBoundedArea().
         * This should not check if *we* are visible, as this method should only be called for
         * components which *are* visible, (i.e: The caller of this method should do the check).
         */
        virtual void boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint);

        /**
         * This method should return the area to use in boundedRepaint,
         * i.e: The area which this component is assumed to take up, when repainting.
         *
         * If a custom component does not fill the entire area, or (like SegmentContainer or ScrollPane) it takes
         * up a larger area than it should, it should override this to return the correct area.
         *
         * By default, this simple returns location.cache.
         */
        virtual Rectangle getBoundedArea(void) const;

    public:
        /**
         * This should choose whether the specified state change should cause a repaint of ourself, or a
         * repaint of our parent. Note that if our parent is repainted, we will be implicitly repainted too, so setting
         * both to true is redundant.
         * By default, only changes to/from the invisible state cause repaints:
         *   > Anything -> invisible   causes a repaint of parent
         *   > Invisible -> anything   causes a repaint of self
         * Custom component implementations should override this, (such as ResponsiveComponent), if for example they want
         * repaints to happen when they change between mouse states.
         *
         * To mark that we (or our parent) should require a repaint, set shouldRepaintSelf or shouldRepaintParent
         * to true. If a repaint is not wanted, just do nothing, (i.e: Setting either one to false is redundant, as this is the default).
         * For example, if a class overrode this method, and it did nothing at all, we would never repaint on a state change.
         *
         * This method is called whenever our Location's state is updated, (with location.updateState()), if we have a top
         * level container, and it is in partial repaint mode.
         */
        virtual void doesStateChangeRequireRepaint(State oldState, State newState, bool * shouldRepaintSelf, bool * shouldRepaintParent);

        /**
         * This ensures that this component is repainted next time a partial repaint happens.
         * If partial repainting is not enabled, this does nothing.
         * This method is provided for convenience: It simply calls addComponent on the top level container's
         * RepaintController, (if it exists).
         *
         * This method should be called from within the TopLevelComponent mutex, and does not modify the
         * component hierarchy. This means it is safe to call from any event, such as
         * onDraw, onMouseEnter, onMouseMove, onMousePress, onMouseDrag, onMouseClick, onMouseRelease, onMouseLeave.
         * If this is required from outside these events, the TopLevelComponent's beforeEvent function queue should be used.
         * This must only be called if we are assigned to a top level container.
         */
        void repaint(void);

        // ==============================================================================================================

        /**
         * Gets required cursor type, for use when we're mouse-overed (see TopLevelContainer's
         * getHighestPriorityCursorType). By default this returns CursorType::inherit, but
         * implementations can choose different cursors.
         * 
         * Note: We should be repainted whenever cursor type is changed, as the theme won't
         * update the cursor until something is repainted! (Though often, like mouse enter/leave
         * on a button, a repaint is going to happen anyway).
         * For components which don't need to repaint (e.g: don't repaint on mouse enter/exit),
         * TopLevelContainer's forceEmptyRepaint can be used instead, to allow the cursor to
         * update without unnecessarily repainting.
         */
        virtual CursorType getCursorType(void) const;

    private:
        /**
         * This method should return true only for container components.
         * Container components override this, and return true.
         */
        virtual bool isContainer(void) const;


        // ----------- Fake mouse events for keyboard integration ------------

        // These automatically use the location's cache rectangle to generate fake mouse coordinates.

        inline void onFakeMouseEnter(void) {
            onMouseEnter(location.cache.centreX(), location.cache.centreY());
        }
        inline void onFakeMousePress(int button) {
            onMousePress(location.cache.centreX(), location.cache.centreY(), button);
        }
        inline void onFakeMouseClick(int button) {
            onMouseClick(location.cache.centreX(), location.cache.centreY(), button);
        }
        inline void onFakeMouseRelease(int button) {
            onMouseRelease(location.cache.outsideX(), location.cache.outsideY(), button);
        }
        inline void onFakeMouseLeave(void) {
            onMouseLeave(location.cache.outsideX(), location.cache.outsideY());
        }




        // ==================================================================================================================

    public:
        /**
         * Gets our current top-level container. Null until this component (or a parent of it)
         * gets added to a top-level container.
         */
        inline TopLevelContainer * getTopLevel(void) const {
            return topLevel;
        }
        
        /**
         * Gets our current parent container, or null if we have not been added to a container.
         */
        inline Container * getParent(void) const {
            return parent;
        }
        
        /**
         * A function simply provided for convenience.
         * Gets the preferred primary mouse button, as defined by our top level container.
         * This has a short name since it is required extremely often.
         *
         * This is exactly the same as running
         *   getTopLevel()->bindings->mouseButton
         */
        int getMb(void) const;


        /**
         * This method returns true we are contained within the specified Container's component hierarchy. For example,
         * this returns true if the given container is either:
         *  > Our parent container
         *  > Our parent's parent container
         *  > Our parent's parent's parent container
         *  .... etc
         */
        bool containedWithin(Container * container);


    // ================================= Active/visible setting methods ==============================================

    private:
        // Used internally to add the state modifier needed for inactive/invisible states.
        void addActiveVisibleStateModifier(void);
        // Used internally to remove the state modifier needed for inactive/invisible states.
        void removeActiveVisibleStateModifier(void);

    public:
        /**
         * Gets whether we are currently active. By default this is true.
         *
         * NOTE: Unlike other things like our mouse state, this is stored internally, and is NOT reset when we are removed from
         *       our top level container.
         *
         * If this returns false, (unless overridden by a call to setVisible(false), or another state modifier since
         * being set to inactive), our location's state should be inactive.
         */
        inline bool isActive(void) const {
            return active;
        }

        /**
         * Sets whether we should be active. Setting active to false will automatically put our location's state
         * into the inactive state and will mean we will no longer receive any mouse click events.
         * NOTE: Inactive components will still receive mouse enter, press, release and leave events.
         *
         * This state can be overridden by setVisible(false).
         * Keyboard selection is blocked by inactive components by ResponsiveComponent only returning that it is keyboard
         * selectable if it is visible AND active.
         *
         * NOTE: This has no affect for containers.
         *
         * NOTE: Unlike other things like our mouse state, this is stored internally, and is NOT reset when we are removed from
         *       our top level container.
         *
         * NOTE: This has the effect of setting our location's state to inactive, by pushing a state modifier.
         *       This is overridden if setVisible(false) is called, at which point our location's state becomes invisible.
         */
        void setActive(bool active);

        /**
         * Gets whether we are currently visible. By default this is true.
         *
         * NOTE: Unlike other things like our mouse state, this is stored internally, and is NOT reset when we are removed from
         *       our top level container.
         *
         * If this returns false, (unless overridden by another state modifier since being set to invisible),
         * our location's state should be invisible.
         */
        inline bool isVisible(void) const {
            return visible;
        }

        /**
         * Sets whether we should be visible. Setting visible to false will automatically put our location's state
         * into the invisible state, meaning we will no longer be drawn, or receive any mouse events.
         * NOTE: Invisible components will NO LONGER RECEIVE even mouse enter, press or release events.
         *
         * Running setVisible(false) will override our state being inactive.
         * Keyboard selection is blocked by invisible components by ResponsiveComponent only returning that it is keyboard
         * selectable if it is visible AND active.
         *
         * NOTE: When a container is made invisible, Container::escapeKeyboardSelection is automatically called to ensure
         *       that keyboard selection is moved out of the container.
         *
         * NOTE: Unlike other things like our mouse state, this is stored internally, and is NOT reset when we are removed from
         *       our top level container.
         *
         * NOTE: This has the effect of setting our location's state to invisible, by pushing a state modifier.
         */
        void setVisible(bool visible);

        /**
         * Returns true if and only if this component is active AND visible.
         * If this returns true, our location's state should be neither inactive nor invisible. (Either of these
         * may still be the case though, if another state modifier overrides this).
         */
        inline bool isActiveAndVisible(void) const {
            return active && visible;
        }

        // ----------------------------------- Keyboard navigation methods --------------------------------------------------

        /**
         * This will return the next keyboard-responsive non-container component after us within our parent container. When the search passes
         * to the end of our container, this method will wrap around to the start. The method will return this if none are found, even if we
         * are not keyboard-responsive.
         *
         * NOTE: This method assumes that we are already within a container, and will cause undefined behaviour if we are not.
         */
        Component * nextKeyboardComponent(void) const;

        /**
         * This will return the previous keyboard-responsive non-container component before us within our parent container. When the search
         * passes to the start of our container, this method will wrap around to the end. The method will return this if none are found, even
         * if we are not keyboard-responsive.
         *
         * NOTE: This method assumes that we are already within a container, and will cause undefined behaviour if we are not.
         */
        Component * previousKeyboardComponent(void) const;

        /**
         * This will return the first keyboard-responsive non-container component within the next keyboard-responsive container after our parent
         * container. Sub-containers after this component are searched first, and containers after our parent are searched recursively afterwards.
         *
         * NOTE: This method assumes that we are already within a container, and will cause undefined behaviour if we are not.
         */
        Component * nextKeyboardContainer(void) const;

        /**
         * This will return the last keyboard-responsive non-container component within the previous keyboard-responsive container before our
         * parent container. Sub-containers before this component are searched first, and containers before our parent are searched recursively
         * afterwards.
         *
         * NOTE: This method assumes that we are already within a container, and will cause undefined behaviour if we are not.
         */
        Component * previousKeyboardContainer(void) const;
    };

}

#endif
