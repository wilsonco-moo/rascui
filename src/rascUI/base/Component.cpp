/*
 * Component.cpp
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#include "Component.h"

#include <cstdlib>
#include <iostream>

#include "../util/RepaintController.h"
#include "TopLevelContainer.h"
#include "Container.h"
#include "Theme.h"

namespace rascUI {

    Component::Component(const Location & location) :
        iter(),
        topLevel(NULL),
        parent(NULL),
        active(true),
        visible(true),
        inactiveStateModifierIter(),
        location(location) {
    }

    Component::~Component(void) {
    }

    void Component::printIndent(int indent) {
        for (int i = 0; i < indent; i++) {
            std::cout << ' ';
        }
        std::cout << name;
        std::cout << ";\n";
    }

    void Component::setTopLevel(TopLevelContainer * topLevel) {
        this->topLevel = topLevel;

        // Tell our location to update it's state, since we have just been added to a top level container.
        location.updateState(this);
    }

    void Component::resetTopLevel(void) {
        topLevel->topLevelRemove(this);
        topLevel = NULL;
    }

    void Component::addIfMouseOver(GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver) {
        // If the specified view position is within this component, and we are visible:
        if (location.cache.contains(viewX, viewY) && isVisible()) {
            if (topLevel->isSelectionLocked()) {
                // If the selection is locked, only add us to the list if we are the component who's selection is locked.
                if (topLevel->getKeyboardSelected() == this) {
                    mouseOver->insert(this);
                }
            } else {
                // If selection is not locked, then always add us to the set.
                mouseOver->insert(this);
            }
        }
    }

    void Component::recalculate(const Rectangle & parentSize) {
        location.fitCacheInto(parentSize, topLevel->getXScale(), topLevel->getYScale());
    }

    // Provide stub implementations for the mouse events, so the user does not always have to define all of them.
    void Component::onMouseEnter  (GLfloat viewX, GLfloat viewY) {}
    void Component::onMouseMove   (GLfloat viewX, GLfloat viewY) {}
    void Component::onMousePress  (GLfloat viewX, GLfloat viewY, int button) {}
    void Component::onMouseDrag   (GLfloat viewX, GLfloat viewY, int button) {}
    void Component::onMouseClick  (GLfloat viewX, GLfloat viewY, int button) {}
    void Component::onMouseRelease(GLfloat viewX, GLfloat viewY, int button) {}
    void Component::onMouseLeave  (GLfloat viewX, GLfloat viewY) {}


    NavigationAction Component::onSelectionLockKeyPress(NavigationAction defaultAction, int key, bool special) {
        return NavigationAction::noAction;
    }

    NavigationAction Component::onSelectionLockKeyRelease(NavigationAction defaultAction, int key, bool special) {
        if (defaultAction == NavigationAction::enter) {
            repaint();
            return NavigationAction::unlockSelection;
        }
        return NavigationAction::noAction;
    }

    void Component::onSelectionLockEnabled(void) {
    }

    void Component::onSelectionLockDisabled(void) {
    }



    NavigationAction Component::getRequiredKeyboardActionPress(NavigationAction action) {
        return action;
    }
    NavigationAction Component::getRequiredKeyboardActionRelease(NavigationAction action) {
        return action;
    }

    // ======================================== Repaint system =====================================================

    void Component::boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) {

        // First get the bounded area - the repaint area which we are assumed to take up.
        Rectangle boundedArea = getBoundedArea();

        // If we overlap the bounding box, or are explicitly listed as wanting repainting, then expand the bounding box and draw.
        if (boundingBox.overlaps(boundedArea) || wantsRepaint.find(this) != wantsRepaint.end()) {
            boundingBox += boundedArea;
            onDraw();
        }
    }

    Rectangle Component::getBoundedArea(void) const {
        return location.cache;
    }

    void Component::doesStateChangeRequireRepaint(State oldState, State newState, bool * shouldRepaintSelf, bool * shouldRepaintParent) {
        if (oldState == State::invisible && newState != State::invisible) {
            // If moving from invisible to anything else...
            *shouldRepaintSelf = true;
        } else if (oldState != State::invisible && newState == State::invisible) {
            // If moving from anything else to invisible...
            *shouldRepaintParent = true;
        }
    }

    void Component::repaint(void) {
        RepaintController * repaintController = getTopLevel()->getRepaintController();
        if (repaintController != NULL) {
            repaintController->addComponent(this);
        }
    }

    // =============================================================================================================

    CursorType Component::getCursorType(void) const {
        return CursorType::inherit;
    }

    bool Component::isContainer(void) const {
        return false;
    }



    int Component::getMb(void) const {
        return getTopLevel()->bindings->mouseButton;
    }


    bool Component::containedWithin(Container * container) {
        // If the container is NULL, then return false: We cannot be a member of a non-existant container.
        if (container == NULL) return false;

        // Set the search container initially to our parent.
        Container * cont = parent;
        // Loop while our search container is not NULL. It will reach null when we have searched upwards the entire component
        // hierarchy.
        while(cont != NULL) {
            // If the search container matches, return true.
            if (cont == container) {
                return true;
            }
            // Otherwise, search the search container's parent.
            cont = cont->parent;
        }
        // Return false if none is found.
        return false;
    }




    // =================================== Active / Visible state setting =============================

    void Component::addActiveVisibleStateModifier(void) {
        // Add a state modifier which returns invisible/inactive where necessary.
        inactiveStateModifierIter = location.pushStateModifier([](State state, Component * component) -> State {
            if (!component->visible) {
                return State::invisible;
            } else if (!component->active) {
                return State::inactive;
            } else {
                return state;
            }
        });
    }
    void Component::removeActiveVisibleStateModifier(void) {
        location.removeStateModifier(inactiveStateModifierIter);
    }


    void Component::setActive(bool active) {
        if (active != this->active) {
            if (active) {
                // If we are going from inactive to active:
                // Remove the state modifier if we are also visible.
                this->active = true;
                if (visible) removeActiveVisibleStateModifier();
            } else {
                // If we are going from active to inactive:
                // Add the state modifier if we are also visible.
                this->active = false;
                if (visible) addActiveVisibleStateModifier();
            }

            // Update the location's state, only if we have a top level container. If we do not,
            // there is no point updating the state, since we cannot possibly be being drawn anywhere anyway.
            if (getTopLevel() != NULL) {
                location.updateState(this);
            }
        }
    }

    void Component::setVisible(bool visible) {
        if (visible != this->visible) {
            if (visible) {
                // If we are going from invisible to visible:
                // Remove the state modifier if we are also active.
                this->visible = true;
                if (active) removeActiveVisibleStateModifier();
            } else {
                // If we are going from visible to invisible:
                // Add the state modifier if we are also active.
                this->visible = false;
                if (active) addActiveVisibleStateModifier();
                
                // If becoming invisible, if we are a container, make sure keyboard selection
                // is removed from us. This ensures keyboard selection does not remain on invisible
                // components.
                if (getTopLevel() != NULL && isContainer()) {
                    ((Container *)this)->escapeKeyboardSelection();
                }
            }

            // Update the location's state, only if we have a top level container. If we do not,
            // there is no point updating the state, since we cannot possibly be being drawn anywhere anyway.
            if (getTopLevel() != NULL) {
                location.updateState(this);
            }
        }
    }

    // -------------------------------------------- Keyboard navigation methods ----------------------------------------------


    Component * Component::nextKeyboardComponent(void) const {
        // Get a copy of our list iterator.
        // NOTE: We do not have to handle the case that the list is empty - we know it isn't: We are in it.
        std::list<Component *>::iterator searchIter = iter;
        while(true) {
            // Increment first, so we don't check against ourself immediately.
            searchIter++;
            // Wrap the iterator around if it has reached the end.
            if (searchIter == parent->components.end()) {
                searchIter = parent->components.begin();
            }
            // Dereference the iterator, We do not have to handle the case that the list is empty - we know it isn't: We are in it.
            Component * component = *searchIter;
            // If we have iterated back around to ourself, then return ourself.
            if (component == this) return component;
            // Otherwise, if the component is NOT a container, but is keyboard responsive, then return it.
            if (!component->isContainer() && component->shouldRespondToKeyboard()) return component;

        }
    }
    Component * Component::previousKeyboardComponent(void) const {
        // Get a copy of our list iterator.
        std::list<Component *>::iterator searchIter = iter;
        while(true) {
            // Wrap the iterator around if we have reached the beginning
            if (searchIter == parent->components.begin()) {
                searchIter = parent->components.end();
            }
            // Then decrement the iterator
            searchIter--;
            // Dereference the iterator, We do not have to handle the case that the list is empty - we know it isn't: We are in it.
            Component * component = *searchIter;
            // If we have iterated back around to ourself, then return ourself.
            if (component == this) return component;
            // Otherwise, if the component is NOT a container, but is keyboard responsive, then return it.
            if (!component->isContainer() && component->shouldRespondToKeyboard()) return component;
        }
    }



    Component * Component::nextKeyboardContainer(void) const {
        // First we search all of our parent container's sub-containers, as these contain components further down the hierarchy than us,
        // so we should switch to them next.
        for (Component * component : parent->components) {
            // Only look at visible containers.
            if (component->isContainer() && component->isVisible()) {
                // If any of our parent's sub-containers contain a keyboard intractable component, then use it.
                Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentNonDepth();
                if (found != NULL) {
                    return found;
                }
            }
        }

        // So... if there do not exist any sub-containers that the selection can move into, then we must search upwards, so call that
        // method for our parent.
        return parent->containerSwitchUpwardSearch();
    }
    Component * Component::previousKeyboardContainer(void) const {
        // If we are not a container, call our parent container's previousKeyboardContainer method.
        // This is a better way of doing this than searching our parent's parent's component list.
        if (!isContainer()) {
            return parent->previousKeyboardContainer();
        }

        // --> FROM HERE WE CAN ASSUME THAT WE ARE A CONTAINER.


        // If we are the root container in the current selection bound, we have reached the beginning of the selection loop,
        // so go back to the end of selection using a downward search.
        if (this == topLevel->getSelectionBound()) {
            // Note: We cast to a Container here, since at this point we assume we are a Container, not just a Component.
            return ((const Container *)this)->containerSwitchDownwardSearch();
        }

        // Otherwise, search through the components before us within our parent container.
        std::list<Component *>::iterator searchIter = iter;
        while(true) {
            if (searchIter == parent->components.begin()) {
                break;
            }
            --searchIter;
            Component * component = *searchIter;

            // If any of these are visible containers, then we should try to switch selection to them. So run a downward search
            // on them, and return the result if they have a keyboard-interactable subcomponent.
            if (component->isContainer() && component->isVisible()) {
                Component * found = ((Container *)component)->containerSwitchDownwardSearch();
                if (found != NULL) return found;
            }
        }

        // If there are no containers before us within our parent that we can switch to, then try searching our parent for standard
        // keyboard-interactable components that are siblings to us. This will move the selection to a component within our parent
        // component, if possible.
        Component * found = parent->getFirstKeyboardResponsiveComponent();
        if (found != NULL) return found;

        // If that has failed, then recursively search upwards, by calling our parent's method.
        return parent->previousKeyboardContainer();
    }


}






/**
 * Old way of doing nextKeyboardContainer
 *  - This is flawed because this will not switch containers reliably, and will ignore subcontainers before this component.
 */
//// Get a copy of our list iterator.
//std::list<Component *>::iterator searchIter = iter;
//while(true) {
//    // Increment first, so we don't check against ourself immediately.
//    searchIter++;
//    // Wrap the iterator around if it has reached the end.
//    if (searchIter == parent->components.end()) {
//        searchIter = parent->components.begin();
//    }
//
//    Component * component = *searchIter;
//    if (component == this) {
//        // If our parent is the top level component, and we have searched all other components contained within it,
//        // then return ourself since there is nothing left to search.
//        if (parent->parent == NULL) {
//            return component;
//        } else {
//            Component * parentSearch = parent->nextKeyboardContainer();
//            if (parentSearch == parent) {
//                return component;
//            } else {
//                return parentSearch;
//            }
//        }
//
//    } else if (component->isContainer()) {
//        Component * subSearch = ((Container *)component)->getFirstKeyboardResponsiveComponentDepth();
//        if (subSearch != NULL) {
//            return subSearch;
//        }
//    } else if (component->shouldRespondToKeyboard()) {
//        return component;
//    }
//}
