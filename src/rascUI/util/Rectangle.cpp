/*
 * Rectangle.cpp
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#include "Rectangle.h"

#include <algorithm>

namespace rascUI {

    Rectangle::Rectangle(GLfloat x, GLfloat y, GLfloat width, GLfloat height) :
        x(x),
        y(y),
        width(width),
        height(height) {
    }

    Rectangle Rectangle::operator + (const Rectangle & other) const {

        // If either rectangle has a width or height of zero, return the other rectangle.
        // If both rectangles have a width and height of zero, return a default constructed rectangle.
        if (width == 0 || height == 0) {
            if (other.width == 0 || other.height == 0) {
                return Rectangle(); // we are zero, other is zero,    so return default rectangle.
            } else {
                return other; // we are zero, other is non-zero,   so return other.
            }
        } else if (other.width == 0 || other.height == 0) {
            return *this;  // we are non-zero, other is zero,   so return this.
        }

        Rectangle ret;
        // Give the return rectangle the smallest x and y coordinates.
        if (x < other.x) { ret.x = x; } else { ret.x = other.x; }
        if (y < other.y) { ret.y = y; } else { ret.y = other.y; }

        // Give the return rectangle the largest width, i.e: The largest right-hand
        // x coordinate.
        {
            GLfloat x2  = x + width,
                    ox2 = other.x + other.width;
            if (x2 > ox2) {
                ret.width = x2 - ret.x;
            } else {
                ret.width = ox2 - ret.x;
            }
        }

        // Give the return rectangle the largest height, i.e: The largest bottom
        // y coordinate.
        {
            GLfloat y2 = y + height,
                    oy2 = other.y + other.height;
            if (y2 > oy2) {
                ret.height = y2 - ret.y;
            } else {
                ret.height = oy2 - ret.y;
            }
        }

        return ret;
    }

    Rectangle Rectangle::operator * (const Rectangle & other) const {

        // Do nothing if the rectangles don't overlap.
        if (!overlaps(other)) {
            return Rectangle();
        }

        // Work out x2,y2 for both rectangles.
        GLfloat x2  = x + width,
                y2  = y + height,
                ox2 = other.x + other.width,
                oy2 = other.y + other.height;

        // Work out the final coordinates with min/max.
        GLfloat finalX1 = std::max(x, other.x),
                finalY1 = std::max(y, other.y),
                finalX2 = std::min(x2, ox2),
                finalY2 = std::min(y2, oy2);

        return Rectangle(finalX1, finalY1, finalX2 - finalX1, finalY2 - finalY1);
    }

    bool Rectangle::isNonZero(void) const {
        return width > 0.0f && height > 0.0f;
    }

    bool Rectangle::contains(GLfloat x, GLfloat y) const {
        GLfloat x2 = this->x + width, y2 = this->y + height;
        return x >= this->x && y >= this->y && x < x2 && y < y2;
    }

    bool Rectangle::overlaps(const Rectangle & other) const {
        return !(width == 0
              || height == 0
              || other.width == 0
              || other.height == 0
              || x + width <= other.x
              || y + height <= other.y
              || other.x + other.width <= x
              || other.y + other.height <= y);
    }

    GLfloat Rectangle::overlapArea(const Rectangle & other) const {
        const Rectangle overlap = (*this) * other;
        return overlap.width * overlap.height;
    }
}
