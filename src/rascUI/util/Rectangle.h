/*
 * Rectangle.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_RECTANGLE_H_
#define RASCUI_UTIL_RECTANGLE_H_

#include <GL/gl.h>

namespace rascUI {

    /**
     * This class represents a simple rectangle, with 4 GLfloat values:
     * x, y, width and height.
     *
     * Note: Do not make subclasses of Rectangle: it does not have a destructor.
     */
    class Rectangle {
    public:
        GLfloat x, y, width, height;

        Rectangle(GLfloat x = 0.0f, GLfloat y = 0.0f, GLfloat width = 0.0f, GLfloat height = 0.0f);

        /**
         * Equality checking with rectangles checks that the x, y, width and height values are identical.
         */
        inline bool operator == (const Rectangle & other) const {
            return x == other.x && y == other.y && width == other.width && height == other.height;
        }
        inline bool operator != (const Rectangle & other) const {
            return x != other.x || y != other.y || width != other.width || height != other.height;
        }

        /**
         * When rectangles are added together, the resulting rectangle is the smallest bounding box which
         * fits both rectangles inside. If either rectangle has a width or height of zero, the other
         * rectangle is returned. If both rectangles have a width and height of zero, a default constructed
         * rectangle is returned.
         */
        Rectangle operator + (const Rectangle & other) const;
        inline Rectangle & operator += (const Rectangle & other) {
            *this = *this + other;
            return *this;
        }

        /**
         * When rectangles are multiplied together, the resulting rectangle is the overlapping region between
         * the two rectangles - (the intersection). If the rectangles do not overlap, a default constructed
         * rectangle is returned.
         * NOTE: Do not use this as a way to check overlap, as it is slower than the overlaps method.
         */
        Rectangle operator * (const Rectangle & other) const;
        inline Rectangle & operator *= (const Rectangle & other) {
            *this = *this * other;
            return *this;
        }

        /**
         * Returns true if this rectangle has a non-zero area - i.e: Our width and height are both
         * greater than zero.
         */
        bool isNonZero(void) const;

        /**
         * Returns true if the specified point is contained within this rectangle.
         */
        bool contains(GLfloat x, GLfloat y) const;

        /**
         * Returns true if the specified rectangle overlaps (collides) with this rectangle.
         * If either rectangle has a width or height of zero, false is automatically returned.
         */
        bool overlaps(const Rectangle & other) const;

        /**
         * Returns the area of the overlapping region between the two rectangles (see operator*),
         * or zero if they don't overlap.
         */
        GLfloat overlapArea(const Rectangle & other) const;

        /**
         * These methods return the centre point of the rectangle.
         */
        inline GLfloat centreX(void) const { return x + width * 0.5f; }
        inline GLfloat centreY(void) const { return y + height * 0.5f; }

        /**
         * These methods return a point which is not within the rectangle, assuming the rectangle
         * has positive width and positive height.
         * I.e:
         * rectangle.contains(rectangle.outsideX(), rectangle.outsideY())
         * Is always false, for a rectangle with positive width and height.
         */
        inline GLfloat outsideX(void) const { return x + width + 1.0f; }
        inline GLfloat outsideY(void) const { return y + height + 1.0f; }
    };
}
#endif
