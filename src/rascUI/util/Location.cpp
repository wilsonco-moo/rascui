/*
 * Location.cpp
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#include "Location.h"

#include <cmath>

#include "../base/TopLevelContainer.h"
#include "../base/Component.h"
#include "RepaintController.h"

namespace rascUI {

    Location::Location(void) :
        cachedState(State::normal),
        stateModifiers(),
        position(),
        weight(0.0f, 0.0f, 1.0f, 1.0f),
        cache(),
        xScale(1.0f),
        yScale(1.0f) {
    }

    Location::Location(GLfloat x, GLfloat y, GLfloat width, GLfloat height) :
        cachedState(State::normal),
        stateModifiers(),
        position(x, y, width, height),
        weight(),
        cache(),
        xScale(1.0f),
        yScale(1.0f) {
    }

    Location::Location(GLfloat x, GLfloat y, GLfloat width, GLfloat height, GLfloat xWeight, GLfloat yWeight, GLfloat widthWeight, GLfloat heightWeight) :
        cachedState(State::normal),
        stateModifiers(),
        position(x, y, width, height),
        weight(xWeight, yWeight, widthWeight, heightWeight),
        cache(),
        xScale(1.0f),
        yScale(1.0f) {
    }

    Location::Location(Rectangle position) :
        cachedState(State::normal),
        stateModifiers(),
        position(position),
        weight(),
        cache(),
        xScale(1.0f),
        yScale(1.0f) {
    }

    Location::Location(Rectangle position, Rectangle weight) :
        cachedState(State::normal),
        stateModifiers(),
        position(position),
        weight(weight),
        cache(),
        xScale(1.0f),
        yScale(1.0f) {
    }

    void Location::fitCacheInto(const Rectangle & parent, GLfloat xScale, GLfloat yScale) {

        this->xScale = xScale;
        this->yScale = yScale;

        // Figure out exact required x,y,width,height.
        GLfloat x = parent.x + position.x * xScale + weight.x * parent.width,
                y = parent.y + position.y * yScale + weight.y * parent.height,
            width = position.width  * xScale + weight.width  * parent.width,
           height = position.height * yScale + weight.height * parent.height;

        // Round down the x and y position, so our x,y values are integers.
        int floorX = std::floor(x),
            floorY = std::floor(y);

        // Use rounded down values for x and y, figure out width and height by rounding
        // down exact right-most position, and finding difference from integer x,y coordinates.
        cache.x      = floorX;
        cache.y      = floorY;
        cache.width  = (int)std::floor(x + width)  - floorX;
        cache.height = (int)std::floor(y + height) - floorY;
    }


    void Location::updateState(Component * component) {
        // Work out the new state, and keep the old state.
        State oldState = cachedState;
        cachedState = State::normal;
        for (std::function<State(State, Component *)> & stateModifier : stateModifiers) {
            cachedState = stateModifier(cachedState, component);
        }

        // Get the repaint controller. Stop here if either no top level container, or not in partial repaint mode.
        if (component->getTopLevel() == NULL) return;
        RepaintController * repaintController = component->getTopLevel()->getRepaintController();
        if (repaintController == NULL) return;

        // Ask the component whether this state change requires a repaint to ourself/our parent.
        bool shouldRepaintSelf = false,
             shouldRepaintParent = false;
        component->doesStateChangeRequireRepaint(oldState, cachedState, &shouldRepaintSelf, &shouldRepaintParent);

        if (shouldRepaintParent && component->getParent() != NULL) {
            // If should repaint parent, AND parent actually exists
            repaintController->addComponent(component->getParent());

        } else if (shouldRepaintSelf || shouldRepaintParent) {
            // If should repaint self, OR should repaint parent but parent didn't exist
            repaintController->addComponent(component);
        }
    }

    std::list<std::function<State(State, Component *)>>::const_iterator Location::pushStateModifier(std::function<State(State, Component *)> stateModifier) {
        stateModifiers.push_back(stateModifier);
        return --stateModifiers.cend();
    }

    void Location::removeStateModifier(std::list<std::function<State(State, Component *)>>::const_iterator stateModifierIter) {
        stateModifiers.erase(stateModifierIter);
    }
    
    void Location::setPositionAndWeight(GLfloat x, GLfloat y, GLfloat width, GLfloat height, GLfloat xWeight, GLfloat yWeight, GLfloat widthWeight, GLfloat heightWeight) {
        position = Rectangle(x, y, width, height);
        weight = Rectangle(xWeight, yWeight, widthWeight, heightWeight);
    }
}
