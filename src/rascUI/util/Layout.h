/*
 * Layout.h
 *
 *  Created on: 24 Mar 2019
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_LAYOUT_H_
#define RASCUI_UTIL_LAYOUT_H_

/**
 * This header file contains a large number of macros for generating the standard 8-number locations required
 * for layout using rascUI.
 *
 *
 * NOTE: If using this header, you must do one of these two things:
 *        > Before of after #including this header (it doesn't matter), but BEFORE USING any macros from it,
 *          you must #define LAYOUT_THEME to be a pointer to some Theme instance, which can be used for borders
 *          and component heights.
 *        > BEFORE #including THIS HEADER, #define UI_BORDER, UI_BWIDTH and UI_BHEIGHT to some sensible values.
 *
 *
 * Quick reference
 * ---------------
 *
 * GEN                                             Allows 8 numbers to be used as a location.
 * GEN_FILL                                        Generates a location which fills the entire area.
 * GEN_FILLMARGIN ... X, Y, XY, LR, TB, TBLR       Fills the area, with the specified margins.
 * GEN_BORDER                                      Fills the entire area with borders around it.
 * GEN_BORDERMARGIN ... X, Y, XY, LR, TB, TBLR     Fills the entire area with borders around it, plus specified margins.
 * GEN_CENTRE ... FIXED, WEIGHT, FIXEDWEIGHT       Generates a centred area with either a fixed size, a weighted size, or a combination of both.
 *
 *
 * SUB_MARGIN ... -, X, Y, XY, T, B, L, R, TB, LR, TBLR          Subtracts the specified margin from the area, (inverse of PIN, without border).
 * SUB_BORDER ... -, T, B, L, R, X, Y                            Subtracts a border width from the specified area edge.
 * SUB_BORDERMARGIN ... -, X, Y, XY, T, B, L, R, TB, LR, TBLR    Subtracts a specified margin plus a border width from the specified edge, (inverse of PIN, with border).
 *
 *
 * MOVE ... X, Y, XY                 Offsets the position of the area by the specified amount.
 * MOVE_NORMAL ... X, Y, XY          Offsets the position of the area by the specified multiple of the normal component size.
 * MOVE_BORDERNORMAL ... X, Y, XY    Offsets the position of the area by the specified multiple of the normal component size, putting a border between each component.
 * SETSIZE ... X, Y, XY              Sets the width and height of the area to the specified values.
 * SETSIZEOFF ... X, Y, XY           Sets the width and height, without overriding the weight of the area.
 * SETSIZEMOVE ... X, Y, XY          Performs SETSIZEOFF equally on both sides, by subtracting half the width/height from the position.
 * SETNORMAL ... X, Y, XY            Sets the specified area dimension to normal component size.
 *
 *
 * PIN ... T, B, L, R            Pins the area to the specified edge, given the specified width/height, (inverse of SUB_MARGIN or SUB_BORDERMARGIN).
 * PIN_NORMAL ... T, B, L, R     Pins the area to the specified edge, using normal component width/height.
 * SPLIT ... T, B, L, R          Splits the area by weight, without a border between, attaches to the specified edge.
 * BORDERSPLIT ... T, B, L, R    Splits the area by weight, with a border between, attaches to the specified edge.
 *
 *
 * TABLE ... X, Y, XY                 Splits the area by equal sized column/row/cell, without a border between column/row/cells.
 * SCALED_TABLE ... X, Y, XY          Splits the area by variable sized column/row/cell, without a border between column/row/cells.
 * BORDERTABLE ... X, Y, XY           Splits the area by equal sized column/row/cell, with a border between column/row/cells.
 * SCALED_BORDERTABLE ... X, Y, XY    Splits the area by variable sized column/row/cell, with a border between column/row/cells.
 *
 * DEFINE_TABLE                                                                                          Defines a table for use with DEFINE_TABLE_* and DEFINED_BORDERTABLE_*.
 * DEFINED_TABLE ... X, Y, XY, COLSPAN_X, ROWSPAN_Y, COLSPAN_XY, ROWSPAN_XY, COLSPAN_ROWSPAN_XY          Selects cell from defined table, using column/row ID with optional colspan/rowspan, without borders between cells.
 * DEFINED_BORDERTABLE ... X, Y, XY, COLSPAN_X, ROWSPAN_Y, COLSPAN_XY, ROWSPAN_XY, COLSPAN_ROWSPAN_XY    Selects cell from defined table, using column/row ID with optional colspan/rowspan, with borders between cells.
 * 
 * COUNT_OUTBORDER ... X, Y    Returns the size of the specified number of components, with borders between and outside.
 * COUNT_ONEBORDER ... X, Y    Returns the size of the specified number of components, with borders between and outside only on one side.
 * COUNT_INBORDER ... X, Y     Returns the size of the specified number of components, with borders between but not outside.
 *
 *
 * Suffixes and their meanings
 * ---------------------------
 *
 *      - = The property applies to top, bottom, left and right equally
 *      X = Applies to x axis, and/or where applicable to left and right equally.
 *      Y = Applies to y axis, and/or where applicable to left and right equally.
 *     XY = Applies to x and y, allows the user to specify two different values.
 *      T = Only applies to top
 *      B = Only applies to bottom
 *      L = Only applies to left
 *      R = Only applies to right
 *     LR = Applies to both left and right, allows the user to specify two different values.
 *     TB = Applies to both top and bottom, allows the user to specify two different values.
 *   TBLR = Applies to top, bottom, left and right, allows the user to specify four different values.
 */

// ===================================== Shorter names for things ====================================

#ifndef UI_BORDER
    #define UI_BORDER \
        (LAYOUT_THEME)->uiBorder
#endif

#ifndef UI_BWIDTH
    #define UI_BWIDTH \
        (LAYOUT_THEME)->normalComponentWidth
#endif

#ifndef UI_BHEIGHT
    #define UI_BHEIGHT \
        (LAYOUT_THEME)->normalComponentHeight
#endif

// ===================================== Base generation macros ======================================

/**
 * GEN_* macros allow generation of a starting point for layout.
 */

/**
 * GEN converts the positions into something that can be used as a location in other macros.
 * This is often the easiest generation method to use, since it allows a lot of flexibility.
 */
#define GEN(x, y, w, h, xw, yw, ww, hw) \
    x, y, w, h,     xw, yw, ww, hw

/**
 * GEN_FILL* macros allow generation of the entire area, with margins subtracted.
 */
#define GEN_FILL \
    0.0f, 0.0f, 0.0f, 0.0f,     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_FILLMARGIN_X(hMargin) \
    hMargin, 0.0f, -2.0f * (hMargin), 0.0f,     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_FILLMARGIN_Y(vMargin) \
    0.0f, vMargin, 0.0f, -2.0f * (vMargin),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_FILLMARGIN_XY(hMargin, vMargin) \
   hMargin, vMargin, -2.0f * (hMargin), -2.0f * (vMargin),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_FILLMARGIN_LR(leftMargin, rightMargin) \
    leftMargin, 0.0f, -(leftMargin)-(rightMargin), 0.0f,     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_FILLMARGIN_TB(topMargin, bottomMargin) \
    0.0f, topMargin, 0.0f, -(topMargin)-(bottomMargin),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_FILLMARGIN_TBLR(topMargin, bottomMargin, leftMargin, rightMargin) \
    leftMargin, topMargin, -(topMargin)-(bottomMargin), -(leftMargin)-(rightMargin),     0.0f, 0.0f, 1.0f, 1.0f

/**
 * GEN_BORDER* macros allow generation of the entire area, with margins subtracted, with borders removed.
 */
#define GEN_BORDER \
    (UI_BORDER), (UI_BORDER), -2.0f * (UI_BORDER), -2.0f * (UI_BORDER),    0.0f, 0.0f, 1.0f, 1.0f

#define GEN_BORDERMARGIN_X(hMargin) \
    (UI_BORDER) + (hMargin), (UI_BORDER), (UI_BORDER) - 2.0f * (hMargin), (UI_BORDER),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_BORDERMARGIN_Y(vMargin) \
    (UI_BORDER), (UI_BORDER) + (vMargin), (UI_BORDER), (UI_BORDER) - 2.0f * (vMargin),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_BORDERMARGIN_XY(hMargin, vMargin) \
    (UI_BORDER) + (hMargin), (UI_BORDER) + (vMargin), (UI_BORDER) - 2.0f * (hMargin), (UI_BORDER) - 2.0f * (vMargin),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_BORDERMARGIN_LR(leftMargin, rightMargin) \
    (UI_BORDER) + (leftMargin), (UI_BORDER), (UI_BORDER) - (leftMargin) - (rightMargin), (UI_BORDER),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_BORDERMARGIN_TB(topMargin, bottomMargin) \
    (UI_BORDER), (UI_BORDER) + (topMargin), (UI_BORDER), (UI_BORDER) - (topMargin) - (bottomMargin),     0.0f, 0.0f, 1.0f, 1.0f

#define GEN_BORDERMARGIN_TBLR(topMargin, bottomMargin, leftMargin, rightMargin) \
    (UI_BORDER) + (leftMargin), (UI_BORDER) + (topMargin), (UI_BORDER) - (topMargin) - (bottomMargin), (UI_BORDER) - (leftMargin) - (rightMargin),     0.0f, 0.0f, 1.0f, 1.0f

/**
 * GEN_CENTRE_* macros generate a centred area with either a fixed size, a weighted size, or a combination of both.
 */
#define GEN_CENTRE_FIXED(width, height) \
    -0.5f * (width), -0.5f * (height), (width), (height),     0.5f, 0.5f, 0.0f, 0.0f

#define GEN_CENTRE_WEIGHT(xWeight, yWeight) \
    0.0f, 0.0f, 0.0f, 0.0f,     0.5f * (1.0f - (xWeight)), 0.5f * (1.0f - (yWeight)), (xWeight), (yWeight)

#define GEN_CENTRE_FIXEDWEIGHT(width, height, xWeight, yWeight) \
    -0.5f * (width), -0.5f * (height), (width), (height),     0.5f * (1.0f - (xWeight)), 0.5f * (1.0f - (yWeight)), (xWeight), (yWeight)

// ================================= Subtraction of borders and margins ==============================

/**
 * SUB_* macros allow subtraction of fixed amounts from the area.
 */

/**
 * SUB_MARGIN_* macros allow subtraction of fixed margins from the area.
 * In essence, this is the inverse operation to PIN_*, without putting a border between.
 *
 * SUB_MARGIN, SUB_MARGIN_X, SUB_MARGIN_Y, SUB_MARGIN_XY, SUB_MARGIN_T, SUB_MARGIN_B, SUB_MARGIN_L, SUB_MARGIN_R, SUB_MARGIN_LR, SUB_MARGIN_TB, SUB_MARGIN_TBLR
 */
#define SUB_MARGIN(margin, location) \
    SUB_MARGIN_INTERNAL(margin, location)
#define SUB_MARGIN_INTERNAL(margin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (margin), (y) + (margin), (w) - 2.0f * (margin), (h) - 2.0f * (margin),     xw, yw, ww, hw

#define SUB_MARGIN_X(hMargin, location) \
    SUB_MARGIN_X_INTERNAL(hMargin, location)
#define SUB_MARGIN_X_INTERNAL(hMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (hMargin), y, (w) - 2.0f * (hMargin), h,     xw, yw, ww, hw

#define SUB_MARGIN_Y(vMargin, location) \
    SUB_MARGIN_Y_INTERNAL(vMargin, location)
#define SUB_MARGIN_Y_INTERNAL(vMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (vMargin), w, (h) - 2.0f * (vMargin),     xw, yw, ww, hw

#define SUB_MARGIN_XY(hMargin, vMargin, location) \
    SUB_MARGIN_XY_INTERNAL(hMargin, vMargin, location)
#define SUB_MARGIN_XY_INTERNAL(hMargin, vMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (hMargin), (y) + (vMargin), (w) - 2.0f * (hMargin), (h) - 2.0f * (vMargin),     xw, yw, ww, hw

#define SUB_MARGIN_T(topMargin, location) \
    SUB_MARGIN_T_INTERNAL(topMargin, location)
#define SUB_MARGIN_T_INTERNAL(topMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (topMargin), w, (h) - (topMargin),     xw, yw, ww, hw

#define SUB_MARGIN_B(bottomMargin, location) \
    SUB_MARGIN_B_INTERNAL(bottomMargin, location)
#define SUB_MARGIN_B_INTERNAL(bottomMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (h) - (bottomMargin),     xw, yw, ww, hw

#define SUB_MARGIN_L(leftMargin, location) \
    SUB_MARGIN_L_INTERNAL(leftMargin, location)
#define SUB_MARGIN_L_INTERNAL(leftMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (leftMargin), y, (w) - (leftMargin), h,     xw, yw, ww, hw

#define SUB_MARGIN_R(rightMargin, location) \
    SUB_MARGIN_R_INTERNAL(rightMargin, location)
#define SUB_MARGIN_R_INTERNAL(rightMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (w) - (rightMargin), h,     xw, yw, ww, hw

#define SUB_MARGIN_TB(topMargin, bottomMargin, location) \
    SUB_MARGIN_TB_INTERNAL(topMargin, bottomMargin, location)
#define SUB_MARGIN_TB_INTERNAL(topMargin, bottomMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (topMargin), w, (h) - (topMargin) - (bottomMargin),     xw, yw, ww, hw

#define SUB_MARGIN_LR(leftMargin, rightMargin, location) \
    SUB_MARGIN_LR_INTERNAL(leftMargin, rightMargin, location)
#define SUB_MARGIN_LR_INTERNAL(leftMargin, rightMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (leftMargin), y, (w) - (leftMargin) - (rightMargin), h,     xw, yw, ww, hw

#define SUB_MARGIN_TBLR(topMargin, bottomMargin, leftMargin, rightMargin, location) \
    SUB_MARGIN_TBLR_INTERNAL(topMargin, bottomMargin, leftMargin, rightMargin, location)
#define SUB_MARGIN_TBLR_INTERNAL(topMargin, bottomMargin, leftMargin, rightMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (leftMargin), (y) + (topMargin), (w) - (leftMargin) - (rightMargin), (h) - (topMargin) - (bottomMargin),     xw, yw, ww, hw

/**
 * SUB_BORDER_* macros allow subtraction of borders from the area.
 *
 * SUB_BORDER, SUB_BORDER_T, SUB_BORDER_B, SUB_BORDER_L, SUB_BORDER_R, SUB_BORDER_X, SUB_BORDER_Y
 */
#define SUB_BORDER(location) \
    SUB_BORDER_INTERNAL(location)
#define SUB_BORDER_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER), (y) + (UI_BORDER), (w) - 2.0f * (UI_BORDER), (h) - 2.0f * (UI_BORDER),     xw, yw, ww, hw

#define SUB_BORDER_T(location) \
    SUB_BORDER_T_INTERNAL(location)
#define SUB_BORDER_T_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (UI_BORDER), w, (h) - (UI_BORDER),     xw, yw, ww, hw

#define SUB_BORDER_B(location) \
    SUB_BORDER_B_INTERNAL(location)
#define SUB_BORDER_B_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (h) - (UI_BORDER),     xw, yw, ww, hw

#define SUB_BORDER_L(location) \
    SUB_BORDER_L_INTERNAL(location)
#define SUB_BORDER_L_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER), y, (w) - (UI_BORDER), h,     xw, yw, ww, hw

#define SUB_BORDER_R(location) \
    SUB_BORDER_R_INTERNAL(location)
#define SUB_BORDER_R_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, y, (w) - (UI_BORDER), h,     xw, yw, ww, hw

#define SUB_BORDER_X(location) \
    SUB_BORDER_X_INTERNAL(location)
#define SUB_BORDER_X_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER), y, (w) - 2.0f * (UI_BORDER), h,     xw, yw, ww, hw

#define SUB_BORDER_Y(location) \
    SUB_BORDER_Y_INTERNAL(location)
#define SUB_BORDER_Y_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (UI_BORDER), w, (h) - 2.0f * (UI_BORDER),     xw, yw, ww, hw

/**
 * SUB_BORDERMARGIN_* macros allow subtraction of fixed margins from the area, where each
 * margin subtraced has a border subtracted from it also.
 * In essence, this is the inverse operation to PIN_*, with putting a border between.
 *
 * SUB_BORDERMARGIN, SUB_BORDERMARGIN_X, SUB_BORDERMARGIN_Y, SUB_BORDERMARGIN_XY, SUB_BORDERMARGIN_T, SUB_BORDERMARGIN_B, SUB_BORDERMARGIN_L, SUB_BORDERMARGIN_R, SUB_BORDERMARGIN_LR, SUB_BORDERMARGIN_TB, SUB_BORDERMARGIN_TBLR
 */
#define SUB_BORDERMARGIN(margin, location) \
    SUB_BORDERMARGIN_INTERNAL(margin, location)
#define SUB_BORDERMARGIN_INTERNAL(margin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER) + (margin), (y) + (UI_BORDER) + (margin), (w) - 2.0f * ((UI_BORDER) + (margin)), (h) - 2.0f * ((UI_BORDER) + (margin)),     xw, yw, ww, hw

#define SUB_BORDERMARGIN_X(hMargin, location) \
    SUB_BORDERMARGIN_X_INTERNAL(hMargin, location)
#define SUB_BORDERMARGIN_X_INTERNAL(hMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER) + (hMargin), y, (w) - 2.0f * ((UI_BORDER) + (hMargin)), h,     xw, yw, ww, hw

#define SUB_BORDERMARGIN_Y(vMargin, location) \
    SUB_BORDERMARGIN_Y_INTERNAL(vMargin, location)
#define SUB_BORDERMARGIN_Y_INTERNAL(vMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (UI_BORDER) + (vMargin), w, (h) - 2.0f * ((UI_BORDER) + (vMargin)),     xw, yw, ww, hw

#define SUB_BORDERMARGIN_XY(hMargin, vMargin, location) \
    SUB_BORDERMARGIN_XY_INTERNAL(hMargin, vMargin, location)
#define SUB_BORDERMARGIN_XY_INTERNAL(hMargin, vMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER) + (hMargin), (y) + (UI_BORDER) + (vMargin), (w) - 2.0f * ((UI_BORDER) + (hMargin)), (h) - 2.0f * ((UI_BORDER) + (vMargin)),     xw, yw, ww, hw

#define SUB_BORDERMARGIN_T(topMargin, location) \
    SUB_BORDERMARGIN_T_INTERNAL(topMargin, location)
#define SUB_BORDERMARGIN_T_INTERNAL(topMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (UI_BORDER) + (topMargin), w, (h) - (UI_BORDER) - (topMargin),     xw, yw, ww, hw

#define SUB_BORDERMARGIN_B(bottomMargin, location) \
    SUB_BORDERMARGIN_B_INTERNAL(bottomMargin, location)
#define SUB_BORDERMARGIN_B_INTERNAL(bottomMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (h) - (UI_BORDER) - (bottomMargin),     xw, yw, ww, hw

#define SUB_BORDERMARGIN_L(leftMargin, location) \
    SUB_BORDERMARGIN_L_INTERNAL(leftMargin, location)
#define SUB_BORDERMARGIN_L_INTERNAL(leftMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER) + (leftMargin), y, (w) - (UI_BORDER) - (leftMargin), h,     xw, yw, ww, hw

#define SUB_BORDERMARGIN_R(rightMargin, location) \
    SUB_BORDERMARGIN_R_INTERNAL(rightMargin, location)
#define SUB_BORDERMARGIN_R_INTERNAL(rightMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (w) - (UI_BORDER) - (rightMargin), h,     xw, yw, ww, hw

#define SUB_BORDERMARGIN_TB(topMargin, bottomMargin, location) \
    SUB_BORDERMARGIN_TB_INTERNAL(topMargin, bottomMargin, location)
#define SUB_BORDERMARGIN_TB_INTERNAL(topMargin, bottomMargin,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (UI_BORDER) + (topMargin), w, (h) - 2.0f * (UI_BORDER) - (topMargin) - (bottomMargin),     xw, yw, ww, hw

#define SUB_BORDERMARGIN_LR(leftMargin, rightMargin, location) \
    SUB_BORDERMARGIN_LR_INTERNAL(leftMargin, rightMargin, location)
#define SUB_BORDERMARGIN_LR_INTERNAL(leftMargin, rightMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER) + (leftMargin), y, (w) - 2.0f * (UI_BORDER) - (leftMargin) - (rightMargin), h,     xw, yw, ww, hw

#define SUB_BORDERMARGIN_TBLR(topMargin, bottomMargin, leftMargin, rightMargin, location) \
    SUB_BORDERMARGIN_TBLR_INTERNAL(topMargin, bottomMargin, leftMargin, rightMargin, location)
#define SUB_BORDERMARGIN_TBLR_INTERNAL(topMargin, bottomMargin, leftMargin, rightMargin,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (UI_BORDER) + (leftMargin), (y) + (UI_BORDER) + (topMargin), (w) - 2.0f * (UI_BORDER) - (leftMargin) - (rightMargin), (h) - 2.0f * (UI_BORDER) - (topMargin) - (bottomMargin),     xw, yw, ww, hw

// ================================ Position and size setting ========================================

/**
 * MOVE_*, SETSIZE_*, SETSIZEOFF_*, SETSIZEMOVE_* and SETNORMAL_* macros apply
 * position and size adjustments. These allow for position offsets, and size setting.
 * NOTE: MOVE is an offset, but SETSIZE and SETNORMAL are absolute values.
 */

/**
 * MOVE_* macros allow offsetting the x and y position by a specified amount.
 *
 * MOVE_X, MOVE_Y, MOVE_XY
 */
#define MOVE_X(xOffset, location) \
    MOVE_X_INTERNAL(xOffset, location)
#define MOVE_X_INTERNAL(xOffset,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (xOffset), y, w, h,     xw, yw, ww, hw

#define MOVE_Y(yOffset, location) \
    MOVE_Y_INTERNAL(yOffset, location)
#define MOVE_Y_INTERNAL(yOffset,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (yOffset), w, h,     xw, yw, ww, hw

#define MOVE_XY(xOffset, yOffset, location) \
    MOVE_XY_INTERNAL(xOffset, yOffset, location)
#define MOVE_XY_INTERNAL(xOffset, yOffset,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (xOffset), (y) + (yOffset), w, h,     xw, yw, ww, hw

/**
 * MOVE_NORMAL_* macros allow offsetting the x and y position by a multiple
 * of the normal component size.
 *
 * MOVE_NORMAL_X, MOVE_NORMAL_Y, MOVE_NORMAL_XY
 */
#define MOVE_NORMAL_X(xOffset, location) \
    MOVE_NORMAL_X_INTERNAL(xOffset, location)
#define MOVE_NORMAL_X_INTERNAL(xOffset,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (xOffset) * (UI_BWIDTH), y, w, h,     xw, yw, ww, hw

#define MOVE_NORMAL_Y(yOffset, location) \
    MOVE_NORMAL_Y_INTERNAL(yOffset, location)
#define MOVE_NORMAL_Y_INTERNAL(yOffset,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (yOffset) * (UI_BHEIGHT), w, h,     xw, yw, ww, hw

#define MOVE_NORMAL_XY(xOffset, yOffset, location) \
    MOVE_NORMAL_XY_INTERNAL(xOffset, yOffset, location)
#define MOVE_NORMAL_XY_INTERNAL(xOffset, yOffset,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (xOffset) * (UI_BWIDTH), (y) + (yOffset) * (UI_BHEIGHT), w, h,     xw, yw, ww, hw

/**
 * MOVE_BORDERNORMAL_* macros allow offsetting the x and y position by a
 * multiple of the normal component size, assuming that there is a border between each component.
 *
 * MOVE_BORDERNORMAL_X, MOVE_BORDERNORMAL_Y, MOVE_BORDERNORMAL_XY
 */
#define MOVE_BORDERNORMAL_X(xOffset, location) \
    MOVE_BORDERNORMAL_X_INTERNAL(xOffset, location)
#define MOVE_BORDERNORMAL_X_INTERNAL(xOffset,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (xOffset) * ((UI_BWIDTH) + (UI_BORDER)), y, w, h,     xw, yw, ww, hw

#define MOVE_BORDERNORMAL_Y(yOffset, location) \
    MOVE_BORDERNORMAL_Y_INTERNAL(yOffset, location)
#define MOVE_BORDERNORMAL_Y_INTERNAL(yOffset,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (yOffset) * ((UI_BHEIGHT) + (UI_BORDER)), w, h,     xw, yw, ww, hw

#define MOVE_BORDERNORMAL_XY(xOffset, yOffset, location) \
    MOVE_BORDERNORMAL_XY_INTERNAL(xOffset, yOffset, location)
#define MOVE_BORDERNORMAL_XY_INTERNAL(xOffset, yOffset,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (xOffset) * ((UI_BWIDTH) + (UI_BORDER)), (y) + (yOffset) * ((UI_BHEIGHT) + (UI_BORDER)), w, h,     xw, yw, ww, hw

/**
 * SETSIZE_* macros allow manually setting the width and height. Manually setting the width
 * and height respectively overrides the width, widthWeight, height and heightWeight.
 *
 * SETSIZE_X, SETSIZE_Y, SETSIZE_XY
 */
#define SETSIZE_X(width, location) \
    SETSIZE_X_INTERNAL(width, location)
#define SETSIZE_X_INTERNAL(width,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (width), h,     xw, yw, 0.0f, hw

#define SETSIZE_Y(height, location) \
    SETSIZE_Y_INTERNAL(height, location)
#define SETSIZE_Y_INTERNAL(height,     x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (height),     xw, yw, ww, 0.0f

#define SETSIZE_XY(width, height, location) \
    SETSIZE_XY_INTERNAL(width, height, location)
#define SETSIZE_XY_INTERNAL(width, height,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (width), (height),     xw, yw, 0.0f, 0.0f

/**
 * SETSIZEOFF_* macros allow manually setting the width and height, without overriding
 * the weight of the component.
 *
 * SETSIZEOFF_X, SETSIZEOFF_Y, SETSIZEOFF_XY
 */
#define SETSIZEOFF_X(width, location) \
    SETSIZEOFF_X_INTERNAL(width, location)
#define SETSIZEOFF_X_INTERNAL(width,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (width), h,     xw, yw, ww, hw

#define SETSIZEOFF_Y(height, location) \
    SETSIZEOFF_Y_INTERNAL(height, location)
#define SETSIZEOFF_Y_INTERNAL(height,     x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (height),     xw, yw, ww, hw

#define SETSIZEOFF_XY(width, height, location) \
    SETSIZEOFF_XY_INTERNAL(width, height, location)
#define SETSIZEOFF_XY_INTERNAL(width, height,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (width), (height),     xw, yw, ww, hw

/**
 * SETSIZEMOVE_* macros allow manually setting the width and height, without overriding
 * the weight of the component. Compared to SETSIZEOFF, this also subtracts half the width,
 * and half the height from the position, so the operation happens equally on both sides.
 *
 * SETSIZEMOVE_X, SETSIZEMOVE_Y, SETSIZEMOVE_XY
 */
#define SETSIZEMOVE_X(width, location) \
    SETSIZEMOVE_X_INTERNAL(width, location)
#define SETSIZEMOVE_X_INTERNAL(width,     x, y, w, h, xw, yw, ww, hw) \
    (x) - 0.5f * (width), y, (width), h,     xw, yw, ww, hw

#define SETSIZEMOVE_Y(height, location) \
    SETSIZEMOVE_Y_INTERNAL(height, location)
#define SETSIZEMOVE_Y_INTERNAL(height,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) - 0.5f * (height), w, (height),     xw, yw, ww, hw

#define SETSIZEMOVE_XY(width, height, location) \
    SETSIZEMOVE_XY_INTERNAL(width, height, location)
#define SETSIZEMOVE_XY_INTERNAL(width, height,     x, y, w, h, xw, yw, ww, hw) \
    (x) - 0.5f * (width), (y) - 0.5f * (height), (width), (height),     xw, yw, ww, hw

/**
 * SETNORMAL_* macros allow manually setting the width and height of components to
 * UI_BWIDTH and UI_BHEIGHT. SETNORMAL_* should ALWAYS BE PREFERRED over SETSIZE_*
 *
 * SETNORMAL_X, SETNORMAL_Y, SETNORMAL_XY
 */
#define SETNORMAL_X(location) \
    SETNORMAL_X_INTERNAL(location)
#define SETNORMAL_X_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, y, (UI_BWIDTH), h,     xw, yw, 0.0f, hw

#define SETNORMAL_Y(location) \
    SETNORMAL_Y_INTERNAL(location)
#define SETNORMAL_Y_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (UI_BHEIGHT),     xw, yw, ww, 0.0f

#define SETNORMAL_XY(location) \
    SETNORMAL_XY(location)
#define SETNORMAL_XY_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, y, (UI_BWIDTH), (UI_BHEIGHT),     xw, yw, 0.0f, 0.0f

// ================================== Pinning macros =================================================

/**
 * PIN_* macros attach the area to a fixed width or height section, attached to one
 * side of the previous area. An example of this is a toolbar, with a fixed height, filling the width,
 * at the top of a program.
 *
 * In essence, this operation is the inverse of SUB_MARGIN_* and SUB_BORDERMARGIN_*, (depending on whether
 * we want a border between this area and the other).
 *
 * PIN_T, PIN_B, PIN_L, PIN_R
 */
#define PIN_T(height, location) \
    PIN_T_INTERNAL(height, location)
#define PIN_T_INTERNAL(height,     x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (height),     xw, yw, ww, 0.0f

#define PIN_B(height, location) \
    PIN_B_INTERNAL(height, location)
#define PIN_B_INTERNAL(height, x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (h) - (height), w, (height),     xw, (yw) + (hw), ww, 0.0f

#define PIN_L(width, location) \
    PIN_L_INTERNAL(width, location)
#define PIN_L_INTERNAL(width,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (width), h,     xw, yw, 0.0f, hw

#define PIN_R(width, location) \
    PIN_R_INTERNAL(width, location)
#define PIN_R_INTERNAL(width,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (w) - (width), y, (width), h,     (xw) + (ww), yw, 0.0f, hw

/**
 * PIN_NORMAL_* macros attach the area to a normal component width/height section, attached to one
 * side of the previous area. An example of this is a toolbar, with a fixed height, filling the width,
 * at the top of a program.
 *
 * In essence, this operation is the inverse of SUB_MARGIN_* and SUB_BORDERMARGIN_*, (depending on whether
 * we want a border between this area and the other).
 *
 * Note that PIN_L and PIN_T are functionally identical to SETNORMAL_X and SETNORMAL_Y.
 *
 * PIN_NORMAL_T, PIN_NORMAL_B, PIN_NORMAL_L, PIN_NORMAL_R
 */
#define PIN_NORMAL_T(location) \
    PIN_NORMAL_T_INTERNAL(location)
#define PIN_NORMAL_T_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (UI_BHEIGHT),     xw, yw, ww, 0.0f

#define PIN_NORMAL_B(location) \
    PIN_NORMAL_B_INTERNAL(location)
#define PIN_NORMAL_B_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (h) - (UI_BHEIGHT), w, (UI_BHEIGHT),     xw, (yw) + (hw), ww, 0.0f

#define PIN_NORMAL_L(location) \
    PIN_NORMAL_L_INTERNAL(location)
#define PIN_NORMAL_L_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    x, y, (UI_BWIDTH), h,     xw, yw, 0.0f, hw

#define PIN_NORMAL_R(location) \
    PIN_NORMAL_R_INTERNAL(location)
#define PIN_NORMAL_R_INTERNAL(x, y, w, h, xw, yw, ww, hw) \
    (x) + (w) - (UI_BWIDTH), y, (UI_BWIDTH), h,     (xw) + (ww), yw, 0.0f, hw

// ==================================== Splitting macros =============================================

/**
 * SPLIT_* and BORDERSPLIT_* macros allow splitting the area horizontally or vertically according to a weight.
 */

/**
 * SPLIT_* macros split horizontally or vertically by weight, and attach to
 * the top, bottom, left or right.
 *
 * SPLIT_T, SPLIT_B, SPLIT_L, SPLIT_R
 */
#define SPLIT_T(yWeight, location) \
    SPLIT_T_INTERNAL(yWeight, location)
#define SPLIT_T_INTERNAL(yWeight,     x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (h) * (yWeight),     xw, yw, ww, (hw) * (yWeight)

#define SPLIT_B(yWeight, location) \
    SPLIT_B_INTERNAL(yWeight, location)
#define SPLIT_B_INTERNAL(yWeight,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (1.0f - (yWeight)) * (h), w, (h) * (yWeight),     xw, (yw) + (hw) * (1.0f - (yWeight)), ww, (hw) * (yWeight)

#define SPLIT_L(xWeight, location) \
    SPLIT_L_INTERNAL(xWeight, location)
#define SPLIT_L_INTERNAL(xWeight,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (w) * (xWeight), h,     xw, yw, (ww) * (xWeight), hw

#define SPLIT_R(xWeight, location) \
    SPLIT_R_INTERNAL(xWeight, location)
#define SPLIT_R_INTERNAL(xWeight,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (1.0f - (xWeight)) * (w), y, (w) * (xWeight), h,     (xw) + (ww) * (1.0f - (xWeight)), yw, (ww) * (xWeight), hw

/**
 * BORDERSPLIT_* macros split horizontally or vertically by weight, and attach to the
 * top, bottom, left or right. Compared to SPLIT_*, these functions add a border of
 * size (UI_BORDER) between the two split areas.
 *
 * BORDERSPLIT_T, BORDERSPLIT_B, BORDERSPLIT_L, BORDERSPLIT_R
 */
#define BORDERSPLIT_T(yWeight, location) \
    BORDERSPLIT_T_INTERNAL(yWeight, location)
#define BORDERSPLIT_T_INTERNAL(yWeight,     x, y, w, h, xw, yw, ww, hw) \
    x, y, w, (yWeight) * ((h) - (UI_BORDER)),     xw, yw, ww, (yWeight) * (hw)

#define BORDERSPLIT_B(yWeight, location) \
    BORDERSPLIT_B_INTERNAL(yWeight, location)
#define BORDERSPLIT_B_INTERNAL(yWeight,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + (h) + (yWeight) * ((UI_BORDER) - (h)), w, (yWeight) * ((h) - (UI_BORDER)),     xw, (yw) + (1 - (yWeight)) * (hw), ww, (yWeight) * (hw)

#define BORDERSPLIT_L(xWeight, location) \
    BORDERSPLIT_L_INTERNAL(xWeight, location)
#define BORDERSPLIT_L_INTERNAL(xWeight,     x, y, w, h, xw, yw, ww, hw) \
    x, y, (xWeight) * ((w) - (UI_BORDER)), h,     xw, yw, (xWeight) * (ww), hw

#define BORDERSPLIT_R(xWeight, location) \
    BORDERSPLIT_R_INTERNAL(xWeight, location)
#define BORDERSPLIT_R_INTERNAL(xWeight,     x, y, w, h, xw, yw, ww, hw) \
    (x) + (w) + (xWeight) * ((UI_BORDER) - (w)), y, (xWeight) * ((w) - (UI_BORDER)), h,     (xw) + (1 - (xWeight)) * (ww), yw, (xWeight) * (ww), hw

/**
 * SPLIT_DOUBLE_* macros act like performing two separate split operations: One on each side. The difference
 * here is that the weight specifies the overall weight, so an xWeight/yWeight of 0.5 specifies an area half the width/height.
 *
 * SPLIT_DOUBLE_X, SPLIT_DOUBLE_Y, SPLIT_DOUBLE_XY
 */
#define SPLIT_DOUBLE_X(xWeight, location) \
    SPLIT_DOUBLE_X_INTERNAL(xWeight, location)
#define SPLIT_DOUBLE_X_INTERNAL(xWeight,     x, y, w, h, xw, yw, ww, hw) \
    (x) + 0.5f * (1.0f - (xWeight)) * (w), y, (xWeight) * (w), h,     (xw) + 0.5f * (1.0f - (xWeight)) * (ww), yw, (xWeight) * (ww), hw

#define SPLIT_DOUBLE_Y(yWeight, location) \
    SPLIT_DOUBLE_Y_INTERNAL(yWeight, location)
#define SPLIT_DOUBLE_Y_INTERNAL(yWeight,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + 0.5f * (1.0f - (yWeight)) * (h), w, (yWeight) * (h),     xw, (yw) + 0.5f * (1.0f - (yWeight)) * (hw), ww, (yWeight) * (hw)

#define SPLIT_DOUBLE_XY(xWeight, yWeight, location) \
    SPLIT_DOUBLE_XY_INTERNAL(xWeight, yWeight, location)
#define SPLIT_DOUBLE_XY_INTERNAL(xWeight, yWeight,     x, y, w, h, xw, yw, ww, hw) \
    (x) + 0.5f * (1.0f - (xWeight)) * (w), (y) + 0.5f * (1.0f - (yWeight)) * (h), (xWeight) * (w), (yWeight) * (h),    (xw) + 0.5f * (1.0f - (xWeight)) * (ww), (yw) + 0.5f * (1.0f - (yWeight)) * (hw), (xWeight) * (ww), (yWeight) * (hw)

// ==================================== Table macros =================================================

/**
 * TABLE_*, SCALED_TABLE_*, BORDERTABLE_* and SCALED_BORDERTABLE_* macros provide table
 * functionality, allowing the area to be split into columns and rows.
 * Regular variants use column/row IDs, and a total count of columns/rows. This results in
 * columns/rows of equal size.
 * Scaled variants accept column/row position and size as ratios, along with a total value
 * of all ratios. This allows columns/rows of variable size. Variable sized columns/rows are
 * made significantly more convenient with the defined table macros, see the section below.
 */

/**
 * TABLE_* macros perform an equal sized column/row split of the area, with no borders between the cells.
 * Columns and rows are specified by ID and total count.
 *
 * TABLE_X, TABLE_Y, TABLE_XY
 */
#define TABLE_X(columnId, columnCount, location) \
    TABLE_X_INTERNAL(columnId, columnCount, location)
#define TABLE_X_INTERNAL(columnId, columnCount,     x, y, w, h, xw, yw, ww, hw) \
   (x) + ((columnId) / (GLfloat)(columnCount)) * (w), y, (w) / (GLfloat)(columnCount), h,      (xw) + ((columnId) / (GLfloat)(columnCount)) * (ww), yw, (ww) / (GLfloat)(columnCount), hw

#define TABLE_Y(rowId, rowCount, location) \
    TABLE_Y_INTERNAL(rowId, rowCount, location)
#define TABLE_Y_INTERNAL(rowId, rowCount,     x, y, w, h, xw, yw, ww, hw) \
    x, (y) + ((rowId) / (GLfloat)(rowCount)) * (h), w, (h) / (GLfloat)(rowCount),      xw, (yw) + ((rowId) / (GLfloat)(rowCount)) * (hw), ww, (hw) / (GLfloat)(rowCount)

#define TABLE_XY(columnId, rowId, columnCount, rowCount, location) \
    TABLE_XY_INTERNAL(columnId, rowId, columnCount, rowCount, location)
#define TABLE_XY_INTERNAL(columnId, rowId, columnCount, rowCount,     x, y, w, h, xw, yw, ww, hw) \
    (x) + ((columnId) / (GLfloat)(columnCount)) * (w),                                            \
    (y) + ((rowId) / (GLfloat)(rowCount)) * (h),                                                  \
    (w) / (GLfloat)(columnCount),                                                                 \
    (h) / (GLfloat)(rowCount),                                                                    \
    (xw) + ((columnId) / (GLfloat)(columnCount)) * (ww),                                          \
    (yw) + ((rowId) / (GLfloat)(rowCount)) * (hw),                                                \
    (ww) / (GLfloat)(columnCount),                                                                \
    (hw) / (GLfloat)(rowCount)

/**
 * SCALED_TABLE_* macros perform a variable sized column/row split of the area, with no borders between the cells.
 * Columns and rows are specified by ratios: position, width and total.
 *
 * SCALED_TABLE_X, SCALED_TABLE_Y, SCALED_TABLE_XY
 */
#define SCALED_TABLE_X(xPos, width, xTotal, location) \
    SCALED_TABLE_X_INTERNAL(xPos, width, xTotal, location)
#define SCALED_TABLE_X_INTERNAL(xPos, width, xTotal,     x, y, w, h, xw, yw, ww, hw) \
    (x) + ((xPos) / (GLfloat)(xTotal)) * (w),                                        \
    y,                                                                               \
    (w) * ((width) / (GLfloat)(xTotal)),                                             \
    h,                                                                               \
    (xw) + ((xPos) / (GLfloat)(xTotal)) * (ww),                                      \
    yw,                                                                              \
    (ww) * ((width) / (GLfloat)(xTotal)),                                            \
    hw

#define SCALED_TABLE_Y(yPos, height, yTotal, location) \
    SCALED_TABLE_Y_INTERNAL(yPos, height, yTotal, location)
#define SCALED_TABLE_Y_INTERNAL(yPos, height, yTotal,     x, y, w, h, xw, yw, ww, hw) \
    x,                                                                                \
    (y) + ((yPos) / (GLfloat)(yTotal)) * (h),                                         \
    w,                                                                                \
    (h) * ((height) / (GLfloat)(yTotal)),                                             \
    xw,                                                                               \
    (yw) + ((yPos) / (GLfloat)(yTotal)) * (hw),                                       \
    ww,                                                                               \
    (hw) * ((height) / (GLfloat)(yTotal))

#define SCALED_TABLE_XY(xPos, yPos, width, height, xTotal, yTotal, location) \
    SCALED_TABLE_XY_INTERNAL(xPos, yPos, width, height, xTotal, yTotal, location)
#define SCALED_TABLE_XY_INTERNAL(xPos, yPos, width, height, xTotal, yTotal,     x, y, w, h, xw, yw, ww, hw) \
    (x) + ((xPos) / (GLfloat)(xTotal)) * (w),                                                               \
    (y) + ((yPos) / (GLfloat)(yTotal)) * (h),                                                               \
    (w) * ((width) / (GLfloat)(xTotal)),                                                                    \
    (h) * ((height) / (GLfloat)(yTotal)),                                                                   \
    (xw) + ((xPos) / (GLfloat)(xTotal)) * (ww),                                                             \
    (yw) + ((yPos) / (GLfloat)(yTotal)) * (hw),                                                             \
    (ww) * ((width) / (GLfloat)(xTotal)),                                                                   \
    (hw) * ((height) / (GLfloat)(yTotal))

/**
 * BORDERTABLE_* macros perform an equal sized column/row split of the area, while placing a standard size border between each cell.
 * Columns and rows are specified by ID and total count.
 *
 * BORDERTABLE_X, BORDERTABLE_Y, BORDERTABLE_XY
 */
#define BORDERTABLE_X(columnId, columnCount, location) \
    BORDERTABLE_X_INTERNAL(columnId, columnCount, location)
#define BORDERTABLE_X_INTERNAL(columnId, columnCount,     x, y, w, h, xw, yw, ww, hw) \
    (x) + ((columnId) / (GLfloat)(columnCount)) * ((w) + (UI_BORDER)),                \
    y,                                                                                \
    ((w) - ((columnCount) - 1.0f) * (UI_BORDER)) / (GLfloat)(columnCount),            \
    h,                                                                                \
    (xw) + ((columnId) / (GLfloat)(columnCount)) * (ww),                              \
    yw,                                                                               \
    (ww) / (columnCount),                                                             \
    hw

#define BORDERTABLE_Y(rowId, rowCount, location) \
    BORDERTABLE_Y_INTERNAL(rowId, rowCount, location)
#define BORDERTABLE_Y_INTERNAL(rowId, rowCount,     x, y, w, h, xw, yw, ww, hw)            \
    x,                                                                                     \
    (y) + ((rowId) / (GLfloat)(rowCount)) * ((h) + (UI_BORDER)),                           \
    w,                                                                                     \
    ((h) - ((rowCount) - 1.0f) * (UI_BORDER)) / (GLfloat)(rowCount),                       \
    xw,                                                                                    \
    (yw) + ((rowId) / (GLfloat)(rowCount)) * (hw),                                         \
    ww,                                                                                    \
    (hw) / (rowCount)

#define BORDERTABLE_XY(columnId, rowId, columnCount, rowCount, location) \
    BORDERTABLE_XY_INTERNAL(columnId, rowId, columnCount, rowCount, location)
#define BORDERTABLE_XY_INTERNAL(columnId, rowId, columnCount, rowCount,     x, y, w, h, xw, yw, ww, hw) \
    (x) + ((columnId) / (GLfloat)(columnCount)) * ((w) + (UI_BORDER)),                                  \
    (y) + ((rowId) / (GLfloat)(rowCount)) * ((h) + (UI_BORDER)),                                        \
    ((w) - ((columnCount) - 1.0f) * (UI_BORDER)) / (GLfloat)(columnCount),                              \
    ((h) - ((rowCount) - 1.0f) * (UI_BORDER)) / (GLfloat)(rowCount),                                    \
    (xw) + ((columnId) / (GLfloat)(columnCount)) * (ww),                                                \
    (yw) + ((rowId) / (GLfloat)(rowCount)) * (hw),                                                      \
    (ww) / (columnCount),                                                                               \
    (hw) / (rowCount)

/**
 * SCALED_BORDERTABLE_* macros perform a variable sized column/row split of the area, while placing a standard size border between each cell.
 * Columns and rows are specified by ratios: position, width and total.
 * 
 * SCALED_BORDERTABLE_X, SCALED_BORDERTABLE_Y, SCALED_BORDERTABLE_XY
 */
#define SCALED_BORDERTABLE_X(xPos, width, xTotal, location) \
    SCALED_BORDERTABLE_X_INTERNAL(xPos, width, xTotal, location)
#define SCALED_BORDERTABLE_X_INTERNAL(xPos, width, xTotal,     x, y, w, h, xw, yw, ww, hw) \
    (x) + ((xPos) / (GLfloat)(xTotal)) * ((w) + (UI_BORDER)),                              \
    y,                                                                                     \
    ((width) / (GLfloat)(xTotal)) * ((w) + (UI_BORDER)) - (UI_BORDER),                     \
    h,                                                                                     \
    (xw) + ((xPos) / (GLfloat)(xTotal)) * (ww),                                            \
    yw,                                                                                    \
    (ww) * ((width) / (GLfloat)(xTotal)),                                                  \
    hw

#define SCALED_BORDERTABLE_Y(yPos, height, yTotal, location) \
    SCALED_BORDERTABLE_Y_INTERNAL(yPos, height, yTotal, location)
#define SCALED_BORDERTABLE_Y_INTERNAL(yPos, height, yTotal,     x, y, w, h, xw, yw, ww, hw) \
    x,                                                                                      \
    (y) + ((yPos) / (GLfloat)(yTotal)) * ((h) + (UI_BORDER)),                               \
    w,                                                                                      \
    ((height) / (GLfloat)(yTotal)) * ((h) + (UI_BORDER)) - (UI_BORDER),                     \
    xw,                                                                                     \
    (yw) + ((yPos) / (GLfloat)(yTotal)) * (hw),                                             \
    ww,                                                                                     \
    (hw) * ((height) / (GLfloat)(yTotal))

#define SCALED_BORDERTABLE_XY(xPos, yPos, width, height, xTotal, yTotal, location) \
    SCALED_BORDERTABLE_XY_INTERNAL(xPos, yPos, width, height, xTotal, yTotal, location)
#define SCALED_BORDERTABLE_XY_INTERNAL(xPos, yPos, width, height, xTotal, yTotal,     x, y, w, h, xw, yw, ww, hw) \
    (x) + ((xPos) / (GLfloat)(xTotal)) * ((w) + (UI_BORDER)),                                                     \
    (y) + ((yPos) / (GLfloat)(yTotal)) * ((h) + (UI_BORDER)),                                                     \
    ((width) / (GLfloat)(xTotal)) * ((w) + (UI_BORDER)) - (UI_BORDER),                                            \
    ((height) / (GLfloat)(yTotal)) * ((h) + (UI_BORDER)) - (UI_BORDER),                                           \
    (xw) + ((xPos) / (GLfloat)(xTotal)) * (ww),                                                                   \
    (yw) + ((yPos) / (GLfloat)(yTotal)) * (hw),                                                                   \
    (ww) * ((width) / (GLfloat)(xTotal)),                                                                         \
    (hw) * ((height) / (GLfloat)(yTotal))

// ================================ Defined table macros =============================================

/**
 * DEFINE_TABLE, DEFINED_TABLE_* and DEFINED_BORDERTABLE_* macros provide more flexible and convenient table
 * functionality, allowing the area to be conveniently split into variable size columns and rows. The sizes of
 * the columns and rows are defined beforehand, and all sizing is calculated internally, making this very easy
 * to use. Defined tables also make it possible for cells to span multiple columns and rows. As with regular
 * table macros, the defined table macros have bordered and non-bordered variants. Internally the defined table
 * macros use variadic templated classes: rascUI::DefinedTableArr and rascUI::DefinedTable, these are below.
 * 
 * The example below uses DEFINE_TABLE to define 3 columns of different widths, within a GEN_FILL. The DEFINED_TABLE_X
 * layout macro is then used for the layout. Note that when using the _XY table macros for a table with columns *and*
 * rows, two DEFINE_TABLEs are needed: one defining column sizes, and another defining row sizes.
 * 
 * DEFINE_TABLE(ExampleTable, 1, 2, 3)
 * rascUI::FrontPanel column0(rascUI::Location(DEFINED_TABLE_X(ExampleTable, 0, GEN_FILL))),
 *                    column1(rascUI::Location(DEFINED_TABLE_X(ExampleTable, 1, GEN_FILL))),
 *                    column2(rascUI::Location(DEFINED_TABLE_X(ExampleTable, 2, GEN_FILL)));
 */

/**
 * DEFINE_TABLE defines either the column or row sizes of a table, and attaches a name to it. The name is then used
 * by the defined table layout macros, for identifying the sizes and positioning of the intended cell. Sizes must be
 * positive integers.
 * 
 * Since DEFINE_TABLE internally is a "using" directive, specifying the column/row sizes as variadic template parameters,
 * it can be placed almost anywhere - e.g: in a function, or in a header file if multiple source files need table information.
 * 
 * Note that after defining the table, the column/row count can be accessed with the "count" member. For example, after
 * running DEFINE_TABLE(SomeTable, 1, 2, 3), the column/row count (in this case 3) can be accessed as "SomeTable::count".
 */
#define DEFINE_TABLE(name, ...) \
    using name = rascUI::DefinedTable<__VA_ARGS__>;

/**
 * DEFINED_TABLE_* macros perform a variable sized column/row split of the area, without a border in between cells,
 * according to a table defined with DEFINE_TABLE. Column and row IDs must be specified with positive integers.
 * Optionally, colspan and rowspan values can be added. Like in html, these represent the number of columns or rows
 * which the cell takes up, with the cell's position being at the top-left.
 */
#define DEFINED_TABLE_X(columnTableName, columnId, location) \
    DEFINED_TABLE_X_INTERNAL(columnTableName, columnId, location)
#define DEFINED_TABLE_X_INTERNAL(columnTableName, columnId,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_X_INTERNAL(columnTableName::positions.values[columnId], columnTableName::sizes.values[columnId], columnTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_TABLE_Y(rowTableName, rowId, location) \
    DEFINED_TABLE_Y_INTERNAL(rowTableName, rowId, location)
#define DEFINED_TABLE_Y_INTERNAL(rowTableName, rowId,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_Y_INTERNAL(rowTableName::positions.values[rowId], rowTableName::sizes.values[rowId], rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_TABLE_XY(columnTableName, rowTableName, columnId, rowId, location) \
    DEFINED_TABLE_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, location)
#define DEFINED_TABLE_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.values[columnId], rowTableName::sizes.values[rowId], columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_TABLE_COLSPAN_X(columnTableName, columnId, colspan, location) \
    DEFINED_TABLE_COLSPAN_X_INTERNAL(columnTableName, columnId, colspan, location)
#define DEFINED_TABLE_COLSPAN_X_INTERNAL(columnTableName, columnId, colspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_X_INTERNAL(columnTableName::positions.values[columnId], columnTableName::sizes.getTotal(columnId, colspan), columnTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_TABLE_ROWSPAN_Y(rowTableName, rowId, rowspan, location) \
    DEFINED_TABLE_ROWSPAN_Y_INTERNAL(rowTableName, rowId, rowspan, location)
#define DEFINED_TABLE_ROWSPAN_Y_INTERNAL(rowTableName, rowId, rowspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_Y_INTERNAL(rowTableName::positions.values[rowId], rowTableName::sizes.getTotal(rowId, rowspan), rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_TABLE_COLSPAN_XY(columnTableName, rowTableName, columnId, rowId, colspan, location) \
    DEFINED_TABLE_COLSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan, location)
#define DEFINED_TABLE_COLSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.getTotal(columnId, colspan), rowTableName::sizes.values[rowId], columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_TABLE_ROWSPAN_XY(columnTableName, rowTableName, columnId, rowId, rowspan, location) \
    DEFINED_TABLE_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, rowspan, location)
#define DEFINED_TABLE_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, rowspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.values[columnId], rowTableName::sizes.getTotal(rowId, rowspan), columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_TABLE_COLSPAN_ROWSPAN_XY(columnTableName, rowTableName, columnId, rowId, colspan, rowspan, location) \
    DEFINED_TABLE_COLSPAN_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan, rowspan, location)
#define DEFINED_TABLE_COLSPAN_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan, rowspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_TABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.getTotal(columnId, colspan), rowTableName::sizes.getTotal(rowId, rowspan), columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

/**
 * DEFINED_BORDERTABLE_* macros perform a variable sized column/row split of the area, with a border in between cells,
 * according to a table defined with DEFINE_TABLE. Column and row IDs must be specified with positive integers.
 * Optionally, colspan and rowspan values can be added. Like in html, these represent the number of columns or rows
 * which the cell takes up, with the cell's position being at the top-left.
 */
#define DEFINED_BORDERTABLE_X(columnTableName, columnId, location) \
    DEFINED_BORDERTABLE_X_INTERNAL(columnTableName, columnId, location)
#define DEFINED_BORDERTABLE_X_INTERNAL(columnTableName, columnId,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_X_INTERNAL(columnTableName::positions.values[columnId], columnTableName::sizes.values[columnId], columnTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_BORDERTABLE_Y(rowTableName, rowId, location) \
    DEFINED_BORDERTABLE_Y_INTERNAL(rowTableName, rowId, location)
#define DEFINED_BORDERTABLE_Y_INTERNAL(rowTableName, rowId,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_Y_INTERNAL(rowTableName::positions.values[rowId], rowTableName::sizes.values[rowId], rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_BORDERTABLE_XY(columnTableName, rowTableName, columnId, rowId, location) \
    DEFINED_BORDERTABLE_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, location)
#define DEFINED_BORDERTABLE_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.values[columnId], rowTableName::sizes.values[rowId], columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_BORDERTABLE_COLSPAN_X(columnTableName, columnId, colspan, location) \
    DEFINED_BORDERTABLE_COLSPAN_X_INTERNAL(columnTableName, columnId, colspan, location)
#define DEFINED_BORDERTABLE_COLSPAN_X_INTERNAL(columnTableName, columnId, colspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_X_INTERNAL(columnTableName::positions.values[columnId], columnTableName::sizes.getTotal(columnId, colspan), columnTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_BORDERTABLE_ROWSPAN_Y(rowTableName, rowId, rowspan, location) \
    DEFINED_BORDERTABLE_ROWSPAN_Y_INTERNAL(rowTableName, rowId, rowspan, location)
#define DEFINED_BORDERTABLE_ROWSPAN_Y_INTERNAL(rowTableName, rowId, rowspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_Y_INTERNAL(rowTableName::positions.values[rowId], rowTableName::sizes.getTotal(rowId, rowspan), rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_BORDERTABLE_COLSPAN_XY(columnTableName, rowTableName, columnId, rowId, colspan, location) \
    DEFINED_BORDERTABLE_COLSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan, location)
#define DEFINED_BORDERTABLE_COLSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.getTotal(columnId, colspan), rowTableName::sizes.values[rowId], columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_BORDERTABLE_ROWSPAN_XY(columnTableName, rowTableName, columnId, rowId, rowspan, location) \
    DEFINED_BORDERTABLE_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, rowspan, location)
#define DEFINED_BORDERTABLE_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, rowspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.values[columnId], rowTableName::sizes.getTotal(rowId, rowspan), columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

#define DEFINED_BORDERTABLE_COLSPAN_ROWSPAN_XY(columnTableName, rowTableName, columnId, rowId, colspan, rowspan, location) \
    DEFINED_BORDERTABLE_COLSPAN_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan, rowspan, location)
#define DEFINED_BORDERTABLE_COLSPAN_ROWSPAN_XY_INTERNAL(columnTableName, rowTableName, columnId, rowId, colspan, rowspan,     x, y, w, h, xw, yw, ww, hw) \
    SCALED_BORDERTABLE_XY_INTERNAL(columnTableName::positions.values[columnId], rowTableName::positions.values[rowId], columnTableName::sizes.getTotal(columnId, colspan), rowTableName::sizes.getTotal(rowId, rowspan), columnTableName::total, rowTableName::total,     x, y, w, h, xw, yw, ww, hw)

namespace rascUI {
    // Helper array class for DefinedTable.
    template<unsigned int Len>
    class DefinedTableArr {
    public:
        unsigned int values[Len];
        // Converts the array to a cumulative array.
        constexpr inline DefinedTableArr<Len> getCumulative(void) const {
            DefinedTableArr<Len> cumulative = {{}};
            unsigned int total = 0;
            for (unsigned int i = 0; i < Len; i++) {
                cumulative.values[i] = total;
                total += values[i];
            }
            return cumulative;
        }
        // Gets the sum of the values in the range.
        constexpr inline unsigned int getTotal(unsigned int pos = 0, unsigned int length = Len) const {
            unsigned int total = 0,
                         end = pos + length;
            for (unsigned int i = pos; i < end; i++) {
                total += values[i];
            }
            return total;
        }
    };
    // Container for table size data, used in DEFINE_TABLE and defined table layout macros.
    template<unsigned int ... Sizes>
    class DefinedTable {
    public:
        constexpr static unsigned int count = sizeof...(Sizes);                               // Count of columns/rows.
        constexpr static DefinedTableArr<sizeof...(Sizes)> sizes = {{ Sizes... }};            // Column/row sizes.
        constexpr static DefinedTableArr<sizeof...(Sizes)> positions = sizes.getCumulative(); // Column/row x/y positions.
        constexpr static unsigned int total = sizes.getTotal();                               // Total size across all columns/rows.
    };
    // Definitions for DefinedTable's constexpr static members, for correct linking.
    template<unsigned int ... Sizes> constexpr unsigned int DefinedTable<Sizes...>::count;
    template<unsigned int ... Sizes> constexpr DefinedTableArr<sizeof...(Sizes)> DefinedTable<Sizes...>::sizes;
    template<unsigned int ... Sizes> constexpr DefinedTableArr<sizeof...(Sizes)> DefinedTable<Sizes...>::positions;
    template<unsigned int ... Sizes> constexpr unsigned int DefinedTable<Sizes...>::total;
}

// ================================= Counting macros =================================================

/**
 * COUNT_* macros allow us to easily express sizes in terms of borders and normal component sizes.
 */

/**
 * COUNT_OUTBORDER_* macros calculate the size of the specified number of components, assuming that
 * there is a border BETWEEN each component, and a border at both sides on the outside.
 *
 * like this:    border [Component] border [Component] border [Component] border
 *
 * COUNT_OUTBORDER_X, COUNT_OUTBORDER_Y
 */
#define COUNT_OUTBORDER_X(componentCount) \
    ((componentCount) * ((UI_BWIDTH) + (UI_BORDER)) + (UI_BORDER))

#define COUNT_OUTBORDER_Y(componentCount) \
    ((componentCount) * ((UI_BHEIGHT) + (UI_BORDER)) + (UI_BORDER))

/**
 * COUNT_ONEBORDER_* macros calculate the size of the specified number of components, assuming that
 * there is a border BETWEEN each component, and a border on ONLY ONE SIDE, on the outside.
 *
 * like this:    border [Component] border [Component] border [Component]
 *
 * COUNT_ONEBORDER_X, COUNT_ONEBORDER_Y
 */
#define COUNT_ONEBORDER_X(componentCount) \
    ((componentCount) * ((UI_BWIDTH) + (UI_BORDER)))

#define COUNT_ONEBORDER_Y(componentCount) \
    ((componentCount) * ((UI_BHEIGHT) + (UI_BORDER)))

/**
 * COUNT_INBORDER_* macros calculate the size of the specified number of components, assuming that
 * there is a border BETWEEN each component, but NO BORDERS on the outside.
 *
 * like this:    [Component] border [Component] border [Component]
 *
 * COUNT_INBORDER_X, COUNT_INBORDER_Y
 */
#define COUNT_INBORDER_X(componentCount) \
    ((componentCount) * ((UI_BWIDTH) + (UI_BORDER)) - (UI_BORDER))

#define COUNT_INBORDER_Y(componentCount) \
    ((componentCount) * ((UI_BHEIGHT) + (UI_BORDER)) - (UI_BORDER))

// ===================================================================================================

#endif
