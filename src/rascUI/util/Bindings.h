/*
 * Bindings.h
 *
 *  Created on: 10 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_BINDINGS_H_
#define RASCUI_UTIL_BINDINGS_H_

#include <unordered_map>

namespace rascUI {

    /**
     * This enum represents keyboard actions for component navigation.
     */
    enum class NavigationAction {

        // ----- General navigation -----
        nextComponent,
        previousComponent,
        nextContainer,
        previousContainer,
        enter,

        // ------ Selection locking actions -------
        lockSelection,     // This action can be returned from Component::getRequiredKeyboardActionPress and
                           // Component::getRequiredKeyboardActionRelease to lock selection to this component.

        unlockSelection,   // This action can be returned from Component::onSelectionLockedKeyPress and
                           // onSelectionLockedKeyRelease to manually unlock selection from the component.

        // --------------- Misc -------------------
        noAction, // This represents that a key is not bound to anything. No key should be bound to this.

        // ----------------------------------------
    };

    /**
     * This enum represents keyboard actions for editing text.
     */
    enum class TextEditAction {
        // ------------ Cursor movement -----------
        moveCursorPrevious,
        moveCursorNext,
        moveCursorFirst,
        moveCursorLast,
        deleteCharacterPrevious,
        deleteCharacterNext,

        // --------------- Misc -------------------
        noAction, // This represents that a key is not bound to anything. No key should be bound to this.

        // ----------------------------------------
    };


    /**
     * This class contains all keyboard and mouse bindings required for the UI system.
     * An instance of this class is required when creating the TopLevelContainer.
     */
    class Bindings {
    public:
        
        /**
         * Default mouse and keyboard bindings for use with OpenGL (GLUT).
         *
         * For mouse bindings:
         *   This uses mouse buttons 0, 3 and 4 for mouseButton, mouseScrollUpButton and mouseScrollDownButton respectively.
         *
         * For key bindings:
         *   This uses arrow keys and enter for component navigation, with tab and grave (`) for container navigation.
         */
        const static Bindings defaultOpenGL;

        /**
         * Default mouse and keyboard bindings for use with X11 (XCB).
         *
         * For mouse bindings:
         *   This uses mouse buttons 1, 4 and 5 for mouseButton, mouseScrollUpButton and mouseScrollDownButton respectively.
         *
         * For key bindings:
         *   This uses arrow keys and enter for component navigation, with tab and shift+tab, (and grave (`) for
         *   consistency with the OpenGL bindings), for container navigation.
         */
        const static Bindings defaultX11;
        
        /**
         * Default mouse and keyboard bindings for use with the Windows API (WinAPI).
         *
         * For mouse bindings:
         *   This uses mouse buttons 0, 3 and 4 for mouseButton, mouseScrollUpButton and mouseScrollDownButton respectively.
         *   (These are not defined by windows, but selected for consistency with OpenGL/GLUT).
         *
         * For key bindings:
         *   This uses arrow keys and enter for component navigation, with tab and shift+tab, (and grave (`) for
         *   consistency with the OpenGL bindings), for container navigation.
         */
        const static Bindings defaultWin;

        // -------------------------- Mouse button bindings ----------------------

        /**
         * This represents the mouse button that our components should use for their main click events.
         * This is also the mouse button given as fake click events within the keyboard interaction.
         *
         * Initially this is set to a dummy value, (-1). Before any mouse events can be used, call either
         * setDefaultMouseBindingsOpenGL or setDefaultMouseBindingsX11, or set mouseButton, mouseScrollUpButton
         * and mouseScrollDownButton manually.
         */
        int mouseButton;

        /**
         * This represents the mouse button that our components, (namely the ScrollPane), should use for scrolling
         * up. This can be rebound easily, but really shouldn't be.
         *
         * Initially this is set to a dummy value, (-1). Before any mouse events can be used, call either
         * setDefaultMouseBindingsOpenGL or setDefaultMouseBindingsX11, or set mouseButton, mouseScrollUpButton
         * and mouseScrollDownButton manually.
         */
        int mouseScrollUpButton;

        /**
         * This represents the mouse button that our components, (namely the ScrollPane), should use for scrolling
         * down. This can be rebound easily, but really shouldn't be.
         *
         * Initially this is set to a dummy value, (-1). Before any mouse events can be used, call either
         * setDefaultMouseBindingsOpenGL or setDefaultMouseBindingsX11, or set mouseButton, mouseScrollUpButton
         * and mouseScrollDownButton manually.
         */
        int mouseScrollDownButton;

        // ---------------------------- Keyboard bindings -----------------------

        /**
         * This map stores the key bindings for keyboard integration. This can be changed easily by the user.
         * Default values for these can be set using setDefaultKeyBindingsOpenGL and setDefaultKeyBindingsX11.
         *
         * These must be modified from synchronised context, such as during an event or function queue (beforeEvent/afterEvent),
         * or more conveniently, immediately after the TopLevelContainer is created.
         */
        std::unordered_map<int, NavigationAction> navigationNormal,
                                                  navigationSpecial;

        std::unordered_map<int, TextEditAction> textEditNormal,
                                                textEditSpecial;

        // ----------------------------------------------------------------------

        /**
         * The default constructor for Bindings. This should be used if you wish to create a custom
         * empty Bindings object, to populate with your own bindings.
         */
        Bindings(void);
        
        /**
         * This returns the relevant navigation action for the specified key and special status.
         * If none exists, NavigationAction::noAction is returned.
         */
        NavigationAction getNavigationAction(int key, bool special) const;

        /**
         * This returns the text edit action for the specified key and special status.
         * If none exists, TextEditAction::noAction is returned.
         */
        TextEditAction getTextEditAction(int key, bool special) const;
    };

}

#endif
