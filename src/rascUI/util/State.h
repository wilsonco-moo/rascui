/*
 * State.h
 *
 *  Created on: 15 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_STATE_H_
#define RASCUI_UTIL_STATE_H_


namespace rascUI {

    /**
     * This enum represents the state of a component. The state of a component serves two purposes:
     *  > Controls how the drawing process of the component will take place. It is up to the Theme to decide what (if any)
     *    differences there are between how the component is to be drawn depending upon which state is in use.
     *  > By using the invisible state, the component will not be drawn at all. If this is a container, the component will also not draw
     *    any of it's child components. This allows the easy hiding of ui elements, with very little overhead.
     */
    enum class State {

        /**
         * This is the normal (default) state. This should cause the Theme implementation to draw the component with no special
         * highlighting etc. This is how the component should be *most* of the time.
         */
        normal,

        /**
         * This is the state that is used when the user mouses over the component. This should cause the Theme implementation to
         * draw the component with some highlighting, to give some tactile feedback that the user has mouse-overed a button (or similar).
         */
        mouseOver,

        /**
         * This is the state that is used between the user pressing down the mouse, and the user releasing the mouse. This should cause
         * the Theme implementation to highlight the component differently to mouseOver, preferably with inverted shading to make it
         * look like the button has been pressed.
         */
        mousePress,

        /**
         * This state is (currently) used primarily for toggle components. This represents a selected state, where the user is not currently mousing
         * over the component. This is not used for ordinary mouse responsive components, rather only for toggle components.
         *
         * When the user mouses over a selected toggle component, the mousePress state is used instead of this.
         *
         * This means that it is reasonable for a Theme implementation to draw the same thing for selected and mousePress, if feedback
         * from mousing over a selected toggle components is not important.
         */
        selected,

        /**
         * TODO: Implement logic behind inactive state.
         *
         * This state is used to deactivate a button. If this state is active, the button WILL STILL RECEIVE MOUSE ENTER, PRESS, RELEASE
         * AND LEAVE EVENTS, but will NOT RECEIVE MOUSE CLICK EVENTS.
         *
         * This should cause the Theme implementation to draw the button more greyed-out, or more transparent, to make obvious to
         * the user that this option is not available.
         */
        inactive,

        /**
         * This state causes a component not to be drawn at all. This is not controlled by the Theme implementation, instead this is
         * controlled by the Container class. If this state is active, the Container won't even call the Component's onDraw method. In the
         * case of a container, this state causes it not to draw any of it's child components.
         */
        invisible,
    };


}

#endif
