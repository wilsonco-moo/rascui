/*
 * OffVal.h
 *
 *  Created on: 24 May 2020
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_OFFVAL_H_
#define RASCUI_UTIL_OFFVAL_H_

#include <cmath>

namespace rascUI {

    /**
     * OffVal provides the ability to conveniently work with numbers, represented as
     * an integer with floating point offset (0 <= offset < 1). This allows extremely
     * large noninteger numbers to be represented, and have arithmetic performed on them,
     * without loss of precision due to size.
     *
     * NOTE: The provided BASE type must be a signed (or unsigned) integer, and the
     *       provided OFF type must be a floating point type (e.g: float, double, GLfloat, etc).
     */
    template<typename BASE, typename OFF>
    class OffVal {
    private:
        BASE base;
        OFF offset;

    public:
        /**
         * Default constructor uses zero for both base and offset.
         */
        OffVal(void) :
            base(0),
            offset(0) {
        }

        /**
         * Creates an OffVal from a base value and offset.
         * NOTE: 0 <= offset < 1
         *       This must be true, or OffVal will give wrong calculations.
         */
        OffVal(BASE base, OFF offset) :
            base(base),
            offset(offset) {
        }

        /**
         * Creates an OffVal such that the sum of its base and
         * offset equal the provided value.
         * For example:
         * OffVal<int, float> ofv(value);
         * if (ofv.base + ofv.offset == value) {
         *     // always true
         * }
         */
        OffVal(OFF value) :
            base((BASE)std::floor(value)),
            offset(value - base) {
        }

        // ============ Accessor methods =========

        /**
         * Allows access to the base value.
         */
        inline BASE getBase(void) const {
            return base;
        }

        /**
         * Allows access to the offset value.
         */
        inline OFF getOffset(void) const {
            return offset;
        }

        // ============ Comparison (with other OffVal instances) ===========

        bool operator == (const OffVal<BASE, OFF> & other) const {
            return base == other.base && offset == other.offset;
        }

        bool operator != (const OffVal<BASE, OFF> & other) const {
            return base != other.base || offset != other.offset;
        }

        bool operator < (const OffVal<BASE, OFF> & other) const {
            return base < other.base || (base == other.base && offset < other.offset);
        }

        bool operator <= (const OffVal<BASE, OFF> & other) const {
            return base < other.base || (base == other.base && offset <= other.offset);
        }

        bool operator > (const OffVal<BASE, OFF> & other) const {
            return base > other.base || (base == other.base && offset > other.offset);
        }

        bool operator >= (const OffVal<BASE, OFF> & other) const {
            return base > other.base || (base == other.base && offset >= other.offset);
        }

        // ============ Comparison (with base type) ===========

        bool operator == (BASE otherBase) const {
            return offset == 0 && base == otherBase;
        }

        bool operator != (BASE otherBase) const {
            return offset != 0 || base != otherBase;
        }

        bool operator < (BASE otherBase) const {
            return base < otherBase;
        }

        bool operator <= (BASE otherBase) const {
            return base < otherBase || (base == otherBase && offset == 0);
        }

        bool operator > (BASE otherBase) const {
            return base > otherBase || (base == otherBase && offset > 0);
        }

        bool operator >= (BASE otherBase) const {
            return base > otherBase || (base == otherBase && offset >= 0);
        }

        // ============ Basic arithmetic (with other OffVal instances) ===========

        OffVal<BASE, OFF> operator + (const OffVal<BASE, OFF> & other) const {
            OFF baseOff,
                newOff = std::modf(offset + other.offset, &baseOff);
            return OffVal<BASE, OFF>(
                base + other.base + (BASE)baseOff,
                newOff
            );
        }

        OffVal<BASE, OFF> & operator += (const OffVal<BASE, OFF> & other) {
            OFF baseOff;
            offset = std::modf(offset + other.offset, &baseOff);
            base += (other.base + (BASE)baseOff);
            return *this;
        }

        OffVal<BASE, OFF> operator - (const OffVal<BASE, OFF> & other) const {
            OFF offSub = offset - other.offset,
                offFloor = std::floor(offSub);
            return OffVal<BASE, OFF>(
                base - other.base + (BASE)offFloor,
                offSub - offFloor
            );
        }

        OffVal<BASE, OFF> & operator -= (const OffVal<BASE, OFF> & other) {
            OFF offSub = offset - other.offset,
                offFloor = std::floor(offSub);
            base -= (other.base - (BASE)offFloor);
            offset = offSub - offFloor;
            return *this;
        }

        // =========================== Other misc methods =======================

        /**
         * If we are below zero, sets us to be equivalent to OffVal<BASE,OFF>(0,0).
         * Otherwise, does nothing.
         */
        void zeroRound(void) {
            if (base < 0) {
                base = 0;
                offset = 0;
            }
        }

        /**
         * Subtracts the other from this, and ensures that the result is not negative.
         * This works when BASE is an unsigned type.
         * For unsigned types, this is generally equivalent to the following:
         *   offVal -= other;
         *   offVal.zeroRound();
         */
        void zeroRoundSubtract(const OffVal<BASE, OFF> & other) {
            if (other >= *this) {
                base = 0;
                offset = 0;
            } else {
                *this -= other;
            }
        }

        /**
         * Returns the "ceiling" base value.
         */
        BASE ceilBase(void) const {
            if (offset == 0) {
                return base;
            } else {
                return base + 1;
            }
        }

        /**
         * Returns the value of the OffVal, as a single value of OFF (floating point) type.
         * Note that this should be avoided if possible, as using it would remove all the precision
         * benefits of using this class in the first place.
         */
        OFF toOffType(void) const {
            return (OFF)base + offset;
        }

        /**
         * Returns the OffVal's value, as the specified type.
         */
        template<typename T>
        T toSingleValue(void) const {
            return ((T)base) + ((T)offset);
        }

        /**
         * An alternative to the constructor of OffVal - this allows the type
         * of the original value to be specified.
         */
        template<typename VALTYPE>
        static OffVal<BASE, OFF> fromSingleValue(VALTYPE value) {
            BASE base = (BASE)std::floor(value);
            return OffVal<BASE, OFF>(base, value - ((VALTYPE)base));
        }

        /**
         * Multiplies us by the provided value of floating point type,
         * in such a ways as to lose as little precision as possible.
         * Note that this floating point type must be able to store BASE,
         * or precision will be lost.
         */
        template<typename T>
        void multiplyByFloat(T floatVal) {
            T newBase = ((T)base) * floatVal,
              newOffset = ((T)offset) * floatVal;
            *this = fromSingleValue<T>(newBase);
            *this += fromSingleValue<T>(newOffset);
        }
    };
}

#endif
