/*
 * FunctionQueue.cpp
 *
 *  Created on: 13 Dec 2018
 *      Author: wilson
 */

#include "FunctionQueue.h"


#include <unordered_set>
#include <chrono>
#include <iostream>

// So we can send messages to the thread ID in windows, and write to the file
// descriptor in linux.
#ifdef _WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif

namespace rascUI {

    // ============================== CLASSES AND STUFF USED INTERNALLY =============================

    // ------------------------------------- Job class --------------------------------------------

    /**
     * This represents a single timed event, which may or not be repeating.
     */
    class FunctionQueue::Job {
    private:
        // The token assigned to this job.
        void * token;
        // The time (since epoch), which this job should start.
        std::chrono::milliseconds timeToStart;
        // The interval between executions of this job. This should be set to zero for a non-repeating job.
        std::chrono::milliseconds interval;
        // The actual std::function job.
        std::function<void(void)> function;
        // The iterator within the multiset of jobs. This can only be set after we are added, so must be mutable.
        mutable std::multiset<Job>::iterator iter;
    public:
        Job(void * token, std::chrono::milliseconds timeToStart, std::chrono::milliseconds interval, std::function<void(void)> function) :
            token(token),
            timeToStart(timeToStart),
            interval(interval),
            function(function) {
        }
        virtual ~Job(void) {
        }
        /**
         * Given the current time, this will calculate the number of milliseconds until execution of this job should be started.
         * 0 is returned if that time has already passed.
         */
        std::chrono::milliseconds getTimeUntilStart(std::chrono::milliseconds currentTime) const {
            if (timeToStart <= currentTime) {
                return std::chrono::milliseconds(0);
            } else {
                return timeToStart - currentTime;
            }
        }
        /**
         * Returns true if this job should be started NOW, (given the current time), and false if we should wait first.
         */
        bool canBeRunNow(std::chrono::milliseconds currentTime) const {
            return currentTime >= timeToStart;
        }
        /**
         * Generates a duplicate Job instance, with a start time a single interval away. This should be added to the
         * job queue to allow the job to repeat.
         */
        FunctionQueue::Job generateRepeatJob(void) const {
            return Job(token, timeToStart + interval, interval, function);
        }
        void * getToken(void) const {
            return token;
        }
        /**
         * Runs our internal job function.
         */
        void operator () (void) const {
            function();
        }
        /**
         * Returns true if this job should be repeated, and false otherwise. We should be repeated if we have an interval
         * greater than 0 milliseconds.
         */
        bool shouldRepeat(void) const {
            return interval > std::chrono::milliseconds(0);
        }
        void setIter(std::multiset<Job>::iterator iter) const {
            this->iter = iter;
        }
        std::multiset<Job>::iterator getIter(void) const {
            return iter;
        }
        inline bool operator == (const Job & other) const { return timeToStart == other.timeToStart; }
        inline bool operator != (const Job & other) const { return timeToStart != other.timeToStart; }
        inline bool operator <  (const Job & other) const { return timeToStart <  other.timeToStart; }
        inline bool operator >  (const Job & other) const { return timeToStart >  other.timeToStart; }
        inline bool operator <= (const Job & other) const { return timeToStart <= other.timeToStart; }
        inline bool operator >= (const Job & other) const { return timeToStart >= other.timeToStart; }
    };

    // ------------------------------------ Token class -------------------------------------------

    /**
     * This represents a token provided to the user, for adding jobs with. Removing this cancels all jobs assigned to this token.
     * The token is converted to a void pointer before providing it to the user.
     */
    class FunctionQueue::Token {
    private:
        // The function queue that we are related to, so that we don't need to keep a pointer to it to remove the token.
        FunctionQueue * functionQueue;
        // The jobs assigned to us, so that we can remove them from the waiting jobs when the token is removed.
        std::unordered_set<const Job *> jobs;
        // An iterator within the token list, so we can remove ourself easily.
        std::list<Token>::iterator iter;
    public:
        Token(FunctionQueue * functionQueue) :
            functionQueue(functionQueue) {
        }
        virtual ~Token(void) {
            // When we are destroyed, erase all waiting jobs assigned to us.
            for (const Job * job : jobs) {
                std::multiset<Job>::iterator iter = job->getIter();
                functionQueue->timedJobs.erase(iter);
            }
        }
        /**
         * This should be called whenever a job is added to the system which is related to this token.
         */
        void onAddJob(const Job * job) {
            jobs.insert(job);
        }
        /**
         * This should be called whenever a job related to us is removed from the system.
         */
        void onDeleteJob(const Job * job) {
            jobs.erase(job);
        }
        void setIter(std::list<Token>::iterator iter) {
            this->iter = iter;
        }
        std::list<Token>::iterator getIter(void) const {
            return iter;
        }
        FunctionQueue * getFunctionQueue(void) const {
            return functionQueue;
        }
    };

    // =========================== CLASSES AND STUFF USED EXTERNALLY ================================

    // -------------------------------- Function queue updater ------------------------------------

    FunctionQueue::FunctionQueueUpdater::FunctionQueueUpdater(FunctionQueue * queue) :
        queue(queue) {
    }

    FunctionQueue::FunctionQueueUpdater::~FunctionQueueUpdater(void) {
        if (queue != NULL) {
            // Update the queue when this class is destroyed.
            queue->update();
        }
    }

    // ==============================================================================================

    /**
     * This macro either sends a message to the windows thread ID, or writes a single byte to the file descriptor,
     * (as long as the thread ID isn't zero, or the file descriptor is not -1 respectively).
     * This should be run every time an event is added to this FunctionQueue.
     */
    #ifdef _WIN32
        #define WRITE_TO_FILE_DESCRIPTOR                                                                      \
            /* Only send a message to the thread, if it is not 0 (i.e: Someone actually set it).           */ \
            if (onUpdateThreadId != 0) {                                                                      \
                PostThreadMessageA(onUpdateThreadId, WM_TIMER, 0, 0);                                         \
            }
    #else
        #define WRITE_TO_FILE_DESCRIPTOR                                                                      \
            /* Only actually write to the file descriptor, if it is not -1 (i.e: Someone actually set it). */ \
            if (onUpdateFileDescriptor != -1) {                                                               \
                char toWrite = 32; /* This can be any arbitrary value.                                     */ \
                write(onUpdateFileDescriptor, &toWrite, 1);                                                   \
            }
    #endif

    FunctionQueue::FunctionQueue(void) :
        mutex(),
        #ifdef _WIN32
            onUpdateThreadId(0),
        #else
            onUpdateFileDescriptor(-1),
        #endif
        functions(),
        tokens(),
        timedJobs() {
    }

    FunctionQueue::~FunctionQueue(void) {
        // If a function queue still has tokens, when the function queue is destroyed, there is a problem. When
        // those remaining tokens are deleted with FunctionQueue::deleteToken, the deleted function queue will
        // end up being accessed.
        if (!tokens.empty()) {
            std::cerr << "WARNING: Function queue has been destroyed, without first deleting all of it's tokens. Undefined behaviour will result.\n";
        }
    }

    void FunctionQueue::addFunction(std::function<void(void)> func) {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        // To add a normal untimed function, just add it to the list after locking to the mutex.
        functions.push_back(func);

        // Do this since we just added a function to the FunctionQueue.
        WRITE_TO_FILE_DESCRIPTOR
    }

    void FunctionQueue::addTimedRepeatingFunction(void * token, unsigned long long timeUntilStartLong, unsigned long long intervalLong, std::function<void(void)> func) {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        // First work out the current time, and thus the time at which the function should start.
        std::chrono::milliseconds timeUntilStart(timeUntilStartLong),
                                  interval(intervalLong),

                                  timeNow = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()),
                                  timeToStart = timeNow + timeUntilStart;

        // Emplace it to the timed jobs set, set it's iterator and notify the token.
        std::multiset<Job>::iterator iter = timedJobs.emplace(token, timeToStart, interval, func);
        iter->setIter(iter);
        ((Token *)token)->onAddJob(&(*iter));

        // Do this since we just added a function to the FunctionQueue.
        WRITE_TO_FILE_DESCRIPTOR
    }

    void FunctionQueue::update(void) {
        std::unique_lock<std::recursive_mutex> lock(mutex);

        // First run all normal waiting functions.
        while(!functions.empty()) {
            std::function<void(void)> func = functions.front();
            functions.pop_front();
            lock.unlock();
            func(); // Ensure that the function is NOT run from within the mutex.
            lock.lock();
        }

        // Next, if there are no timed jobs, there is nothing to do, so do nothing.
        if (timedJobs.empty()) return;

        // Now, if we *do have* timed jobs, then get the current time.
        std::chrono::milliseconds timeNow = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch());

        do {
            // Get the next timed job which is supposed to be run. If it can't be run yet, then break the loop, since we obviously can't do anything else either.
            std::multiset<Job>::iterator nextJob = timedJobs.begin();
            if (!nextJob->canBeRunNow(timeNow)) {
                break;
            }

            // Make a copy of the job to run. We will later run the function from this copy.
            Job jobToRun = *nextJob;

            // Get the token assigned to this job, and notify it that this job is going to be removed.
            Token * token = (Token *)nextJob->getToken();
            token->onDeleteJob(&(*nextJob));

            // If the job should repeate, generate from it a repeat job, set it's iterator, and notify the token that it now exists.
            if (nextJob->shouldRepeat()) {
                std::multiset<Job>::iterator iter = timedJobs.insert(nextJob->generateRepeatJob());
                iter->setIter(iter);
                token->onAddJob(&(*iter));
            }

            // Now we can remove this job from the timed jobs set.
            timedJobs.erase(nextJob);

            // Finally, run the job from the copy, and execute it AFTER removing it from the jobs set.
            // Making a copy must be done, since the job *could* delete it's own token with deleteToken.
            // That would result in the job getting deleted also, which we would then access.
            lock.unlock();
            jobToRun();
            lock.lock();

        // End the loop if we run out of jobs to run.
        } while (!timedJobs.empty());

        // Any of the timed jobs could have added a normal function. So run any which exist, since we guarantee that
        // any normal function added to a timed job is executed immediately after processing all timed jobs, and that
        // in any update cycle, waiting untimed jobs are executed first.
        // First run all normal waiting functions.
        while(!functions.empty()) {
            std::function<void(void)> func = functions.front();
            functions.pop_front();
            lock.unlock();
            func(); // Ensure that the function is NOT run from within the mutex.
            lock.lock();
        }
    }

    void * FunctionQueue::createToken(void) {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        // To create a new token, simply add one to the back of the list, set it's iterator and return a pointer to it.
        tokens.emplace_back(this);
        std::list<Token>::iterator iter = --tokens.end();
        iter->setIter(iter);
        return &(*iter);
    }

    void FunctionQueue::deleteToken(void * tokenVoid) {

        // To delete a token, first access it's function queue ...
        Token * token = (Token *)tokenVoid;
        FunctionQueue * functionQueue = token->getFunctionQueue();

        // ... only after doing that can we lock to the mutex.
        std::lock_guard<std::recursive_mutex> lock(functionQueue->mutex);

        // Then erase it from the tokens list. This will call the token's destructor, which will remove all the jobs related to it.
        functionQueue->tokens.erase(token->getIter());
    }

    void FunctionQueue::hasWaitingTimedFunctions(unsigned long long timeNow, unsigned long long * minTimeUntil, bool * hasWaiting) {
        std::lock_guard<std::recursive_mutex> lock(mutex);

        // If we have no timed jobs, do nothing. Otherwise, get the first one,
        // get the time until it starts, and set minTimeUntil appropriately.
        if (!timedJobs.empty()) {
            *hasWaiting = true;
            unsigned long long nextJobTime = timedJobs.begin()->getTimeUntilStart(std::chrono::milliseconds(timeNow)).count();
            if (nextJobTime < *minTimeUntil) {
                *minTimeUntil = nextJobTime;
            }
        }
    }

    #ifdef _WIN32
        void FunctionQueue::setUpdateThreadId(DWORD threadId) {
            std::lock_guard<std::recursive_mutex> lock(mutex);
            onUpdateThreadId = threadId;
        }
    #else
        void FunctionQueue::setUpdateFileDescriptor(int fileDescriptor) {
            std::lock_guard<std::recursive_mutex> lock(mutex);
            onUpdateFileDescriptor = fileDescriptor;
        }
    #endif
}
