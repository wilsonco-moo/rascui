/*
 * RepaintController.cpp
 *
 *  Created on: 6 Jun 2019
 *      Author: wilson
 */

#include "RepaintController.h"

#include "../util/Rectangle.h"
#include "../base/Component.h"
#include "../base/Container.h"
#include "../base/TopLevelContainer.h"

namespace rascUI {

    RepaintController::RepaintController(TopLevelContainer * topLevel) :
        topLevel(topLevel),
        wantsRepaint({topLevel}),        // Add the top level container to the repaint
        childWantsRepaint(),             // list by default, as to force a total
        shouldForceEmptyRepaint(false) { // repaint the first time.
    }

    RepaintController::~RepaintController(void) {
    }

    void RepaintController::addComponent(Component * component) {
        wantsRepaint.insert(component);

        // Make sure all parents, and parents of parents etc up to top level
        // are in the childWantsRepaint set.
        Container * parent = component->getParent();
        while(parent != NULL) {
            if (childWantsRepaint.find(parent) == childWantsRepaint.end()) {
                childWantsRepaint.insert(parent);
            } else {
                break;
            }
            parent = parent->getParent();
        }
    }

    void RepaintController::removeComponent(Component * component) {
        wantsRepaint.erase(component);
        if (component->isContainer()) {
            childWantsRepaint.erase((Container *)component);
        }
    }
    
    void RepaintController::forceEmptyRepaint(void) {
        shouldForceEmptyRepaint = true;
    }

    Rectangle RepaintController::doRepaint(void) {
        Rectangle boundingBox;

        if (topLevel->location.getState() != State::invisible) {
            ((Component *)topLevel)->boundedRepaint(boundingBox, wantsRepaint, childWantsRepaint);
        }

        wantsRepaint.clear();
        childWantsRepaint.clear();
        shouldForceEmptyRepaint = false;

        return boundingBox;
    }
    
    
    void RepaintController::clearRepaintSets(void) {
        wantsRepaint.clear();
        childWantsRepaint.clear();
        shouldForceEmptyRepaint = false;
    }
}
