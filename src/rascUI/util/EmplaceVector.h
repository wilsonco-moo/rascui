/*
 * EmplaceVector.h
 *
 *  Created on: 17 Jul 2020
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_EMPLACEVECTOR_H_
#define RASCUI_UTIL_EMPLACEVECTOR_H_

#include <cstddef>

namespace rascUI {

    /**
     * EmplaceVector allows a vector of classes, without moving
     * or copying them when initialising the vector. This makes a container of
     * classes which forbid copying/moving possible. EmplaceVector does not
     * require the container size to be known at compile time. If the container
     * size *is* known at compile time, use EmplaceArray instead.
     *
     * EmplaceArray and EmplaceVector is convenient for assembling lists of
     * components, since rascUI components often are not practical to copy/move
     * due to being related to each other with pointers. This is especially useful
     * when dealing with scroll panes.
     *
     * Example use for EmplaceArray
     * ----------------------------
     *
     * Suppose I had a class "Potato", which is neither copyable nor moveable.
     * You might think that putting this class in a std::vector would be possible,
     * as long as the vector isn't ever resized (which would require copying).
     * For example, the following doesn't require copying the class, since
     * reserve is used:
     *     std::vector<Potato> potatoes;
     *     potatoes.reserve(3);
     *     potatoes.emplace_back("zero");
     *     potatoes.emplace_back("one");
     *     potatoes.emplace_back("two");
     * But, this is not true. The compiler still complains, since emplace_back
     * actually *can* resize the vector, even if it doesn't in this example.
     *
     * To solve this, EmplaceVector can be used - the following is possible:
     *
     * EmplaceVector<Potato> potatoes(3);
     * potatoes.emplace("zero");
     * potatoes.emplace("one");
     * potatoes.emplace("two");
     *
     * Note that you MUST NOT emplace more elements to the array, than there is space for.
     * Unlike a std::vector, we cannot auto resize because that would require moving elements.
     * However, just like EmplaceArray, elements (at the back) *can* be removed from an
     * EmplaceVector. Clearing is also possible - see the following example:
     *
     * EmplaceVector<Potato> potatoes(3);
     * potatoes.emplace("zero");
     * potatoes.emplace("one");
     * potatoes.emplace("two");
     * potatoes.pop_back();             // Remove last element
     * potatoes.emplace("two again");   // Add last element again
     * potatoes.clear();                // Clear the EmplaceVector
     * potatoes.emplace("null");        // Re-fill it again with 3 elements
     * potatoes.emplace("ein");
     * potatoes.emplace("zwei");
     *
     * Also, since the size of an EmplaceVector is controlled at runtime, an EmplaceVector
     * can be resized. Note however that resizing AUTOMATICALLY DELETES ALL ELEMENTS in
     * the EmplaceVector (by calling clear()), first. See the following example:
     *
     * EmplaceVector<Potato> potatoes(3);  // Start with initial size of 3.
     * potatoes.emplace("zero");
     * potatoes.emplace("one");
     * potatoes.emplace("two");
     * potatoes.resize(4);                 // Resize AND CLEAR the EmplaceVector.
     * potatoes.emplace("null");           // Re-fill it again, this time with 4 elements.
     * potatoes.emplace("ein");
     * potatoes.emplace("zwei");
     * potatoes.emplace("drei");
     */
    template<class T>
    class EmplaceVector {
    private:
        // Our internal data.
        char * data;

        // The number of elements in our internal data.
        size_t elementCount;

        // The number of elements initialised so far.
        // This must never exceed dataSize.
        size_t initialised;

        // Don't allow copying/moving: The whole point of this class
        // is to allow vectors of classes which don't allow copying/moving.
        // If the classes *do* allow copying/moving, just us std::vector instead.
        EmplaceVector(const EmplaceVector & other);
        EmplaceVector & operator = (const EmplaceVector & other);

    public:
        /**
         * All elements must be initialised with the emplace method, NOT with the constructor.
         */
        EmplaceVector(size_t elementCount = 0) :
            data(new char[elementCount * sizeof(T)]),
            elementCount(elementCount),
            initialised(0) {
        }

        /**
         * The destructors of all initialised elements are called when EmplaceVector is destroyed.
         */
        ~EmplaceVector(void) {
            for (size_t i = 0; i < initialised; i++) {
                (((T *)data) + i)->~T();
            }
            delete[] data;
        }


        // ------------------- Initialisation, removal and access --------------------

        /**
         * Emplace initialises (using placement new), an element into the vector.
         * Elements are initialised in consecutive order.
         * NOTE: Calling this always INCREASES our size by one. This must never
         *       be called if doing so would increase the size to something larger
         *       than max_size (N).
         */
        template<class ... Args>
        void emplace(Args&&... args) {
            new (((T *)data) + initialised) T(args...);
            initialised++;
        }

        /**
         * Removes the last element in the vector, and calls its destructor.
         * NOTE: Calling this always DECREASES our size by one. This must never
         *       be called if doing so would decrease the size to something
         *       below zero.
         */
        void pop_back(void) {
            initialised--;
            (((T *)data) + initialised)->~T();
        }

        /**
         * Removes all elements from the vector, calling all of their destructors.
         * After this call, our size is always zero.
         */
        void clear(void) {
            for (size_t i = 0; i < initialised; i++) {
                (((T *)data) + i)->~T();
            }
            initialised = 0;
        }

        /**
         * Clears the vector (deletes ALL existing elements), then resizes our allocation,
         * to the specified number of elements.
         * After this call, our size is always zero, and our max_size is always set
         * to the specified new element count.
         */
        void resize(size_t newElementCount) {
            clear();
            delete[] data;
            data = new char[newElementCount * sizeof(T)];
            elementCount = newElementCount;
        }


        // -------------------------------- Access -----------------------------------

        /**
         * Allows const access to elements.
         */
        inline const T & operator[] (size_t id) const {
            return *(((const T *)data) + id);
        }

        /**
         * Allows non-const access to elements.
         */
        inline T & operator[] (size_t id) {
            return *(((T *)data) + id);
        }


        // ----------------------- Iteration and capacity ----------------------------

        /**
         * Returns the total number of elements which have so far been
         * initialised.
         */
        inline size_t size(void) const {
            return initialised;
        }

        /**
         * Returns the maximum number of elements which can be stored.
         */
        constexpr size_t max_size(void) const {
            return elementCount;
        }

        /**
         * Returns true if no elements have been initialised yet.
         */
        inline bool empty(void) const {
            return size() == 0;
        }

        /**
         * Iterator methods. These return either const or non-const
         * iterators (pointers) to either the first element, or one after the
         * last element.
         */
        inline T * begin(void) { return (T *)data; }
        inline const T * begin(void) const { return (const T *)data; }
        inline T * end(void) { return ((T *)data) + initialised; }
        inline const T * end(void) const { return ((const T *)data) + initialised; }
        inline const T * cbegin(void) const { return (const T *)data; }
        inline T * cend(void) { return ((const T *)data) + initialised; }

        /**
         * This returns either a const or non-const reference to the first
         * element in the vector. This is undefined if the vector is empty.
         */
        inline const T & front(void) const {
            return *((const T *)data);
        }
        inline T & front(void) {
            return *((T *)data);
        }

        /**
         * This returns either a const or non-const reference to the last
         * element in the vector. This is undefined if the vector is empty.
         */
        inline const T & back(void) const {
            return *(((const T *)data) + (initialised - 1));
        }
        inline T & back(void) {
            return *(((T *)data) + (initialised - 1));
        }
    };
}

#endif
