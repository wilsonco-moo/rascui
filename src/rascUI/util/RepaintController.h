/*
 * RepaintController.h
 *
 *  Created on: 6 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_REPAINTCONTROLLER_H_
#define RASCUI_UTIL_REPAINTCONTROLLER_H_

#include <unordered_set>
#include "Rectangle.h"

namespace rascUI {

    class Component;
    class Container;
    class TopLevelContainer;

    /**
     * RepaintController is a simple class which stores which components want repainting.
     * It allows the top level container's boundedRepaint method to be run using it's
     * stored data structures.
     */
    class RepaintController {
    private:
        TopLevelContainer * topLevel;
        std::unordered_set<Component *> wantsRepaint;
        std::unordered_set<Container *> childWantsRepaint;
        bool shouldForceEmptyRepaint;

    public:
        RepaintController(TopLevelContainer * topLevel);
        virtual ~RepaintController(void);

    public:
        /**
         * Adds a component to the repaint system. This component (and implicitly it's children), are guaranteed
         * to be repainted next time doRepaint is called.
         */
        void addComponent(Component * component);

        /**
         * Ensures that the specified component is no longer explicitly listed in any of our data structures.
         * This MUST be called if a component is removed from the component hierarchy.
         *
         * NOTE: On the face of it, this seems unnecessary since if we remove a component from the hierarchy,
         *       it's entry in our data structures will never actually be dereferenced again. BUT, if the component
         *       is deleted, the same pointer could be reused for a new component, (by memory re-use etc). This would
         *       cause the new component to be incorrectly listed in our data structures, and would cause strange
         *       behaviour of the repainting system.
         */
        void removeComponent(Component * component);

        /**
         * Sets the "force empty repaint" flag. This causes isRepaintRequired to return true, even if no components
         * need to be repainted. See documentation for TopLevelContainer's forceEmptyRepaint method.
         */
        void forceEmptyRepaint(void);

        /**
         * Repaints the top level container according to our repaint sets. If the top level container
         * is in the invisible state, this will automatically do nothing.
         *
         * This will repaint all the components which were added with addComponent, in draw order. Also repainted
         * are any components which overlap (on top of) these components, to maintain the correct visible component
         * hierarchy.
         *
         * The area which contains components which have been redrawn - (the bounding box) is returned.
         * Note that this may go off the edge of the screen.
         */
        Rectangle doRepaint(void);
        
        /**
         * Returns true if at least one component wants repainting, or the force
         * empty repaint flag has been set.
         * If this returns false, there is no point calling the TopLevelContainer's doPartialRepaint method.
         */
        inline bool isRepaintRequired(void) const {
            return !wantsRepaint.empty() || shouldForceEmptyRepaint;
        }
        
        /**
         * Clears the sets storing which components want repainting, and clears
         * the force empty repaint flag. After this call, isRepaintRequired will
         * return false, and doRepaint will do nothing. This is helpful when
         * drawing in buffered repaint mode, see documentation in Theme.h.
         */
        void clearRepaintSets(void);
    };
}

#endif
