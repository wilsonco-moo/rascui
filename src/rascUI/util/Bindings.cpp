/*
 * Bindings.cpp
 *
 *  Created on: 10 Jun 2019
 *      Author: wilson
 */

#include "Bindings.h"

namespace rascUI {
    
    // Generates OpenGL bindings.
    static Bindings generateDefaultOpenGL(void) {
        Bindings bindings;

        // ========================= Mouse bindings =============================

        bindings.mouseButton = 0;           // Left button, in GLUT
        bindings.mouseScrollUpButton = 3;   // Scroll up button, in GLUT
        bindings.mouseScrollDownButton = 4; // Scroll down button, in GLUT

        // ======================== Keyboard bindings ===========================
        // (Defined values from freeglut_std.h)

        // ------------- Component navigation actions -------
        bindings.navigationSpecial[0x0064] = NavigationAction::previousComponent;    // GLUT_KEY_LEFT  (left arrow key)
        bindings.navigationSpecial[0x0065] = NavigationAction::previousComponent;    // GLUT_KEY_UP    (up arrow key)
        bindings.navigationSpecial[0x0066] = NavigationAction::nextComponent;        // GLUT_KEY_RIGHT (right arrow key)
        bindings.navigationSpecial[0x0067] = NavigationAction::nextComponent;        // GLUT_KEY_DOWN  (down arrow key)
        bindings.navigationNormal [0x0009] = NavigationAction::nextContainer;        // ?              (tab key)
        bindings.navigationNormal [0x0060] = NavigationAction::previousContainer;    // ?              (grave key (`))
        bindings.navigationNormal [0x000d] = NavigationAction::enter;                // ?              (enter key)

        // ------------- Text editing actions ---------------
        bindings.textEditSpecial[0x0064] = TextEditAction::moveCursorPrevious;       // GLUT_KEY_LEFT  (left arrow key)
        bindings.textEditSpecial[0x0066] = TextEditAction::moveCursorNext;           // GLUT_KEY_RIGHT (right arrow key)
        bindings.textEditSpecial[0x006a] = TextEditAction::moveCursorFirst;          // GLUT_KEY_HOME  (home key)
        bindings.textEditSpecial[0x006b] = TextEditAction::moveCursorLast;           // GLUT_KEY_END   (end key)
        bindings.textEditNormal [0x0008] = TextEditAction::deleteCharacterPrevious;  // ?              (backspace key)
        bindings.textEditNormal [0x007f] = TextEditAction::deleteCharacterNext;      // ?              (delete key)

        // ======================================================================
        return bindings;
    }
    
    // Generates X11 bindings.
    static Bindings generateDefaultX11(void) {
        Bindings bindings;

        // ========================= Mouse bindings =============================
        bindings.mouseButton = 1;           // Left button, in X11
        bindings.mouseScrollUpButton = 4;   // Scroll up button, X11
        bindings.mouseScrollDownButton = 5; // Scroll down button, X11

        // ======================== Keyboard bindings ===========================
        // (Defined values from X11/keysymdef.h, from x11proto-core-dev package in Debian)

        // ------------- Component navigation actions -------
        bindings.navigationNormal[0xff51] = NavigationAction::previousComponent;   // XK_Left         (left arrow key)
        bindings.navigationNormal[0xff52] = NavigationAction::previousComponent;   // XK_Up           (up arrow key)
        bindings.navigationNormal[0xff53] = NavigationAction::nextComponent;       // XK_Right        (right arrow key)
        bindings.navigationNormal[0xff54] = NavigationAction::nextComponent;       // XK_Down         (down arrow key)
        bindings.navigationNormal[0xff09] = NavigationAction::nextContainer;       // XK_Tab          (tab key)
        bindings.navigationNormal[0x0060] = NavigationAction::previousContainer;   // XK_grave        (grave key (`))
        bindings.navigationNormal[0xfe20] = NavigationAction::previousContainer;   // XK_ISO_Left_Tab (shift+tab key combo)
        bindings.navigationNormal[0xff0d] = NavigationAction::enter;               // XK_Return       (enter key)
        bindings.navigationNormal[0xff96] = NavigationAction::previousComponent;   // XK_KP_Left      (numpad left key)
        bindings.navigationNormal[0xff97] = NavigationAction::previousComponent;   // XK_KP_Up        (numpad up key)
        bindings.navigationNormal[0xff98] = NavigationAction::nextComponent;       // XK_KP_Right     (numpad right key)
        bindings.navigationNormal[0xff99] = NavigationAction::nextComponent;       // XK_KP_Down      (numpad down key)
        bindings.navigationNormal[0xff8d] = NavigationAction::enter;               // XK_KP_Enter     (numpad enter key)

        // ------------- Text editing actions ---------------
        bindings.textEditNormal[0xff51] = TextEditAction::moveCursorPrevious;      // XK_Left         (left arrow key)
        bindings.textEditNormal[0xff53] = TextEditAction::moveCursorNext;          // XK_Right        (right arrow key)
        bindings.textEditNormal[0xff50] = TextEditAction::moveCursorFirst;         // XK_Home         (home key)
        bindings.textEditNormal[0xff57] = TextEditAction::moveCursorLast;          // XK_End          (end key)
        bindings.textEditNormal[0xff08] = TextEditAction::deleteCharacterPrevious; // XK_BackSpace    (backspace key)
        bindings.textEditNormal[0xffff] = TextEditAction::deleteCharacterNext;     // XK_Delete       (delete key)
        bindings.textEditNormal[0xff96] = TextEditAction::moveCursorPrevious;      // XK_KP_Left      (numpad left key)
        bindings.textEditNormal[0xff98] = TextEditAction::moveCursorNext;          // XK_KP_Right     (numpad right key)
        bindings.textEditNormal[0xff95] = TextEditAction::moveCursorFirst;         // XK_KP_Home      (numpad home key)
        bindings.textEditNormal[0xff9c] = TextEditAction::moveCursorLast;          // XK_KP_End       (numpad end key)

        // ======================================================================
        return bindings;
    }
    
    // Generates Windows API bindings.
    static Bindings generateDefaultWin(void) {
        Bindings bindings;

        // ========================= Mouse bindings =============================
        bindings.mouseButton = 0;           // Left button, chosen to be compatible with OpenGL/GLUT
        bindings.mouseScrollUpButton = 3;   // Scroll up button, chosen to be compatible with OpenGL/GLUT
        bindings.mouseScrollDownButton = 4; // Scroll down button, chosen to be compatible with OpenGL/GLUT

        // ======================== Keyboard bindings ===========================
        // (Defined values from winuser.h, from mingw-w64)
        // Note that windows makes no distinction between normal and numpad versions of keys.

        // ------------- Component navigation actions -------
        bindings.navigationSpecial[0x0025] = NavigationAction::previousComponent;   // VK_LEFT         (left arrow key)
        bindings.navigationSpecial[0x0026] = NavigationAction::previousComponent;   // VK_UP           (up arrow key)
        bindings.navigationSpecial[0x0027] = NavigationAction::nextComponent;       // VK_RIGHT        (right arrow key)
        bindings.navigationSpecial[0x0028] = NavigationAction::nextComponent;       // VK_DOWN         (down arrow key)
        bindings.navigationNormal [0x0009] = NavigationAction::nextContainer;       // VK_TAB          (tab key)
        bindings.navigationNormal [0x0060] = NavigationAction::previousContainer;   // ?               (grave key (`))
        bindings.navigationSpecial[0x0009] = NavigationAction::previousContainer;   // VK_TAB          (shift+tab key combo - normal tab converted to special variant by rascUIwin::Context::winInterpretVirtualKeycode)
        bindings.navigationNormal [0x000d] = NavigationAction::enter;               // VK_RETURN       (enter key)

        // ------------- Text editing actions ---------------
        bindings.textEditSpecial[0x0025] = TextEditAction::moveCursorPrevious;      // VK_LEFT         (left arrow key)
        bindings.textEditSpecial[0x0027] = TextEditAction::moveCursorNext;          // VK_RIGHT        (right arrow key)
        bindings.textEditSpecial[0x0024] = TextEditAction::moveCursorFirst;         // VK_HOME         (home key)
        bindings.textEditSpecial[0x0023] = TextEditAction::moveCursorLast;          // VK_END          (end key)
        bindings.textEditNormal [0x0008] = TextEditAction::deleteCharacterPrevious; // VK_BACK         (backspace key)
        bindings.textEditSpecial[0x002e] = TextEditAction::deleteCharacterNext;     // VK_DELETE       (delete key)

        // ======================================================================
        return bindings;
    }
    
    // Assign the static default bindings using the above functions.
    const Bindings Bindings::defaultOpenGL = generateDefaultOpenGL(),
                   Bindings::defaultX11    = generateDefaultX11(),
                   Bindings::defaultWin    = generateDefaultWin();

    Bindings::Bindings(void) :
        mouseButton(-1),
        mouseScrollUpButton(-1),
        mouseScrollDownButton(-1),
        navigationNormal(),
        navigationSpecial() {
    }

    NavigationAction Bindings::getNavigationAction(int key, bool special) const {
        NavigationAction action = NavigationAction::noAction;
        if (special) {
            std::unordered_map<int, NavigationAction>::const_iterator keyIter = navigationSpecial.find(key);
            if (keyIter != navigationSpecial.end()) {
                action = keyIter->second;
            }
        } else {
            std::unordered_map<int, NavigationAction>::const_iterator keyIter = navigationNormal.find(key);
            if (keyIter != navigationNormal.end()) {
                action = keyIter->second;
            }
        }
        return action;
    }

    TextEditAction Bindings::getTextEditAction(int key, bool special) const {
        TextEditAction action = TextEditAction::noAction;
        if (special) {
            std::unordered_map<int, TextEditAction>::const_iterator keyIter = textEditSpecial.find(key);
            if (keyIter != textEditSpecial.end()) {
                action = keyIter->second;
            }
        } else {
            std::unordered_map<int, TextEditAction>::const_iterator keyIter = textEditNormal.find(key);
            if (keyIter != textEditNormal.end()) {
                action = keyIter->second;
            }
        }
        return action;
    }
}
