/*
 * FunctionQueue.h
 *
 *  Created on: 13 Dec 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_FUNCTIONQUEUE_H_
#define RASCUI_UTIL_FUNCTIONQUEUE_H_

#include <functional>
#include <mutex>
#include <list>
#include <set>

#ifdef _WIN32
    #include <windef.h>
#endif

namespace rascUI {

    /**
     * This class represents a queue of events (as std::functions), which can be queued, then executed when necessary.
     * This is useful for delaying actions until a time where it is possible to execute them, such as:
     *   > Avoiding modifying the component hierarchy of a top level container while it is being drawn
     *   > Getting around situations which would otherwise cause deadlock.
     *
     * This class also provides functionality to run events after a specified amount of time, which can optionally
     * repeat every specified time interval.
     *
     * To add an event who's execution is delayed, a token is required. When a token is deleted, it is guaranteed that
     * no event which was assigned to this token, will ever be started again. It is even possible for a timed job,
     * assigned to a specific token, to delete it's own token (or any other token), from the event itself. This way,
     * a timed repeating event is able to cancel itself, meaning that it will never repeat again.
     *
     * NOTE: Each function queue contains a mutex, to make all operations thread-safe.
     *       BUT, the user provided functions, provided to the FunctionQueue, are NOT executed within this mutex.
     */
    class FunctionQueue {
    private:
        // These classes are used internally, and defined in FunctionQueue.cpp.
        class Token; class Job;

        // A mutex which allows FunctionQueue access to be thread safe.
        std::recursive_mutex mutex;
        #ifdef _WIN32
            // On Windows we can hold a thread ID, which we send a message to each time a new function
            // is added to the FunctionQueue. This is initially set to 0, until setUpdateThreadId is called.
            DWORD onUpdateThreadId;
        #else
            // On Unix/Linux we can hold a file descriptor, which we write a byte of data to each time a new function
            // is added to the FunctionQueue. This is initially set to -1, until setUpdateFileDescriptor is called.
            int onUpdateFileDescriptor;
        #endif
        // The list of simple, non-timed functions waiting to be executed.
        std::list<std::function<void(void)>> functions;
        // All the tokens we have assigned.
        std::list<Token> tokens;
        // All the timed jobs that we have. These are removed from here just before executing
        // them, then added back if they repeat.
        std::multiset<Job> timedJobs;

        /**
         * Function queues cannot be copied: We hold raw resources.
         */
        FunctionQueue(const FunctionQueue & other);
        FunctionQueue & operator = (const FunctionQueue & other);

    public:
        FunctionQueue(void);
        virtual ~FunctionQueue(void);

        /**
         * Adds a function to the queue. This will be executed next time update is called.
         * No timing is associated with this function, and it will never repeat.
         * In any given update cycle, waiting normal (untimed) functions are always executed before
         * any timed functions.
         *
         * NOTE: Any untimed functions, (or timed functions with a timeUntilStart of 0), added from within a timed or untimed
         *       function, will always be executed in the same update cycle, (unless it's respective token is deleted).
         */
        void addFunction(std::function<void(void)> func);

        /**
         * Adds a timed function to the queue. To do this, a token must be used.
         * The time until the function starts, (in milliseconds), must be defined.
         * If zero, this acts like an untimed function, but is able to be cancelled
         * by deleting the token. The addFunction(token, func) overload is helpful
         * for doing that.
         *
         * An interval can also be specified. If an interval greater than zero is specified, the job will repeat every <interval> milliseconds.
         *
         * TIMING ACCURACY IS NOT GUARANTEED, depending on the outside implementation.
         * BUT, timing consistency is guaranteed. For example, if time is set to 1000ms (1 second):
         *  > It is NOT GUARANTEED that this will be run precisely on the mark of each second.
         *  > BUT, it IS guaranteed that in an N second window, the function WILL be run precisely
         *    N times. On average, it WILL be called exactly once per second.
         *
         * NOTE: If functions with the same time until start are added, the order in which they will
         *       be executed is UNDEFINED.
         * NOTE: A timed (possibly repeating) function is able to delete it's own token, from within the
         *       execution of the event, to cancel itself.
         * NOTE: In a given update cycle, any waiting untimed functions are always executed before any waiting timed functions.
         * NOTE: Any untimed functions, (or timed functions with a timeUntilStart of 0), added from within a timed or untimed
         *       function, will always be executed in the same update cycle, (unless it's respective token is deleted).
         */
        inline void addFunction(void * token, std::function<void(void)> func) {
            addTimedRepeatingFunction(token, 0, 0, func);
        }
        inline void addTimedFunction(void * token, unsigned long long timeUntilStart, std::function<void(void)> func) {
            addTimedRepeatingFunction(token, timeUntilStart, 0, func);
        }
        void addTimedRepeatingFunction(void * token, unsigned long long timeUntilStart, unsigned long long interval, std::function<void(void)> func);

        /**
         * Runs all functions waiting in the queue, including timed functions, as appropriate.
         */
        void update(void);

        /**
         * Creates a token, to allow the caller to use timed functions.
         */
        void * createToken(void);

        /**
         * Deletes the token, and cancels any waiting timed functions related to this token.
         * After this call, no timed functions related to this token will ever be run again.
         * This MUST ONLY EVER BE CALLED ONCE, for a particular token generated by createToken,
         * or undefined behaviour will result.
         *
         * The user does not actually have to keep a pointer to the FunctionQueue, since the token
         * contains all the required information internally. As a result, deleting a token requires
         * only a static method.
         *
         * NOTE: It is perfectly fine for a timed job to delete it's own token from within its execution.
         *
         * This should be accessed with FunctionQueue::deleteToken(token).
         */
        static void deleteToken(void * token);

        /**
         * If we have any waiting timed functions, hasWaiting should be set to true. Otherwise, it's value won't be changed.
         * If we have any timed functions, due to start within less than minTimeUntil, then minTimeUntil will be set to
         * the time until they start.
         * NOTE: timeNow must be the time from the following:
         *       std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()
         */
        void hasWaitingTimedFunctions(unsigned long long timeNow, unsigned long long * minTimeUntil, bool * hasWaiting);
        
        #ifdef _WIN32
            /**
             * If on Windows, a thread ID can be provided to the function queue. Each time a new function (untimed or timed)
             * is added to the queue, the thread is sent a WM_TIMER message. This allows the function queue to interrupt
             * a call to GetMessage or similar, each time a new event is added to the queue. This is used in rascUIwin, so
             * an external thread can interrupt the waiting of the main UI loop.
             * To reset the file descriptor to none, set it to 0.
             * 
             * NOTE: This thread MUST have a message queue already set up. To ensure a message queue is set up,
             *       PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE) can be run in the thread.
             *       See: https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-postthreadmessagea
             *            http://web.archive.org/web/20190818075230/https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-postthreadmessagea
             */
            void setUpdateThreadId(DWORD threadId = 0);
        #else
            /**
             * If on Unix/Linux, a file descriptor (preferably a pipe) can be provided to the function queue.
             * Each time a new function (untimed or timed) is added to the queue, a single byte will be written to this.
             * This allows the function queue to interrupt a call to pselect or similar, each time a new event is added
             * to the queue. This is used in rascUIxcb, so an external thread can interrupt the waiting of the main UI loop.
             * To reset the file descriptor to none, set it to -1.
             *
             * NOTE: This file descriptor (pipe) MUST BE NON-BLOCKING.
             */
            void setUpdateFileDescriptor(int fileDescriptor = -1);
        #endif
        
        // ----------------------- Function queue updater ----------------

        /**
         * This class will update the given function queue, when this instance is destroyed.
         * This is useful for example, if you want a function queue to be updated as the last thing in a method,
         * or after a mutex has been unlocked.
         *
         * NULL can be provided as the Function Queue, which will mean nothing will happen.
         * Note that this class does not have a virtual destructor, so do not make subclasses.
         */
        class FunctionQueueUpdater {
        private:
            FunctionQueue * queue;
        public:
            FunctionQueueUpdater(FunctionQueue * queue);
            ~FunctionQueueUpdater(void);
        };
    };
}

#endif
