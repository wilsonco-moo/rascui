/*
 * EmplaceArray.h
 *
 *  Created on: 29 Feb 2020
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_EMPLACEARRAY_H_
#define RASCUI_UTIL_EMPLACEARRAY_H_

#include <cstddef>

namespace rascUI {

    /**
     * EmplaceArray allows an array of classes, without moving
     * or copying them when initialising the array. This makes a container of
     * classes which forbid copying/moving possible. Note that EmplaceArray
     * requires the container size to be known at compile time. If that is not
     * true, use EmplaceVector instead.
     *
     * EmplaceArray and EmplaceVector is convenient for assembling lists of
     * components, since rascUI components often are not practical to copy/move
     * due to being related to each other with pointers. This is especially useful
     * when dealing with scroll panes.
     *
     * Example use for EmplaceArray
     * ----------------------------
     *
     * Suppose I had a class "Potato", which is neither copyable nor moveable.
     * You might think a C-style array is possible, as long as the array itself
     * is never copied or moved. BUT, if I try to initialise it (like this):
     *     Potato potatoes[3] = { Potato("zero"), Potato("one"), Potato("two")};
     * then it won't compile, since array initialisation requires copying the class.
     * But what about std::array? Can I not do this instead:
     *     std::array<Potato, 3> potatoes = { Potato("zero"), Potato("one"), Potato("two")};
     * Nope - it has the same initialisation problem.
     *
     * To solve this, EmplaceArray can be used - the following is possible:
     *
     * EmplaceArray<Potato, 3> potatoes;
     * potatoes.emplace("zero");
     * potatoes.emplace("one");
     * potatoes.emplace("two");
     *
     * Note that you MUST NOT emplace more elements to the array, than there is space for.
     * Elements (at the back) can also be removed, and the array can be reset with clear().
     * See the following example:
     *
     * EmplaceArray<Potato, 3> potatoes;
     * potatoes.emplace("zero");
     * potatoes.emplace("one");
     * potatoes.emplace("two");
     * potatoes.pop_back();             // Remove last element
     * potatoes.emplace("two again");   // Add last element again
     * potatoes.clear();                // Clear the EmplaceVector
     * potatoes.emplace("null");        // Re-fill it again with 3 elements
     * potatoes.emplace("ein");
     * potatoes.emplace("zwei");
     *
     */
    template<class T, size_t N>
    class EmplaceArray {
    private:
        // Our internal data.
        char data[N * sizeof(T)];
        // The number of elements initialised so far.
        size_t initialised;

        // Don't allow copying/moving: The whole point of this class
        // is to allow arrays of classes which don't allow copying/moving.
        // If the classes *do* allow copying/moving, just us std::array instead.
        EmplaceArray(const EmplaceArray & other);
        EmplaceArray & operator = (const EmplaceArray & other);

    public:
        /**
         * All elements must be initialised with the emplace method, NOT with the constructor.
         */
        EmplaceArray(void) :
            initialised(0) {
        }

        /**
         * The destructors of all initialised elements are called when EmplaceArray is destroyed.
         */
        ~EmplaceArray(void) {
            for (size_t i = 0; i < initialised; i++) {
                (((T *)data) + i)->~T();
            }
        }


        // ----------------------- Initialisation and removal ------------------------

        /**
         * Emplace initialises (using placement new), an element into the array.
         * Elements are initialised in consecutive order.
         * NOTE: Calling this always INCREASES our size by one. This must never
         *       be called if doing so would increase the size to something larger
         *       than max_size (N).
         */
        template<class ... Args>
        void emplace(Args&&... args) {
            new (((T *)data) + initialised) T(args...);
            initialised++;
        }

        /**
         * Removes the last element in the array, and calls its destructor.
         * NOTE: Calling this always DECREASES our size by one. This must never
         *       be called if doing so would decrease the size to something
         *       below zero.
         */
        void pop_back(void) {
            initialised--;
            (((T *)data) + initialised)->~T();
        }

        /**
         * Removes all elements from the array, calling all of their destructors.
         * After this call, our size is always zero.
         */
        void clear(void) {
            for (size_t i = 0; i < initialised; i++) {
                (((T *)data) + i)->~T();
            }
            initialised = 0;
        }


        // -------------------------------- Access -----------------------------------

        /**
         * Allows const access to elements.
         */
        inline const T & operator[] (size_t id) const {
            return *(((const T *)data) + id);
        }

        /**
         * Allows non-const access to elements.
         */
        inline T & operator[] (size_t id) {
            return *(((T *)data) + id);
        }


        // ----------------------- Iteration and capacity ----------------------------

        /**
         * Returns the total number of elements which have so far been
         * initialised.
         */
        inline size_t size(void) const {
            return initialised;
        }

        /**
         * Returns the maximum number of elements which can be stored.
         */
        constexpr static size_t max_size(void) {
            return N;
        }

        /**
         * Returns true if no elements have been initialised yet.
         */
        inline bool empty(void) const {
            return size() == 0;
        }

        /**
         * Iterator methods. These return either const or non-const
         * iterators (pointers) to either the first element, or one after the
         * last element.
         */
        inline T * begin(void) { return (T *)data; }
        inline const T * begin(void) const { return (const T *)data; }
        inline T * end(void) { return ((T *)data) + initialised; }
        inline const T * end(void) const { return ((const T *)data) + initialised; }
        inline const T * cbegin(void) const { return (const T *)data; }
        inline T * cend(void) { return ((const T *)data) + initialised; }

        /**
         * This returns either a const or non-const reference to the first
         * element in the array. This is undefined if the array is empty.
         */
        inline const T & front(void) const {
            return *((const T *)data);
        }
        inline T & front(void) {
            return *((T *)data);
        }

        /**
         * This returns either a const or non-const reference to the last
         * element in the array. This is undefined if the array is empty.
         */
        inline const T & back(void) const {
            return *(((const T *)data) + (initialised - 1));
        }
        inline T & back(void) {
            return *(((T *)data) + (initialised - 1));
        }
    };
}

#endif
