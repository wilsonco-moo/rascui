/*
 * Location.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_LOCATION_H_
#define RASCUI_UTIL_LOCATION_H_

#include <GL/gl.h>
#include <list>
#include <functional>

#include "State.h"
#include "Rectangle.h"

namespace rascUI {

    class Component;


    /**
     * This class defines all the information required to define the position and
     * state of a rascUI component. It has a rectangle for position, and a rectangle
     * for weight. The cache rectangle is calculated when resizing takes place, from
     * the scale, and the values of the position and weight rectangles.
     * 
     * Cache coordinates are rounded down using floor, and widths/heights are treated
     * carefully to ensure consistent rounding. This significantly improves the
     * consistency of the layout.
     * 
     * This class also stores an x scale, y scale and state field. As a result, a
     * Location instance contains ALL the sizing information to draw a component.
     *
     * A note about the storage of states:
     *  > States are used to define what each component should look like when drawn. This is used for example to highlight a button
     *    when the user mouse-overs it, and show it pushed in when the user presses their mouse on it.
     *  > The state is represented by the enum State, from State.h.
     *  > Instead of simply storing a state variable, we store a list of std::function<State(State)> instances. When update
     *    is called, we take an initial value of state, (State::normal), and pass it through each of these functions in series.
     *  > The resulting state is cached in the variable cachedState, and is returned when getState() is called.
     *  > All of this allows any class in the class hierarchy to do one of two things:
     *     - Provide a state modifier that overrides the state modifiers of classes further up the class hierarchy, and can
     *       be overidden by classes further down the hierarchy. This can be done by adding a permanent state modifier in the constructor.
     *     - Temporarily override the state modifiers defined by ALL other classes in the hierarchy, by adding a new state modifier
     *       later on, then removing it when we no longer want to override the state. This is used for active/inactive component behaviour.
     *
     * Note: Do not make subclasses of Location: it does not have a destructor.
     */
    class Location {
    private:
        /**
         * This is our cached state, worked out by the state modifier state system.
         */
        State cachedState;

        /**
         * This is the list in which all the state modifiers are stored.
         */
        std::list<std::function<State(State, Component *)>> stateModifiers;

    public:
        /**
         * Position is the position of the component, which is multiplied by the scale.
         * Weight is the weighted position of the component, as a factor of the parent component.
         * Cache is the actual on-screen position of the component, which is calculated from position and weight, when recalculate is called.
         */
        Rectangle position,
                  weight,
                  cache;

        /**
         * This is a copy of the current x scale and y scale. These are used by the drawing process to define the scale to draw
         * features within the cached location.
         */
        GLfloat xScale, yScale;

    public:

        Location(void);

        Location(GLfloat x, GLfloat y, GLfloat width, GLfloat height);
        Location(GLfloat x, GLfloat y, GLfloat width, GLfloat height, GLfloat xWeight, GLfloat yWeight, GLfloat widthWeight, GLfloat heightWeight);

        Location(Rectangle position);
        Location(Rectangle position, Rectangle weight);

        /**
         * This method is called when resizing. This calculates our cache rectangle from our position and weight,
         * fitted into the specified parent rectangle.
         * This method also updates our x scale and y scale.
         */
        void fitCacheInto(const Rectangle & parent, GLfloat xScale, GLfloat yScale);


        /**
         * This method gets our currently cached state. This should be used for drawing and checking current state.
         */
        inline State getState(void) const {
            return cachedState;
        }

        /**
         * This method updates our cached state from the current list of state modifiers. This should be called each time
         * something has changed that would have resulted in our cached state being now wrong.
         *
         * This SHOULD NOT be called every frame if at all possible, instead this should be called ONLY when something changes.
         * This is because if we have a large class hierarchy with lots of different behaviours, the list of state modifiers may
         * be very long. Also, each state modifier may take a long time to execute.
         *
         * The supplied component MUST BE THE OWNER OF THIS LOCATION.
         *
         * This method is allowed to be called, even if the Component does not have a top level container.
         * If the component *does* have a top level container, and partial repainting is enabled, this will cause
         * an automatic repaint, ONLY IF the Component says that we should, through it's doesStateChangeRequireRepaint method.
         */
        void updateState(Component * component);

        /**
         * Pushes a new state modifier to the back of the state modifier list, returning it's iterator.
         * The iterator can then be used in the method removeStateModifier to remove this state modifier.
         */
        std::list<std::function<State(State, Component *)>>::const_iterator pushStateModifier(std::function<State(State, Component *)> stateModifier);

        /**
         * Removes a state modifier, using an iterator returned from pushStateModifier.
         */
        void removeStateModifier(std::list<std::function<State(State, Component *)>>::const_iterator stateModifierIter);
        
        /**
         * A utility method which allows setting the position and weight after creation,
         * using 8 numbers. This makes it convenient to set a new position and weight using
         * the layout system.
         */
        void setPositionAndWeight(GLfloat x, GLfloat y, GLfloat width, GLfloat height, GLfloat xWeight, GLfloat yWeight, GLfloat widthWeight, GLfloat heightWeight);
    };
}









/**
 * DESIGN SPEC FOR STATE MODIFIER STATE SYSTEM
 * -------------------------------------------
 *
 *   > Suppose we did not store a State variable, instead we had a std::list<std::function<State(State)>>.
 *   > Use a std::list so we can have constant time removals using an iterator, and retain order of the state modifiers.
 *   > Then we cache our state, and only update it when a method like updateState() (or similar) is called.
 *   > To work out our state:
 *         void updateState(void) {
 *             state = State::normal;
 *             for (std::function<State(State)>> & stateModifier : stateModifiers) {
 *                 state = stateModifier(state);
 *             }
 *         }
 *   > This way each of the std::function instances would have the opportunity to modify the state.
 *   > Each class in the hierarchy which wants to modify the state would push a std::function, it it's constructor.
 *   > Sudden modifications to state, e.g: Switching to State::inactive, would be pushed to the end of the list,
 *     then removed again when the modification is disabled, (this can be done with an iterator).
 *   > We would need two methods: pushStateModifier(), returning an iterator to the new state modifier, and
 *     removeStateModifier(iterator), which can remove a state modifier based on an iterator returned by the previous
 *     method.
 * BUT: This new state system would not allow components that continually assign their state. How would ToggleComponent work?
 *   > It could cache it's previous value of isToggleComponentSelected() each frame. If it changes, it could request
 *     a recalculation of state.
 * BUT: How would we ensure updateState is called at the right times?
 *   > Make it the component code and user code's responsibility to call it.
 * BUT: How would we ensure that updateState is called at the right times when adding and removing the component?
 *   > Call updateState each time a component has it's top level container set.
 * BUT: Consistency between setting inactive, and other mouse state (e.g: From keyboard integration)?
 *   > That would not matter - the internal mouse state which would still be calculated by ResponsiveComponent would simply be
 *     overridden, and would continue to be there once the component is made active again.
 */








#endif
