/*
 * WindowBase.cpp
 *
 *  Created on: 12 May 2020
 *      Author: wilson
 */

#include "WindowBase.h"

#include "ContextBase.h"

namespace rascUI {

    WindowBase::WindowBase(ContextBase * context, const WindowConfig & config, Theme * theme, const Location & location, const Bindings * bindings, const GLfloat * xScalePtr, const GLfloat * yScalePtr) :
        TopLevelContainer((theme == NULL) ? context->defaultTheme : theme,
                          (bindings == NULL) ? context->defaultBindings : bindings,
                          (xScalePtr == NULL) ? context->defaultXScalePtr : xScalePtr,
                          (yScalePtr == NULL) ? context->defaultYScalePtr : yScalePtr,
                          location,
                          context->allowPartialRepaint,
                          &context->beforeEvent,
                          &context->afterEvent),
        
        context(context),
        mapped(false),
        userRequestCloseFunc([this](void) {
            setMapped(false);
        }) {
    }
    
    WindowBase::~WindowBase(void) {
    }
    
    void WindowBase::setMapped(bool status) {
        if (mapped != status) {
            if (status) {
                onMapWindow();
            } else {
                onUnmapWindow();
            }
            mapped = status;
        }
    }
    
    void WindowBase::onUserRequestClose(void) {
        userRequestCloseFunc();
    }
    
    void WindowBase::onUnusedMouseMove(GLfloat viewX, GLfloat viewY) {
    }
    void WindowBase::onUnusedMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY) {
    }
    void WindowBase::onUnusedKeyPress(int key, bool special) {
    }
    void WindowBase::onUnusedKeyRelease(int key, bool special) {
    }
}
