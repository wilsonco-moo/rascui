/*
 * WindowConfig.h
 *
 *  Created on: 12 May 2020
 *      Author: wilson
 */

#ifndef RASCUI_PLATFORM_WINDOWCONFIG_H_
#define RASCUI_PLATFORM_WINDOWCONFIG_H_

#include <string>

namespace rascUI {

    /**
     * RascUI platform interface.
     * 
     * WindowConfig must contain all of the required configuration for creating
     * a Window.
     * 
     * By default, enough generic configuration parameters are provided for
     * a basic window in any implementation. However, implementations are free
     * to require a WindowConfig subclass, if more options are needed.
     * 
     * A WindowConfig is required by Window when the window
     * is created. After that, WindowConfig is no longer needed.
     */
    class WindowConfig {
    public:
        /**
         * The required window title title, to be shown in the window border.
         */
        std::string title;
        
        /**
         * The required x and y position of the window, within the desktop.
         */
        int screenX, screenY;
        
        /**
         * Whether to actually use screenX and screenY. When this is set to
         * false screenX and screenY can be ignored: the implementation is
         * free to automatically place the window in the most appropriate
         * location.
         */
        bool enableScreenPosition;
        
        /**
         * The required width and height of the window.
         */
        unsigned int screenWidth, screenHeight;
        
        /**
         * The width of the window border. This is passed to X in unix, but
         * seems to be rarely used.
         */
        unsigned int borderWidth;
        
        WindowConfig(void);
        virtual ~WindowConfig(void);
    };
}

#endif
