/*
 * ContextBase.cpp
 *
 *  Created on: 12 May 2020
 *      Author: wilson
 */

#include "ContextBase.h"

#include "WindowBase.h"

#include <cmath>

namespace rascUI {

    ContextBase::ContextBase(Theme * defaultTheme, const Bindings * defaultBindings, const GLfloat * defaultXScalePtr, const GLfloat * defaultYScalePtr, bool allowPartialRepaint) :
        defaultTheme(defaultTheme),
        defaultBindings(defaultBindings),
        allowPartialRepaint(allowPartialRepaint),
        defaultXScalePtr(defaultXScalePtr),
        defaultYScalePtr(defaultYScalePtr),
        beforeEvent(),
        afterEvent() {
    }
    
    ContextBase::ContextBase(void) :
        ContextBase(NULL, NULL) {
    }

    ContextBase::~ContextBase(void) {
    }
    
    GLfloat ContextBase::scaleExpToScale(GLfloat scaleExp) {
        return std::pow(2.0f, scaleExp / 8.0f);
    }
}
