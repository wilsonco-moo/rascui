/*
 * WindowBase.h
 *
 *  Created on: 12 May 2020
 *      Author: wilson
 */

#ifndef RASCUI_PLATFORM_WINDOWBASE_H_
#define RASCUI_PLATFORM_WINDOWBASE_H_

#include <functional>
#include <cstddef>

#include "../base/TopLevelContainer.h"

namespace rascUI {
    class WindowConfig;
    class ContextBase;
    class Theme;

    /**
     * RascUI platform interface.
     * 
     * Implementations should make a subclass of WindowBase, and call it
     * Window. By consistent naming, different implementations can easily
     * be swapped ONLY by changing the namespace.
     * 
     * Window must represent a window in the desktop environment. To create
     * a Window, a WindowConfig and ContextBase are required. The context
     * must remain existing for the duration of the existence of the Window, but
     * the WindowConfig is only required for creating the window.
     * 
     * A custom Theme can be provided to each WindowBase instance, instead
     * of using the Theme provided by the Context. Note that if this is
     * the case, then the init and destroy methods will not be called
     * automatically, so these must be done manually. See the documentation
     * in Context.h and Theme.h.
     * 
     * A newly created window must not be visible to users. At this point
     * components can be added to it, since it *is* a top level container.
     * Once the window is mapped, it should appear to users.
     */
    class WindowBase : public TopLevelContainer {
    private:
        // Our associated context.
        ContextBase * context;
        
        // Whether the window is currently mapped. By default, windows are
        // NOT mapped.
        bool mapped;
    public:
        /**
         * As an alternative to overriding onUserRequestClose, this can
         * be set. See the documentation for onUserRequestClose.
         */
        std::function<void(void)> userRequestCloseFunc;
        
    private:
        // Disable copying: we hold raw resources.
        WindowBase(const WindowBase & other);
        WindowBase & operator = (const WindowBase & other);
               
    public:
        /**
         * The main constructor for Window.
         * 
         * Mandatory parameters
         * context:   The existing context to be associated with.
         * config:    A WindowConfig to provide initial window configuration.
         * 
         * Optional parameters
         * theme:     A Theme to use for displaying this window. If NULL is
         *            supplied (the default), then the default theme from the
         *            Context is used.
         * location:  The location to supply to the underlying top level
         *            container.
         * bindings:  Keyboard and mouse bindings to use. If NULL is supplied
         *            (the default), then the default bindings from the Context
         *            is used.
         * xScalePtr: UI X scale pointer to use. If NULL is suppplied
         *            (the default), then the default X scale pointer from the
         *            Context is used.
         * yScalePtr: UI Y scale pointer to use. If NULL is suppplied
         *            (the default), then the default Y scale pointer from the
         *            Context is used.
         */
        WindowBase(ContextBase * context, const WindowConfig & config,
               Theme * theme = NULL, const Location & location = Location(),
               const Bindings * bindings = NULL,
               const GLfloat * xScalePtr = NULL, const GLfloat * yScalePtr = NULL);
        
        virtual ~WindowBase(void);
        
    protected:
        /**
         * This is called when the user sets the window from unmapped to mapped.
         * Here, the implementation should map the window.
         */
        virtual void onMapWindow(void) = 0;
        
        /**
         * This is called when the user sets the window from mapped to unmapped.
         * Here, the implementation should unmap the window.
         */
        virtual void onUnmapWindow(void) = 0;
    
    public:
        /**
         * Allows access to our associated context.
         */
        inline ContextBase * getContext(void) const {
            return context;
        }
        
        /**
         * Returns true if the window is currently mapped, false otherwise.
         */
        inline bool isMapped(void) const {
            return mapped;
        }
        
        /**
         * Sets whether the window should be mapped. A value of true will
         * map the window (make it appear), and a value of false will
         * unmap the window (make it disappear).
         */
        void setMapped(bool status);
        
        /**
         * This should be called by the Context implementation's mainLoop
         * method, when users of the program request that a window is closed.
         * 
         * As a sensible default, this runs onUserRequestClose, which by
         * default runs setMapped(false). When writing a gui program, to
         * change this behaviour, either set userRequestCloseFunc or
         * override this method (onUserRequestClose). For example, this could
         * be changed to provide an "are you sure" message or something similar.
         */
        virtual void onUserRequestClose(void);
        
        // --------------------- Unused keyboard/mouse events ------------------
        
        /**
         * This should be called by the Context implementation's mainLoop
         * method, when the mouse moves while not over any UI components,
         * i.e: there are no components to handle the mouse event, since the
         *      mouse was over the background.
         * 
         * The Context is able to know this, as the call to
         * TopLevelContainer::mouseMove would have returned true.
         * 
         * This is intended to be (optionally) overridden by users, for the
         * purpose of handling events outside the UI system. By default this
         * method does nothing.
         */
        virtual void onUnusedMouseMove(GLfloat viewX, GLfloat viewY);
        
        /**
         * This should be called by the Context implementation's mainLoop
         * method, when the mouse event happens while not over any UI
         * components,
         * i.e: there are no components to handle the mouse event, since the
         *      mouse was over the background.
         * 
         * The Context is able to know this, as the call to
         * TopLevelContainer::mouseEvent would have returned true.
         * 
         * This is intended to be (optionally) overridden by users, for the
         * purpose of handling events outside the UI system. By default this
         * method does nothing.
         */
        virtual void onUnusedMouseEvent(int button, int state, GLfloat viewX, GLfloat viewY);
        
        /**
         * This should be called by the Context implementation's mainLoop
         * method, when a key press event was not handled by any components,
         * i.e: the key event was not a navigation action, and no component
         *      currently has keyboard selection locked.
         * 
         * The Context is able to know this, as the call to
         * TopLevelContainer::keyPress would have returned true.
         * 
         * This is intended to be (optionally) overridden by users, for the
         * purpose of handling events outside the UI system. By default this
         * method does nothing.
         */
        virtual void onUnusedKeyPress(int key, bool special);
        
        /**
         * This should be called by the Context implementation's mainLoop
         * method, when a key release event was not handled by any components,
         * i.e: the key event was not a navigation action, and no component
         *      currently has keyboard selection locked.
         * 
         * The Context is able to know this, as the call to
         * TopLevelContainer::keyRelease would have returned true.
         * 
         * This is intended to be (optionally) overridden by users, for the
         * purpose of handling events outside the UI system. By default this
         * method does nothing.
         */
        virtual void onUnusedKeyRelease(int key, bool special);
    };
}

#endif
