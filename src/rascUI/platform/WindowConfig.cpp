/*
 * WindowConfig.cpp
 *
 *  Created on: 12 May 2020
 *      Author: wilson
 */

#include "WindowConfig.h"

namespace rascUI {

    WindowConfig::WindowConfig(void) :
        title("rascUI"),
        screenX(128),
        screenY(128),
        enableScreenPosition(false),
        screenWidth(320),
        screenHeight(240),
        borderWidth(0) {
    }
    
    WindowConfig::~WindowConfig(void) {
    }
}
