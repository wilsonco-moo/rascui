/*
 * ContextBase.h
 *
 *  Created on: 12 May 2020
 *      Author: wilson
 */

#ifndef RASCUI_PLATFORM_CONTEXTBASE_H_
#define RASCUI_PLATFORM_CONTEXTBASE_H_

#include <GL/gl.h>
#include <cstddef>

#include "../util/FunctionQueue.h"

namespace rascUI {
    class Bindings;
    class Theme;

    /**
     * RascUI platform interface.
     * 
     * Implementations should make a subclass of ContextBase, and call it
     * Context. By consistent naming, different implementations can easily
     * be swapped ONLY by changing the namespace.
     * 
     * By default all this class does is keeps track of registered WindowBase
     * instances. Implementations must encapsulate all of the platform-specific
     * details required to interact with the relevant desktop system, and
     * free them when the context is destroyed. For example, on a Unix system
     * Context should manage the connection to the X server, opening it
     * upon construction, and closing it upon destruction.
     * 
     * Context must also provide a main loop, which must wait for user
     * interaction and send appropriate events to the relevant windows. 
     * 
     * A ContextBase instance is required when creating a WindowBase instance.
     * ContextBase keeps track of all Window instances which are registered to
     * it, and must remain existing for the duration of the existence of
     * these windows.
     */
    class ContextBase {
    private:

        // Disable copying: we hold raw resources.
        ContextBase(const ContextBase & other);
        ContextBase & operator = (const ContextBase & other);
        
    public:
        /**
         * The default theme which is used for new Window instances.
         * This can be changed at any time, and will be used by subsequent
         * new windows. Note that if the Theme is changed during the existence
         * of the Context, it is the responsibility of the user to call
         * the old Theme's destroy method, and the new Theme's init method.
         */
        Theme * defaultTheme;
    
        /**
         * The default bindings to use. Unless specified by the user in
         * the constructor, this is initialised from getDefaultBindings.
         */
        const Bindings * defaultBindings;
        
        /**
         * Whether partial repaint is enabled for windows which are associated
         * with this context.
         */
        bool allowPartialRepaint;
        
        /**
         * UI scale pointers which is used for new Window instances.
         * This can be changed at any time, and will be used by subsequent
         * new windows.
         */
        const GLfloat * defaultXScalePtr,
                      * defaultYScalePtr;
        
        /**
         * Function queues, which are updated before and after events
         * respectively. These are used by all associated windows.
         */
        FunctionQueue beforeEvent, afterEvent;
    
        /**
         * A Theme to use for NEWLY CREATED windows can be supplied. If
         * themes are always set manually, it is fine to provide NULL here.
         * Otherwise, the Theme MUST remain existing for the duration
         * of the existence of the windows.
         * 
         * It is the responsibility of the ContextBase subclass to call the
         * init and destroy methods of the Theme. If no theme is supplied,
         * and the user chooses to manage Themes manually, it is the user's
         * responsibility to call init and destroy on the Theme manually.
         * Init must be called in the subclasses constructor, if the platform
         * loaded successfully. Destroy must be called in the subclasses
         * destructor, if init was called in the constructor.
         * 
         * A Bindings instance must also be provided, to use by default.
         * Implementations should make this an optional parameter, and give
         * a default set of bindings. Implementations should also move this
         * parameter to IMMEDIATELY AFTER defaultYScalePtr.
         * 
         * Optionally, pointers to a default UI scale to use for
         * NEWLY CREATED Windows (TopLevelContainers) can also be supplied.
         * 
         * It must be set whether windows created from this context allow
         * partial repainting. Implementations should set default values for
         * this, or even discard the parameter completely.
         * 
         * As a result of this, the constructor of subclasses must look like
         * this, where it is optional whether to even allow the last
         * parameter: (some implementations may ONLY work with partial
         * repaint enabled).
         * 
         * defaultTheme, defaultXScalePtr = NULL, defaultYScalePtr = NULL, defaultBindings = NULL, [allowPartialRepaint = ?]
         */
        ContextBase(Theme * defaultTheme, const Bindings * defaultBindings, const GLfloat * defaultXScalePtr = NULL, const GLfloat * defaultYScalePtr = NULL, bool allowPartialRepaint = false);
        
        /**
         * Default constructor:
         * This passes NULL for both default theme and default bindings.
         * 
         * It is recommended that ContextBase implementations provide
         * a default constructor, which calls this constructor, then
         * AUTOMATICALLY sets default theme, default bindings, default
         * X/Y scale pointers, and default partial repaint status,
         * for use by windows created from this ContextBase. For more
         * information about each of these, see documentation for
         * constructor above.
         * 
         * For this "automatic" constructor, it should ALWAYS be the
         * responsibility of the ContextBase subclass to call init and
         * destroy methods on the theme, as by definition, here the user
         * has let the platform implementation take charge of choosing
         * a theme.
         * 
         * It is recommended that choice of theme, UI scale etc, should
         * be done using a central config system (such as a rascUItheme
         * config file).
         */
        ContextBase(void);
        
        virtual ~ContextBase(void);
        
        /**
         * This method should return true all operations have, up until this
         * point, been successful.
         * This method should return false if there have been errors,
         * most notably errors from initialising the context - for example,
         * on a unix system the connection to the X server may have failed.
         * 
         * This method MUST BE CHECKED by users, after the Context has been
         * created. If this returns false, the Context must not be used.
         */
        virtual bool wasSuccessful(void) const = 0;
        
        /**
         * This should make sure any pending draw operations, or anything
         * else relating to the Context are processed. This does not
         * necessarily have to wait for them to complete, it must simply
         * force them to happen.
         * 
         * Platform users must assume that this method MUST BE CALLED, before
         * things they have done using the platform become visible to users
         * of the program.
         * 
         * For example, on a unix system this should flush the connection
         * to the X server.
         */
        virtual void flush(void) = 0;
        
        /**
         * This should run the main event loop, for this context. This should
         * read and process the events of all windows registered to this
         * context. This should end once all windows have either been closed
         * by the program user, unmapped/destroyed, or once endMainLoop is
         * called.
         * Note that "flush()" should automatically be called at the start
         * of mainLoop, to avoid the user having to call it manually.
         * 
         * This function must return the number of events which have been
         * processed, during the running of the main loop. When this function
         * returns zero, it can notify users that there are no mapped windows
         * to process events from.
         */
        virtual unsigned long long mainLoop(void) = 0;
        
        /**
         * This should cause mainLoop to return, while leaving all associated
         * resources and windows in existence. This must only be called from
         * events, during the running of mainLoop.
         * 
         * This could as an efficient way to implement a popup window: During
         * the event triggered when a button is pressed, the main loop could
         * end, a new window could be allocated on the stack, then the main loop
         * could be restarted.
         * 
         * Alternatively, this is the cleanest way to close the program:
         * End the main loop, and do nothing else. The destructors of all
         * windows, the context and Theme would be automatically called to
         * nicely tidy everything up.
         */
        virtual void endMainLoop(void) = 0;
        
        /**
         * This should ensure that after this call, next time drawing takes
         * place, the entire contents of every (mapped) window is completely
         * repainted. This should have an effect equivalent to calling the
         * repaint method on all mapped windows.
         * Calling this method should be avoided, as repainting everything can
         * be slow. This should only be called if there is a global change
         * affecting all UI, such as a change to the UI scale.
         */
        virtual void repaintEverything(void) = 0;
        
        
        // ------------------- Utility functions -----------------------
        
        /**
         * Converts an "exponent scale" to a standard UI scale.
         * Typically, UI scales should be stored in exponent form, as
         * it is easier to store, modify and represent. This way, UI
         * scale "zooms" as users expect.
         * 
         * The formula is: scale = 2 ^ (scaleExp / 8)
         * This is so a scaleExp change of one corresponds to a
         * meaningful (but still fairly granular) change of actual UI
         * scale.
         */
        static GLfloat scaleExpToScale(GLfloat scaleExp);
    };
}

#endif
