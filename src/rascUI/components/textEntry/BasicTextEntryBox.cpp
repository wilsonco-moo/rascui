/*
 * BasicTextEntryBox.cpp
 *
 *  Created on: 16 Nov 2018
 *      Author: wilson
 */

#include "BasicTextEntryBox.h"

#include "../../base/TopLevelContainer.h"
#include "../../util/FunctionQueue.h"
#include "../../util/Layout.h"
#include "../../base/Theme.h"

namespace rascUI {

    // Milliseconds:
    // (0.3f seconds)
    #define BLINK_HALF_TIME 300
    // (0.6f seconds)
    #define BLINK_FULL_TIME 600

    BasicTextEntryBox::BasicTextEntryBox(const Location & location, std::string text, std::function<void(const std::string &)> onChangeFunc) :
        ToggleComponent(location),
        TextObject(text),
        currentlyEditing(false),
        showCursor(false),
        token(NULL),
        onChangeFunc(onChangeFunc),
        cursorBlinkFunc([](void){}) {
    }

    BasicTextEntryBox::~BasicTextEntryBox(void) {
        if (token != NULL) {
            rascUI::FunctionQueue::deleteToken(token);
        }
    }

    void BasicTextEntryBox::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getMb() && !getTopLevel()->isSelectionLocked()) {
            getTopLevel()->afterEvent->addFunction([this](void) {
                getTopLevel()->setKeyboardSelected(this);
                getTopLevel()->lockSelection();
            });
        }
    }

    void BasicTextEntryBox::onSelectionLockEnabled(void) {
        currentlyEditing = true;
        showCursor = true;
        cursorBlinkFunc = [this](void) {
            showCursor = !showCursor;
            repaint();
        };
        if (token == NULL) {
            token = getTopLevel()->beforeEvent->createToken();
        }
        getTopLevel()->beforeEvent->addTimedRepeatingFunction(token, BLINK_HALF_TIME, BLINK_HALF_TIME, cursorBlinkFunc);
    }
    void BasicTextEntryBox::onSelectionLockDisabled(void) {
        if (token != NULL) {
            rascUI::FunctionQueue::deleteToken(token);
            token = NULL;
        }
        currentlyEditing = false;
        showCursor = false;
        repaint();
        onChange(getText());
    }


    NavigationAction BasicTextEntryBox::onSelectionLockKeyPress(NavigationAction defaultAction, int key, bool special) {

        // Each time we type a character, cancel our event, then reassign it. This stops the cursor blinking while typing.
        showCursor = true;
        if (token != NULL) {
            rascUI::FunctionQueue::deleteToken(token);
            token = getTopLevel()->beforeEvent->createToken();
            getTopLevel()->beforeEvent->addTimedRepeatingFunction(token, BLINK_HALF_TIME, BLINK_HALF_TIME, cursorBlinkFunc);
        }

        textAddCharacter(getTopLevel()->bindings, key, special);
        repaint();
        return NavigationAction::noAction;
    }

    bool BasicTextEntryBox::isToggleComponentSelected(void) const {
        return currentlyEditing;
    }

    void BasicTextEntryBox::onDraw(void) {
        // Call the superclass onDraw method, so the location's state is updated correctly. See ToggleComponent.h.
        ToggleComponent::onDraw();

        getTopLevel()->theme->drawTextEntryBox(location, getText(), getTextCursorPosition(), showCursor);
    }

    void BasicTextEntryBox::onChange(const std::string & str) {
        onChangeFunc(str);
    }
    
    CursorType BasicTextEntryBox::getCursorType(void) const {
        // If active/visible, pick "text" cursor, else use parent class' cursor.
        return isActiveAndVisible() ? CursorType::text : ToggleComponent::getCursorType();
    }
}
