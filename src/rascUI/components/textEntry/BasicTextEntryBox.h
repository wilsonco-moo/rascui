/*
 * BasicTextEntryBox.h
 *
 *  Created on: 16 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_TEXTENTRY_BASICTEXTENTRYBOX_H_
#define RASCUI_COMPONENTS_TEXTENTRY_BASICTEXTENTRYBOX_H_

#include <functional>

#include "../abstract/ToggleComponent.h"
#include "../abstract/TextObject.h"

namespace rascUI {

    /**
     * BasicTextEntryBox is a text component, able to have selection locked to it, by clicking.
     * Once the user has locked selection, they can use the keyboard to enter text.
     * This component displays a cursor, and responds to arrow keys, home/end and backspace/delete.
     *
     * To run something when the value is changed, (i.e: When the user presses enter to finish editing),
     * you can either:
     *   > Override onSelectionLockDisabled, (ensuring that the super method is called).
     *   > Provide a std::function in the constructor.
     */
    class BasicTextEntryBox : public ToggleComponent, public TextObject {
    private:
        bool currentlyEditing,
             showCursor;
        void * token;
        std::function<void(const std::string &)> onChangeFunc;
        std::function<void(void)> cursorBlinkFunc;

    public:

        BasicTextEntryBox(const Location & location = Location(), std::string text = "",
            std::function<void(const std::string &)> onChangeFunc = [](const std::string & str){});
        virtual ~BasicTextEntryBox(void);

    protected:
        virtual bool isToggleComponentSelected(void) const override;

        virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;

        virtual void onSelectionLockEnabled(void) override;
        virtual void onSelectionLockDisabled(void) override;


        virtual NavigationAction onSelectionLockKeyPress(NavigationAction defaultAction, int key, bool special) override;

        virtual void onDraw(void) override;

        /**
         * Instead of providing a std::function, (onChangeFunc), this method can be overridden to have the same effect.
         */
        virtual void onChange(const std::string & str);
    
    public:
        /**
         * Here we return a text cursor while active/visible.
         */
        CursorType getCursorType(void) const override;
    };

}

#endif
