/*
 * ToggleButton.h
 *
 *  Created on: 12 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_TOGGLEBUTTON_TOGGLEBUTTON_H_
#define RASCUI_COMPONENTS_TOGGLEBUTTON_TOGGLEBUTTON_H_

#include "../abstract/ToggleComponent.h"
#include "../abstract/TextObject.h"
#include "ToggleButtonSeries.h"

namespace rascUI {

    class ToggleButtonSeries;

    /**
     * A ToggleButton is a button which can be connected to a ToggleButtonSeries.
     * This allows the user to select a single ToggleButton in the ToggleButtonSeries.
     * For more info see ToggleButtonSeries.
     *
     * ToggleButtons should not be copied, or you will end up with ToggleButtons which the
     * ToggleButtonSeries does not know about.
     * Copying *is* allowed though, as otherwise, even with correct usage, ToggleButtons cannot
     * be put in STL containers.
     */
    class ToggleButton : public ToggleComponent, public TextObject {
    private:
        // Make ToggleButtonSeries a friend class so it can nullify our pointer to it when it gets destroyed.
        friend class ToggleButtonSeries;

        /**
         * Our respective toggle button series. This is automatically set to NULL
         * when the series is destroyed.
         */
        ToggleButtonSeries * series;

        /**
         * Our id within the toggle button series.
         */
        const unsigned long toggleButtonId;

    public:

        /**
         * This is called each time selection changes to us.
         */
        std::function<void(GLfloat, GLfloat, int)> onSelectFunc;

        /**
         * This is the main constructor for ToggleButton.
         * This constructor automatically gives us an id. Automatic ids are assigned incrementally in ascending order.
         * See the other constructor for more info about the id assignments.
         *
         * The series must NOT be NULL, or undefined behaviour will result.
         *
         * NOTE: For toggle buttons, the lambda statement is only run when we transition from unselected to selected,
         *       NOT every time we are actually clicked.
         */
        ToggleButton(
            ToggleButtonSeries * series,
            const Location & location = Location(),
            std::string text = "",
            std::function<void(GLfloat, GLfloat, int)> onSelectFunc = [](GLfloat viewX, GLfloat viewY, int button){}
        );

        /**
         * This is the main constructor for ToggleButton.
         *
         * This constructor allows us to manually assign ourself a toggle button id. This id should be different to the id of all other toggle
         * buttons which exist already within this series, or undefined behaviour will result, and a warning will be printed.
         * After assigning an id manually, the next automatically assigned id will be one after this manual id.
         * BUT, if we manually assign an id which is less than or equal to the previous automatic id assignment,
         * the next automatic id will not be affected. For example, the series of assignments could go like this:
         *   auto-assign 0, auto-assign 1, auto-assign 2, MANUAL-ASSIGN 5, auto-assign 6, ...    (If the manual assignment was greater than the previous automatic assignment).
         *   auto-assign 0, auto-assign 1, auto-assign 2, MANUAL-ASSIGN 2, auto-assign 3, ...    (If the manual assignment was equal to the previous automatic assignment. Unless id 2 was destroyed before that manual assignment, this will cause undefined behaviour).
         *   auto-assign 0, auto-assign 1, auto-assign 2, MANUAL-ASSIGN 0, auto-assign 3, ...    (If the manual assignment was lass than the previous automatic assignment. Unless id 2 was destroyed before that manual assignment, this will cause undefined behaviour).
         *
         *
         * The series must NOT be NULL, or undefined behaviour will result.
         *
         * NOTE: For toggle buttons, the lambda statement is only run when we transition from unselected to selected,
         *       NOT every time we are actually clicked.
         */
        ToggleButton(
            ToggleButtonSeries * series,
            unsigned long toggleButtonId,
            const Location & location = Location(),
            std::string text = "",
            std::function<void(GLfloat, GLfloat, int)> onSelectFunc = [](GLfloat viewX, GLfloat viewY, int button){}
        );

        virtual ~ToggleButton(void);

        /**
         * This gets the ID within the toggle button series of this toggle button.
         */
        inline unsigned long getToggleButtonId(void) const {
            return toggleButtonId;
        }

        /**
         * This returns our toggle button series.
         * This will be a non-NULL value until the toggle button series is destroyed.
         */
        inline ToggleButtonSeries * getSeries(void) const {
            return series;
        }

        /**
         * Returns true if this toggle button is currently selected within the toggle button series, false otherwise.
         * After the toggle button series is destroyed, this will always return false.
         */
        virtual bool isToggleComponentSelected(void) const override;


        /**
         * This is called each time we transition from being unselected to selected.
         */
        virtual void onToggleButtonSelect(GLfloat viewX, GLfloat viewY, int button);

        /**
         * A convenient way to automate calls to onToggleButtonSelect.
         */
        inline void onFakeToggleButtonSelect(int button) {
            onToggleButtonSelect(location.cache.centreX(), location.cache.centreY(), button);
        }

    protected:
        /**
         * Here we override the onMouseClick method, to do the toggle button selection.
         *
         * This SHOULD NOT be overridden by implementing classes, instead onToggleButtonSelect
         * should be overridden.
         */
        virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;

        virtual void onDraw(void) override;

        /**
         * This is provided so custom implementations of ToggleButton, which respond to a different
         * mouse button than top level's mouseButton, can be designed.
         */
        virtual bool shouldChangeSelection(int mouseButton);
    };

}

#endif
