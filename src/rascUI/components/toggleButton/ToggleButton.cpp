/*
 * ToggleButton.cpp
 *
 *  Created on: 12 Nov 2018
 *      Author: wilson
 */

#include "ToggleButton.h"
#include "../../base/TopLevelContainer.h"
#include "../../base/Theme.h"

namespace rascUI {

    ToggleButton::ToggleButton(ToggleButtonSeries * series, const Location & location, std::string text, std::function<void(GLfloat, GLfloat, int)> onSelectFunc) :
        ToggleComponent(location),
        TextObject(text),
        series(series),
        toggleButtonId(series->registerToggleButton(this)),
        onSelectFunc(onSelectFunc) {
    }

    ToggleButton::ToggleButton(ToggleButtonSeries * series, unsigned long toggleButtonId, const Location & location, std::string text, std::function<void(GLfloat, GLfloat, int)> onSelectFunc) :
        ToggleComponent(location),
        TextObject(text),
        series(series),
        toggleButtonId(toggleButtonId),
        onSelectFunc(onSelectFunc) {
        series->registerToggleButton(this, toggleButtonId);
    }

    ToggleButton::~ToggleButton(void) {
        if (series != NULL) {
            series->onDeleteToggleButton(this);
        }
    }


    bool ToggleButton::isToggleComponentSelected(void) const {
        if (series == NULL) return false;
        return series->selectedToggleButtonId == toggleButtonId;
    }

    void ToggleButton::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        // Don't notify the toggle button series, if the mouse button is not valid
        if (series != NULL && shouldChangeSelection(button)) {
            series->onToggleButtonClick(toggleButtonId, viewX, viewY, button);
        }
    }

    void ToggleButton::onToggleButtonSelect(GLfloat viewX, GLfloat viewY, int button) {
        onSelectFunc(viewX, viewY, button);
    }


    void ToggleButton::onDraw(void) {
        // Call the superclass onDraw method, so the location's state is updated correctly. See ToggleComponent.h.
        ToggleComponent::onDraw();

        getTopLevel()->theme->drawButton(location);
        getTopLevel()->theme->drawText(location, getText());
    }

    bool ToggleButton::shouldChangeSelection(int mouseButton) {
        return mouseButton == getMb();
    }
}
