/*
 * ToggleButtonSeries.h
 *
 *  Created on: 12 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_TOGGLEBUTTON_TOGGLEBUTTONSERIES_H_
#define RASCUI_COMPONENTS_TOGGLEBUTTON_TOGGLEBUTTONSERIES_H_

#include <GL/gl.h>
#include <cstddef>
#include <unordered_map>
#include <functional>

namespace rascUI {

    class ToggleButton;


    /**
     * ToggleButtonSeries controls a set of ToggleButton instances. Being connected to the same ToggleButtonSeries allows
     * only one of them to be selected at any one time.
     * When the user selects one of the ToggleButtons, it deselects any which are already selected.
     *
     * When a ToggleButton is created, it must be given a pointer to a ToggleButtonSeries.
     * Every ToggleButton is related to a single ToggleButtonSeries. A ToggleButtonSeries can be related to arbitrarily
     * many toggle buttons.
     *
     * Each ToggleButton within a ToggleButtonSeries has a unique ID. These can be assigned automatically, wherein they
     * are assigned incrementally in ascending order. They can also be assigned manually, in the ToggleButton's constructor,
     * to an ID not already existing, (see the constructor of ToggleButton for more info). If a ToggleButton is assigned
     * an ID which currently already exists, a warning is printed to stderr, and undefined behaviour WILL result.
     *
     * The user must choose whether a ToggleButtonSeries allows deselection. When a user clicks on a toggle button which is
     * already selected, if deselection is allowed, it will be unselected, otherwise not. If no toggle button is selected,
     * the special toggle button id: ToggleButtonSeries::NO_TOGGLE_BUTTON is used as the selected ID.
     *
     * By default, no toggle buttons in the series are selected. The user can initially set this with setSelectedId and
     * setSelectedButton.
     *
     * Upon creation of a ToggleButton, it is registered to a ToggleButtonSeries. When a ToggleButton is destroyed, it
     * is automatically unregistered from the series. If the currently selected toggle button is destroyed, selection
     * is automatically switched to NO_TOGGLE_BUTTON, regardless of whether deselection is allowed.
     *
     * Since an unordered_map is internally used to store the ToggleButtons within ToggleButtonSeries, ToggleButton
     * instances can be created (and registered to the series), and destroyed (and unregistered from the series),
     * arbitrarily many times, without affecting performance. IDs are automatically assigned each time.
     */
    class ToggleButtonSeries {

    public:
        /**
         * This represents the ID where no toggle button is currently selected.
         */
        const static unsigned long NO_TOGGLE_BUTTON;

        /**
         * This std::function (lambda statement) is called every time a new toggle button is selected.
         * This is called automatically in our overridable method: onChangeSelection.
         */
        std::function<void(unsigned long, ToggleButton *)> onChangeSelectionFunc;

    private:
        // Make ToggleButton a friend class so it can call our addToggleButton and removeToggleButton methods.
        friend class ToggleButton;

        /**
         * This stores whether we allow deselection.
         */
        const bool allowDeselect;

        /**
         * This stores the id of the next button we will add.
         */
        unsigned long idUpTo;

        /**
         * This stores the id of the button currently selected.
         * This IS NEVER a toggle button which does not exist, or is not available in this series.
         */
        unsigned long selectedToggleButtonId;

        /**
         * A map of all the toggle buttons associated with this series.
         * All buttons listed here *do* exist, since toggle buttons remove themselves from here automatically when they are destroyed.
         */
        std::unordered_map<unsigned long, ToggleButton *> toggleButtons;

        // Don't allow copying - that would mess up the pointers which toggle buttons have to us.
        ToggleButtonSeries(const ToggleButtonSeries & other);
        ToggleButtonSeries & operator = (const ToggleButtonSeries & other);

    public:

        /**
         * The main constructor for ToggleButtonSeries.
         *
         * allowDeselect controls whether this toggle button series should allow buttons to be deselected.
         * If allowDeselect is true, when a user clicks on an already selected button, it will become deselected, and no buttons will be pressed.
         * If allowDeselect is false, when a user clicks on an already selected button, nothing will happen.
         *
         * If allowDeselect is false, there are three circumstances that we will have a case that no buttons are selected:
         *   > If setSelectedButton/setSelectedId have not been called, or are explicitly set to NULL or NO_TOGGLE_BUTTON respectively.
         *   > If the user has not clicked on any buttons yet, AND setSelectedButton/setSelectedId have not been called
         *   > If the currently selected toggle button is deleted (removed) from the series.
         *
         * Here, like in RascUI::Button, we can specify a std::function (lambda statement), which is called each time selection changes.
         * The two std::function parameters will either be a pair of the id and toggle button just selected, or NO_TOGGLE_BUTTON and NULL
         * if selection has switched to nothing.
         * Note: This std::function is always called before the respective call to the ToggleButton's onToggleButtonSelect method.
         */
        ToggleButtonSeries(bool allowDeselect, std::function<void(unsigned long, ToggleButton *)> onChangeSelectionFunc = [](unsigned long toggleButtonId, ToggleButton * toggleButton){});
        virtual ~ToggleButtonSeries(void);

        /**
         * Returns the id of the button that is currently pressed down, or NO_TOGGLE_BUTTON if none are currently pressed.
         */
        inline unsigned long getSelectedId(void) const {
            return selectedToggleButtonId;
        }

        /**
         * Returns the currently selected toggle button, or NULL if none are currently selected.
         * The method getSelectedId should be preferred over this, since getting a pointer to the actual button requires
         * looking it up in our unordered_map of buttons.
         */
        ToggleButton * getSelectedToggleButton(void) const;

        /**
         * Returns the toggle button at the specified id.
         * If that ID is NO_TOGGLE_BUTTON, or is not assigned to this ToggleButtonSeries, NULL is returned.
         */
        ToggleButton * getToggleButtonAtId(unsigned long toggleButtonId) const;

        /**
         * This method sets which toggle button is selected, based on it's id. NO_TOGGLE_BUTTON can be provided to deselect
         * all toggle buttons.
         *
         * By default, this will not automatically call ToggleButtonSeries::onChangeSelection, but this can
         * be changed optionally with the callOnChangeSelection parameter.
         *
         * By default, this will not automatically call the newly selected toggle button's onToggleButtonSelect method either,
         * but this can be changed optionally with the callOnToggleButtonSelect parameter.
         *
         * NOTE: The button's onToggleButtonSelect will only be called if the button is a member of a top level container.
         *
         * NOTE: This ID MUST EXIST within the series, or this will cause undefined behaviour.
         */
        void setSelectedId(unsigned long toggleButtonId, bool callOnChangeSelection = false, bool callOnToggleButtonSelect = false);

        /**
         * This allows the user to change the currently selected button. Providing NULL as the button will make it so no buttons
         * are selected, so the selected button is NO_TOGGLE_BUTTON.
         *
         * By default, this will not automatically call ToggleButtonSeries::onChangeSelection, but this can
         * be changed optionally with the callOnChangeSelection parameter.
         *
         * By default, this will not automatically call the newly selected toggle button's onToggleButtonSelect method either,
         * but this can be changed optionally with the callOnToggleButtonSelect parameter.
         *
         * NOTE: The button's onToggleButtonSelect will only be called if the button is a member of a top level container.
         *
         * NOTE: This BUTTON MUST EXIST within the series, or this will cause undefined behaviour.
         *
         * NOTE: This is pretty much just shorthand for  setSelectedId(button->getToggleButtonId(), something, something)
         */
        void setSelectedButton(ToggleButton * button, bool callOnChangeSelection = false, bool callOnToggleButtonSelect = false);

    private:

        // ---------------------- Registering new toggle buttons -----------------------------

        /**
         * One of these are called by ToggleButton when ToggleButton is created.
         * If an id is picked manually, the next toggle button to be registered will have an id one greater than this.
         * Otherwise, ids are automatically picked ascending incrementally.
         * For more info on how the id assignments work, see the constructor for ToggleButton.
         */
        void registerToggleButton(ToggleButton * button, unsigned long id);
        unsigned long registerToggleButton(ToggleButton * button);

        // --------------------------- Deleting toggle buttons --------------------------------

        /**
         * This is called by a toggle button when it is deleted. This ensures that it gets removed
         * from the series.
         */
        void onDeleteToggleButton(ToggleButton * button);

        // ------------------------------ Normal actions --------------------------------------

        /**
         * This is called by our toggle buttons, when they are clicked on. This method will change selection
         * to the specified button. If the user clicks on the already selected button, and we are allowing
         * deselection, this will cause no toggle button to become selected.
         */
        void onToggleButtonClick(unsigned long toggleButtonId, GLfloat viewX, GLfloat viewY, int mouseButton);

    protected:

        /**
         * This overridable method is called each time a new toggle button is selected.
         *
         * The two parameters will either be a pair of the id and toggle button just selected, or NO_TOGGLE_BUTTON and NULL
         * if selection has switched to nothing.
         *
         * Note: This method is always called before the respective call to the ToggleButton's onToggleButtonSelect method.
         *
         * Like in Button, this automatically calls onChangeSelectionFunc, if this method is not overridden.
         */
        virtual void onChangeSelection(unsigned long toggleButtonId, ToggleButton * toggleButton);
    };
}

#endif
