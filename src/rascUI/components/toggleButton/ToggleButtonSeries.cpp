/*
 * ToggleButtonSeries.cpp
 *
 *  Created on: 12 Nov 2018
 *      Author: wilson
 */

#include "ToggleButtonSeries.h"

#include <iostream>  // (So we can print a warning about id assignment to std::cerr).

#include "ToggleButton.h"
#include "../../base/TopLevelContainer.h"

namespace rascUI {

    const unsigned long ToggleButtonSeries::NO_TOGGLE_BUTTON = (unsigned long)-1;

    ToggleButtonSeries::ToggleButtonSeries(bool allowDeselect, std::function<void(unsigned long, ToggleButton *)> onChangeSelectionFunc) :
        onChangeSelectionFunc(onChangeSelectionFunc),
        allowDeselect(allowDeselect),
        idUpTo(0),
        selectedToggleButtonId(NO_TOGGLE_BUTTON),
        toggleButtons() {
    }

    ToggleButtonSeries::~ToggleButtonSeries(void) {
        // Nullify all our toggle button's pointers to the series, if the series gets destroyed
        // before the toggle buttons.
        for (std::pair<const unsigned long, ToggleButton *> & pair : toggleButtons) {
            pair.second->series = NULL;
        }
    }

    ToggleButton * ToggleButtonSeries::getSelectedToggleButton(void) const {
        if (selectedToggleButtonId == NO_TOGGLE_BUTTON) {
            return NULL;
        } else {
            return toggleButtons.at(selectedToggleButtonId);
        }
    }

    ToggleButton * ToggleButtonSeries::getToggleButtonAtId(unsigned long toggleButtonId) const {
        if (toggleButtonId == NO_TOGGLE_BUTTON) {
            return NULL;
        } else {
            std::unordered_map<unsigned long, ToggleButton *>::const_iterator iter = toggleButtons.find(toggleButtonId);
            if (iter == toggleButtons.end()) {
                return NULL;
            } else {
                return iter->second;
            }
        }
    }

    void ToggleButtonSeries::setSelectedId(unsigned long newId, bool callOnChangeSelection, bool callOnToggleButtonSelect) {
        unsigned long oldId = getSelectedId();

        // Only do something if the new id is different.
        if (newId != oldId) {

            // Get the actual button, from the map.
            ToggleButton * newToggleButton;
            if (newId != NO_TOGGLE_BUTTON) {
                newToggleButton = toggleButtons.at(newId); // Use .at instead of operator[], so that if newId
            } else {                                       // does not exist, we get an exception immediately.
                newToggleButton = NULL;
            }

            // Set the new ID.
            selectedToggleButtonId = newId;

            // If the user wants us to, call onChangeSelection.
            if (callOnChangeSelection) {
                onChangeSelection(newId, newToggleButton);
            }

            // If the user wants us to, call the respective button's onToggleButtonSelect. Also call repaint.
            // Both can only be done if the new toggle button exists, and has a top level container.
            if (newId != NO_TOGGLE_BUTTON && newToggleButton->getTopLevel() != NULL) {
                newToggleButton->repaint();
                if (callOnToggleButtonSelect) {
                    newToggleButton->onFakeToggleButtonSelect(newToggleButton->getMb());
                }
            }

            // Now, if there used to be a button selected, repaint that also.
            if (oldId != NO_TOGGLE_BUTTON) {
                ToggleButton * oldToggleButton = toggleButtons[oldId];
                if (oldToggleButton->getTopLevel() != NULL) oldToggleButton->repaint();
            }
        }
    }


    void ToggleButtonSeries::setSelectedButton(ToggleButton * button, bool callOnChangeSelection, bool callOnToggleButtonSelect) {
        setSelectedId(button->getToggleButtonId(), callOnChangeSelection, callOnToggleButtonSelect);
    }

    // ---------------------- Registering new toggle buttons -----------------------------

    void ToggleButtonSeries::registerToggleButton(ToggleButton * button, unsigned long id) {
        if (toggleButtons.find(id) != toggleButtons.end()){
            std::cerr << "WARNING: Toggle button with id: " << id << ", registered to ToggleButtonSeries. This ID already currently exists within the series, so UNDEFINED BEHAVIOUR WILL RESULT.\n";
        }
        toggleButtons[id] = button;
        if (id >= idUpTo) {
            idUpTo = id + 1;
        }
    }

    unsigned long ToggleButtonSeries::registerToggleButton(ToggleButton * button) {
        unsigned long idUsed = idUpTo;
        registerToggleButton(button, idUpTo);
        return idUsed;
    }

    // --------------------------- Deleting toggle buttons --------------------------------

    void ToggleButtonSeries::onDeleteToggleButton(ToggleButton * button) {
        // Note: Do no repainting or anything else special here, since the button will no
        // longer exist.
        if (button->getToggleButtonId() == selectedToggleButtonId) {
            selectedToggleButtonId = NO_TOGGLE_BUTTON;
        }
        toggleButtons.erase(button->getToggleButtonId());
    }

    // ------------------------------ Normal actions --------------------------------------

    void ToggleButtonSeries::onToggleButtonClick(unsigned long toggleButtonId, GLfloat viewX, GLfloat viewY, int mouseButton) {

        // If the user clicked on the one already selected, deselect it if we are supposed to.
        if (toggleButtonId == getSelectedId()) {
            if (allowDeselect) {
                setSelectedId(NO_TOGGLE_BUTTON, true, true);
            }

        // Otherwise just run the setSelectedId method appropriately.
        } else {
            setSelectedId(toggleButtonId, true, true);
        }
    }


    void ToggleButtonSeries::onChangeSelection(unsigned long toggleButtonId, ToggleButton * toggleButton) {
        onChangeSelectionFunc(toggleButtonId, toggleButton);
    }
}













