/*
 * ScrollPane.h
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_SCROLL_SCROLLPANE_H_
#define RASCUI_COMPONENTS_SCROLL_SCROLLPANE_H_

#include "AnimatedSegmentContainer.h"
#include "../../base/Container.h"
#include "ScrollBar.h"

namespace rascUI {

    /**
     * ScrollPane is a fairly simple class, which "glues together" a ScrollBar
     * and an AnimatedSegmentContainer. The scroll bar is put on the right
     * hand side for vertical orientation, and put at the bottom for horizontal
     * orientation.
     *
     * When using ScrollPane, any components which are intended to be scrolled,
     * must be added to the internal AnimatedSegmentContainer (the contents
     * member), rather than the scroll pane itself. For example:
     *
     * Button button;
     * ScrollPane scrPane(...);
     * scrPane.add(&button);          // Don't do this - it will result in the button being plopped over the top of the scroll pane.
     * scrPane.contents.add(&button); // Do this: the button will be scrolled up and down, as is (probably) intended.
     */
    class ScrollPane : public Container {
    private:
        // ------------------- Scroll pane segment container class --------------------------
        /**
         * Custom subclass of AnimatedSegmentContainer, which notifies us when
         * the segment position changes.
         */
        class ScrollPaneSegmentContainer : public AnimatedSegmentContainer {
        private:
            ScrollPane * scrollPane;
        public:
            ScrollPaneSegmentContainer(const Location & location, GLfloat segmentSize, bool drawBackground, bool vertical, ScrollPane * scrollPane);
            virtual ~ScrollPaneSegmentContainer(void);

            // This is overridden to return the scroll distance defined by our ScrollPane.
            virtual OffVal<size_t, GLfloat> getScrollDistance(ScrollAction action) override;

            // This is used to update the scroll bar when we move.
            virtual void onChangeSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos) override;
        };

        // ------------------- Scroll pane scroll bar class --------------------------------
        /**
         * A custom subclass of ScrollBar, used internally within ScrollPane. ScrollPane
         * uses this to control its internal segment container.
         */
        class ScrollPaneScrollBar : public ScrollBar {
        private:
            ScrollPane * scrollPane;
        public:
            ScrollPaneScrollBar(const Location & location, Theme * theme, bool vertical, bool startBorder, bool endBorder, ScrollPane * scrollPane);
            ~ScrollPaneScrollBar(void);
            virtual GLfloat getViewSize(void) const override;
            virtual size_t getSegmentCount(void) const override;
            virtual OffVal<size_t, GLfloat> getViewPosition(void) const override;
            virtual OffVal<size_t, GLfloat> getMaximumViewPosition(void) const override;
            virtual void onScroll(ScrollAction action) override;
            virtual void onScrollMove(OffVal<size_t, GLfloat> position) override;
        };

        // ---------------------------------------------------------------------------------

        // Our scroll bar.
        ScrollPaneScrollBar scrollBar;
    public:

        /**
         * The SegmentContainer that users of the ScrollPane should add components to.
         */
        ScrollPaneSegmentContainer contents;

        /**
         * By default, drawBackground is true. If the user wants a ScrollPane which does
         * not draw a scroll contents background, then they must specify false.
         *
         * By default, the segment size is COUNT_ONEBORDER_X(1) or COUNT_ONEBORDER_Y(1)
         * depending on orientation, (i.e: the normal component width/height plus one border).
         */
        ScrollPane(Theme * theme, const Location & location = Location(), bool drawBackground = true, bool vertical = true, bool startBorder = true, bool endBorder = true);
        ScrollPane(Theme * theme, const Location & location, GLfloat segmentSize, bool drawBackground = true, bool vertical = true, bool startBorder = true, bool endBorder = true);
        virtual ~ScrollPane(void);

    protected:
        // Just like SegmentContainer, here we return an area which is slightly larger than our actual area.
        virtual Rectangle getBoundedArea(void) const override;

    public:
        // --------------------------- Methods which subclasses can implement -------------------------

        /**
         * Returns the scroll distance, used by our internal segment container
         * and scroll bar. This can be overridden to provide custom scroll
         * distances. By default, this method uses the scroll distances
         * defined by the parent class of our segment container.
         */
        virtual OffVal<size_t, GLfloat> getScrollDistance(ScrollAction action);
    };
}

#endif
