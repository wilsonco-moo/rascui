/*
 * ScrollBar.cpp
 *
 *  Created on: 26 May 2020
 *      Author: wilson
 */

#include "ScrollBar.h"

#include <algorithm>

#include "../../base/TopLevelContainer.h"
#include "../../util/Layout.h"
#include "../../base/Theme.h"

namespace rascUI {

    #define LAYOUT_THEME theme

    // Picks either the vertical or horizontal version of a location.
    #define HV_LOCATION(loc) \
        (vertical ? Location(loc##_V) : Location(loc##_H))

    // Scroll puck's location is shifted depending on startBorder, filling width of scroll bar.
    #define SCROLL_PUCK_LOC_V                    \
        MOVE_Y(startBorder * UI_BORDER,          \
         PIN_T(0.0f,                             \
          GEN_FILL                               \
        ))
    #define SCROLL_PUCK_LOC_H                    \
        MOVE_X(startBorder * UI_BORDER,          \
         PIN_L(0.0f,                             \
          GEN_FILL                               \
        ))

    // =================== Scroll pane puck class ========================================

    ScrollBar::ScrollPanePuck::ScrollPanePuck(ScrollBar * scrollBar, Theme * theme, bool vertical, bool startBorder, bool endBorder) :
        ResponsiveComponent(HV_LOCATION(SCROLL_PUCK_LOC)),
        scrollBar(scrollBar),
        // Note: Always use height, even for horizontal mode. This means that horizontal scroll bars
        //       actually look exactly like 90 degree rotated vertical scroll bars.
        startMargin(theme->normalComponentHeight + theme->uiBorder * (startBorder ? 2.0f : 1.0f)),
        totalMargin(startMargin + theme->normalComponentHeight + theme->uiBorder * (endBorder ? 2.0f : 1.0f)),
        minSize(theme->normalComponentHeight) {
    }

    ScrollBar::ScrollPanePuck::~ScrollPanePuck(void) {
    }

    void ScrollBar::ScrollPanePuck::onDraw(void) {
        getTopLevel()->theme->drawScrollPuck(location, scrollBar->isVertical());
    }

    void ScrollBar::ScrollPanePuck::recalculate(const Rectangle & parentSizeRect) {
        // Find properties which depend on orientation. Get the scale from top level, as the one in our location might be out of date, (if the scale has changed).
        bool vertical = scrollBar->isVertical();
        GLfloat scale, parentSize;
        if (vertical) {
            scale = getTopLevel()->getYScale();
            parentSize = parentSizeRect.height;
        } else {
            scale = getTopLevel()->getXScale();
            parentSize = parentSizeRect.width;
        }

        // Make it seem like there is always at least one component, to avoid rounding issues.
        size_t componentCount = scrollBar->getSegmentCount();
        componentCount = (componentCount == 0 ? 1 : componentCount);

        // Work out the proportion of the scoll pane that the view covers vertically, make sure it is not greater than one.
        GLfloat coverage = std::min(scrollBar->getViewSize() / componentCount, 1.0f);

        // Work out the position within the scroll pane, as a proportion of the total height.
        // Make sure it does not lie past the end of the area.
        GLfloat position = scrollBar->getViewPosition().toSingleValue<long double>() / componentCount;
        position = std::min(position, 1.0f - coverage);

        // Work out the actual amount of space we can move.
        GLfloat moveableSpace = (parentSize - (totalMargin + minSize) * scale) / scale;

        // Work out the final position and size of the scroll puck, from these values.
        GLfloat puckPosition = startMargin + (moveableSpace * position),
                puckSize     = minSize     + (moveableSpace * coverage);

        // Store in the appropriate place in location.
        if (vertical) {
            location.position.y      = puckPosition;
            location.position.height = puckSize;
        } else {
            location.position.x      = puckPosition;
            location.position.width  = puckSize;
        }

        // Call our superclass recalculate method, to update our location's cache.
        ResponsiveComponent::recalculate(parentSizeRect);
    }

    void ScrollBar::ScrollPanePuck::onMousePress(GLfloat viewX, GLfloat viewY, int button) {
        ResponsiveComponent::onMousePress(viewX, viewY, button);
        initialMousePos = (scrollBar->isVertical() ? viewY : viewX);
        initialSegmentPos = scrollBar->getViewPosition();
    }

    void ScrollBar::ScrollPanePuck::onMouseDrag(GLfloat viewX, GLfloat viewY, int button) {
        ResponsiveComponent::onMouseDrag(viewX, viewY, button);
        if (button == getMb()) {

            // Get the properties which depend on orientation.
            GLfloat parentSize, puckSize, scale, mousePos;
            if (scrollBar->isVertical()) {
                parentSize = getParent()->location.cache.height;
                puckSize = location.cache.height;
                scale = location.yScale;
                mousePos = viewY;
            } else {
                parentSize = getParent()->location.cache.width;
                puckSize = location.cache.width;
                scale = location.xScale;
                mousePos = viewX;
            }

            // Work out the space moveable vertically (measured in true view pixels).
            GLfloat moveableSpace = parentSize - puckSize - (totalMargin * scale);
            // Work out how much we have changed, in multiples of the moveable space.
            GLfloat positionChange = (mousePos - initialMousePos) / moveableSpace;

            OffVal<size_t, GLfloat> newSegmentPos = initialSegmentPos,
                                    maximum = scrollBar->getMaximumViewPosition();
            if (positionChange > 0.0f) {
                // For positive changes, add to initial segment position, avoiding going over max.
                OffVal<size_t, GLfloat> change = maximum;
                change.multiplyByFloat<long double>(positionChange);
                newSegmentPos = std::min(maximum, newSegmentPos + change);
            } else {
                // For negative changes, zero round subtract change from initial segment position.
                maximum.multiplyByFloat<long double>(-positionChange);
                newSegmentPos.zeroRoundSubtract(maximum);
            }

            if (newSegmentPos != scrollBar->getViewPosition()) {
                scrollBar->onScrollMove(newSegmentPos);
            }
        }
    }
    void ScrollBar::ScrollPanePuck::onMouseRelease(GLfloat viewX, GLfloat viewY, int button) {
        ResponsiveComponent::onMouseRelease(viewX, viewY, button);
        onMouseDrag(viewX, viewY, button);
    }
    NavigationAction ScrollBar::ScrollPanePuck::getRequiredKeyboardActionPress(NavigationAction action) {
        if (action == NavigationAction::enter) return NavigationAction::noAction;
        return ResponsiveComponent::getRequiredKeyboardActionPress(action);
    }
    NavigationAction ScrollBar::ScrollPanePuck::getRequiredKeyboardActionRelease(NavigationAction action) {
        if (action == NavigationAction::enter) return NavigationAction::lockSelection;
        return ResponsiveComponent::getRequiredKeyboardActionRelease(action);
    }
    NavigationAction ScrollBar::ScrollPanePuck::onSelectionLockKeyPress(NavigationAction defaultAction, int key, bool special) {
        switch(defaultAction) {
        case NavigationAction::enter:
            return NavigationAction::unlockSelection;
        case NavigationAction::nextComponent:
            scrollBar->onScroll(ScrollAction::scrollDown);
            break;
        case NavigationAction::previousComponent:
            scrollBar->onScroll(ScrollAction::scrollUp);
            break;
        case NavigationAction::nextContainer:
            scrollBar->onScroll(ScrollAction::pageDown);
            break;
        case NavigationAction::previousContainer:
            scrollBar->onScroll(ScrollAction::pageUp);
            break;
        default:
            break;
        }
        return NavigationAction::noAction;
    }

    // ============================ Scroll background class ==============================

    ScrollBar::ScrollBarBackground::ScrollBarBackground(const Location & location) :
        Component(location) {
    }
    ScrollBar::ScrollBarBackground::~ScrollBarBackground(void) {
    }
    void ScrollBar::ScrollBarBackground::onDraw(void) {
        getTopLevel()->theme->drawScrollBarBackground(location);
    }
    bool ScrollBar::ScrollBarBackground::shouldRespondToKeyboard(void) const {
        return false;
    }

    // =========================== Scroll up button class ================================

    ScrollBar::ScrollUpButton::ScrollUpButton(const Location & location, ScrollBar * scrollBar) :
        ResponsiveComponent(location),
        scrollBar(scrollBar) {
    }
    ScrollBar::ScrollUpButton::~ScrollUpButton(void) {
    }
    void ScrollBar::ScrollUpButton::onDraw(void) {
        getTopLevel()->theme->drawScrollUpButton(location, scrollBar->isVertical());
    }
    void ScrollBar::ScrollUpButton::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        scrollBar->onScroll(ScrollAction::scrollUp);
    }

    // =========================== Scroll Down button class ==============================

    ScrollBar::ScrollDownButton::ScrollDownButton(const Location & location, ScrollBar * scrollBar) :
        ResponsiveComponent(location),
        scrollBar(scrollBar) {
    }
    ScrollBar::ScrollDownButton::~ScrollDownButton(void) {
    }
    void ScrollBar::ScrollDownButton::onDraw(void) {
        getTopLevel()->theme->drawScrollDownButton(location, scrollBar->isVertical());
    }
    void ScrollBar::ScrollDownButton::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        scrollBar->onScroll(ScrollAction::scrollDown);
    }

    // ===================================================================================

    // Leave border before/after scroll background depending on startBorder and endBorder status.
    #define SCROLL_BACKGROUND_LOC_V                                     \
        SUB_MARGIN_TB(startBorder * UI_BORDER, endBorder * UI_BORDER,   \
         GEN_FILL                                                       \
        )
    #define SCROLL_BACKGROUND_LOC_H                                     \
        SUB_MARGIN_LR(startBorder * UI_BORDER, endBorder * UI_BORDER,   \
         GEN_FILL                                                       \
        )

    // Scroll up/down buttons are pinned within scroll background location.
    // This intentionally always uses UI_BHEIGHT, even for horizontal. This keeps
    // horizontal scroll bars looking the same as vertical ones.
    #define SCROLL_UP_LOC_V \
        PIN_T(UI_BHEIGHT, SCROLL_BACKGROUND_LOC_V)
    #define SCROLL_UP_LOC_H \
        PIN_L(UI_BHEIGHT, SCROLL_BACKGROUND_LOC_H)

    #define SCROLL_DOWN_LOC_V \
        PIN_B(UI_BHEIGHT, SCROLL_BACKGROUND_LOC_V)
    #define SCROLL_DOWN_LOC_H \
        PIN_R(UI_BHEIGHT, SCROLL_BACKGROUND_LOC_H)

    ScrollBar::ScrollBar(const Location & location, Theme * theme, bool vertical, bool startBorder, bool endBorder) :
        Container(location),
        vertical(vertical),
        background(HV_LOCATION(SCROLL_BACKGROUND_LOC)),
        scrollUpButton(HV_LOCATION(SCROLL_UP_LOC), this),
        scrollPuck(this, theme, vertical, startBorder, endBorder),
        scrollDownButton(HV_LOCATION(SCROLL_DOWN_LOC), this) {

        add(&background);
        add(&scrollUpButton);
        add(&scrollPuck);
        add(&scrollDownButton);
    }

    ScrollBar::~ScrollBar(void) {
    }

    void ScrollBar::onMouseRelease(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getTopLevel()->bindings->mouseScrollUpButton) {
            onScroll(ScrollAction::scrollUp);
        } else if (button == getTopLevel()->bindings->mouseScrollDownButton) {
            onScroll(ScrollAction::scrollDown);
        }
    }

    void ScrollBar::onScroll(ScrollAction action) {
        scrollPuck.recalculate(location.cache);
        repaint();
    }

    void ScrollBar::onScrollMove(OffVal<size_t, GLfloat> position) {
        scrollPuck.recalculate(location.cache);
        repaint();
    }
}
