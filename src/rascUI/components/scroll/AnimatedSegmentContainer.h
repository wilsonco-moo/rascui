/*
 * AnimatedSegmentContainer.h
 *
 *  Created on: 26 May 2020
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_SCROLL_ANIMATEDSEGMENTCONTAINER_H_
#define RASCUI_COMPONENTS_SCROLL_ANIMATEDSEGMENTCONTAINER_H_

#include "SegmentContainer.h"

namespace rascUI {

    /**
     * This is a custom variant of SegmentContainer, providing (optional) smooth
     * motion and animation when moving.
     *
     * When smoothMoveSpeed is set to zero, this will act exactly the same as
     * an ordinary SegmentContainer.
     */
    class AnimatedSegmentContainer : public SegmentContainer {
    private:
        // The target segment position which we are moving towards.
        OffVal<size_t, GLfloat> targetSegmentPos;

    public:
        /**
         * This is the speed at which smooth scrolling motion should happen. This is measured as:
         *  > The proportion by which the view approaches its destination, per second.
         * So the difference between the actual position, and the target position is multiplied by
         * smoothMoveSpeed each second. (This is done each frame, using the elapsed time and some maths).
         *
         * smoothMoveSpeed = 0
         *   > This will mean that all scrolling is done instantly, with no animation.
         *
         * Small values of smoothMoveSpeed
         *   > This will result in very fast scrolling animations.
         *
         * Large values of smoothMoveSpeed
         *   > This will result in very slow scrolling animations.
         *
         * smoothMoveSpeed = 1
         *   > No scrolling will ever occur? Is this actually useful?
         *
         * By default this is 0.05.
         */
        GLfloat smoothMoveSpeed;

        AnimatedSegmentContainer(const Location & location, GLfloat segmentSize, bool drawBackground, bool vertical = true);
        virtual ~AnimatedSegmentContainer(void);

    private:
        // This is called at the start of any call to onDraw or boundedRepaint. This updates the segment
        // position of the scroll pane, to carry out any smooth animation, then sets the appropriate
        // components to recalculate and repaint.
        void updateSegmentPosFromTarget(void);
    protected:
        // Overridden methods (from SegmentContainer).

        // These are overridden, such that updateSegmentPosFromTarget always gets called by each of them.
        virtual void boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) override;
        virtual void onDraw(void) override;

        // This is modified from the SegmentContainer implementation, such that the target segment position
        // is changed instead of the segment position, from keyboard selection.
        virtual void onChildKeyboardSelect(Component * component) override;

        // This is modified from the SegmentContainer implementation, by setting target segment position
        // instead of real segment position. Scrolling is also done using the distances specified by getScrollDistance.
        virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button) override;

        // This is also modified from the SegmentContainer implementation, to adjust target segment position instead.
        virtual bool adjustViewBounds(void);

        // ------------------------------ Accessor methods --------------------------------------------
    public:
        /**
         * Returns the current segment position.
         */
        inline const OffVal<size_t, GLfloat> & getTargetSegmentPos(void) const {
            return targetSegmentPos;
        }

        // -------------------------- Methods to set target segment position ------------------------

        /**
         * Sets the target segment position: This will result in us
         * slowly scrolling to the specified position.
         * NOTE: If changing the segment position of an AnimatedSegmentContainer is required
         *       immediately, FIRST call setTargetSegmentPos, THEN immediately afterwards
         *       call setSegmentPos. The call to setSegmentPos will immediately run a
         *       recalculate and call to onChangeSegmentPos, updating everything properly.
         */
        void setTargetSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos);

        /**
         * Sets the target segment position to the minimum - this results in the view being
         * slowly moved to either the top or left most position, depending on orientation.
         */
        void scrollSlowlyToStart(void);

        /**
         * Sets the target segment position to the maximum - this results in the view being
         * slowly moved to either the bottom or right most position, depending on orientation.
         */
        void scrollSlowlyToEnd(void);

        /**
         * Scrolls the view (slowly) up by the specified amount.
         */
        void scrollSlowlyUpBy(const OffVal<size_t, GLfloat> & distance);

        /**
         * Scrolls the view (slowly) down by the specified amount.
         */
        void scrollSlowlyDownBy(const OffVal<size_t, GLfloat> & distance);
    };
}

#endif
