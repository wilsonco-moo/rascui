/*
 * SegmentContainer.cpp
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#include "SegmentContainer.h"

#include <algorithm>
#include <iostream>
#include <cstdlib>

#include "../../base/TopLevelContainer.h"
#include "../../base/Theme.h"

namespace rascUI {

    /**
     * Gets the following two values:
     * scaledSize:   The (UI scale adjusted) size of each segment.
     * locationSize: The (UI scale adjusted) size of the view area.
     *               It is ensured that locationSize is NOT below zero,
     *               even if our width/height *is* negative.
     */
    #define GET_LOCATION_AND_SCALED_SIZE                          \
        GLfloat scaledSize, locationSize;                         \
        if (vertical) {                                           \
            scaledSize = segmentSize * location.yScale;           \
            locationSize = std::max(location.cache.height, 0.0f); \
        } else {                                                  \
            scaledSize = segmentSize * location.xScale;           \
            locationSize = std::max(location.cache.width, 0.0f);  \
        }

    SegmentContainer::SegmentContainer(const Location & location, GLfloat segmentSize, bool drawBackground, bool vertical) :
        Container(location),
        vertical(vertical),
        segmentSize(segmentSize),
        segmentPos(0, 0.0f),
        componentsBySegmentPos(),
        componentPositions(),
        firstSegmentCache(0),
        lastSegmentCache(0),
        firstFullyVisibleSegmentCache(0),
        lastFullyVisibleSegmentCache(0),
        drawBackground(drawBackground){
    }

    SegmentContainer::~SegmentContainer(void) {
    }

    void SegmentContainer::internalRecalculate(void) {
        // ------------------------ Update the segment id cache values ----------------------

        // If we have no components, set both the cache values to zero and do nothing.
        if (componentsBySegmentPos.empty()) {
            firstSegmentCache = 0;
            lastSegmentCache = 0;
            firstFullyVisibleSegmentCache = 0;
            lastFullyVisibleSegmentCache = 0;
            return;
        }

        GET_LOCATION_AND_SCALED_SIZE

        // Total component count, and position of the bottom of the view.
        size_t componentCount = componentsBySegmentPos.size();
        OffVal<size_t, GLfloat> viewBottom = segmentPos + OffVal<size_t, GLfloat>(locationSize / scaledSize);

        // -------------- Work out the segment id cache ---------------------

        // Work out the first and last segment ids that we will need to look at.
        // Note: This is not actually the last one, but one after the last one.
        size_t first = segmentPos.getBase(),
               last  = viewBottom.ceilBase();

        // Make sure neither first nor last are larger than number of components.
        first = std::min(componentCount, first); // This will only be equal to the size, if we have run off the end vertically.
        last = std::min(componentCount, last); // This can be one after the last component.

        firstSegmentCache = first;
        lastSegmentCache = last;

        // -------------- Work out the fully visible segment id cache ----------------

        // Work out the first and last segment ids that are FULLY VISIBLE.
        if (segmentPos >= 1.0f) {
            first = segmentPos.getBase() + 1;
        } else {
            first = segmentPos.ceilBase();
        }
        last = viewBottom.getBase();

        // Make sure neither first nor last are larger than number of components.
        first = std::min(componentCount, first); // This will only be equal to the size, if we have run off the end vertically.
        last = std::min(componentCount, last); // This can be one after the last component.

        firstFullyVisibleSegmentCache = first;
        lastFullyVisibleSegmentCache = last;

        // ------------------------ Do the recalculation ---------------------------------

        if (vertical) {
            // Generate the rectangle for the first segment. Offset the y position using the segment position.
            Rectangle rect(location.cache.x, location.cache.y - (segmentPos.getOffset()) * scaledSize, location.cache.width, scaledSize);

            // Recalculate each visible child component. The y position is the only thing to change between segments.
            for (size_t segmentId = firstSegmentCache; segmentId < lastSegmentCache; segmentId++) {
                childRecalculate(componentsBySegmentPos[segmentId], rect);
                rect.y += scaledSize;
            }

        } else {
            // Generate the rectangle for the first segment. Offset the x position using the segment position.
            Rectangle rect(location.cache.x - (segmentPos.getOffset()) * scaledSize, location.cache.y, scaledSize, location.cache.height);

            // Recalculate each visible child component. The x position is the only thing to change between segments.
            for (size_t segmentId = firstSegmentCache; segmentId < lastSegmentCache; segmentId++) {
                childRecalculate(componentsBySegmentPos[segmentId], rect);
                rect.x += scaledSize;
            }
        }

        // Ensure that we get repainted next time, since we recalculated our child component positions.
        repaint();
    }

    void SegmentContainer::onAdd(Component * component) {
        if (getLastComponent() != component) {
            std::cerr << "CRITICAL ERROR: SegmentContainer: " << "Adding components out of order is not supported, please do NOT use addBefore or addAfter.\n";
            exit(EXIT_FAILURE);
        }
        componentPositions[component] = componentsBySegmentPos.size();
        componentsBySegmentPos.push_back(component);
    }

    void SegmentContainer::onRemove(Component * component) {
        // Generate an iterator from within the component -> position map
        std::unordered_map<Component *, size_t>::iterator mapIter = componentPositions.find(component);
        // Generate an iterator from within the components vector
        std::vector<Component *>::iterator vectorIter = componentsBySegmentPos.begin() + mapIter->second;

        // Erase our component from both the vector and the map.
        componentsBySegmentPos.erase(vectorIter);
        componentPositions.erase(mapIter);

        // Subtract one from the segment position of all components after this one, in the component -> position map.
        while(vectorIter != componentsBySegmentPos.end()) {
            componentPositions[*(vectorIter++)]--;
        }
        
        // Since all components below need to now move up, and since component does not do this
        // automatically for remove operations, tell the top level container to recalculate it's stuff next frame.
        if (getTopLevel() != NULL) {
            getTopLevel()->triggerRecalculateNextFrame();
        }
        
        // That recalculate won't happen until next time we're drawn. But, in the meantime methods like addIfMouseOver
        // may run, and will need to use the segment id cache. Since we don't want to have to do a full recalculate
        // *every* time a component is removed, just make sure none of the cache values are beyond the end of the
        // component list. Otherwise, things like addIfMouseOver will crash if called before the next time we're drawn -
        // (e.g: scroll pane is updated when user clicks mouse, then mouse is released immediately before drawing happens).
        const size_t newComponentCount = componentsBySegmentPos.size();
        firstSegmentCache = std::min(newComponentCount, firstSegmentCache);
        lastSegmentCache = std::min(newComponentCount, lastSegmentCache);
        firstFullyVisibleSegmentCache = std::min(newComponentCount, firstFullyVisibleSegmentCache);
        lastFullyVisibleSegmentCache = std::min(newComponentCount, lastFullyVisibleSegmentCache);
    }

    void SegmentContainer::recalculate(const Rectangle & parentSize) {
        // First we must recalculate our own size. This can be done by calling the Component super method.
        Component::recalculate(parentSize);
        // Next, if the view area has not been changed by adjustViewBounds, call internalRecalculate.
        // If the view area *was* changed, internalRecalculate will have already been called, so it is
        // not needed.
        if (adjustViewBounds()) {
            internalRecalculate();
        }
    }

    void SegmentContainer::onDraw(void) {
        if (drawBackground) {
            getTopLevel()->theme->drawScrollContentsBackground(location);
        }
        // Loop through each visible segment.
        for (size_t segmentId = firstSegmentCache; segmentId < lastSegmentCache; segmentId++) {
            // Draw it.
            Component * comp = componentsBySegmentPos[segmentId];
            if (comp->location.getState() != State::invisible) {
                childOnDraw(comp);
            }
        }
    }

    Rectangle SegmentContainer::getBoundedArea(void) const {
        if (vertical) {
            GLfloat sgSize = segmentSize * location.yScale;
            return Rectangle(location.cache.x, location.cache.y - sgSize, location.cache.width, location.cache.height + sgSize * 2.0f);
        } else {
            GLfloat sgSize = segmentSize * location.xScale;
            return Rectangle(location.cache.x - sgSize, location.cache.y, location.cache.width + sgSize * 2.0f, location.cache.height);
        }
    }

    void SegmentContainer::boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) {

        // First get the bounded area - the repaint area which we are assumed to take up.
        Rectangle boundedArea = getBoundedArea();

        // If we explicitly want repainting, or we overlap the bounding box area
        if (wantsRepaint.find(this) != wantsRepaint.end() || boundingBox.overlaps(boundedArea)) {
            onDraw();
            boundingBox += boundedArea;
            return;
        }

        // If we don't overlap the bounding box, and none of our child components want repainting, do nothing.
        if (childWantsRepaint.find(this) == childWantsRepaint.end()) {
            return;
        }

        // Now, we can assume that at least one child component wants repainting, but nothing already overlaps us,
        // so just run boundedRepaint for all visible child components.
        // THIS IS MODIFIED, compared to Component::boundedRepaint, since here we only iterate through the
        // segments which are currently visible.
        for (size_t segmentId = firstSegmentCache; segmentId < lastSegmentCache; segmentId++) {
            Component * component = componentsBySegmentPos[segmentId];

            if (component->location.getState() != State::invisible) {
                childBoundedRepaint(component, boundingBox, wantsRepaint, childWantsRepaint);
            }
        }
    }

    void SegmentContainer::addIfMouseOver(GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver) {
        // If the mouse is over us, and we are visible:
        if (location.cache.contains(viewX, viewY) && isVisible()) {
            mouseOver->insert(this);
            // Loop through each visible segment.
            for (size_t segmentId = firstSegmentCache; segmentId < lastSegmentCache; segmentId++) {
                callAddIfMouseOverChildComponent(componentsBySegmentPos[segmentId], viewX, viewY, mouseOver);
            }
        }
    }

    void SegmentContainer::onChildKeyboardSelect(Component * component) {
        // Set the segment position such that the child component is within view.
        setSegmentPos(getNearestSegmentPos(getChildSegmentId(component)));
        Container::onChildKeyboardSelect(component);
    }

    void SegmentContainer::onMouseRelease(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getTopLevel()->bindings->mouseScrollUpButton) {
            scrollUpBy(getScrollDistance(ScrollAction::scrollUp));
        } else if (button == getTopLevel()->bindings->mouseScrollDownButton) {
            scrollDownBy(getScrollDistance(ScrollAction::scrollDown));
        }
    }

    void SegmentContainer::onChangeSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos) {
    }

    bool SegmentContainer::adjustViewBounds(void) {
        OffVal<size_t, GLfloat> maxSegmentPos = getMaximumSegmentPos();
        if (segmentPos > maxSegmentPos) {
            setSegmentPos(maxSegmentPos);
            return false;
        } else {
            return true;
        }
    }

    OffVal<size_t, GLfloat> SegmentContainer::getScrollDistance(ScrollAction action) {
        // For pageUp/pageDown, for large (ish) view sizes, use two segments less than view height.
        if (action == ScrollAction::pageUp || action == ScrollAction::pageDown) {
            GET_LOCATION_AND_SCALED_SIZE
            OffVal<size_t, GLfloat> viewSize(locationSize / scaledSize);
            if (viewSize.getBase() >= 3) {
                return OffVal<size_t, GLfloat>(viewSize.getBase() - 2, viewSize.getOffset());
            }
        }
        // Otherwise (for pageUp/pageDown in small views, or scrollUp/scrollDown), just use one segment.
        return OffVal<size_t, GLfloat>(1, 0.0f);
    }

    OffVal<size_t, GLfloat> SegmentContainer::getMaximumSegmentPos(void) const {
        GET_LOCATION_AND_SCALED_SIZE
        // Zero round subtract view size (in segments) from total size of area.
        OffVal<size_t, GLfloat> pos(componentsBySegmentPos.size(), 0.0f);
        pos.zeroRoundSubtract(OffVal<size_t, GLfloat>(locationSize / scaledSize));
        return pos;
    }

    OffVal<size_t, GLfloat> SegmentContainer::getNearestSegmentPos(size_t segmentId) const {
        GET_LOCATION_AND_SCALED_SIZE
        OffVal<size_t, GLfloat> viewSize(locationSize / scaledSize);

        // Work out *just* the offset of the segment position of the bottom of the view.
        GLfloat viewBottomOffset = segmentPos.getOffset() + viewSize.getOffset();
        viewBottomOffset -= (int)viewBottomOffset;

        // Work out the (one after the) last fully visible segment, with a whole segment below it.
        // If the bottom of the view's offset is less than 0.5, then always use one up from the
        // last fully visible segment. This avoids us selecting components which are only *just* visible.
        size_t lastWithBelow = (viewBottomOffset < 0.5f) ? (std::max(lastFullyVisibleSegmentCache, (size_t)1) - 1) : lastFullyVisibleSegmentCache;

        // If the segment is already completely in view, then just return our existing segmentPos.
        if (segmentId >= firstFullyVisibleSegmentCache && segmentId < lastWithBelow) {
            return segmentPos;
        }

        OffVal<size_t, GLfloat> maxSegmentPos = getMaximumSegmentPos();

        // If the height of the container is less than three segments, we cannot move it up or down
        // with one segment above/below it, so just return the required segment id (make sure we don't go past maximum).
        if (viewSize.getBase() < 3) {
            return std::min(maxSegmentPos, OffVal<size_t, GLfloat>(segmentId, 0.0f));
        }

        // If the segment is above, then return segmentId-1, (or zero if segmentId is zero).
        // Again, don't go past maximum.
        if (segmentId < firstFullyVisibleSegmentCache) {
            return std::min(maxSegmentPos, OffVal<size_t, GLfloat>((segmentId > 0) ? (segmentId - 1) : 0, 0.0f));
        }

        // If segment is below:

        // Subtract the view size from two segments after the requested one, and make sure it is not larger than maximum.
        OffVal<size_t, GLfloat> scrollTo(segmentId + 2, 0.0f);
        scrollTo.zeroRoundSubtract(viewSize);
        return std::min(maxSegmentPos, scrollTo);
    }
    
    GLfloat SegmentContainer::getViewSize(void) const {
        GET_LOCATION_AND_SCALED_SIZE
        return locationSize / scaledSize;
    }

    void SegmentContainer::setSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos) {
        if (newSegmentPos != segmentPos) {
            segmentPos = newSegmentPos;
            internalRecalculate();
            onChangeSegmentPos(newSegmentPos);
        }
    }

    void SegmentContainer::scrollToStart(void) {
        setSegmentPos(OffVal<size_t, GLfloat>(0, 0.0f));
    }

    void SegmentContainer::scrollToEnd(void) {
        setSegmentPos(getMaximumSegmentPos());
    }

    void SegmentContainer::scrollUpBy(const OffVal<size_t, GLfloat> & distance) {
        OffVal<size_t, GLfloat> newSegmentPos = segmentPos;
        newSegmentPos.zeroRoundSubtract(distance);
        setSegmentPos(newSegmentPos);
    }

    void SegmentContainer::scrollDownBy(const OffVal<size_t, GLfloat> & distance) {
        OffVal<size_t, GLfloat> newSegmentPos = std::min(segmentPos + distance, getMaximumSegmentPos());
        setSegmentPos(newSegmentPos);
    }


    // ========================================= Modified keyboard navigation =========================================

    Component * SegmentContainer::getFirstKeyboardResponsiveComponent(void) const {
        // Loop through each fully visible segment.
        for (size_t segmentId = firstFullyVisibleSegmentCache; segmentId < lastFullyVisibleSegmentCache; segmentId++) {
            Component * component = componentsBySegmentPos[segmentId];
            // Return the first one we find that is not a container, and is keyboard responsive
            if (!childIsContainer(component) && childShouldRespondToKeyboard(component)) {
                return component;
            }
        }
        // If none are found within the visible area, then return the superclass method.
        return Container::getFirstKeyboardResponsiveComponent();
    }

    Component * SegmentContainer::getFirstKeyboardResponsiveComponentDepth(void) const {
        // Loop through each visible segment.
        for (size_t segmentId = firstFullyVisibleSegmentCache; segmentId < lastFullyVisibleSegmentCache; segmentId++) {
            Component * component = componentsBySegmentPos[segmentId];
            // Only look at visible components.
            if (component->isVisible()) {
                if (childIsContainer(component)) {
                    // If the component is a Container, then recursively call.
                    Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentDepth();
                    // If they return a valid component, then return it.
                    if (found != NULL) {
                        return found;
                    }
                } else if (childShouldRespondToKeyboard(component)) {
                    // If the component is not a container, but is keyboard responsive, then return it.
                    return component;
                }
            }
        }
        // If none are found within the visible area, then return the superclass method.
        return Container::getFirstKeyboardResponsiveComponentDepth();
    }

    Component * SegmentContainer::getFirstKeyboardResponsiveComponentNonDepth(void) const {
        // Loop through each visible segment.
        for (size_t segmentId = firstFullyVisibleSegmentCache; segmentId < lastFullyVisibleSegmentCache; segmentId++) {
            Component * component = componentsBySegmentPos[segmentId];
            if (!childIsContainer(component) && childShouldRespondToKeyboard(component)) {
                return component;
            }
        }
        // Loop through each visible segment.
        for (size_t segmentId = firstFullyVisibleSegmentCache; segmentId < lastFullyVisibleSegmentCache; segmentId++) {
            Component * component = componentsBySegmentPos[segmentId];
            // Only look at visible containers.
            if (childIsContainer(component) && component->isVisible()) {
                Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentNonDepth();
                if (found != NULL) {
                    return found;
                }
            }
        }
        // If none are found within the visible area, then return the superclass method.
        return Container::getFirstKeyboardResponsiveComponentNonDepth();
    }

    Component * SegmentContainer::getLastKeyboardResponsiveComponent(void) const {
        // Loop through each fully visible segment.
        for (size_t segmentId = lastFullyVisibleSegmentCache - 1; segmentId >= firstFullyVisibleSegmentCache; segmentId--) {
            Component * component = componentsBySegmentPos[segmentId];
            // Return the first one we find that is not a container, and is keyboard responsive
            if (!childIsContainer(component) && childShouldRespondToKeyboard(component)) {
                return component;
            }
        }
        // If none are found within the visible area, then return the superclass method.
        return Container::getLastKeyboardResponsiveComponent();
    }

    Component * SegmentContainer::getLastKeyboardResponsiveComponentDepth(void) const {
        // Loop through each visible segment.
        for (size_t segmentId = lastFullyVisibleSegmentCache - 1; segmentId >= firstFullyVisibleSegmentCache; segmentId--) {
            Component * component = componentsBySegmentPos[segmentId];
            // Only look at visible components.
            if (component->isVisible()) {
                if (childIsContainer(component)) {
                    // If the component is a Container, then recursively call.
                    Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentDepth();
                    // If they return a valid component, then return it.
                    if (found != NULL) {
                        return found;
                    }
                } else if (childShouldRespondToKeyboard(component)) {
                    // If the component is not a container, but is keyboard responsive, then return it.
                    return component;
                }
            }
        }
        // If none are found within the visible area, then return the superclass method.
        return Container::getLastKeyboardResponsiveComponentDepth();
    }

    Component * SegmentContainer::getLastKeyboardResponsiveComponentNonDepth(void) const {
        // Loop through each visible segment.
        for (size_t segmentId = lastFullyVisibleSegmentCache - 1; segmentId >= firstFullyVisibleSegmentCache; segmentId--) {
            Component * component = componentsBySegmentPos[segmentId];
            if (!childIsContainer(component) && childShouldRespondToKeyboard(component)) {
                return component;
            }
        }
        // Loop through each visible segment.
        for (size_t segmentId = lastFullyVisibleSegmentCache - 1; segmentId >= firstFullyVisibleSegmentCache; segmentId--) {
            Component * component = componentsBySegmentPos[segmentId];
            // Only look at visible containers.
            if (childIsContainer(component) && component->isVisible()) {
                Component * found = ((Container *)component)->getFirstKeyboardResponsiveComponentNonDepth();
                if (found != NULL) {
                    return found;
                }
            }
        }
        // If none are found within the visible area, then return the superclass method.
        return Container::getLastKeyboardResponsiveComponentNonDepth();
    }
}
