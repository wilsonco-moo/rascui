/*
 * ScrollAction.h
 *
 *  Created on: 27 May 2020
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_SCROLL_SCROLLACTION_H_
#define RASCUI_COMPONENTS_SCROLL_SCROLLACTION_H_

namespace rascUI {

    /**
     * This enum stores all the scroll actions that a user can do.
     *  > pageUp/pageDown are triggered by scroll bars, when the user
     *    presses previous/next container keys, while the selection is
     *    locked to the scroll puck.
     *  > scrollUp/scrollDown are triggered by the scroll up/down buttons
     *    in a scroll bar, or by the mouse scroll wheel in a scroll bar
     *    or segment container.
     */
    enum class ScrollAction {
        pageUp, scrollUp, scrollDown, pageDown
    };
}

#endif
