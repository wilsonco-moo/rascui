/*
 * ScrollBar.h
 *
 *  Created on: 26 May 2020
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_SCROLL_SCROLLBAR_H_
#define RASCUI_COMPONENTS_SCROLL_SCROLLBAR_H_

#include "../../components/abstract/ResponsiveComponent.h"
#include "../../base/Container.h"
#include "../../util/OffVal.h"
#include "ScrollAction.h"

namespace rascUI {
    class Theme;

    /**
     * ScrollBar is an (abstract) component which provides the horizontal/vertical
     * scroll bar used within ScrollPane. ScrollBar is also usable on its own
     * (outside ScrollPane). ScrollBar automatically works out the position of
     * the "scroll puck", from provided data about the size of the list and view
     * area, and notifies (using methods) when the user scrolls.
     *
     * To use a ScrollBar, the following methods must be implemented: getViewSize,
     * getSegmentCount, getViewPosition, getMaximumViewPosition, onScroll, onScrollMove.
     */
    class ScrollBar : public rascUI::Container {
    private:
        class ScrollUpButton; class ScrollDownButton;

        // ---------------------------- Scroll pane puck class -----------------------------

        class ScrollPanePuck : public ResponsiveComponent {
        private:
            friend class ScrollUpButton; friend class ScrollDownButton; friend class ScrollBar; // So these can call our recalculate method.
            ScrollBar * scrollBar;
            GLfloat startMargin, totalMargin, minSize, initialMousePos;
            OffVal<size_t, GLfloat> initialSegmentPos;
        public:
            ScrollPanePuck(ScrollBar * scrollBar, Theme * theme, bool vertical, bool startBorder, bool endBorder);
            virtual ~ScrollPanePuck(void);
        protected:
            virtual void onDraw(void) override;
            virtual void recalculate(const Rectangle & parentSize) override;
            virtual void onMousePress(GLfloat viewX, GLfloat viewY, int button) override;
            virtual void onMouseDrag(GLfloat viewX, GLfloat viewY, int button) override;
            virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button) override;
            virtual NavigationAction getRequiredKeyboardActionPress(NavigationAction action) override;
            virtual NavigationAction getRequiredKeyboardActionRelease(NavigationAction action) override;
            virtual NavigationAction onSelectionLockKeyPress(NavigationAction defaultAction, int key, bool special) override;
        };

        // ---------------------------- Scroll bar background class ------------------------

        class ScrollBarBackground : public Component {
        public:
            ScrollBarBackground(const Location & location = Location());
            virtual ~ScrollBarBackground(void);
        protected:
            virtual void onDraw(void) override;
            virtual bool shouldRespondToKeyboard(void) const override;
        };

        // --------------------------- Scroll up button class ------------------------------

        class ScrollUpButton : public ResponsiveComponent {
        private:
            ScrollBar * scrollBar;
        public:
            ScrollUpButton(const Location & location, ScrollBar * scrollBar);
            virtual ~ScrollUpButton(void);
        protected:
            virtual void onDraw(void) override;
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };

        // --------------------------- Scroll Down button class ----------------------------

        class ScrollDownButton : public ResponsiveComponent {
        private:
            ScrollBar * scrollBar;
        public:
            ScrollDownButton(const Location & location, ScrollBar * scrollBar);
            virtual ~ScrollDownButton(void);
        protected:
            virtual void onDraw(void) override;
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };

        // ---------------------------------------------------------------------

        // Stores our orientation.
        bool vertical;

        // The components which constitute a scroll bar.
        ScrollBarBackground background;
        ScrollUpButton scrollUpButton;
        ScrollPanePuck scrollPuck;
        ScrollDownButton scrollDownButton;

    public:
        /**
         * The main constructor for ScrollBar. A theme is required for initial layout.
         * Orientation can be specified with the parameter vertical.
         * Whether there is a border before/after the scroll buttons can be specified
         * with the startBorder/endBorder parameters.
         */
        ScrollBar(const Location & location, Theme * theme, bool vertical = true, bool startBorder = true, bool endBorder = true);
        virtual ~ScrollBar(void);

    protected:
        // This is used for mouse scroll wheel scrolling. It does not matter if this is
        // a press or a release event, since a release event always immediately follows
        // from a scroll wheel button press event.
        virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button) override;
    public:

        // ------------------------ Accessor methods ---------------------------

        /**
         * Returns our orientation.
         */
        inline bool isVertical(void) const {
            return vertical;
        }

        // ------ Methods to be implemented by ScrollBar subclasses ------------

        /**
         * This should return the size, (in segments) of the view area.
         */
        virtual GLfloat getViewSize(void) const = 0;

        /**
         * This should return the total number of segments which are able to be
         * scrolled across.
         */
        virtual size_t getSegmentCount(void) const = 0;

        /**
         * This should return the current view position, in terms of segments.
         * This is called by ScrollBar when recalculating, to ensure the
         */
        virtual OffVal<size_t, GLfloat> getViewPosition(void) const = 0;

        /**
         * This should return the maximum view position which is possible.
         */
        virtual OffVal<size_t, GLfloat> getMaximumViewPosition(void) const = 0;

        /**
         * This is run each time the user requests to scroll either up or down.
         * This is not pure virtual, and the default implementation is to
         * recalculate (the scroll puck) and repaint. If that is not needed,
         * do not call the ScrollBar superclass method.
         */
        virtual void onScroll(ScrollAction action);

        /**
         * This is called when the user drags the scroll puck with their mouse.
         * The position value represents the segment position which the user
         * has moved to.
         * This is not pure virtual, and the default implementation is to
         * recalculate (the scroll puck) and repaint. If that is not needed,
         * do not call the ScrollBar superclass method.
         */
        virtual void onScrollMove(OffVal<size_t, GLfloat> position);
    };
}

#endif
