/*
 * SegmentContainer.h
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_SCROLL_SEGMENTCONTAINER_H_
#define RASCUI_COMPONENTS_SCROLL_SEGMENTCONTAINER_H_

#include <unordered_map>
#include <cstddef>
#include <GL/gl.h>
#include <vector>

#include "../../base/Container.h"
#include "../../util/OffVal.h"
#include "ScrollAction.h"

namespace rascUI {
    class Rectangle;

    /**
     * The SegmentContainer is a specialised type of container, which allows many components
     * to be arranged horizontally or vertically, in such a way that ONLY the ones within the
     * visible area are actually drawn.
     *
     * NOTE: Removing components from the middle of a SegmentContainer can be very expensive, as all components
     *       after it would have to be moved backwards in our internal data structures.
     *
     * NOTE: The bounded area (for partial repainting) of SegmentContainer is larger by segmentSize,
     *       at either top/bottom depending on orientation. Any parent container MUST allow for this
     *       additional area, or partial repainting will not work correctly.
     */
    class SegmentContainer : public Container {
    private:
        // The orientation of the segment container.
        bool vertical;
        // The size of segments in this container. In vertical mode this represents height, in horizontal
        // mode this represents width.
        GLfloat segmentSize;
        // The current position that the top of the container is, within the segments. By default this is set to zero.
        // This value is measured in multiples of segments.
        OffVal<size_t, GLfloat> segmentPos;
        // This vector stores our child components by their segment position.
        std::vector<Component *> componentsBySegmentPos;
        // This map stores the position in the previous std::vector, for each component. This allows us to remove
        // components by pointer easily.
        std::unordered_map<Component *, size_t> componentPositions;

        // These values cache the first and last components to draw, within the visible range.
        size_t firstSegmentCache, // Stores the segment id of the first segment to draw.
               lastSegmentCache;  // Stores the segment id immediately after the last segment to draw.

        // These values cache the range of segments which are fully visible. This is used by the keyboard navigation.
        size_t firstFullyVisibleSegmentCache, // Stores the segment id of the first segment.
               lastFullyVisibleSegmentCache;  // Stores the segment id immediately after the last segment.

        // This stores whether we should draw a background behind our components.
        bool drawBackground;

    public:
        /**
         * The main constructor for SegmentContainer.
         * The segment size represents the size (in pixels) of each component in the container.
         * If drawBackground is set to true, a scroll background will be drawn (by the theme)
         * behind all components.
         * The orientation can be specified with the "vertical" parameter.
         */
        SegmentContainer(const Location & location, GLfloat segmentSize, bool drawBackground, bool vertical = true);
        virtual ~SegmentContainer(void);

    private:
        // Used internally for recalculation. This also automatically causes a repaint of the entire SegmentContainer.
        void internalRecalculate(void);

    protected:
        // When components are added, we must add them to our internal data structures.
        virtual void onAdd(Component * component) override;
        // When components are removed, we must remove them from our internal data structures.
        virtual void onRemove(Component * component) override;

        // Here we only recalculate the components that are currently visible. This is done efficiently,
        // in time proportional to the number of VISIBLE components, NOT the total number of components.
        // even if we have thousands of components.
        virtual void recalculate(const Rectangle & parentSize) override;

        // Here we only draw the components that are currently visible. This is done efficiently,
        // in time proportional to the number of VISIBLE components, NOT the total number of components.
        virtual void onDraw(void) override;

        // Here we return an area, which has segmentSize added to either top/bottom or left/right, depending on our orientation.
        virtual Rectangle getBoundedArea(void) const override;

        // Here we modify Container's boundedRepaint method so we only actually draw components which are visible.
        virtual void boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) override;

        // To avoid components which have fallen outside the visible range from receiving mouse events,
        // here we only add those child components which are currently visible.
        virtual void addIfMouseOver(GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver) override;

        // Here we adjust the segment position, to ensure the keyboard selected component always
        // remains within the view.
        virtual void onChildKeyboardSelect(Component * component) override;

        // This is used for mouse scroll wheel scrolling. It does not matter if this is
        // a press or a release event, since a release event always immediately follows
        // from a scroll wheel button press event. Here we scroll the distances specified
        // by getScrollDistance.
        virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button) override;

        // --------------------------- Methods which subclasses can implement -------------------------
        /**
         * This is run each time the segment position changes.
         * Subclasses can use this to perform actions which rely on the
         * segment position, such as updating a scroll bar.
         *
         * By default, onChangeSegmentPos does nothing (is not pure virtual).
         */
        virtual void onChangeSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos);

        /**
         * This checks whether the view area has gone "off the bottom". If it has,
         * the segment position is adjusted.
         * This returns true if the view was correct to start with, and returns false
         * if the view had to be adjusted.
         * This can be overridden to handle view area checks differently: For example,
         * AnimatedSegmentContainer overrides this to adjust the view using an animation
         * instead.
         *
         * This is called during the recalculate method.
         */
        virtual bool adjustViewBounds(void);

    public:
        /**
         * This should return the distance to scroll (in segments), for each
         * scroll action. This can be overridden to change the default behaviour.
         * For upward scrolling, the return value is subtracted from the segment position.
         * For downward scrolling, the return value is added to the segment position.
         *
         * By default ScrollAction::scrollUp/ScrollAction::scrollDown move by one
         * segment, and ScrollAction::pageUp/ScrollAction::pageDown move by two fewer
         * segments than the view height.
         *
         * For a plain SegmentContainer, only ScrollAction::scrollUp and ScrollAction::scrollDown
         * will be used. When used in a ScrollPane ScrollAction::pageUp and ScrollAction::pageDown
         * are also used, since this method is used for the scroll pane's default behaviour.
         */
        virtual OffVal<size_t, GLfloat> getScrollDistance(ScrollAction action);

        // ------------------------------ Accessor methods --------------------------------------------
        /**
         * Returns our orientation.
         */
        bool isVertical(void) const {
            return vertical;
        }

        /**
         * Returns the current segment position.
         */
        inline const OffVal<size_t, GLfloat> & getSegmentPos(void) const {
            return segmentPos;
        }

        /**
         * Returns the segment size that is in use.
         */
        inline GLfloat getSegmentSize(void) const {
            return segmentSize;
        }

        /**
         * Returns the segment id of the specified component, assuming the component is a child component
         * of this SegmentContainer. If it is not, this method will cause undefined behaviour.
         */
        inline size_t getChildSegmentId(Component * component) const {
            return componentPositions.at(component);
        }

        /**
         * This returns the maximum segment position which should be used, (before we scroll
         * past the bottom), with the current number of components.
         * This represents the segment position if we placed the bottom of our view at the bottom of the
         * last component. If there are not enough components to completely fill the view area,
         * this will be clipped to zero.
         */
        OffVal<size_t, GLfloat> getMaximumSegmentPos(void) const;

        /**
         * This method gets the nearest segmentPos to the current segmentPos, for the specified
         * segment id to be completely in view. This should be used to control scrolling upon keyboard
         * selection.
         */
        OffVal<size_t, GLfloat> getNearestSegmentPos(size_t segmentId) const;
        
        /**
         * Returns the size (in segments) of the view area.
         */
        GLfloat getViewSize(void) const;
        
        /**
         * Returns the number of segments in the segment container. This method is provided
         * only for clarity, since it returns exactly the same result as Container::getComponentCount.
         */
        inline size_t getSegmentCount(void) const {
            return componentsBySegmentPos.size();
        }

        // -------------------------- Methods to set segment position -------------------------------

        /**
         * Sets the segment position. This will trigger a recalculation of the currently visible
         * child components and a repaint of the entire segment container, (if something has changed).
         */
        void setSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos);

        /**
         * Sets the segment position to the minimum - this results in the view being moved to
         * either the top or left most position, depending on orientation.
         */
        void scrollToStart(void);

        /**
         * Sets the segment position to the maximum - this results in the view being moved to
         * either the bottom or right most position, depending on orientation.
         */
        void scrollToEnd(void);

        /**
         * Scrolls the view up by the specified amount.
         */
        void scrollUpBy(const OffVal<size_t, GLfloat> & distance);

        /**
         * Scrolls the view down by the specified amount.
         */
        void scrollDownBy(const OffVal<size_t, GLfloat> & distance);

        // --------------------------------- Keyboard navigation --------------------------------------

        // Override these methods, so that when navigating using the keyboard, we do not always pick the component
        // at the top, then cause any ScrollPane to scroll to the top.

        // These methods modify the original methods by first searching the visible segment area for components.
        // If that fails, the equivalent superclass method is then called, to search the rest of the area.

        virtual Component * getFirstKeyboardResponsiveComponent(void) const override;
        virtual Component * getFirstKeyboardResponsiveComponentDepth(void) const override;
        virtual Component * getFirstKeyboardResponsiveComponentNonDepth(void) const override;
        virtual Component * getLastKeyboardResponsiveComponent(void) const override;
        virtual Component * getLastKeyboardResponsiveComponentDepth(void) const override;
        virtual Component * getLastKeyboardResponsiveComponentNonDepth(void) const override;
    };
}

#endif
