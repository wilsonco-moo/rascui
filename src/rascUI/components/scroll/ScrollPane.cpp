/*
 * ScrollPane.cpp
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#include "ScrollPane.h"

#include "../../util/Layout.h"
#include "../../base/Theme.h"

namespace rascUI {

    // =================== Scroll pane segment container class ==========================

    ScrollPane::ScrollPaneSegmentContainer::ScrollPaneSegmentContainer(const Location & location, GLfloat segmentSize, bool drawBackground, bool vertical, ScrollPane * scrollPane) :
        AnimatedSegmentContainer(location, segmentSize, drawBackground, vertical),
        scrollPane(scrollPane) {
    }
    ScrollPane::ScrollPaneSegmentContainer::~ScrollPaneSegmentContainer(void) {
    }

    OffVal<size_t, GLfloat> ScrollPane::ScrollPaneSegmentContainer::getScrollDistance(ScrollAction action) {
        // Use the scroll distance implementation defined by our segment container.
        return scrollPane->getScrollDistance(action);
    }

    void ScrollPane::ScrollPaneSegmentContainer::onChangeSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos) {
        childRecalculate(&scrollPane->scrollBar, scrollPane->location.cache);
        scrollPane->scrollBar.repaint();
    }

    // =================== Scroll pane scroll bar class =================================

    ScrollPane::ScrollPaneScrollBar::ScrollPaneScrollBar(const Location & location, Theme * theme, bool vertical, bool startBorder, bool endBorder, ScrollPane * scrollPane) :
        ScrollBar(location, theme, vertical, startBorder, endBorder),
        scrollPane(scrollPane) {
    }
    ScrollPane::ScrollPaneScrollBar::~ScrollPaneScrollBar(void) {
    }

    GLfloat ScrollPane::ScrollPaneScrollBar::getViewSize(void) const {
        return scrollPane->contents.getViewSize();
    }
    size_t ScrollPane::ScrollPaneScrollBar::getSegmentCount(void) const  {
        return scrollPane->contents.getSegmentCount();
    }
    OffVal<size_t, GLfloat> ScrollPane::ScrollPaneScrollBar::getViewPosition(void) const {
        return scrollPane->contents.getSegmentPos();
    }
    OffVal<size_t, GLfloat> ScrollPane::ScrollPaneScrollBar::getMaximumViewPosition(void) const {
        return scrollPane->contents.getMaximumSegmentPos();
    }
    void ScrollPane::ScrollPaneScrollBar::onScroll(ScrollAction action) {
        // For scroll up actions.
        if (action == ScrollAction::pageUp || action == ScrollAction::scrollUp) {
            scrollPane->contents.scrollSlowlyUpBy(scrollPane->getScrollDistance(action));
        // For scroll down actions.
        } else {
            scrollPane->contents.scrollSlowlyDownBy(scrollPane->getScrollDistance(action));
        }
        // Since we set the target segment position, the segment position will be called
        // next time the scroll pane is repainted. This will result in onChangeSegmentPos
        // being called, which will repaint the scroll bar.
    }
    void ScrollPane::ScrollPaneScrollBar::onScrollMove(OffVal<size_t, GLfloat> position) {
        // Set target THEN segment position. Setting both results in scrolling immediately.
        // Since we used setSegmentPos directly, this will immediately call onChangeSegmentPos
        // and repaint the scroll bar.
        scrollPane->contents.setTargetSegmentPos(position);
        scrollPane->contents.setSegmentPos(position);
    }

    // ==================================================================================

    #define LAYOUT_THEME theme

    // Picks either the vertical or horizontal version of a location.
    #define HV_LOCATION(loc) \
        (vertical ? Location(loc##_V) : Location(loc##_H))

    // The scroll bar sits on the right hand side or bottom, depending on orientation.
    // Note that scroll bars always have a width/height of normal component WIDTH, as to
    // remain consistent regardless of orientation.
    #define SCROLLBAR_LOC_V \
        PIN_R(COUNT_INBORDER_X(1), GEN_FILL)
    #define SCROLLBAR_LOC_H \
        PIN_B(COUNT_INBORDER_X(1), GEN_FILL)

    // The scroll pane contents leave a margin on the right or bottom for the scroll bar.
    #define CONTENTS_LOC_V                  \
        SUB_MARGIN_R(COUNT_INBORDER_X(1),   \
         GEN_FILL                           \
        )
    #define CONTENTS_LOC_H                  \
        SUB_MARGIN_B(COUNT_INBORDER_X(1),   \
         GEN_FILL                           \
        )

    ScrollPane::ScrollPane(Theme * theme, const Location & location, bool drawBackground, bool vertical, bool startBorder, bool endBorder) :
        ScrollPane(theme, location, (vertical ? UI_BHEIGHT : UI_BWIDTH) + UI_BORDER, drawBackground, vertical, startBorder, endBorder) {
    }
    ScrollPane::ScrollPane(Theme * theme, const Location & location, GLfloat segmentSize, bool drawBackground, bool vertical, bool startBorder, bool endBorder) :
        Container(location),
        scrollBar(HV_LOCATION(SCROLLBAR_LOC), theme, vertical, startBorder, endBorder, this),
        contents(HV_LOCATION(CONTENTS_LOC), segmentSize, drawBackground, vertical, this) {

        // Note that the contents MUST BE first, so events generated when the segment container
        // recalculates/repaints are available when the scroll bar recalculates/repaints.
        add(&contents);
        add(&scrollBar);
    }

    ScrollPane::~ScrollPane(void) {
    }

    Rectangle ScrollPane::getBoundedArea(void) const {
        if (contents.isVertical()) {
            GLfloat sgSize = contents.getSegmentSize() * location.yScale;
            return Rectangle(location.cache.x, location.cache.y - sgSize, location.cache.width, location.cache.height + sgSize * 2.0f);
        } else {
            GLfloat sgSize = contents.getSegmentSize() * location.xScale;
            return Rectangle(location.cache.x - sgSize, location.cache.y, location.cache.width + sgSize * 2.0f, location.cache.height);
        }
    }

    OffVal<size_t, GLfloat> ScrollPane::getScrollDistance(ScrollAction action) {
        // By default run the implementation of getScrollDistance from the
        // parent class of ScrollPaneSegmentContainer.
        return contents.AnimatedSegmentContainer::getScrollDistance(action);
    }
}
