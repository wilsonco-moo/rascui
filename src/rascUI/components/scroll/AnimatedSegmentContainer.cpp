/*
 * AnimatedSegmentContainer.cpp
 *
 *  Created on: 26 May 2020
 *      Author: wilson
 */

#include "AnimatedSegmentContainer.h"

#include <cmath>

#include "../../base/TopLevelContainer.h"

namespace rascUI {

    AnimatedSegmentContainer::AnimatedSegmentContainer(const Location & location, GLfloat segmentSize, bool drawBackground, bool vertical) :
        SegmentContainer(location, segmentSize, drawBackground, vertical),
        targetSegmentPos(getSegmentPos()),
        smoothMoveSpeed(0.05f) {
    }

    AnimatedSegmentContainer::~AnimatedSegmentContainer(void) {
    }

    void AnimatedSegmentContainer::updateSegmentPosFromTarget(void) {
        // If there is a difference between actual segment pos and target, then do animation.
        OffVal<size_t, GLfloat> segmentPos = getSegmentPos();
        if (targetSegmentPos != segmentPos) {
            OffVal<size_t, GLfloat> moveTo = targetSegmentPos;

            // Only do the animated movement if the smooth move speed is not zero.
            if (smoothMoveSpeed != 0.0f) {

                // The fraction which we will move, towards the target position.
                long double moveRatio = std::pow((long double)smoothMoveSpeed, (long double)getTopLevel()->getElapsed());

                // If the difference is less than this, the segment position must be moved immediately to avoid
                // the animation taking too long.
                OffVal<size_t, GLfloat> minimumDiff(1.0f / (getSegmentSize() * (isVertical() ? location.yScale : location.xScale)));

                // Offset the target by a proportion of the difference, and set segment to target.
                // Only offset by difference if it is larger than the minimum difference.
                if (moveTo < segmentPos) {
                    OffVal<size_t, GLfloat> difference = segmentPos - moveTo;
                    difference.multiplyByFloat<long double>(moveRatio);
                    if (difference > minimumDiff) {
                        moveTo += difference;
                    }
                } else {
                    OffVal<size_t, GLfloat> difference = moveTo - segmentPos;
                    difference.multiplyByFloat<long double>(moveRatio);
                    if (difference > minimumDiff) {
                        moveTo.zeroRoundSubtract(difference);
                    }
                }
            }
            setSegmentPos(moveTo);
        }
    }

    // Note that this is called not only when WE want to be repainted, but also whenever ANY other components
    // within the scroll pane want repainting.
    void AnimatedSegmentContainer::boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) {
        updateSegmentPosFromTarget();
        SegmentContainer::boundedRepaint(boundingBox, wantsRepaint, childWantsRepaint);
    }

    // Ensure that updateSegmentPosFromTarget() gets called at the start of onDraw AND boundedRepaint.
    void AnimatedSegmentContainer::onDraw(void) {
        updateSegmentPosFromTarget();
        SegmentContainer::onDraw();
    }

    void AnimatedSegmentContainer::onChildKeyboardSelect(Component * component) {
        // Set the TARGET segment position such that the child component is within view.
        setTargetSegmentPos(getNearestSegmentPos(getChildSegmentId(component)));
        Container::onChildKeyboardSelect(component);
    }

    void AnimatedSegmentContainer::onMouseRelease(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getTopLevel()->bindings->mouseScrollUpButton) {
            scrollSlowlyUpBy(getScrollDistance(ScrollAction::scrollUp));
        } else if (button == getTopLevel()->bindings->mouseScrollDownButton) {
            scrollSlowlyDownBy(getScrollDistance(ScrollAction::scrollDown));
        }
    }

    bool AnimatedSegmentContainer::adjustViewBounds(void) {
        OffVal<size_t, GLfloat> maxSegmentPos = getMaximumSegmentPos();
        if (getTargetSegmentPos() > maxSegmentPos) {
            setTargetSegmentPos(maxSegmentPos);
        }
        
        // Return value of true here means SegmentContainer must immediately do an internal recalculate.
        // Unlike SegmentContainer::adjustViewBounds, we can't assume that setTargetSegmentPos always
        // triggers a recalculate: Next time we're drawn, we'll only recalculate if target is different
        // to segment pos (so causes a move).
        // So require a recalculate if that's not gonna happen next time we're drawn (i.e: if target is
        // equal).
        return getTargetSegmentPos() == getSegmentPos();
    }

    void AnimatedSegmentContainer::setTargetSegmentPos(const OffVal<size_t, GLfloat> & newSegmentPos) {
        targetSegmentPos = newSegmentPos;
        // Make sure we repaint later - this will ensure updateSegmentPosFromTarget (and
        // resultantly setSegmentPos) gets called.
        repaint();
    }

    void AnimatedSegmentContainer::scrollSlowlyToStart(void) {
        setTargetSegmentPos(OffVal<size_t, GLfloat>(0, 0.0f));
    }

    void AnimatedSegmentContainer::scrollSlowlyToEnd(void) {
        setTargetSegmentPos(getMaximumSegmentPos());
    }

    void AnimatedSegmentContainer::scrollSlowlyUpBy(const OffVal<size_t, GLfloat> & distance) {
        OffVal<size_t, GLfloat> newTargetSegmentPos = targetSegmentPos;
        newTargetSegmentPos.zeroRoundSubtract(distance);
        setTargetSegmentPos(newTargetSegmentPos);
    }

    void AnimatedSegmentContainer::scrollSlowlyDownBy(const OffVal<size_t, GLfloat> & distance) {
        OffVal<size_t, GLfloat> newTargetSegmentPos = std::min(targetSegmentPos + distance, getMaximumSegmentPos());
        setTargetSegmentPos(newTargetSegmentPos);
    }
}
