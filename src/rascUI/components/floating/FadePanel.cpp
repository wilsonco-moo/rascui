/*
 * FadePanel.cpp
 *
 *  Created on: 4 Jan 2019
 *      Author: wilson
 */

#include "FadePanel.h"

#include "../../base/Theme.h"
#include "../../base/TopLevelContainer.h"

namespace rascUI {

    FadePanel::FadePanel(const Location & location) :
        Component(location) {
    }

    FadePanel::~FadePanel(void) {
    }

    bool FadePanel::shouldRespondToKeyboard(void) const {
        return false;
    }

    void FadePanel::onDraw(void) {
        getTopLevel()->theme->drawFadePanel(location);
    }
}
