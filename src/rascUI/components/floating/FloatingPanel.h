/*
 * SimpleFloatingPanel.h
 *
 *  Created on: 28 Mar 2019
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_FLOATING_FLOATINGPANEL_H_
#define RASCUI_COMPONENTS_FLOATING_FLOATINGPANEL_H_

#include <functional>

#include "../../base/Container.h"
#include "../generic/BackPanel.h"
#include "FadePanel.h"

namespace rascUI {

    /**
     * FloatingPanel provides a container and BackPanel, which are in front of a FadePanel.
     * This forms a convenient base for a popup menu.
     *
     * By default FloatingPanel is not visible. The methods show and hide can be called at any time.
     *
     * NOTE: FloatingPanel's setVisible method should NOT be called directly, instead call show and hide.
     */
    class FloatingPanel : public Container {
    private:
        // The component to keyboard select when this panel is hidden.
        Component * selectWhenHide;

        FadePanel fadePanel;
        BackPanel backPanel;

    protected:
        std::function<void(void)> onShowFunc, onHideFunc;

    private:
        bool currentlyOpen;

    public:
        /**
         * Anything added to the FloatingPanel should be added TO THIS CONTAINER,
         * NOT THE FLOATING PANEL ITSELF.
         */
        Container contents;

        /**
         * The provided location specifies the location of the content container, NOT the location
         * of the FloatingPanel itself. The FloatingPanel, (and as such it's FadePanel), fills the entire area.
         *
         * The user can provide std::functions, which are run at some point after calls to show and hide.
         *
         * The parameter addBackPanelToContent specifies whether the BackPanel should be added to the FloatingPanel,
         * or it's content container, (the default). Adding it to the contents container allows easy resizing of the contents
         * container. Adding it to the FloatingPanel itself allows all components to be removed from the content container
         * without removing the BackPanel.
         *
         * The default location of FloatingPanels is centred, with a size of 320x240.
         */
        FloatingPanel(const Location & location = Location(-160, -120, 320, 240, 0.5f, 0.5f, 0.0f, 0.0f),
                std::function<void(void)> onShowFunc = [](void){},
                std::function<void(void)> onHideFunc = [](void){},
                bool addBackPanelToContent = true);

        virtual ~FloatingPanel(void);

        /**
         * Shows the FloatingPanel, and locks selection to it's content container, at some point later,
         * (using getTopLevel()->afterEvent). This will call onShowFunc and/or onShow.
         */
        void show(void);

        /**
         * Hides the FloatingPanel, and resets all selection locking, at some point later,
         * (using getTopLevel()->afterEvent). This will call onHideFunc and/or onHide.
         */
        void hide(void);

    protected:
        /**
         * This can be overridden instead of providing an onShowFunc, if more convenient.
         */
        virtual void onShow(void);

        /**
         * This can be overridden instead of providing an onHideFunc, if more convenient.
         */
        virtual void onHide(void);
    public:

        /**
         * Returns whether we are currently open.
         */
        inline bool isOpen(void) const {
            return currentlyOpen;
        }

        /**
         * Sets the component to keyboard select when we are hidden, which ideally
         * should be done. NULL can be set here to not select a component.
         * If this is called, it should be called before we are shown.
         */
        inline void setSelectWhenHide(Component * component) {
            selectWhenHide = component;
        }
    };

}

#endif
