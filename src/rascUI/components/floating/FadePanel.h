/*
 * FadePanel.h
 *
 *  Created on: 4 Jan 2019
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_FLOATING_FADEPANEL_H_
#define RASCUI_COMPONENTS_FLOATING_FADEPANEL_H_

#include "../../base/Component.h"

namespace rascUI {

    /**
     * FadePanel is intended to be displayed behind a popup menu, to fade out all other parts of
     * the user interface.
     * This component should be drawn by theme implementations as a semi-transparent rectangle.
     */
    class FadePanel : public Component {
    public:
        FadePanel(const Location & location = Location());
        virtual ~FadePanel(void);

    protected:
        virtual bool shouldRespondToKeyboard(void) const override;
        virtual void onDraw(void) override;
    };

}

#endif
