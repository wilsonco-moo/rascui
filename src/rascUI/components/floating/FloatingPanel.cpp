/*
 * SimpleFloatingPanel.cpp
 *
 *  Created on: 28 Mar 2019
 *      Author: wilson
 */

#include "FloatingPanel.h"

#include "../../base/TopLevelContainer.h"
#include "../../util/FunctionQueue.h"

namespace rascUI {

    FloatingPanel::FloatingPanel(const Location & location, std::function<void(void)> onShowFunc, std::function<void(void)> onHideFunc, bool addBackPanelToContent) :
        Container(),
        selectWhenHide(NULL),
        fadePanel(),
        backPanel(addBackPanelToContent ? Location() : Location(location)),
        onShowFunc(onShowFunc),
        onHideFunc(onHideFunc),
        currentlyOpen(false),
        contents(location) {
        setVisible(false);

        add(&fadePanel);
        add(&contents);

        if (addBackPanelToContent) {
            contents.add(&backPanel);
        } else {
            add(&backPanel);
        }
    }

    FloatingPanel::~FloatingPanel(void) {
    }

    void FloatingPanel::show(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (!currentlyOpen) {
                setVisible(true);
                getTopLevel()->setSelectionBound(&contents);
                currentlyOpen = true;
                onShow();
            }
        });
    }

    void FloatingPanel::hide(void) {
        getTopLevel()->afterEvent->addFunction([this](void) {
            if (currentlyOpen) {
                setVisible(false);
                getTopLevel()->resetSelectionBound();
                currentlyOpen = false;
                if (selectWhenHide != NULL) {
                    getTopLevel()->setKeyboardSelected(selectWhenHide);
                }
                onHide();
            }
        });
    }

    void FloatingPanel::onShow(void) {
        onShowFunc();
    }

    void FloatingPanel::onHide(void) {
        onHideFunc();
    }
}
