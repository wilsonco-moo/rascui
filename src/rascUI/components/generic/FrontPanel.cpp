/*
 * TestRectangle.cpp
 *
 *  Created on: 3 Oct 2018
 *      Author: wilson
 */

#include "FrontPanel.h"

#include "../../base/Theme.h"
#include "../../base/TopLevelContainer.h"

namespace rascUI {

    FrontPanel::FrontPanel(const Location & location) : Component(location) {
    }

    FrontPanel::~FrontPanel(void) {
    }

    void FrontPanel::onDraw(void) {
        getTopLevel()->theme->drawFrontPanel(location);
    }

    bool FrontPanel::shouldRespondToKeyboard(void) const {
        return false;
    }
}
