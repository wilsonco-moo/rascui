/*
 * TestRectangle.cpp
 *
 *  Created on: 3 Oct 2018
 *      Author: wilson
 */

#include "BackPanel.h"

#include "../../base/Theme.h"
#include "../../base/TopLevelContainer.h"

namespace rascUI {

    BackPanel::BackPanel(const Location & location) : Component(location) {
    }

    BackPanel::~BackPanel(void) {
    }

    void BackPanel::onDraw(void) {
        getTopLevel()->theme->drawBackPanel(location);
    }

    bool BackPanel::shouldRespondToKeyboard(void) const {
        return false;
    }
}
