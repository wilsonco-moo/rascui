/*
 * ProgressBar.h
 *
 *  Created on: 29 Aug 2020
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_GENERIC_PROGRESSBAR_H_
#define RASCUI_COMPONENTS_GENERIC_PROGRESSBAR_H_

#include "../../base/Component.h"

namespace rascUI {

    /**
     * This component provides a (non user interactable) visual representation
     * of progress, which is specified as a floating point value between zero
     * and one. Progress bars can be either displayed horizontally or
     * vertically, and can be optionally inverted.
     * 
     * For more information vertical and inverted status, see documentation for
     * drawProgressBar in Theme.h.
     */
    class ProgressBar : public Component {
    private:
        GLfloat progress;
        bool vertical, inverted;

    public:
        ProgressBar(const Location & location = Location(), GLfloat progress = 0.0f, bool vertical = false, bool inverted = false);
        virtual ~ProgressBar(void);
        
    protected:
        virtual void onDraw(void) override;
        virtual bool shouldRespondToKeyboard(void) const override;
    
    public:
        /**
         * Allows access to our current progress value.
         */
        inline GLfloat getProgress(void) const {
            return progress;
        }
        
        /**
         * Allows setting our progress value.
         */
        inline void setProgress(GLfloat newProgress) {
            progress = newProgress;
        }
        
        /**
         * Allows access to our current vertical status.
         */
        inline bool isVertical(void) const {
            return vertical;
        }
        
        /**
         * Allows changing our vertical status.
         */
        inline void setVertical(bool newVertical) {
            vertical = newVertical;
        }
        
        /**
         * Allows access to our current inverted status.
         */
        inline bool isInverted(void) const {
            return inverted;
        }
        
        /**
         * Allows changing our inverted status.
         */
        inline void setInverted(bool newInverted) {
            inverted = newInverted;
        }
    };
}

#endif
