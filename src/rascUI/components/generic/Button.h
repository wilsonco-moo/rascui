/*
 * Button.h
 *
 *  Created on: 15 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_GENERIC_BUTTON_H_
#define RASCUI_COMPONENTS_GENERIC_BUTTON_H_

#include <functional>

#include "../abstract/ResponsiveComponent.h"
#include "../abstract/TextObject.h"

namespace rascUI {

    /**
     * This is a simple text-based button.
     *
     * This button class uses the mouse enter, press, release and leave events to update state automatically.
     * If the user overrides any of these, then the superclass method must be called, or the button visible state
     * functionality will break.
     *
     * The user should override onMouseClick to implement what happens when the user presses the mouse.
     * Note: The superclass onMouseClick method does not need to be called.
     *
     * ALSO: Instead of overriding onMouseClick, the user can provide to the constructor a std::function (lambda statement).
     *       This will run every time the button is clicked. Also, if the user provides a lambda statement, AND overrides the
     *       onMouseClick method, AND calls the superclass method from within it, then both the user implemented onMouseClick,
     *       and the lambda statement will run.
     *
     * The user may optionally provide text for the button. The default text is nothing.
     * The user may also optionally provide a lambda statement (std::function), which is run automatically when the button is pressed.
     *   NOTE: If the user overrides our onMouseClick method, the onClick lambda will never be run.
     *
     */
    class Button : public ResponsiveComponent, public TextObject {
    public:

        /**
         * This is automatically called when we are clicked, or never if the user overrides our onMouseClick method.
         */
        std::function<void(GLfloat, GLfloat, int)> onMouseClickFunc;

        /**
         * The constructor for button.
         *
         * The user can optionally provide a std::function (lambda statement), which will be called each time the button is clicked.
         */
        Button(
            const Location & location = Location(),
            std::string text = "",
            std::function<void(GLfloat, GLfloat, int)> onMouseClickFunc = [](GLfloat viewX, GLfloat viewY, int button){}
        );


        virtual ~Button(void);

    protected:
        /**
         * This automatically calls onMouseClickFunc, if this method is not overridden.
         */
        virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;

        virtual void onDraw(void) override;

    };
}

#endif
