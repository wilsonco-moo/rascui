/*
 * Label.cpp
 *
 *  Created on: 15 Oct 2018
 *      Author: wilson
 */

#include "Label.h"

#include "../../base/Theme.h"
#include "../../base/TopLevelContainer.h"

namespace rascUI {

    Label::Label(const Location & location, std::string text) :
        Component(location),
        TextObject(text) {
    }

    Label::~Label(void) {
    }

    void Label::onDraw(void) {
        getTopLevel()->theme->drawText(location, getText());
    }

    bool Label::shouldRespondToKeyboard(void) const {
        return false;
    }
}
