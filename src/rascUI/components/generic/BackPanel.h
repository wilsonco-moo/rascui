/*
 * TestRectangle.h
 *
 *  Created on: 3 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_GENERIC_BACKPANEL_H_
#define RASCUI_COMPONENTS_GENERIC_BACKPANEL_H_

#include "../../base/Component.h"

namespace rascUI {

    /**
     * BackPanel is a box that does very little.
     * This Component is intended to be used as a background for any user interface.
     */
    class BackPanel : public Component {
    public:
        BackPanel(const Location & location = Location());
        virtual ~BackPanel(void);

    protected:
        virtual void onDraw(void) override;

        /**
         * Here we return false, since Panel does nothing.
         */
        virtual bool shouldRespondToKeyboard(void) const override;
    };

}

#endif
