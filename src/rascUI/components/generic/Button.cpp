/*
 * Button.cpp
 *
 *  Created on: 15 Oct 2018
 *      Author: wilson
 */

#include "Button.h"

#include <iostream>

#include "../../util/Location.h"
#include "../../base/Theme.h"
#include "../../base/TopLevelContainer.h"

namespace rascUI {

    Button::Button(const Location & location, std::string text, std::function<void(GLfloat, GLfloat, int)> onMouseClickFunc) :
        ResponsiveComponent(location),
        TextObject(text),
        onMouseClickFunc(onMouseClickFunc) {
    }

    Button::~Button(void) {
    }

    void Button::onDraw(void) {
        getTopLevel()->theme->drawButton(location);
        getTopLevel()->theme->drawText(location, getText());
    }

    void Button::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        onMouseClickFunc(viewX, viewY, button);
    }
}
