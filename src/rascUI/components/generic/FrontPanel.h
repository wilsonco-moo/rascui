/*
 * TestRectangle.h
 *
 *  Created on: 3 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_GENERIC_FRONTPANEL_H_
#define RASCUI_COMPONENTS_GENERIC_FRONTPANEL_H_

#include "../../base/Component.h"

namespace rascUI {

    /**
     * FrontPanel is a box that does very little.
     * This Component is intended to be put in front of a BackPanel, to show a defined section within the UI.
     *
     * For backgrounds to be put more generally behind other UI components, please use BackPanel instead.
     */
    class FrontPanel : public Component {
    public:
        FrontPanel(const Location & location = Location());
        virtual ~FrontPanel(void);

    protected:
        virtual void onDraw(void) override;

        /**
         * Here we return false, since Panel does nothing.
         */
        virtual bool shouldRespondToKeyboard(void) const override;
    };

}

#endif
