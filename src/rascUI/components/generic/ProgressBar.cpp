/*
 * ProgressBar.cpp
 *
 *  Created on: 29 Aug 2020
 *      Author: wilson
 */

#include "ProgressBar.h"

#include "../../base/TopLevelContainer.h"
#include "../../base/Theme.h"

namespace rascUI {

    ProgressBar::ProgressBar(const Location & location, GLfloat progress, bool vertical, bool inverted) :
        Component(location),
        progress(progress),
        vertical(vertical),
        inverted(inverted) {
    }
    
    ProgressBar::~ProgressBar(void) {
    }
    
    void ProgressBar::onDraw(void) {
        getTopLevel()->theme->drawProgressBar(location, progress, vertical, inverted);
    }
    
    bool ProgressBar::shouldRespondToKeyboard(void) const {
        return false;
    }
}

