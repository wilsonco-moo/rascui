/*
 * Label.h
 *
 *  Created on: 15 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_GENERIC_LABEL_H_
#define RASCUI_COMPONENTS_GENERIC_LABEL_H_

#include "../../base/Component.h"
#include "../abstract/TextObject.h"

namespace rascUI {

    /**
     * This is a simple component consisting only of a piece of text.
     * Label will simply display the piece of text, in it's location, and will not actually show any kind of
     * box behind it.
     *
     * Label is a simple subclass of TextObject and Component.
     */
    class Label : public Component, public TextObject {
    public:

        Label(const Location & location = Location(), std::string text = "");
        virtual ~Label(void);

    protected:
        virtual void onDraw(void) override;

        /**
         * Here we return false because Label does nothing.
         */
        virtual bool shouldRespondToKeyboard(void) const override;
    };

}

#endif
