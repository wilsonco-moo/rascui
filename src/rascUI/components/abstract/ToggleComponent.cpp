/*
 * ToggleComponent.cpp
 *
 *  Created on: 17 Nov 2018
 *      Author: wilson
 */

#include "ToggleComponent.h"

namespace rascUI {

    ToggleComponent::ToggleComponent(const Location & location) :
        ResponsiveComponent(location),
        currentlySelected(false) { // Assume that we are initially not selected, (if that is not the case then it will
                                   // trigger a location state update anyway, so it doesn't really matter).

        this->location.pushStateModifier([](State state, Component * component) -> State {
            ToggleComponent * t = (ToggleComponent *)component;
            if (t->currentlySelected) {
                // If we are selected, then return the selected state.
                // In the case we are selected, AND mouse-overed, then return the mousePress state. This gives the user
                // visual feedback when we get mouse-overed, while we are selected.
                return state == State::mouseOver ? State::mousePress : State::selected;
            } else {
                // If we are not selected, then use the default state.
                // In the case that we are mouse pressed BUT NOT selected, then use the selected state instead. This gives
                // the user visual feedback between pressing their mouse on a deselected toggle component, and releasing their
                // mouse to leave a selected but mouse-overed toggle component.
                return state == State::mousePress ? State::selected : state;
            }
        });
    }

    ToggleComponent::~ToggleComponent(void) {
    }

    void ToggleComponent::onDraw(void) {
        // Trigger a location state update if our selection has changed since last frame.
        bool newSelected = isToggleComponentSelected();
        if (newSelected != currentlySelected) {
            currentlySelected = newSelected;
            location.updateState(this);
        }
    }
}
