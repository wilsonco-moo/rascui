/*
 * TextObject.cpp
 *
 *  Created on: 17 Nov 2018
 *      Author: wilson
 */

#include "TextObject.h"

#include "../../util/Bindings.h"

namespace rascUI {

    TextObject::TextObject(std::string text) :
        text(text),
        cursorPos(text.length()){
    }

    TextObject::~TextObject(void) {
    }

    void TextObject::setTextCursorPosition(size_t cursorPos) {
        if (cursorPos <= text.length()) {
            this->cursorPos = cursorPos;
        }
    }

    void TextObject::moveTextCursorPosition(int amount) {
        if (amount < 0) {
            unsigned int amountLeft = ((unsigned int)-amount);
            if (amountLeft <= cursorPos) {
                cursorPos -= amountLeft;
            }
        } else {
            size_t newPos = cursorPos + amount;
            if (newPos <= text.length()) {
                cursorPos = newPos;
            }
        }
    }

    void TextObject::textAddCharacter(const Bindings * bindings, int character, bool special) {
        switch(bindings->getTextEditAction(character, special)) {
        case TextEditAction::moveCursorPrevious:
            moveTextCursorPosition(-1);
            break;
        case TextEditAction::moveCursorNext:
            moveTextCursorPosition(1);
            break;
        case TextEditAction::moveCursorFirst:
            textCursorFirst();
            break;
        case TextEditAction::moveCursorLast:
            textCursorLast();
            break;
        case TextEditAction::deleteCharacterPrevious:
            if (cursorPos >= 1) {
                text.erase(--cursorPos, 1);
            }
            break;
        case TextEditAction::deleteCharacterNext:
            if (cursorPos <= text.length()) {
                text.erase(cursorPos, 1);
            }
            break;
        case TextEditAction::noAction:
            // Only add non-special characters in the printable ASCII character range.
            if (character >= 32 && character < 127 && !special) {
                text.insert(cursorPos++, 1, character);
            }
            break;
        default:
            break;
        }
    }

    void TextObject::textAddString(const std::string & string) {
        text.insert(cursorPos, string);
        cursorPos += string.length();
    }

}
