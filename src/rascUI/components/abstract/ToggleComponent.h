/*
 * ToggleComponent.h
 *
 *  Created on: 17 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_ABSTRACT_TOGGLECOMPONENT_H_
#define RASCUI_COMPONENTS_ABSTRACT_TOGGLECOMPONENT_H_

#include "ResponsiveComponent.h"

namespace rascUI {

    /**
     * ToggleComponent defines a component who's selection can be toggled. Examples of these include
     * ToggleButton and BasicTextEntryBox.
     *
     * ToggleComponent provides a pure virtual method: isToggleComponentSelected.
     * The onDraw method is overidden such that isToggleComponentSelected is used to give the user feedback
     * of whether we are selected.
     *
     * Implementing components should provide an implementation for onToggleComponentDraw, which is called from
     * within onDraw, after applying state modification based on isToggleComponentSelected().
     */
    class ToggleComponent : public ResponsiveComponent {
    private:
        /**
         * Here we keep a record of whether we were selected last frame, so we can tell our location
         * to update our state if that has changed since last frame.
         */
        bool currentlySelected;

    public:
        ToggleComponent(const Location & location = Location());
        virtual ~ToggleComponent(void);

        /**
         * Toggle components must implement this, which returns whether it is selected.
         */
        virtual bool isToggleComponentSelected(void) const = 0;

    protected:

        /**
         * NOTE: ALL IMPLEMENTING CLASSES MUST CALL THIS SUPERCLASS DRAW METHOD BEFORE THEY DO DRAWING, OTHERWISE
         *       THE LOCATION'S STATE WILL NOT UPDATE AUTOMATICALLY WHEN WE GET DESELECTED.
         */
        virtual void onDraw(void) override;

    };
}

#endif
