/*
 * MouseResponsiveComponent.h
 *
 *  Created on: 17 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_ABSTRACT_RESPONSIVECOMPONENT_H_
#define RASCUI_COMPONENTS_ABSTRACT_RESPONSIVECOMPONENT_H_

#include "../../base/Component.h"

namespace rascUI {

    /**
     * ResponsiveComponent is a component which automatically sets the state of the component
     * when the user's mouse enters, presses, releases and leaves the component.
     * 
     * It is also automatically keyboard responsive, so can be selected by the user using keyboard
     * interaction.
     * 
     * By default, ResponsiveComponent's cusor is CursorType::hand.
     */
    class ResponsiveComponent : public Component {
    private:
        /**
         * This stores our current internal state, which we change when we receive mouse events.
         * Our state modifier function returns this field.
         */
        State internalState;

    public:
        ResponsiveComponent(const Location & location = Location());
        virtual ~ResponsiveComponent(void);


        /**
         * Since we are a ResponsiveComponent, we should be repainted whenever there is any state change,
         * and should also repaint our parent if we go from visible to invisible.
         */
        virtual void doesStateChangeRequireRepaint(State oldState, State newState, bool * shouldRepaintSelf, bool * shouldRepaintParent) override;

        CursorType getCursorType(void) const override;

    protected:
        virtual void onMouseEnter(GLfloat viewX, GLfloat viewY) override;
        virtual void onMousePress(GLfloat viewX, GLfloat viewY, int button) override;
        virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button) override;
        virtual void onMouseLeave(GLfloat viewX, GLfloat viewY) override;

        /**
         * Here we return true, since button can do things.
         */
        virtual bool shouldRespondToKeyboard(void) const override;
    };

}

#endif
