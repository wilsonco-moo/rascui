/*
 * MouseResponsiveComponent.cpp
 *
 *  Created on: 17 Nov 2018
 *      Author: wilson
 */

#include "ResponsiveComponent.h"

#include "../../base/Theme.h"

namespace rascUI {

    ResponsiveComponent::ResponsiveComponent(const Location & location) :
        Component(location),
        internalState(State::normal) {

        // Push a state modifier to our location, which simply returns our internal state,
        // which is set by the mouse interaction functions below.
        this->location.pushStateModifier([](State state, Component * component) -> State {
            return ((ResponsiveComponent *)component)->internalState;
        });
    }

    ResponsiveComponent::~ResponsiveComponent(void) {
    }

    void ResponsiveComponent::doesStateChangeRequireRepaint(State oldState, State newState, bool * shouldRepaintSelf, bool * shouldRepaintParent) {
        if (oldState == newState) return;
        if (newState == State::invisible) {
            *shouldRepaintParent = true;
        } else {
            *shouldRepaintSelf = true;
        }
    }
    
    CursorType ResponsiveComponent::getCursorType(void) const {
        // If active/visible, pick "hand" cursor, else use parent class' cursor.
        return isActiveAndVisible() ? CursorType::hand : Component::getCursorType();
    }

    void ResponsiveComponent::onMouseEnter(GLfloat viewX, GLfloat viewY) {
        internalState = State::mouseOver;
        location.updateState(this);
    }
    void ResponsiveComponent::onMousePress(GLfloat viewX, GLfloat viewY, int button) {
        internalState = State::mousePress;
        location.updateState(this);
    }
    void ResponsiveComponent::onMouseRelease(GLfloat viewX, GLfloat viewY, int button) {
        internalState = State::mouseOver;
        location.updateState(this);
    }
    void ResponsiveComponent::onMouseLeave(GLfloat viewX, GLfloat viewY) {
        internalState = State::normal;
        location.updateState(this);
    }

    bool ResponsiveComponent::shouldRespondToKeyboard(void) const {
        return isActiveAndVisible();
    }
}
