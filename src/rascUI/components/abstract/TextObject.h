/*
 * TextObject.h
 *
 *  Created on: 17 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_ABSTRACT_TEXTOBJECT_H_
#define RASCUI_COMPONENTS_ABSTRACT_TEXTOBJECT_H_

#include <string>

namespace rascUI {

    class Bindings;

    /**
     * This represents an object that stores text.
     * A component that stores text should extend from this.
     *
     * This class includes functionality for cursor based addition and removal of text, such as for the input of text.
     * NOTE: This text input system only supports text within the printable ASCII range.
     */
    class TextObject {
    private:

        /**
         * Our internal piece of text.
         */
        std::string text;

        /**
         * Our internal cursor position.
         */
        size_t cursorPos;

    public:

        /**
         * The main constructor. Here a piece of text can optionally be given.
         * The cursor will be automatically put at the end of the text.
         */
        TextObject(std::string text = "");
        virtual ~TextObject(void);


        /**
         * Moves the text cursor position to the specified place.
         * If that position would put it off the end of the text, then nothing
         * will be done.
         */
        void setTextCursorPosition(size_t cursorPos);

        /**
         * Moves the text cursor position by the specified amount.
         * If that position would put it off the end of the text, or before the beginning, then nothing
         * will be done.
         */
        void moveTextCursorPosition(int amount);

        /**
         * This types a single character at the current cursor position. If the character is within the
         * printable ASCII range, and according to the Bindings object this character represents
         * TextEditAction::noAction, then the character will be typed. Otherwise, the following
         * operations will take place:
         *  > If the character is an invalid (outside printable ACII range) character:
         *    then nothing will be done.
         *  > If the character maps to TextEditAction::moveCursorPrevious (usually left arrow):
         *    (if possible), the cursor will be moved to the previous character. This is analogous to calling moveTextCursorPosition(-1).
         *  > If the character maps to TextEditAction::moveCursorNext (usually right arrow):
         *    (if possible), the cursor will be moved to the next character. This is analogous to calling moveTextCursorPosition(1).
         *  > If the character maps to TextEditAction::moveCursorFirst (usually home key):
         *    the cursor will be moved to the first possible cursor position. This is analogous to calling textCursorFirst().
         *  > If the character maps to TextEditAction::moveCursorLast (usually end key):
         *    the cursor will be moved to the last possible cursor position. This is analogous to calling textCursorLast().
         *  > If the character maps to TextEditAction::deleteCharacterPrevious (usually backspace key):
         *    (if possible), the character before the cursor will be removed, and the cursor will be moved back.
         *  > If the character maps to TextEditAction::deleteCharacterNext (usually delete key):
         *    (if possible), the character after the cursor will be removed, and the cursor will not move.
         */
        void textAddCharacter(const Bindings * bindings, int character, bool special);

        /**
         * This types a piece of text at the current cursor position.
         * Any control characters or out of range characters are ignored here.
         */
        void textAddString(const std::string & string);

        /**
         * Moves the cursor to the first possible cursor location, useful if the user presses the home key.
         */
        inline void textCursorFirst(void) {
            cursorPos = 0;
        }

        /**
         * Moves the cursor to the last possible cursor location, useful if the user presses the end key.
         */
        inline void textCursorLast(void) {
            cursorPos = text.length();
        }

        /**
         * Gets our text cursor position.
         */
        inline size_t getTextCursorPosition(void) const {
            return cursorPos;
        }

        /**
         * Sets our internal text based on a c-style string.
         * This method puts the cursor to the end.
         */
        inline void setText(const char * text) {
            this->text = text;
            cursorPos = this->text.length();
        }
        
        /**
         * Sets our internal text based on a character buffer and length. Can
         * be used for strings which are not null terminated.
         * This method puts the cursor to the end.
         */
        inline void setText(const char * buffer, size_t length) {
            this->text.assign(buffer, length);
            cursorPos = this->text.length();
        }

        /**
         * Sets our internal text based on a std::string.
         * This method puts the cursor to the end.
         */
        inline void setText(const std::string & text) {
            this->text = text;
            cursorPos = this->text.length();
        }

        /**
         * Returns our internal text, as a std::string reference.
         */
        inline const std::string & getText(void) const {
            return text;
        }
    };
}

#endif
