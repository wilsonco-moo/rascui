/*
 * Self contained rascUI demonstration (and stress test) program.
 * 
 * Yes I am aware of the fact that this file is an enormous mess. It has been
 * botched and repeatedly added to, over a long period of time, purely for the
 * purpose of testing rascUI features.
 * This file should NOT be considered an example of how to best use the UI
 * system.
 */

#include <iostream>
#include <iterator>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <cctype>
#include <chrono>
#include <string>
#include <thread>
#include <vector>
#include <mutex>
#include <cmath>

#include <rascUI/components/scroll/AnimatedSegmentContainer.h>
#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/scroll/SegmentContainer.h>
#include <rascUI/components/abstract/TextObject.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUItheme/utils/DynamicThemeLoader.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/platform/WindowConfig.h>
#include <rascUI/util/FunctionQueue.h>
#include <rascUI/util/EmplaceArray.h>
#include <rascUI/util/Rectangle.h>
#include <rascUI/util/Location.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#ifdef _WIN32
    // RascUI plaform system.
    #ifdef FORCE_ENABLE_WIN_SCALABLE_THEME
        #include <rascUItheme/themes/win/scalableTheme/ScalableTheme.h>
    #endif
    #ifdef FORCE_ENABLE_WIN_BASIC_THEME
        #include <rascUItheme/themes/win/basicTheme/BasicTheme.h>
    #endif
    #include <rascUIwin/platform/Window.h>
    #include <rascUIwin/platform/Context.h>
    namespace platform = rascUIwin;
    #define THEME_NAME "scalableTheme.dll"
    // Other platform specific includes.
    #include <windows.h>
#else
    // RascUI plaform system.
    #include <rascUIxcb/platform/Window.h>
    #include <rascUIxcb/platform/Context.h>
    namespace platform = rascUIxcb;
    #define THEME_NAME "libscalableTheme.so"
    // Platform specific includes.
    #define XK_MISCELLANY
    #include <X11/keysymdef.h>
#endif


class CustomContainer : public rascUI::Container {
public:
    CustomContainer(rascUI::Location location) :
        rascUI::Container(location) {
    }
    virtual ~CustomContainer(void) {
    }

protected:
    virtual void onDraw(void) override {
        //std::cout << "DRAW RIGHT CONTAINER.\n";
        rascUI::Container::onDraw();
    }
    virtual void boundedRepaint(rascUI::Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) override {
        /*if (boundingBox.overlaps(getBoundedArea())) {
            std::cout << "[" << boundingBox.x << ", " << boundingBox.y << ", " << boundingBox.width << ", " << boundingBox.height << "] ";
            std::cout << "COLLIDES\n";
        }*/
        rascUI::Container::boundedRepaint(boundingBox, wantsRepaint, childWantsRepaint);
    }
};

class CustomWindow : public platform::Window {
private:    
    GLfloat * scale;
    int * scaleExp;
public:
    CustomWindow(GLfloat * scale, int * scaleExp, platform::Context * context, const rascUI::WindowConfig & config, rascUI::Theme * theme = NULL, rascUI::Location location = rascUI::Location(), const rascUI::Bindings * bindings = NULL, const GLfloat * xScalePtr = NULL, const GLfloat * yScalePtr = NULL) :
        platform::Window(context, config, theme, location, bindings, xScalePtr, yScalePtr),
        scale(scale),
        scaleExp(scaleExp) {
    }
    virtual ~CustomWindow(void) {
    }
    virtual void onUnusedKeyPress(int key, bool special) override {
        int change = 0;
        #ifdef _WIN32
            if (special) {
                if (key == VK_F1) {
                    change = 1;
                } else if (key == VK_F2) {
                    change = -1;
                } else if (key == VK_F3) {
                    change = -*scaleExp;
                }
            }
        #else
            if (key == XK_F1) {
                change = 1;
            } else if (key == XK_F2) {
                change = -1;
            } else if (key == XK_F3) {
                change = -*scaleExp;
            }
        #endif
        if (change == 0) return;
        *scaleExp += change;
        *scale = std::pow(2.0f, ((GLfloat)*scaleExp) / 8.0f);
        getContext()->repaintEverything();
    }
};


int main(void) {
    
    #ifdef FORCE_ENABLE_WIN_SCALABLE_THEME
        winScalableTheme::ScalableTheme scalableTheme;
        rascUI::Theme * theme = &scalableTheme;
    #else
        #ifdef FORCE_ENABLE_WIN_BASIC_THEME
            winBasicTheme::BasicTheme basicTheme;
            rascUI::Theme * theme = &basicTheme;
        #else
            rascUItheme::DynamicThemeLoader themeLoader(THEME_NAME);
            if (!themeLoader.loadedSuccessfully()) {
                std::cerr << "Failed to load theme.\n";
                return EXIT_FAILURE;
            }
            rascUI::Theme * theme = themeLoader.getTheme();
        #endif
    #endif
    
    #define LAYOUT_THEME theme
    
    GLfloat scale = 1.0f;
    int scaleExp = 0;
    
    platform::Context context(theme, &scale, &scale);
    if (!context.wasSuccessful()) {
        std::cerr << "Failed to start context.\n";
        return EXIT_FAILURE;
    }
    
    #define SCRT_PANETEST_LOC     GEN_BORDER
    
    #define SCRT_TOPP(loc)        PIN_T_INTERNAL(COUNT_ONEBORDER_Y(1), loc)
    #define SCRT_BOTTOMP(loc)     PIN_B_INTERNAL(COUNT_ONEBORDER_Y(1), loc)
    #define SCRT_CENTREP(loc)     SUB_BORDERMARGIN_Y_INTERNAL(UI_BHEIGHT, loc)
    
    #define SCRT_LEFTP(loc)       PIN_L_INTERNAL(COUNT_ONEBORDER_Y(1), loc)
    #define SCRT_RIGHTP(loc)      PIN_R_INTERNAL(COUNT_ONEBORDER_Y(1), loc)
    #define SCRT_MIDDLEP(loc)     SUB_BORDERMARGIN_X_INTERNAL(UI_BHEIGHT, loc)
    
    #define SCRT_NSEGMENT_LOC     BORDERTABLE_XY(0, 0, 3, 2, SCRT_PANETEST_LOC)
    #define SCRT_ASEGMENT_LOC     BORDERTABLE_XY(1, 0, 3, 2, SCRT_PANETEST_LOC)
    #define SCRT_SCROLLPANE_LOC   BORDERTABLE_XY(2, 0, 3, 2, SCRT_PANETEST_LOC)
    #define SCRT_NSEGMENT_H_LOC   BORDERTABLE_XY(0, 1, 3, 2, SCRT_PANETEST_LOC)
    #define SCRT_ASEGMENT_H_LOC   BORDERTABLE_XY(1, 1, 3, 2, SCRT_PANETEST_LOC)
    #define SCRT_SCROLLPANE_H_LOC BORDERTABLE_XY(2, 1, 3, 2, SCRT_PANETEST_LOC)
    
    rascUI::WindowConfig windowConfig;
    windowConfig.screenWidth = 500;
    windowConfig.screenHeight = 360;
    windowConfig.title = "Scroll demonstration";
    CustomWindow scrollWindow(&scale, &scaleExp, &context, windowConfig);
    
    rascUI::SegmentContainer nsegmentContainer(rascUI::Location(SCRT_CENTREP(SCRT_NSEGMENT_LOC)), COUNT_ONEBORDER_Y(1), true);
    rascUI::FrontPanel nsegmentTopPanel(rascUI::Location(SCRT_TOPP(SCRT_NSEGMENT_LOC))),
                       nsegmentBottomPanel(rascUI::Location(SCRT_BOTTOMP(SCRT_NSEGMENT_LOC)));
    rascUI::Label nsegmentTopLabel(rascUI::Location(SCRT_TOPP(SCRT_NSEGMENT_LOC)), "Normal seg");
    scrollWindow.add(&nsegmentContainer);
    scrollWindow.add(&nsegmentTopPanel);
    scrollWindow.add(&nsegmentTopLabel);
    scrollWindow.add(&nsegmentBottomPanel);
    #define NSEGMENT_BUTTON_COUNT 15
    rascUI::EmplaceArray<rascUI::Button, NSEGMENT_BUTTON_COUNT> nsegmentButtons;
    while(nsegmentButtons.size() < NSEGMENT_BUTTON_COUNT) {
        nsegmentButtons.emplace(rascUI::Location(SUB_BORDER_B(GEN_FILL)), std::string("N button ") + std::to_string(nsegmentButtons.size()));
        nsegmentContainer.add(&nsegmentButtons.back());
    }
    
    rascUI::AnimatedSegmentContainer asegmentContainer(rascUI::Location(SCRT_CENTREP(SCRT_ASEGMENT_LOC)), COUNT_ONEBORDER_Y(1), true);
    asegmentContainer.smoothMoveSpeed = 0.0f;
    rascUI::FrontPanel asegmentTopPanel(rascUI::Location(SCRT_TOPP(SCRT_ASEGMENT_LOC))),
                       asegmentBottomPanel(rascUI::Location(SCRT_BOTTOMP(SCRT_ASEGMENT_LOC)));
    rascUI::Label asegmentTopLabel(rascUI::Location(SCRT_TOPP(SCRT_ASEGMENT_LOC)), "Animated seg");
    scrollWindow.add(&asegmentContainer);
    scrollWindow.add(&asegmentTopPanel);
    scrollWindow.add(&asegmentTopLabel);
    scrollWindow.add(&asegmentBottomPanel);
    #define ASEGMENT_BUTTON_COUNT 15
    rascUI::EmplaceArray<rascUI::Button, ASEGMENT_BUTTON_COUNT> asegmentButtons;
    while(asegmentButtons.size() < ASEGMENT_BUTTON_COUNT) {
        asegmentButtons.emplace(rascUI::Location(SUB_BORDER_B(GEN_FILL)), std::string("A button ") + std::to_string(asegmentButtons.size()));
        asegmentContainer.add(&asegmentButtons.back());
    }
    
    rascUI::ScrollPane scrollpaneContainer(theme, rascUI::Location(SCRT_CENTREP(SCRT_SCROLLPANE_LOC)), true, true, false, false);
    scrollpaneContainer.contents.smoothMoveSpeed = 0.0f;
    rascUI::FrontPanel scrollpaneTopPanel(rascUI::Location(SCRT_TOPP(SCRT_SCROLLPANE_LOC))),
                       scrollpaneBottomPanel(rascUI::Location(SCRT_BOTTOMP(SCRT_SCROLLPANE_LOC)));
    rascUI::Label scrollpaneTopLabel(rascUI::Location(SCRT_TOPP(SCRT_SCROLLPANE_LOC)), "Scroll pane");
    scrollWindow.add(&scrollpaneContainer);
    scrollWindow.add(&scrollpaneTopPanel);
    scrollWindow.add(&scrollpaneTopLabel);
    scrollWindow.add(&scrollpaneBottomPanel);
    #define SCROLLPANE_BUTTON_COUNT 15
    rascUI::EmplaceArray<rascUI::Button, SCROLLPANE_BUTTON_COUNT> scrollpaneButtons;
    while(scrollpaneButtons.size() < SCROLLPANE_BUTTON_COUNT) {
        scrollpaneButtons.emplace(rascUI::Location(SUB_BORDER_B(GEN_FILL)), std::string("S button ") + std::to_string(scrollpaneButtons.size()));
        scrollpaneContainer.contents.add(&scrollpaneButtons.back());
    }
    
    rascUI::SegmentContainer nsegmenthContainer(rascUI::Location(SCRT_MIDDLEP(SCRT_NSEGMENT_H_LOC)), COUNT_ONEBORDER_Y(1), true, false);
    rascUI::FrontPanel nsegmenthTopPanel(rascUI::Location(SCRT_LEFTP(SCRT_NSEGMENT_H_LOC))),
                       nsegmenthBottomPanel(rascUI::Location(SCRT_RIGHTP(SCRT_NSEGMENT_H_LOC)));
    rascUI::Label nsegmenthTopLabel(rascUI::Location(SCRT_LEFTP(SCRT_NSEGMENT_H_LOC)), "NS");
    scrollWindow.add(&nsegmenthContainer);
    scrollWindow.add(&nsegmenthTopPanel);
    scrollWindow.add(&nsegmenthTopLabel);
    scrollWindow.add(&nsegmenthBottomPanel);
    #define NSEGMENT_H_BUTTON_COUNT 15
    rascUI::EmplaceArray<rascUI::Button, NSEGMENT_H_BUTTON_COUNT> nsegmenthButtons;
    while(nsegmenthButtons.size() < NSEGMENT_H_BUTTON_COUNT) {
        nsegmenthButtons.emplace(rascUI::Location(SUB_BORDER_R(GEN_FILL)), std::to_string(nsegmenthButtons.size()));
        nsegmenthContainer.add(&nsegmenthButtons.back());
    }
    
    rascUI::AnimatedSegmentContainer asegmenthContainer(rascUI::Location(SCRT_MIDDLEP(SCRT_ASEGMENT_H_LOC)), COUNT_ONEBORDER_Y(1), true, false);
    asegmenthContainer.smoothMoveSpeed = 0.0f;
    rascUI::FrontPanel asegmenthTopPanel(rascUI::Location(SCRT_LEFTP(SCRT_ASEGMENT_H_LOC))),
                       asegmenthBottomPanel(rascUI::Location(SCRT_RIGHTP(SCRT_ASEGMENT_H_LOC)));
    rascUI::Label asegmenthTopLabel(rascUI::Location(SCRT_LEFTP(SCRT_ASEGMENT_H_LOC)), "AS");
    scrollWindow.add(&asegmenthContainer);
    scrollWindow.add(&asegmenthTopPanel);
    scrollWindow.add(&asegmenthTopLabel);
    scrollWindow.add(&asegmenthBottomPanel);
    #define ASEGMENT_H_BUTTON_COUNT 15
    rascUI::EmplaceArray<rascUI::Button, ASEGMENT_H_BUTTON_COUNT> asegmenthButtons;
    while(asegmenthButtons.size() < ASEGMENT_H_BUTTON_COUNT) {
        asegmenthButtons.emplace(rascUI::Location(SUB_BORDER_R(GEN_FILL)), std::to_string(asegmenthButtons.size()));
        asegmenthContainer.add(&asegmenthButtons.back());
    }
    
    rascUI::ScrollPane scrollpanehContainer(theme, rascUI::Location(SCRT_MIDDLEP(SCRT_SCROLLPANE_H_LOC)), COUNT_ONEBORDER_Y(1), true, false, false, false);
    scrollpanehContainer.contents.smoothMoveSpeed = 0.0f;
    rascUI::FrontPanel scrollpanehTopPanel(rascUI::Location(SCRT_LEFTP(SCRT_SCROLLPANE_H_LOC))),
                       scrollpanehBottomPanel(rascUI::Location(SCRT_RIGHTP(SCRT_SCROLLPANE_H_LOC)));
    rascUI::Label scrollpanehTopLabel(rascUI::Location(SCRT_LEFTP(SCRT_SCROLLPANE_H_LOC)), "SP");
    scrollWindow.add(&scrollpanehContainer);
    scrollWindow.add(&scrollpanehTopPanel);
    scrollWindow.add(&scrollpanehTopLabel);
    scrollWindow.add(&scrollpanehBottomPanel);
    #define SCROLLPANE_H_BUTTON_COUNT 15
    rascUI::EmplaceArray<rascUI::Button, SCROLLPANE_H_BUTTON_COUNT> scrollpanehButtons;
    while(scrollpanehButtons.size() < SCROLLPANE_H_BUTTON_COUNT) {
        scrollpanehButtons.emplace(rascUI::Location(SUB_BORDER_R(GEN_FILL)), std::to_string(scrollpanehButtons.size()));
        scrollpanehContainer.contents.add(&scrollpanehButtons.back());
    }

    
    
    
    windowConfig.screenWidth = 400;
    windowConfig.screenHeight = 400;
    windowConfig.title = "RascUI demonstration";
    CustomWindow window(&scale, &scaleExp, &context, windowConfig);
    
    #define TOPH (UI_BHEIGHT * 4 + UI_BORDER * 9)
    #define LEFT_SCROLL_BUTTON_COUNT 200
    #define RIGHT_SCROLL_BUTTON_COUNT 35


    #define MAX_SEGMENT_HEIGHT (COUNT_OUTBORDER_Y(1) + UI_BORDER)

    #define H_CONTAINER_LOC(id)  \
        BORDERTABLE_X(id, 2,     \
         PIN_T(TOPH,             \
          GEN_BORDER             \
        ))

    #define V_CONTAINER_LOC(id)  \
        BORDERTABLE_Y(id, 2,     \
         GEN_BORDER              \
        )

    #define BOTTOM_SECTION_LOC      \
        SUB_BORDERMARGIN_T(TOPH,    \
         GEN_BORDER                 \
        )

    #define SCROLLPANE_LOC(id)                    \
        TABLE_X(id, 2,                            \
         SUB_BORDER_R(                            \
          SUB_MARGIN_Y(MAX_SEGMENT_HEIGHT,        \
           BOTTOM_SECTION_LOC                     \
        )))
    
    #define RIGHT_FILLER_BACKPANEL_LOC           \
        PIN_R(UI_BORDER,                         \
         SUB_MARGIN_Y(MAX_SEGMENT_HEIGHT,        \
          BOTTOM_SECTION_LOC                     \
        ))
        
    #define ABOVE_SCROLLPANE_PANEL_LOC           \
        PIN_T(MAX_SEGMENT_HEIGHT,                \
         BOTTOM_SECTION_LOC                      \
        )
        
    #define BELOW_SCROLLPANE_PANEL_LOC           \
        PIN_B(MAX_SEGMENT_HEIGHT,                \
         BOTTOM_SECTION_LOC                      \
        )
    
    unsigned long long seconds = 0; char partialSeconds = 0;
    void * topButtonToken = NULL;
    bool openPopup = false;

    rascUI::Container leftContainer(rascUI::Location(H_CONTAINER_LOC(0)));
        rascUI::BackPanel leftBackPanel;
        rascUI::Container leftTopContainer(rascUI::Location(V_CONTAINER_LOC(0)));
            rascUI::FrontPanel leftTopFrontPanel;
            std::function<void(void)> buttonChangeFunc;
            rascUI::ToggleButtonSeries leftTopSeries(true, [&topButtonToken, &buttonChangeFunc, &context](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                if (toggleButton == NULL) {
                    rascUI::FunctionQueue::deleteToken(topButtonToken);
                    topButtonToken = NULL;
                } else {
                    topButtonToken = context.beforeEvent.createToken();
                    context.beforeEvent.addTimedRepeatingFunction(topButtonToken, 225, 100, buttonChangeFunc);
                }
            });
            rascUI::ToggleButton leftTopTopButton(&leftTopSeries, rascUI::Location(V_CONTAINER_LOC(0)), "Time: 0.0 seconds");
            buttonChangeFunc = [&leftTopTopButton, &seconds, &partialSeconds](void) {
                partialSeconds++;
                if (partialSeconds >= 10) {
                    partialSeconds = 0;
                    seconds++;
                }
                leftTopTopButton.setText(std::string("Time: ") + std::to_string(seconds) + "." + std::to_string((int)partialSeconds) + " seconds");
                leftTopTopButton.repaint();
            };
            rascUI::Button leftTopBottomButton(rascUI::Location(V_CONTAINER_LOC(1)), "Scroll test", [&scrollWindow](GLfloat viewX, GLfloat viewY, int button) {
                scrollWindow.setMapped(!scrollWindow.isMapped());
            });
        rascUI::Container leftBottomContainer(rascUI::Location(V_CONTAINER_LOC(1)));
            rascUI::FrontPanel leftBottomFrontPanel;
            rascUI::ToggleButtonSeries series(true);
            rascUI::ToggleButton leftBottomTopButton(&series, rascUI::Location(V_CONTAINER_LOC(0)), "Left bottom top");
            rascUI::ToggleButton leftBottomBottomButton(&series, rascUI::Location(V_CONTAINER_LOC(1)), "Left bottom bottom");
    CustomContainer rightContainer(rascUI::Location(H_CONTAINER_LOC(1)));
        rascUI::BackPanel rightBackPanel;
        rascUI::Container rightTopContainer(rascUI::Location(V_CONTAINER_LOC(0)));
            rascUI::FrontPanel rightTopFrontPanel;
            rascUI::Button rightTopTopButton(rascUI::Location(V_CONTAINER_LOC(0)), "Open popup window", [&openPopup, &context](GLfloat viewX, GLfloat viewY, int button) {
                if (!openPopup) {
                    openPopup = true;
                    context.endMainLoop();
                }
            });
            rascUI::Button rightTopBottomButton(rascUI::Location(V_CONTAINER_LOC(1)), "Close popup window", [&openPopup, &context](GLfloat viewX, GLfloat viewY, int button) {
                if (openPopup) {
                    openPopup = false;
                    context.endMainLoop();
                }
            });
        rascUI::Container rightBottomContainer(rascUI::Location(V_CONTAINER_LOC(1)));
            rascUI::FrontPanel rightBottomFrontPanel;
            rascUI::Button rightBottomTopButton(rascUI::Location(V_CONTAINER_LOC(0)), "Right bottom top");
            rascUI::BasicTextEntryBox rightBottomBottomButton(rascUI::Location(V_CONTAINER_LOC(1)), "Text entry!!");

    rascUI::ScrollPane leftScrollPane(theme, rascUI::Location(SCROLLPANE_LOC(0)), true, true, false, false);
    leftScrollPane.contents.smoothMoveSpeed = 0;

    std::vector<rascUI::Button> leftScrollButtons;
    leftScrollButtons.reserve(LEFT_SCROLL_BUTTON_COUNT);
    for (int i = 0; i < LEFT_SCROLL_BUTTON_COUNT; i++) {
        leftScrollButtons.emplace_back(rascUI::Location(SUB_BORDER_X(SUB_BORDER_B(GEN_FILL))), std::string("Button number: ") + std::to_string(i));
        leftScrollPane.contents.add(&(*leftScrollButtons.rbegin()));
    }

    rascUI::ScrollPane rightScrollPane(theme, rascUI::Location(SCROLLPANE_LOC(1)), COUNT_OUTBORDER_Y(1) + UI_BORDER, true, true, false, false);
    rightScrollPane.contents.smoothMoveSpeed = 0;

    std::vector<rascUI::Container> rightScrollContainers;
    std::vector<rascUI::FrontPanel> rightScrollFrontPanels;
    std::vector<rascUI::BasicTextEntryBox> rightScrollTextEntrys;
    std::vector<rascUI::Button> rightScrollButtons;
    rightScrollContainers.reserve(LEFT_SCROLL_BUTTON_COUNT);
    rightScrollFrontPanels.reserve(LEFT_SCROLL_BUTTON_COUNT);
    rightScrollTextEntrys.reserve(LEFT_SCROLL_BUTTON_COUNT * 2);
    rightScrollButtons.reserve(LEFT_SCROLL_BUTTON_COUNT);
    for (int i = 0; i < RIGHT_SCROLL_BUTTON_COUNT; i++) {
        rightScrollContainers.emplace_back(rascUI::Location(SUB_BORDER_X(SUB_BORDER_B(GEN_FILL))));
        rascUI::Container * cont = &(*rightScrollContainers.rbegin());
        rightScrollPane.contents.add(cont);
        rightScrollFrontPanels.emplace_back();
        cont->add(&(*rightScrollFrontPanels.rbegin()));
        rightScrollTextEntrys.emplace_back(rascUI::Location(BORDERTABLE_X(0, 3, GEN_BORDER)), std::string("TL ")+std::to_string(i));
        cont->add(&(*rightScrollTextEntrys.rbegin()));
        if ((i & 3) == 1) rightScrollTextEntrys.rbegin()->setActive(false);
        rightScrollButtons.emplace_back(rascUI::Location(BORDERTABLE_X(1, 3, GEN_BORDER)), std::string("B ")+std::to_string(i));
        cont->add(&(*rightScrollButtons.rbegin()));
        if ((i & 3) == 1) rightScrollButtons.rbegin()->setActive(false);
        rightScrollTextEntrys.emplace_back(rascUI::Location(BORDERTABLE_X(2, 3, GEN_BORDER)), std::string("TR ")+std::to_string(i));
        cont->add(&(*rightScrollTextEntrys.rbegin()));
        if ((i & 3) == 1) rightScrollTextEntrys.rbegin()->setActive(false);
    }


    rascUI::BackPanel aboveScrollPanePanel(rascUI::Location(ABOVE_SCROLLPANE_PANEL_LOC));
    rascUI::FrontPanel aboveScrollPaneFrontPanel(rascUI::Location(SUB_BORDER(ABOVE_SCROLLPANE_PANEL_LOC)));
    rascUI::BackPanel belowScrollPanePanel(rascUI::Location(BELOW_SCROLLPANE_PANEL_LOC));
    rascUI::FrontPanel belowScrollPaneFrontPanel(rascUI::Location(SUB_BORDER(BELOW_SCROLLPANE_PANEL_LOC)));
    rascUI::BackPanel rightFillerBackPanel(rascUI::Location(RIGHT_FILLER_BACKPANEL_LOC));

    series.setSelectedButton(&leftBottomTopButton);

    window.add(&leftScrollPane);
    window.add(&rightScrollPane);
        leftContainer.add(&leftBackPanel);
            leftTopContainer.add(&leftTopFrontPanel);
            leftTopContainer.add(&leftTopTopButton);
            leftTopContainer.add(&leftTopBottomButton);
        leftContainer.add(&leftTopContainer);
            leftBottomContainer.add(&leftBottomFrontPanel);
            leftBottomContainer.add(&leftBottomTopButton);
            leftBottomContainer.add(&leftBottomBottomButton);
        leftContainer.add(&leftBottomContainer);
    window.add(&leftContainer);
        rightContainer.add(&rightBackPanel);
            rightTopContainer.add(&rightTopFrontPanel);
            rightTopContainer.add(&rightTopTopButton);
            rightTopContainer.add(&rightTopBottomButton);
        rightContainer.add(&rightTopContainer);
            rightBottomContainer.add(&rightBottomFrontPanel);
            rightBottomContainer.add(&rightBottomTopButton);
            rightBottomContainer.add(&rightBottomBottomButton);
        rightContainer.add(&rightBottomContainer);
    window.add(&rightContainer);
    window.add(&aboveScrollPanePanel);
    window.add(&aboveScrollPaneFrontPanel);
    window.add(&belowScrollPanePanel);
    window.add(&belowScrollPaneFrontPanel);
    window.add(&rightFillerBackPanel);

    bool continueSeparateThread = true;
    std::mutex separateThreadMutex;
    std::thread separateThread([&continueSeparateThread, &separateThreadMutex, &context, &rightBottomTopButton](void) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
        unsigned long count = 0;
        while(true) {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            context.beforeEvent.addFunction([count, &rightBottomTopButton](void) {
                rightBottomTopButton.setText(std::string("Separate thread: ") + std::to_string(count));
                rightBottomTopButton.repaint();
            });
            count++;
            std::lock_guard<std::mutex>lock(separateThreadMutex);
            if (!continueSeparateThread) break;
        }
    });

    void * token = window.beforeEvent->createToken();

    window.setMapped(true);
    
    while(true) {
        if (openPopup) {
            windowConfig.screenWidth = 300;
            windowConfig.screenHeight = 160;
            windowConfig.title = "RascUI popup window";
            CustomWindow popupWindow(&scale, &scaleExp, &context, windowConfig);
            
            #define POPUP_BACK_PANEL_LOC        GEN_BORDER
            #define POPUP_TOP_BOX_LOC           PIN_NORMAL_T(SUB_BORDER(POPUP_BACK_PANEL_LOC))
            #define POPUP_MIDDLE_BOX_LOC        SUB_BORDERMARGIN_TB(COUNT_INBORDER_Y(1), COUNT_OUTBORDER_Y(1), SUB_BORDER(POPUP_BACK_PANEL_LOC))
            #define POPUP_MIDDLE_LABEL_LOC      SETSIZEMOVE_Y(COUNT_INBORDER_Y(1), SPLIT_B(0.0f, SPLIT_T(0.5f, SUB_BORDER(POPUP_MIDDLE_BOX_LOC))))
            #define POPUP_BOTTOM_BOX_LOC        PIN_B(COUNT_OUTBORDER_Y(1), SUB_BORDER(POPUP_BACK_PANEL_LOC))
            #define POPUP_BOTTOM_BUTTON_COUNT   4
            #define POPUP_BOTTOM_BUTTON_LOC(id) BORDERTABLE_X(id, POPUP_BOTTOM_BUTTON_COUNT, SUB_BORDER(POPUP_BOTTOM_BOX_LOC))
            
            void * popupToken = NULL;
            
            rascUI::BackPanel popupBackPanel(rascUI::Location(POPUP_BACK_PANEL_LOC));
            rascUI::BasicTextEntryBox popupTopBox(rascUI::Location(POPUP_TOP_BOX_LOC), "Some text in a popup!");
            rascUI::FrontPanel popupMiddleBox(rascUI::Location(POPUP_MIDDLE_BOX_LOC));
            rascUI::Label popupMiddleLabel(rascUI::Location(POPUP_MIDDLE_LABEL_LOC), "Vertically centre-aligned text! ");
            rascUI::FrontPanel popupBottomBoxLoc(rascUI::Location(POPUP_BOTTOM_BOX_LOC));
            rascUI::Button popupUnmapOtherButton(rascUI::Location(POPUP_BOTTOM_BUTTON_LOC(0)), "Unm mn", [&window](GLfloat viewX, GLfloat viewY, int button) {
                               window.setMapped(false);
                           }),
                           popupMapOtherButton(rascUI::Location(POPUP_BOTTOM_BUTTON_LOC(1)), "Map mn", [&window](GLfloat viewX, GLfloat viewY, int button) {
                               window.setMapped(true);
                           });
            rascUI::ToggleButtonSeries popupEventSeries(true, [&popupToken, &context, &popupMiddleLabel, &popupMiddleBox](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                if (toggleButton == NULL) {
                    rascUI::FunctionQueue::deleteToken(popupToken);
                    popupToken = NULL;
                } else {
                    popupToken = context.beforeEvent.createToken();
                    context.beforeEvent.addTimedRepeatingFunction(popupToken, 225, 100, [&popupMiddleLabel, &popupMiddleBox](void) {
                        std::string text = popupMiddleLabel.getText();
                        popupMiddleLabel.setText(text.substr(1) + text[0]);
                        popupMiddleBox.repaint();
                    });
                }
            });
            rascUI::ToggleButton popupEventButton(&popupEventSeries, rascUI::Location(POPUP_BOTTOM_BUTTON_LOC(2)), "Event");
            rascUI::Button popupCloseButton(rascUI::Location(POPUP_BOTTOM_BUTTON_LOC(3)), "Close", [&openPopup, &context](GLfloat viewX, GLfloat viewY, int button) {
                openPopup = false;
                context.endMainLoop();
            });
            
            popupWindow.add(&popupBackPanel);
            popupWindow.add(&popupTopBox);
            popupWindow.add(&popupMiddleBox);
            popupWindow.add(&popupMiddleLabel);
            popupWindow.add(&popupBottomBoxLoc);
            popupWindow.add(&popupUnmapOtherButton);
            popupWindow.add(&popupMapOtherButton);
            popupWindow.add(&popupEventButton);
            popupWindow.add(&popupCloseButton);
            
            popupWindow.userRequestCloseFunc = [&openPopup, &context](void) {
                openPopup = false;
                context.endMainLoop();
            };
            
            popupWindow.setMapped(true);
            unsigned long long events = context.mainLoop();
            if (popupToken != NULL) rascUI::FunctionQueue::deleteToken(popupToken);
            if (events == 0) break;
        } else {
            if (context.mainLoop() == 0) break;
        }
    }
    
    {
        std::lock_guard<std::mutex> lock(separateThreadMutex);
        continueSeparateThread = false;
    }
    
    try {
        separateThread.join();
    } catch(const std::system_error & e) {}
    
    rascUI::FunctionQueue::deleteToken(token);
    if (topButtonToken != NULL) {
        rascUI::FunctionQueue::deleteToken(topButtonToken);
    }
    
    
    return 0;
}
