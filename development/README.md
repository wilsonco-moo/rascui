Installing Rasc packages from wilsonco.mooo.com repository
===

See: [rasc webpage](http://wilsonco.mooo.com/rasc) and [package builds](http://wilsonco.mooo.com/rasc/packageBuilds).

The easiest way to grab RascUI packages (in Debian), is by putting [rasc.sources](rasc.sources)
in `/etc/apt/sources.list.d`.

This file contains the public key of my build server, and all packages and
checksums on the respository are signed with this. I can test this by manually
modifying one of the packages, and trying to spoof all the checksums: Apt
rejects it with an invalid signature error:

![Image](images/failedSignature.png)

So even though the packages are hosted over http (not https), as long as:
 * You've downloaded this public key from a trusted source (gitlab is https),
   and nobody has changed it on my gitlab account,
 * My private key hasn't been compromised,
 * You trust *me* (you shouldn't! Compile them yourself!),

Then packages you download are guaranteed to be valid, and generated only by
my build server.
