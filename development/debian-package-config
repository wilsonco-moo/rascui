packageName="librascui"
packageSoname="0"
packageLibName="rascUI"
packageExclude=()

# Note:
#  > For build depends, list basic build depdencies (debhelper etc),
#    plus "-dev" packages for headers we use when building, INCLUDING
#    any "custom" libraries (custom libraries need = $version).
#  > For binary package depends, list shlib/misc depends, AND "custom"
#    libraries we LINK TO (custom libraries need = $version). Other
#    "system" library dependencies can be worked out by dpkg-shlibdeps.
#  > For "-dev" package depends, list only misc and binary package,
#    plus "-dev" packages for any headers *our* headers include.
packageControl="Source: $packageName
Maintainer: $defaultAuthorName <$defaultAuthorEmail>
Section: misc
Priority: optional
Standards-Version: 4.5.1
Build-Depends: debhelper (>= 9),
               libgl-dev

Package: $packageName$packageSoname
Architecture: any
Multi-Arch: same
Section: libs
Depends: "'$'"{shlibs:Depends},
         "'$'"{misc:Depends}
Description: Simple UI library, used by Rasc.
 rascUI is a simple UI library, build for maximum flexibility and simplicity.

Package: $packageName-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: "'$'"{misc:Depends},
         $packageName$packageSoname (= "'$'"{binary:Version}),
         libgl-dev
Description: Development headers for rascUI.
 Development headers for rascUI, which is a simple UI library, build for maximum flexibility and simplicity."

packageInstall="# Create library directories
mkdir debian/$packageName$packageSoname
mkdir debian/$packageName$packageSoname/usr
mkdir debian/$packageName$packageSoname/usr/lib
mkdir debian/$packageName$packageSoname/usr/lib/"'$'"(DEB_HOST_MULTIARCH)

# Copy (link) libraries.
ln build/export/lib$packageLibName.so debian/$packageName$packageSoname/usr/lib/"'$'"(DEB_HOST_MULTIARCH)/lib$packageLibName.so.$packageSoname

# Create development directories
mkdir debian/$packageName-dev
mkdir debian/$packageName-dev/usr
mkdir debian/$packageName-dev/usr/include
mkdir debian/$packageName-dev/usr/lib
mkdir debian/$packageName-dev/usr/lib/"'$'"(DEB_HOST_MULTIARCH)

# Create non-soname symlinks in development directory
ln -sr debian/$packageName-dev/usr/lib/"'$'"(DEB_HOST_MULTIARCH)/lib$packageLibName.so.$packageSoname debian/$packageName-dev/usr/lib/"'$'"(DEB_HOST_MULTIARCH)/lib$packageLibName.so

# Copy over headers - just copy everything (we can get away with hard links), then use find to delete
# all but headers. Trying to do this using find and cp seems to lead to permissions issues for some reason.
cp -lr src/$packageLibName debian/$packageName-dev/usr/include/
find debian/$packageName-dev/usr/include/$packageLibName -type f -not \( -name '*.h' -o -name '*.inl' \) -delete"

packageWscript='
buildVariants = ["", "export"]
additionalGitIgnore = []

def _setup(conf):
    
    _defineRemoteLibraries([])
    
    _defineBuildSetup("", BuildSetup(
        compileOptions = CompileOptions(
            compilerC = "gcc",
            compilerCpp = "g++",
            compileFlags = ["-O0", "-g1", "-Wall"],
            prependIncludePaths = [],
            defines = []
        ),
        linkOptions = LinkOptions(
            linkFlags = [],
            prependDynamicLibs = [],
            prependStaticLibs = [],
            extraExportLibs = [],
            prependLibraryPaths = []
        ),
        libraries = [
            Library(
                libraryName = "'"$packageLibName"'", sourceLocations = ["'"$packageLibName"'"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined", "-Wl,-soname,lib'"$packageLibName.so.$packageSoname"'"],
                libs = ["m"], staticLibs = []
            )
        ],
        mainProgram = MainProgram(
            name = "uiTestProgram",
            sourceLocation = "src", defines = [],
            compileFlags = [], linkFlags = [], excludeDirectories = [],
            libs = ["m", "'"$packageLibName"'"], staticLibs = []
        )
    ))
    
    _defineBuildSetup("export", BuildSetup(
        compileOptions = CompileOptions(
            compilerC = "gcc",
            compilerCpp = "g++",
            compileFlags = ["-flto", "-flto-partition=none", "-fuse-linker-plugin", "-Os", "-Wall"],
            prependIncludePaths = [],
            defines = []
        ),
        linkOptions = LinkOptions(
            linkFlags = ["-flto", "-flto-partition=none", "-fuse-linker-plugin", "-s"],
            prependDynamicLibs = [],
            prependStaticLibs = [],
            extraExportLibs = [],
            prependLibraryPaths = []
        ),
        libraries = [
            Library(
                libraryName = "'"$packageLibName"'", sourceLocations = ["'"$packageLibName"'"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined", "-Wl,-soname,lib'"$packageLibName"'.so.'"$packageSoname"'"],
                libs = ["m"], staticLibs = []
            )
        ],
        mainProgram = MainProgram(
            name = "testMain",
            sourceLocation = "src", defines = [],
            compileFlags = ["-fwhole-program"], linkFlags = ["-fwhole-program"], excludeDirectories = [],
            libs = ["m", "'"$packageLibName"'"], staticLibs = []
        )
    ))
'
