#! /usr/bin/env python3

# Waf-based C++ dependency managing build script VERSION 7
# Copyright (C) 10 July 2019  Daniel Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Additions in version 3:
# beforeBuildCommands, in BuildSetup
#   Extra commands run before each build process. Specified as list of lists,
#   such as [["echo", "hello 1"], ["echo", "hello 2"]]. Useful if some files
#   in project require pre-processing before compiling. Working directory:
#   directory containing this script.
# configureCommands, in RemoteLibrary
#   Commands to run after cloning the library, to get the library into a state
#   where it can be used in the project. Commands specified in same way as
#   before build commands. Working directory: Library repository directory.
# resetBeforeConfigure, in RemoteLibrary
#   This parameter, by default set to False, causes "git reset HEAD --hard" and
#   "git clean -f -d" to be run on the library if configure is called and the
#   library already exists. Useful as it reverses effects of configure commands.

# Additions in version 4:
# excludeDirectories, in MainProgram
#   There is an extra parameter to MainProgram, which allows directories to be
#   excluded. This allows building for different platforms, where each platform
#   only uses *some* of the libraries - e.g: rascUIxcb vs rascUIwin. On each
#   platform's build setup, the libraries which are not used should have their
#   directories excluded.
# symbolic links
#   All symbolic links are now relative (using ln -sr instead of just ln -s).

# Additions in version 5:
# Library directory management:
#   The directories of libraries can now overlap: Each library now only contains
#   the files which have *not* yet been used in other libraries. This allows
#   parts of the source code of libraries to be compiled separately.

# Additions in version 6:
# Custom files in .gitignore:
#   Additional files can be added to the main .gitignore file now, using the
#   additionalGitIgnore list.

# Additions in version 7:
# Comments marking the start and end of the "user config" section, to allow
# automated building of full wscript from just the small user config section.

top = "."     # The project location. Keep this as . unless this wscript file cannot be in the top level project folder.
out = "build" # The location of the build folder
libs = "libs" # The location of the libraries folder

# Begin user config
# This MUST be updated if new build setups are added. "" is the default build variant, able to be run with the command "build".
buildVariants = ["", "export", "build_windows", "export_windows", "export_windows_static"]

# Additional files added to the main .gitignore in the project's folder.
additionalGitIgnore = []

def _setup(conf):
    
    # Remote libraries are cloned from a git repository, into the libs folder. Their code is then symbolically
    # linked into the source tree. The tag parameter can be optionally specified to keep the remote library to
    # a specific version, (for stability). Otherwise the latest version is used.
    _defineRemoteLibraries([
        RemoteLibrary(
            libraryName       = "rascUIxcb",
            repoName          = "rascuixcb",
            repo              = "https://gitlab.com/wilsonco-moo/rascuixcb.git",
            remoteLocation    = "src/rascUIxcb",
            tag               = None
        ),
        RemoteLibrary(
            libraryName       = "rascUIwin",
            repoName          = "rascuiwin",
            repo              = "https://gitlab.com/wilsonco-moo/rascuiwin.git",
            remoteLocation    = "src/rascUIwin",
            tag               = None
        ),
        RemoteLibrary(
            libraryName       = "rascUItheme",
            repoName          = "rascuitheme",
            repo              = "https://gitlab.com/wilsonco-moo/rascuitheme.git",
            remoteLocation    = "src/rascUItheme",
            tag               = None
        )
    ])
    
    _defineBuildSetup("", BuildSetup(
        compileOptions = CompileOptions(
            compilerC = "gcc",
            compilerCpp = "g++",
            compileFlags = ["-O0", "-g1", "-Wall"],
            prependIncludePaths = [],
            defines = []
        ),
        linkOptions = LinkOptions(
            linkFlags = [],
            prependDynamicLibs = [],
            prependStaticLibs = [],
            extraExportLibs = [],
            prependLibraryPaths = []
        ),
        libraries = [
            Library(
                libraryName = "rascUI", sourceLocations = ["rascUI"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["m"], staticLibs = []
            ),
            Library(
                libraryName = "rascUItheme", sourceLocations = ["rascUItheme/utils"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["dl"], staticLibs = []
            ),
            Library(
                libraryName = "rascUIxcb", sourceLocations = ["rascUIxcb"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["xcb", "xcb-keysyms", "xcb-util", "X11", "X11-xcb", "rascUI", "rascUItheme"], staticLibs = []
            ),
            Library(
                libraryName = "scalableTheme", sourceLocations = ["rascUItheme/themes/xcb/scalableTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = ["-I/usr/include/freetype2"], linkFlags = ["-Wl,--no-undefined"],
                libs = ["xcb", "rascUI", "rascUIxcb", "Xft"], staticLibs = []
            ),
            Library(
                libraryName = "basicTheme", sourceLocations = ["rascUItheme/themes/xcb/basicTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["xcb", "rascUI", "rascUIxcb"], staticLibs = []
            )
        ],
        mainProgram = MainProgram(
            name = "uiTestProgram",
            sourceLocation = "src", defines = [],
            compileFlags = [], linkFlags = [], excludeDirectories = ["rascUIwin"],
            libs = ["xcb", "xcb-keysyms", "xcb-util", "X11", "X11-xcb", "m", "pthread", "dl", "rascUI", "rascUItheme", "rascUIxcb"], staticLibs = []
        )
    ))
    
    _defineBuildSetup("export", BuildSetup(
        compileOptions = CompileOptions(
            compilerC = "gcc",
            compilerCpp = "g++",
            compileFlags = ["-flto", "-flto-partition=none", "-fuse-linker-plugin", "-Os", "-Wall"],
            prependIncludePaths = [],
            defines = []
        ),
        linkOptions = LinkOptions(
            linkFlags = ["-flto", "-flto-partition=none", "-fuse-linker-plugin", "-s"],
            prependDynamicLibs = [],
            prependStaticLibs = [],
            extraExportLibs = [],
            prependLibraryPaths = []
        ),
        libraries = [
            Library(
                libraryName = "rascUI", sourceLocations = ["rascUI"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["m"], staticLibs = []
            ),
            Library(
                libraryName = "rascUItheme", sourceLocations = ["rascUItheme/utils"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["dl"], staticLibs = []
            ),
            Library(
                libraryName = "rascUIxcb", sourceLocations = ["rascUIxcb"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["xcb", "xcb-keysyms", "xcb-util", "X11", "X11-xcb", "rascUI", "rascUItheme"], staticLibs = []
            ),
            Library(
                libraryName = "scalableTheme", sourceLocations = ["rascUItheme/themes/xcb/scalableTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = ["-I/usr/include/freetype2"], linkFlags = ["-Wl,--no-undefined"],
                libs = ["xcb", "rascUI", "rascUIxcb", "Xft"], staticLibs = []
            ),
            Library(
                libraryName = "basicTheme", sourceLocations = ["rascUItheme/themes/xcb/basicTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["xcb", "rascUI", "rascUIxcb"], staticLibs = []
            )
        ],
        mainProgram = MainProgram(
            name = "uiTestProgram",
            sourceLocation = "src", defines = [],
            compileFlags = ["-fwhole-program"], linkFlags = ["-fwhole-program"], excludeDirectories = ["rascUIwin"],
            libs = ["xcb", "xcb-keysyms", "xcb-util", "X11", "X11-xcb", "m", "pthread", "dl", "rascUI", "rascUItheme", "rascUIxcb"], staticLibs = []
        )
    ))
    
    _defineBuildSetup("build_windows", BuildSetup(
        compileOptions = CompileOptions(
            compilerC = "x86_64-w64-mingw32-gcc-posix",
            compilerCpp = "x86_64-w64-mingw32-g++-posix",
            compileFlags = ["-O0", "-g1", "-Wall"],
            prependIncludePaths = [],
            defines = []
        ),
        linkOptions = LinkOptions(
            linkFlags = [],
            prependDynamicLibs = ["stdc++"],
            prependStaticLibs = [],
            extraExportLibs = [
                "/usr/x86_64-w64-mingw32/lib/libwinpthread-1.dll",
                "/usr/lib/gcc/x86_64-w64-mingw32/8.3-posix/libgcc_s_seh-1.dll",
                "/usr/lib/gcc/x86_64-w64-mingw32/8.3-posix/libstdc++-6.dll"],
            prependLibraryPaths = []
        ),
        libraries = [
            Library(
                libraryName = "rascUI", sourceLocations = ["rascUI"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["pthread", "m"], staticLibs = []
            ),
            Library(
                libraryName = "rascUItheme", sourceLocations = ["rascUItheme/utils"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = [], staticLibs = []
            ),
            Library(
                libraryName = "rascUIwin", sourceLocations = ["rascUIwin"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["pthread", "m", "gdi32", "rascUI", "rascUItheme"], staticLibs = []
            ),
            Library(
                libraryName = "scalableTheme", sourceLocations = ["rascUItheme/themes/win/scalableTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["m", "gdi32","rascUIwin"], staticLibs = []
            ),
            Library(
                libraryName = "basicTheme", sourceLocations = ["rascUItheme/themes/win/basicTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["m", "gdi32","rascUIwin"], staticLibs = []
            )
        ],
        mainProgram = MainProgram(
            name = "uiTestProgram",
            sourceLocation = "src", defines = [],
            compileFlags = [], linkFlags = [], excludeDirectories = ["rascUIxcb"],
            libs = ["pthread", "m", "gdi32", "rascUI", "rascUItheme", "rascUIwin"], staticLibs = []
        )
    ))
    
    _defineBuildSetup("export_windows", BuildSetup(
        compileOptions = CompileOptions(
            compilerC = "x86_64-w64-mingw32-gcc-posix",
            compilerCpp = "x86_64-w64-mingw32-g++-posix",
            compileFlags = ["-flto", "-flto-partition=none", "-fuse-linker-plugin", "-Os", "-Wall"],
            prependIncludePaths = [],
            defines = []
        ),
        linkOptions = LinkOptions(
            linkFlags = ["-flto", "-flto-partition=none", "-fuse-linker-plugin", "-Os", "-s"],
            prependDynamicLibs = ["stdc++"],
            prependStaticLibs = [],
            extraExportLibs = [
                "/usr/x86_64-w64-mingw32/lib/libwinpthread-1.dll",
                "/usr/lib/gcc/x86_64-w64-mingw32/10-posix/libgcc_s_seh-1.dll",
                "/usr/lib/gcc/x86_64-w64-mingw32/10-posix/libstdc++-6.dll"],
            prependLibraryPaths = []
        ),
        libraries = [
            Library(
                libraryName = "rascUI", sourceLocations = ["rascUI"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["pthread", "m"], staticLibs = []
            ),
            Library(
                libraryName = "rascUItheme", sourceLocations = ["rascUItheme/utils"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = [], staticLibs = []
            ),
            Library(
                libraryName = "rascUIwin", sourceLocations = ["rascUIwin"], defines = [],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["pthread", "m", "gdi32", "rascUI", "rascUItheme"], staticLibs = []
            ),
            Library(
                libraryName = "scalableTheme", sourceLocations = ["rascUItheme/themes/win/scalableTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["m", "gdi32", "rascUIwin"], staticLibs = []
            ),
            Library(
                libraryName = "basicTheme", sourceLocations = ["rascUItheme/themes/win/basicTheme"], defines = ["ENABLE_RASCUI_THEME"],
                compileFlags = [], linkFlags = ["-Wl,--no-undefined"],
                libs = ["m", "gdi32", "rascUIwin"], staticLibs = []
            )
        ],
        mainProgram = MainProgram(
            name = "uiTestProgram",
            sourceLocation = "src", defines = [],
            compileFlags = ["-fwhole-program"], linkFlags = ["-fwhole-program"], excludeDirectories = ["rascUIxcb"],
            libs = ["pthread", "m", "gdi32", "rascUI", "rascUItheme", "rascUIwin"], staticLibs = []
        )
    ))
    
    _defineBuildSetup("export_windows_static", BuildSetup(
        compileOptions = CompileOptions(
            compilerC = "x86_64-w64-mingw32-gcc-posix",
            compilerCpp = "x86_64-w64-mingw32-g++-posix",
            compileFlags = ["-flto", "-flto-partition=none", "-fuse-linker-plugin", "-Os"],
            prependIncludePaths = [],
            defines = ["FORCE_ENABLE_WIN_SCALABLE_THEME"]
        ),
        linkOptions = LinkOptions(
            linkFlags = ["-static-libgcc", "-static-libstdc++", "-flto", "-flto-partition=none", "-fuse-linker-plugin", "-Os", "-s", "-mwindows"],
            prependDynamicLibs = [],
            prependStaticLibs = ["stdc++"],
            extraExportLibs = [],
            prependLibraryPaths = []
        ),
        libraries = [
        ],
        mainProgram = MainProgram(
            name = "uiTestProgram",
            sourceLocation = "src", defines = ["USE_BASIC_THEMES"],
            compileFlags = ["-fwhole-program"], linkFlags = ["-fwhole-program"], excludeDirectories = ["rascUIxcb"],
            libs = ["m", "gdi32"], staticLibs = ["pthread"]
        )
    ))



# End user config
class CompileOptions:
    def __init__(self, compilerC = None, compilerCpp = None, compileFlags = [], prependIncludePaths = [], defines = []):
        self.compilerC = compilerC
        self.compilerCpp = compilerCpp
        self.compileFlags = compileFlags
        self.prependIncludePaths = prependIncludePaths
        self.defines = defines
class LinkOptions:
    def __init__(self, linkFlags = [], prependDynamicLibs = [], prependStaticLibs = [], extraExportLibs = [], prependLibraryPaths = []):
        self.linkFlags = linkFlags
        self.prependDynamicLibs = prependDynamicLibs
        self.prependStaticLibs = prependStaticLibs
        self.extraExportLibs = extraExportLibs
        self.prependLibraryPaths = prependLibraryPaths
class Library:
    def __init__(self, libraryName = None, sourceLocations = [], defines = [], compileFlags = [], linkFlags = [], libs = [], staticLibs = []):
        self.libraryName = libraryName
        self.sourceLocations = sourceLocations
        self.defines = defines
        self.compileFlags = compileFlags
        self.linkFlags = linkFlags
        self.libs = libs
        self.staticLibs = staticLibs
class MainProgram:
    def __init__(self, name = None, sourceLocation = None, defines = [], compileFlags = [], linkFlags = [], excludeDirectories = [], libs = [], staticLibs = []):
        self.name = name
        self.sourceLocation = sourceLocation
        self.defines = defines
        self.compileFlags = compileFlags
        self.linkFlags = linkFlags
        self.excludeDirectories = excludeDirectories
        self.libs = libs
        self.staticLibs = staticLibs
class BuildSetup:
    def __init__(self, compileOptions = CompileOptions(), linkOptions = LinkOptions(), libraries = [], mainProgram = MainProgram(), beforeBuildCommands = []):
        self.compileOptions = compileOptions
        self.linkOptions = linkOptions
        self.libraries = libraries
        self.mainProgram = mainProgram
        self.beforeBuildCommands = beforeBuildCommands
class RemoteLibrary:
    def __init__(self, libraryName = None, repoName = None, repo = None, remoteLocation = None, tag = None, configureCommands = [], resetBeforeConfigure = False):
        self.libraryName = libraryName
        self.repoName = repoName
        self.repo = repo
        self.remoteLocation = remoteLocation
        self.tag = tag
        self.configureCommands = configureCommands
        self.resetBeforeConfigure = resetBeforeConfigure
def _defineBuildSetup(name, buildSetup):
    global buildSetups
    buildSetups[name] = buildSetup
def _defineRemoteLibraries(libraries):
    global remoteLibraries
    remoteLibraries = libraries

buildSetups = {}
remoteLibraries = []

def options(opt):
    opt.load("compiler_c")
    opt.load("compiler_c++")
    opt.add_option("-D", "--def", action="append", type="string", dest="userDefines", help="Define a macro, e.g: -Dmacro or -Dmacro=value, applied to every file compiled. This is relevant when building using any build variant.")
    opt.add_option("-C", "--compile", action="append", type="string", dest="userCompileFlags", help="Add an extra compile flag. For example, -C-Werror will append the compile flag -Werror to every file.")
    opt.add_option("-L", "--link", action="append", type="string", dest="userLinkFlags", help="Add an extra link flag. For example, -L-s will apply the link flag -s to every file.")

# Converts a list of project-relative paths to a list of absolute paths.
def _convToAbsPaths(cxt, paths):
    output = []
    for path in paths:
        if path.startswith("/"):
            output.append(path)
        else:
            output.append(cxt.path.make_node(path).abspath())
    return output
    
# Gets the macro definitions supplied by the user on the command-line.
def _getUserDefines(cxt):
    userDefines = cxt.options.userDefines
    if userDefines == None:
        return []
    return userDefines

# Gets the compile flags supplied by the user on the command-line.
def _getUserCompileFlags(cxt):
    userCompileFlags = cxt.options.userCompileFlags
    if userCompileFlags == None:
        return []
    return userCompileFlags

# Gets the link flags supplied by the user on the command-line.
def _getUserLinkFlags(cxt):
    userLinkFlags = cxt.options.userLinkFlags
    if userLinkFlags == None:
        return []
    return userLinkFlags

# Create functions for each build variant.
from waflib.Context import Context
from waflib.Build import BuildContext
for variantName in buildVariants:
    variant = None
    if variantName == "":
        variant = "build"
    else:
        variant = variantName
    # The build variant actual function
    def someBuildFunc(cxt):
        if len(buildSetups) == 0:
            _setup(cxt)
        _internalBuild(cxt, buildSetups[cxt.variant])
    globals()[variant] = someBuildFunc
    # The custom context class to ensure it is a build context
    class CustomContext(BuildContext):
        cmd     = variant
        fun     = variant
        variant = variantName

# The internal build function.
def _internalBuild(cxt, buildSetup):
    srcDir = cxt.path.find_node(buildSetup.mainProgram.sourceLocation)
    
    # Only import call if actually needed by something.
    if len(buildSetup.linkOptions.extraExportLibs) > 0 or len(buildSetup.beforeBuildCommands) > 0:
        from subprocess import call
        # Copy extra export libraries to the build path
        for lib in buildSetup.linkOptions.extraExportLibs:
            call(["cp", lib, cxt.path.get_bld().abspath()+"/"])
        # Run any beforeBuildCommands, (from current working directory).
        for command in buildSetup.beforeBuildCommands:
            if call(command) != 0:
                sys.stderr.write("Failed to execute before build command:\n"+(" ".join(command))+"\n")
                sys.exit(1)
    
    # Generate a set of all the source files in repository.
    allSourceFiles = srcDir.ant_glob(["**/*.c", "**/*.cpp"])
    srcSet = set()
    for node in allSourceFiles:
        srcSet.add(node.path_from(srcDir))
    
    # Get the extra defines, link flags and compile flags provided by the user.
    userDefines = _getUserDefines(cxt)
    userCompileFlags = _getUserCompileFlags(cxt)
    userLinkFlags = _getUserLinkFlags(cxt)
    
    for library in buildSetup.libraries:
        # Find all the source files associated with this library. Note that the library might have many source locations.
        libraryFilesOriginal = []
        for libSrcDir in library.sourceLocations:
            libraryFilesOriginal.extend(srcDir.find_node(libSrcDir).ant_glob(["**/*.c", "**/*.cpp"]))
        # Only include files which are still in the source set (i.e: haven't been used yet in another library),
        # and for each one remove it from the source set.
        libraryFiles = []
        for node in libraryFilesOriginal:
            path = node.path_from(srcDir)
            if path in srcSet:
                libraryFiles.append(node)
                srcSet.remove(path)
        # Compile this as a shared library.
        cxt.shlib(
            source       = libraryFiles,
            target       = library.libraryName,
            includes     = _convToAbsPaths(cxt, buildSetup.compileOptions.prependIncludePaths) + [srcDir],
            lib          = buildSetup.linkOptions.prependDynamicLibs                           + library.libs,
            stlib        = buildSetup.linkOptions.prependStaticLibs                            + library.staticLibs,
            libpath      = _convToAbsPaths(cxt, buildSetup.linkOptions.prependLibraryPaths)    + [cxt.path.get_bld().abspath()],
            cflags       = buildSetup.compileOptions.compileFlags                              + library.compileFlags + userCompileFlags,
            cxxflags     = buildSetup.compileOptions.compileFlags                              + library.compileFlags + userCompileFlags,
            linkflags    = buildSetup.linkOptions.linkFlags                                    + library.linkFlags + userLinkFlags,
            defines      = buildSetup.compileOptions.defines                                   + library.defines + userDefines
        )
        
        # Make sure that we wait for one library to compile, then do the next one.
        # Otherwise if there is a dependency, waf is too stupid to wait for the previous one
        # to *actually* finish, so you get a file truncated issue.
        cxt.add_group()
    
    # Remove any excluded directories from the source file set.
    for excludeDirectory in buildSetup.mainProgram.excludeDirectories:
        excludedFiles = srcDir.find_node(excludeDirectory).ant_glob(["**/*.c", "**/*.cpp"])
        for node in excludedFiles:
            srcSet.remove(node.path_from(srcDir))
    
    # Convert the remaining source files to a list of nodes.
    mainProgramSources = []
    for path in srcSet:
        mainProgramSources.append(srcDir.find_node(path))
    # .. and compile the main program.
    cxt.program(
        source       = mainProgramSources,
        target       = buildSetup.mainProgram.name,
        includes     = _convToAbsPaths(cxt, buildSetup.compileOptions.prependIncludePaths) + [srcDir],
        lib          = buildSetup.linkOptions.prependDynamicLibs                           + buildSetup.mainProgram.libs,
        stlib        = buildSetup.linkOptions.prependStaticLibs                            + buildSetup.mainProgram.staticLibs,
        libpath      = _convToAbsPaths(cxt, buildSetup.linkOptions.prependLibraryPaths)    + [cxt.path.get_bld().abspath()],
        cflags       = buildSetup.compileOptions.compileFlags                              + buildSetup.mainProgram.compileFlags + userCompileFlags,
        cxxflags     = buildSetup.compileOptions.compileFlags                              + buildSetup.mainProgram.compileFlags + userCompileFlags,
        linkflags    = buildSetup.linkOptions.linkFlags                                    + buildSetup.mainProgram.linkFlags + userLinkFlags,
        defines      = buildSetup.compileOptions.defines                                   + buildSetup.mainProgram.defines + userDefines
    )

def configure(cxt):
    from subprocess import call
    import sys, os
    from stat import S_ISLNK
    
    # Do the setup.
    if len(buildSetups) == 0:
        _setup(cxt)
    mainProgram = buildSetups[""].mainProgram
    
    # Set the compiler for all build variants
    failedVariants = 0
    for variant in buildSetups:
        cxt.setenv(variant)
        cxt.env.CC = buildSetups[variant].compileOptions.compilerC
        cxt.env.CXX = buildSetups[variant].compileOptions.compilerCpp
        try:
            cxt.load("compiler_c")
            cxt.load("compiler_c++")
        except:
            failedVariants += 1
            sys.stderr.write("WARNING: Failed to load build setup " + ("build" if variant == "" else variant) + ".\nThis is likely due to missing the compiler " + buildSetups[variant].compileOptions.compilerC + " or " + buildSetups[variant].compileOptions.compilerCpp + ".\n")
    
    # If all build variants failed to initialise, complain and exit.
    if failedVariants == len(buildSetups):
        sys.stderr.write("ERROR: All " + str(len(buildSetups)) + " build setups failed to load.\n")
        sys.exit(1)
    
    # Reset to default variant
    cxt.setenv("")
    
    # Check that the user actually defined the main program, and that the source directory they specified exists.
    if mainProgram == None:
        sys.stderr.write("ERROR: Main program not set. Please use _defineMainProgram function in _setup.\n")
        sys.exit(1)
    srcDir = cxt.path.find_node(mainProgram.sourceLocation)
    if srcDir == None:
        sys.stderr.write("ERROR: Cannot find source directory "+cxt.path.make_node(mainProgram.sourceLocation).abspath()+".\n")
        sys.exit(1)
    elif not srcDir.isdir():
        sys.stderr.write("ERROR: Something which is not a directory exists instead of the source directory "+srcDir.abspath()+".\n")
        sys.exit(1)
    
    # Create the libraries directory if it does not already exist. Complain if there is something which is not a directory there.
    libNode = cxt.path.find_node(libs)
    if libNode == None:
        print("Creating libraries directory...")
        libNode = cxt.path.make_node(libs)
        libNode.mkdir()
    elif not libNode.isdir():
        sys.stderr.write("ERROR: Something which is not a directory exists instead of the libraries directory "+libNode.abspath()+".\n")
        sys.exit(1)
    
    # Clone all the repositories into the libraries directory. Once we have finished, deal with tags: Pull all without a tag, checkout all with a tag.
    print("Cloning/pulling repositories...")
    for library in remoteLibraries:
        repoDir = libNode.find_node(library.repoName)
        # First clone the repository if it doesn't already exist.
        if repoDir == None:
            print("Cloning "+library.repo+" ...")
            result = call(["git", "clone", library.repo], cwd = libNode.abspath())
            repoDir = libNode.find_node(library.repoName)
            if result != 0 or repoDir == None:
                sys.stderr.write("ERROR: Failed to clone git repository "+library.repo+"\n")
                sys.exit(1)
        # If the repository *does* exist, and resetBeforeConfigure is set to true, do a reset and clean.
        elif library.resetBeforeConfigure:
            print("Resetting and cleaning library "+library.repo+" before configure...")
            if call(["git", "reset", "HEAD", "--hard"], cwd = repoDir.abspath()) != 0 or call(["git", "clean", "-f", "-d"], cwd = repoDir.abspath()) != 0:
                sys.stderr.write("ERROR: Failed to reset library "+library.repo+" before configure.\n")
                sys.exit(1)
        # Fetch latest tags.
        if call(["git", "fetch", "--all", "--tags", "--prune"], cwd = repoDir.abspath()) != 0:
            sys.stderr.write("ERROR: Failed to fetch all recent repository tags.\n")
            sys.exit(1)
        # Checkout master and pull if there is no tag, checkout the tag if there is one.
        if library.tag == None:
            print("Using latest commit of master branch for remote library: "+library.repo+" ...")
            if call(["git", "checkout", "master"], cwd = repoDir.abspath()) != 0 or call(["git", "pull"], cwd = repoDir.abspath()) != 0:
                sys.stderr.write("ERROR: Failed to checkout master branch and/or pull "+library.repo+"\n")
                sys.exit(1)
        else:
            print("Using tag "+library.tag+" for remote library: "+library.repo+" ...")
            if call(["git", "checkout", library.tag], cwd = repoDir.abspath()) != 0:
                sys.stderr.write("ERROR: Failed to checkout tag "+library.tag+" for remote library: "+library.repo+"\n")
                sys.exit(1)
        # Run all configureCommands, complain if any fail.
        for command in library.configureCommands:
            if call(command, cwd = repoDir.abspath()) != 0:
                sys.stderr.write("Failed to execute configure command:\n"+(" ".join(command))+"\nfor library "+library.repo+".\n")
                sys.exit(1)
    
    # Create symlinks from the source tree to wherever the source in each library is located, (specified by the user).
    # If any symlinks already exist, check that they point to the correct place.
    print("Creating symlinks...")
    for library in remoteLibraries:
        symSource = srcDir.find_node(library.libraryName)
        symTarget = libNode.find_node(library.repoName).find_node(library.remoteLocation)
        # Create main source directory symlink.
        if symTarget == None:
            sys.stderr.write("ERROR: Cannot find remote location "+library.remoteLocation+" within repository "+library.repo+", which should be at the filepath "+libNode.find_node(library.repoName).make_node(library.remoteLocation).abspath()+".\n")
            sys.exit(1)
        if symSource == None:
            symSource = srcDir.make_node(library.libraryName)
            if call(["ln", "-sr", symTarget.abspath(), symSource.abspath()], cwd = repoDir.abspath()) != 0:
                sys.stderr.write("ERROR: Cannot create symlink to "+library.remoteLocation+" within repository "+library.repo+", which should be at the filepath "+libNode.find_node(library.repoName).make_node(library.remoteLocation).abspath()+".\n")
                sys.exit(1)
        else:
            mode = os.lstat(symSource.abspath()).st_mode
            if S_ISLNK(mode):
                targetFromSymlink = os.path.realpath(os.path.join(os.path.dirname(symSource.abspath()), os.readlink(symSource.abspath())))
                if targetFromSymlink != os.path.realpath(symTarget.abspath()):
                    sys.stderr.write("ERROR: Cannot symlink library "+library.libraryName+" into source tree: the following symlink already exists there: ["+symTarget.abspath()+"] -> ["+targetFromSymlink+"].\n")
                    sys.exit(1)
            else:
                sys.stderr.write("ERROR: Cannot symlink library "+library.libraryName+" into source tree, something else already exists at "+symSource.abspath()+".\n")
                sys.exit(1)
    
    # Create .gitignore files which ignore all the stuff we have added.
    print("Creating .gitignore files...")
    rootGitIgnore = cxt.path.make_node(".gitignore")
    try:
        with open(rootGitIgnore.abspath(), "w") as f:
            f.write(".gitignore\n"+out+"\n"+libs+"\n.waf*\n.lock-waf*\n")
            for ignoreFile in additionalGitIgnore:
                f.write(ignoreFile+"\n")
    except IOError:
        sys.stderr.write("ERROR: Cannot write to .gitignore file "+rootGitIgnore.abspath()+".\n")
        sys.exit(1)
    srcGitIgnore = srcDir.make_node(".gitignore")
    try:
        with open(srcGitIgnore.abspath(), "w") as f:
            f.write(".gitignore\n")
            for library in remoteLibraries:
                f.write(library.libraryName+"\n")
    except IOError:
        sys.stderr.write("ERROR: Cannot write to .gitignore file "+srcGitIgnore.abspath()+".\n")
        sys.exit(1)

def _customDistclean(cxt):
    from subprocess import call
    if len(buildSetups) == 0:
        _setup(cxt)
    mainProgram = buildSetups[""].mainProgram
    
    # Delete the libraries directory, if it exists.
    print("Ensuring libraries directory is deleted...")
    libNode = cxt.path.find_node(libs)
    if libNode != None:
        libNode.delete()
    
    # Delete the build directory, if it exists.
    print("Ensuring build directory is deleted...")
    buildNode = cxt.path.find_node(out)
    if buildNode != None:
        buildNode.delete()
    
    # Delete the symlinks we created in the source tree.
    print("Removing any existing symlinks...")
    srcDir = cxt.path.find_node(mainProgram.sourceLocation)
    for library in remoteLibraries:
        symSource = srcDir.make_node(library.libraryName)
        call(["rm", "-f", symSource.abspath()], cwd = srcDir.abspath())
        
    # Delete the .gitignore files we created.
    print("Removing any existing .gitignore files...")
    rootGitIgnore = cxt.path.find_node(".gitignore")
    if rootGitIgnore != None:
        rootGitIgnore.delete()
    srcGitIgnore = srcDir.make_node(".gitignore")
    if srcGitIgnore != None:
        srcGitIgnore.delete()

# Run _customDistclean each time distclean is done, AS WELL AS the default distclean implementation.
class CustomDistCleanContext(Context):
    cmd = "distclean"
    def execute(self):
        super(CustomDistCleanContext, self).execute()
        _customDistclean(self)
